Energy Weapons
==============

> Particle accelerator project beams of energized particles against enemy
> ships to cause damage, while energy screens are capable of dissipating
> incoming beams.

The typical ship-mounted weapons in Stardust Empire are particle accelerators
that project beams of atomic or subatomic particles to disrupt the target at
a molecular level. They are large weapons, require massive power sources, and
are mostly computer-controlled to take advantage of optimal firing solutions
rather than a human taking pot-shots by eye. At outer space combat ranges,
accuracy and effectiveness of a shot approaches a fifty per-cent hit rate.

Ships are hardened against beam weapons by armor plating, and on larger ships
a technology known as "screens" are projected to dissipate the coherence and
effectiveness of incoming beams. High-powered lasers are another defensive
system employed on ships, capable of destroying or disabling rockets and
missiles on approach.

Energy weapons appropriate for personnel use are a different story.
