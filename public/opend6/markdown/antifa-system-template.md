<img style="float:right;" src="images/d6logo150.png" />

Character Creation Template
===========================

Game World Name: Anti Fantasy

Game Designer: Nox Banners Game Design

Character Information
---------------------

Species: to be determined.

Age Requirements: to be determined.

Groups:

Starting Attribute Dice:

Starting Skill Dice:


Attributes and Skills
---------------------

- Coordination

- Endurance

- Reflexes

- Strength

- Magic


Game System Template
====================

Genre: high fantasy

World Overview:

Technology Level: medi-eval


Powers Section
--------------

### Types

Magic

### Limitations & Restrictions

To be determined.

### Power Skill Names

To be determined.


Combat Section
--------------

### Damage System

Body Points Formula: 20 + Endurance roll.

### Round Structure

To be determined.

### Options


Miscellaneous Notes
-------------------

This system uses bare attributes and a damage system akin to hit points. 
It's intended for small-scale tabletop warfare. Ranges and 
area-of-effect determined in inches or centimeters on a ruler or 
blast/range template.
