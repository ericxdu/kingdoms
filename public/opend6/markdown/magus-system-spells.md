Mageworld
=========

Spells most often require an enchanted instrument, a gesture,
and an incantation.

### Charm

### Farsight

View a remote area through a magic crystal.

- Skill: Divination
- Difficulty: 10
- Effect: 9 (simulates 3D of Search)
- Range: 1 kilometer (+15)
- Speed: +5
- Duration: 1 minute (+9)
- Casting Time: 40 seconds (-8)
- Aspects
  + Components (-5): enchanted crystal (very rare)
  + Concentration (-3): 1 minute (willpower 9)
  + Gesture (-2): hands waved around the crystal
  + Incantation (-1): a few magic words
  

### Healing (Aid)

### Illusion (Displacement)

### Imprison (Cage)

### Rod of Light

Cause a magic object to emit light like a torch.

- Skill: Conjuration
- Difficulty: 10
- Effect: 12 (negates 4D of darkness)
- Range: Touch (0)
- Speed: 0
- Duration: 5 minutes (+13)
- Casting Time: 1 second (0)
- Aspects
  + Area Effect (+5): 1 meter radius sphere
  + Components (-5): magical rod (very rare)
  + Gesture (-2): spin the rod three turns
  + Incantation (-1): a magic word


### Portal (Doorway Home)

- Difficulty:
- Effect:
- Range:
- Speed:
- Duration:
- Casting Time:
- Aspects
  + Components
  + Gestures
  + Incantation

### Slow (Alter Movement)

### Telekinesis (Helping Hand)


Difficulty Reference
--------------------

Spells are cast by rolling dice from the character's corresponding
skill and meeting or exceeding the spell difficulty. An average skilled mage probably has a skill of 1D+2 (roll 2d6 and add 1).

**Automatic (0)**: Almost anyone can perform this action; there is no need to roll. (Generally, this difficulty is not listed in a pre-generated adventure; it is included here for reference purposes.)

**Very Easy (1–5)**: Nearly everyone can accomplish this task. Typically, only tasks with such a low difficulty that are crucial to the scenario are rolled.

**Easy (6–10)**: Although characters usually have no difficulty with these tasks, an untrained character may ﬁnd them challenging.

**Moderate (11–15)**: There is a fair chance that the average character will fail at this type of task. Tasks of this type require skill, effort, and concentration.

**Difficult (16–20)**: Those with little experience in the task must have a lot of luck to accomplish these actions.

**Very Difficult (21–25)**: The average character only rarely succeeds at these kinds of task. Only the most talented regularly succeed.

**Heroic (26–30)**, Legendary (31 or more): These kinds of tasks are nearly impossible, though there’s still that chance that lucky average or highly experienced characters can accomplish them.
