<img style="float:right;" src="images/d6logo150.png" />

Character Creation Template
===========================

Game World Name: Children of Gaia

Game Designer: Nox Banners Game Design


Character Information
---------------------

Species: Golem, Lycan, Siren, or Witch. See species templates for details.

Age Requirements: to be determined.

Groups: chaotic, neutral, good, lawful, evil

Starting Attribute Dice: 20

Starting Skill Dice: 7


Attributes and Skills
---------------------

- Coordination
  + Dexterity
  + Throwing

- Endurance
  + Swimming

- Reflexes
  + Acrobatics
  + Brawling
  + Dodge
  + Running
  + Sneak

- Strength
  + Climbing
  + Jumping

- Perception
  + Contacts
  + Healing
  + Tracking
  + Willpower


Game System Template
====================

Genre: modern fantasy adventure

World Overview: the world is populated in secret by four types of 
superhumans with supernatural qualities and abilities. They subscribe to 
a gamut of moral codes between good and evil, trickser and benefactor. 
They are generally long-lived and sometimes seemingly immortal. Some of 
them were known as gods in ancient times, and are seen as cryptids in 
modern times. Some form powerful factions and war with each other, or 
rule over their own kind.

Technology Level: early 21st century


Powers Section
--------------

### Type(s)

+ Magic
+ Psychic Power

### Limitations & Restrictions

To be determined.

### Power Skill Names

- Magic Power
- Psychic Ability


Combat Section
--------------

### Damage System

Wounds #: 5

### Round Structure

Initiative rounds

### Options


Miscellaneous Notes
-------------------

The four superhuman species will be described more thoroughly in species 
templates.
