<img style="float:right;" src="images/d6logo150.png" />

Character Creation Template
===========================

Game World Name: Magus Experiment

Game Designer: Nox Banners Game Design

Character Information
---------------------

Species: human magician

Age Requirements:

Groups:

Starting Attribute Dice: 15

Starting Skill Dice: 7

Starting Money:


Attributes and Skills
---------------------

- Agility
  + Aim
  + Dodge

- Strength

- Wisdom
  + Alchemy
  + Arcane Lore
  + Theurgy

- Spellcraft
  + Charm
  + Farsight
  + Healing
  + Illusion
  + Light
  + Teleportal
  + Slow
  + Telekinesis

- Willpower
  + Meditation


Game System Template
====================

Genre: modern fantasy

World Overview: experimental system where character building is focused on
magical ability.

Technology Level: late 20th century

Powers Section
--------------

Combat Section
--------------

### Damage System

Wounds #: 5

### Round Structure

To be determined.

### Options


Miscellaneous Notes
-------------------
