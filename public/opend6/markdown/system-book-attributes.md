### Coordination

> Coordination represents a character's ability to perform feats that
> require manual dexterity or hand-eye cooperation, i.e., fine motor
> skills. Such tasks include firing a bow or gun, picking a lock, and
> throwing a grenade. &mdash; **COORDINATION**, *System Book*

> Shooting any kind of mechanical device - such as a bow or sling -
> that projects missiles across a distance. &mdash; **marksmanship**,
> *D6 Fantasy*

> Nimbleness with the fingers, including picking pockets and palming
> items. &mdash; **sleight of hand**, *D6 Adventure*, *D6 Fantasy*,
> *D6 Space*

Shooting is the primary

**Alternate Names**: Aim, Dexterity<sup>Star Wars</sup>

---

### Endurance

> Endurance is a measure of a character's bodily resistance, i.e., how
> well his body stands up to attack, whether from direct injury or more
> insidious sources like poison, disease, or magical sickness. &mdash;
> **ENDURANCE**, *System Book*

> Physical endurance and resistance to pain, disease, and poison.
> &mdash; **stamina**, *D6 Adventure*, *D6 Fantasy*, *D6 Space*

In the genre books and *Star Wars*, Stamina is always governed by the
attribute Strength or its alternate name, Physique.

**Alternate Names**: Constitution, Stamina

---

### Reflexes

> Reflexes guages a character's gross motor coordination, i.e., the
> ability of his mind and his muscles to react to a potential threat or
> a sudden occurence. Examples of skills that rely on Reflexes include
> dodging an attack, fighting with a melee weapon (a sword, a knife, et
> cetera), and balancing on a tight rope. &mdash; **REFLEXES**,
> *System Book*

> Slipping out of danger's way, whether avoiding an attack or a sprung
> booby trap. &mdash; **dodge**, *D6 Adventure*, *D6 Fantasy*,
> *D6 Space*

> Wielding hand·to-hand weapons. &mdash; **melee combat**, *D6 Adventure*,
> *D6 Fantasy*, *D6 Space*

**Alternate Names**: Agility<sup>Space/Fantasy</sup>, Balance

---

### Strength

> Strength represents a character's physical power&mdash;his ability to
> lift heavy objects and to inflict damage with a handheld weapon (like
> a sword or a knife). &mdash; **STRENGTH**, *System Book*

> Moving or lifting heavy objects, inflicting damage with
> strength-powered weapons. &mdash; **lifting**, *D6 Adventure*,
> *D6 Fantasy*

**Alternate Names**: Athletics, Physique<sup>Adventure/Fantasy</sup>

---

### Intellect

> This attribute measures the mathematical, conceptual, and deductive
> capabilities of a character. Typical skills which is could govern
> include estimation (mentally figuring out values), deciphering
> languages, or code-breaking. &mdash; **INTELLECT**, *System Book*

*D6 Fantasy*

**Alternate Names**: Intelligence, Reasoning

---

### Knowledge

> The Knowledge attribute represents a character's level of education
> in various fields, from scientific pursuits like physics to
> philosophical concepts, from history and languages to magical lore
> and planetary systems. Any information a character could know in the
> game world could fall underneath this attribute. &mdash;
> **KNOWLEDGE**, *System Book*

> This skill represents knowledge and/or education in areas not
> covered under any other skill[...]. This may be restricted to a
> specific field (represented by specializations) or a general
> knowledge of a wide range of subjects. It is used to remember
> details, rumors, tales, legends, theories, important people, and the
> like, as appropriate for the subject in question. However, the
> broader the category, the fewer the details that can be recalled. It
> covers what the character himself can recall. Having another skill
> as a specialization of the scholar skill means that the character
> knows the theories and history behind the skill but can't actually
> use it. Scholar can be useful with investigation to narrow a search
> for information. &mdash; **scholar**, *D6 Adventure*, *D6 Space*,
> *D6 Fantasy*

**Alternate Names**: Lore, Wisdom, Science

---

### Mechanical

> Mechanics represents a character's ability to repair machinery,
> vehicles, weapons, armor, android, and so on. It can also measure
> ability in skills which require a combination of Reflexes and
> Knowledge, like shield operation, riding, and driving (you must first
> learn how to operate the device, but then you must rely on quickness
> to use the device to its potential). &mdash; **MECHANICAL**,
> *System Book*

> Creating, fixing, or modifying gadgets, weapons, armor, and
> vehicles. &mdash; **repair**, *D6 Adventure*

**Alternate Names**: Mechanics, Sensory Extension, Symbiotic Attachment

---

### Perception

> Sometimes a character may have the opportunity to notice something in
> his surroundings that might provide an important piece of
> information. For example, a character might spot a bulging pocket on
> an adversary, which may indicate the presence of a concealed weapon.
> The Perception attribute covers such instances as well as those
> skills that require the ability to read the emotions or logical
> reasoning of another, like bargaining, commanding, or persuading.
> &mdash; **PERCEPTION**, *System Book*

> Spotting hidden objects or people, reconnoitering, lipreading, or
> eavesdropping on or watching another person. &mdash; **search**,
> *D6 Adventure* and *D6 Fantasy*

> Spotting hidden objects or people, reconnoitering, lipreading,
> eavesdropping on or watching other people, or tracking the trails
> they’ve left behind. &mdash; **search**, *D6 Space*

**Alternate Names**: Awareness, Cognition, Observation, Sense

---

### Confidence

> This attribute represents a character's personal effect on others. It
> includes such skills as oration, acting, and grooming. &mdash;
> **CONFIDENCE**, *System Book*

> Using friendliness, flattery, or seduction to influence someone
> else. Also useful in business transactions, putting on performances
> (such as singing, acting, or storytelling), and situations involving
> etiquette. &mdash; **charm**, *D6 Adventure*

> Bluffing, lying, tricking, or deceiving others, as well as verbal
> evasiveness, misdirection, and blustering. Also useful in putting on
> acting performances. &mdash; **con**, *D6 Adventure*

**Alternate Names**: Charm, Presence<sup>Adventure</sup>

---

### Technical

> The Technical attribute measures a character's aptitude for
> technological equipment, from computers to electronic listening
> devices to electronic security, a well as those skills that
> require a combination of Knowledge and Coordination, like
> *first aid* and *forgery*. &mdash; **TECHNICAL**, *System Book*

> Using and designing (not making) complex mechanical or electronic
> equipment, such as programming and operating computers and
> manipulating communication devices. &mdash; **tech**, *D6 Adventure*

**Alternate Names**: Technology

---

### Willpower

> A character's Willpower represents his ability to withstand mental
> attacks, whether they come from situational pressures, like stress,
> or direct assault, like magical or psychic phenomena. &mdash;
> **WILLPOWER**, *System Book*

> Ability to withstand stress, temptation, mental attacks, and pain.
> &mdash; **willpower**, *D6 Adventure*, *D6 Space*

**Alternate Names**: Mental Fortitude, Mind, Spirit

---
