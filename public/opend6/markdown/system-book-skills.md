<img style="float:right;" src="images/d6logo150.png" />

<a href="index.html">goto index</a>

System Book Skills
==================

The D6 *System Book* outlines a wide range of 48 individual skills. The Game
Master is meant to draw from this list to create an appropriate skill set for
the game setting, and add any extra skills that are needed. These skills are
listed in this document under Core attributes and Optional attributes.

The Game Master is advised to start with the four "core attributes" and select
"optional attributes" as needed to facilitate the skill set. However this is
not strictly required. For instance, in place of the Endurance core attribute a
"resist damage" skill can instead be provided.

Several "professions" are listed on pages 23-26 of *System Book*, each of which
describes a skill set. These skills have been included among the main 48 skills
below with a note referencing which professions listed it.


Core Attributes
---------------

### Coordination (Aim, Dexterity)

> **Coordination** represents a character's ability to perform feats 
> that require manual dexterity or hand-eye cooperation, i.e., fine 
> motor skills. Such tasks include firing a bow or gun, picking a lock, 
> and throwing a grenade.

- **LOCKPICKING** (Coordination)
  + Unlock (ARCHAEOLOGIST, DEMON HUNTER, INVESTIGATOR, SPELLJACKER, SPY)
- **&lsaquo;MISSILE WEAPON&rsaquo;** (Coordination)
  + Bows
  + Blowguns
  + Pistols
  + Rifles
  + Blasters
  + Flame-throwers

### Endurance (Constitution, Stamina)

> **Endurance** is a measure of a character's bodily resistance, i.e., 
> how well his body stands up to attack, whether from direct injury or 
> more insidious sources like poison, disease, or magical sickness.

- **SWIMMING** (Endurance)
  + Swimming (ARCHAEOLOGIST, EXPLORER, MERCENARY)

### Reflexes (Agility, Balance)

> **Reflexes** guages a character's gross motor coordination, i.e., the 
> ability of his mind and his muscles to react to a potential threat or 
> a sudden occurence. Examples of skills that rely on Reflexes include 
> dodging an attack, fighting with a melee weapon (a sword, a knife, et 
> cetera), and balancing on a tight rope.

- ACROBATICS (Reflexes)
- JUMPING (Reflexes, Strength)
- &lsaquo;MELEE WEAPON&rsaquo; (Reflexes)
  + &lsaquo;Weapon&rsaquo; (BODYGUARD, DEMON HUNTER, EVOCATOR, EXPLORER, INFILTRATOR,
    INVESTIGATOR, KNIGHT, MERCENARY, OUTRIDER, SHERRIF, SPELLJACKER, SPY
    TRACKER/BOUNTY HUNTER, WANDERER, WEAPON MASTER)
  + Swords
  + Axes
  + Clubs
- SCALING (Reflexes, Strength)
  + Rock Climbing (EXPLORER)
  + Scaling (INVESTIGATOR, SABOTEUR, SPELLJACKER, SPY)
- SNEAK (Reflexes, Perception)
  + Sneak (CARDSHARPER, DEMON HUNTER, INVESTIGATOR, PERFORMER, SABOTEUR,
    SMUGGLER, SPELLJACKER, SPY, TRACKER/BOUNTY HUNTER)
- &lsaquo;WEAPONLESS FIGHTING STYLE&rsaquo; (Reflexes)
  + &lsaquo;Weaponless Fighting Style&rsaquo; (BODYGUARD, INVESTIGATOR, MERCENARY, SHERRIF,
    SPELLSLINGER, SPY, TRACKER/BOUNTY HUNTER, WEAPON MASTER)
  + Martial Arts
  + Wrestling
  + Brawling

### Strength (Athletics, Physique)

> **Strength** represents a character's physical power&mdash;his ability 
> to lift heavy objects and to inflict damage with a handheld weapon 
> (like a sword or a knife).


Optional Attributes
-------------------

### Intellect (Intelligence, Reasoning)

> This attribute [**Intellect**] measures the mathematical, conceptual, 
> and deductive capabilities of a character. Typical skills which it 
> could govern include estimation (mentally figuring out values), 
> deciphering languages, or code-breaking.

- **ALCHEMICAL/CHEMICAL CONCOCTION** (Intellect, Endurance)
  + Alchemical Concoction (ALCHEMIST, SABOTEUR)
  + Concoct Poison (ALCHEMIST)
  + Fire Knowledge (ALCHEMIST, EVOCATOR, EXPLORER, WANDERER)
- **ANATOMY** (Intellect)
- **ARCANE LORE** (Intellect, Knowledge)
  + Appraising (CARDSHARPER, SMUGGLER)
  + Arcane Lore (EVOCATOR, THEURIST, WARLOCK)
  + &lsaquo;Dimension&rsaquo; Knowledge (SCHOLAR, WARLOCK)
  + Netherlore (WARLOCK)
  + Realmlore (SPELLJACKER)
- **COMPUTER OPERATIONS** (Intellect, Knowledge, Technical)
- **CUSTOMS OF &lsaquo;REGION OR CULTURES&rsaquo;** (Intellect, Perception)
  + Customs of &lsaquo;Region, People&rsaquo; (ARCHAEOLOGIST, INFILTRATOR)
  + Military Command (KNIGHT, MERCENARY)
  + Military Tactics (EVOCATOR, KNIGHT, MERCENARY)
- **DIAGNOSE MALADY** (Intellect, Perception)
  + Diagnose Malaise (CORPOREALIST/BIOLOGIST, HEALER/APOTHECRIST/DOCTOR)
- **LAW** (Intellect, Knowledge)
  + Corpus Juris (SCHOLAR, SHERRIF)

### Knowledge (Lore, Wisdom, Science)

> The **Knowledge** attribute represents a character's level of 
> education in various fields, from scientific pursuits like physics to 
> philosophical concepts, from history and languages to magical lore and 
> planetary systems. Any information a character could know in the game 
> world could fall underneath this attribute.

- **ETIQUETTE** (Knowledge, Intellect, Perception)
  + Etiquette (INFILTRATOR)
- **FORGERY** (Knowledge, Coordination, Mechanical)
  + Forgery (INFILTRATOR, SMUGGLER)
- **HEALING** (Knowledge, Intellect, Perception, Reflexes)
  * First Aid (EXPLORER, HEALER/APOTHECRIST/DOCTOR)
  + Healing (CORPOREALIST/BIOLOGIST, HEALER/APOTHECRIST/DOCTOR)
- **HERALDRY** (Knowledge, Intellect, Perception)
  + Heraldry (ARCHAEOLOGIST, KNIGHT)
- **HISTORY OF &lsaquo;AREA&rsaquo;** (Knowledge, Intellect)
  + History of &lsaquo;Area&rsaquo; (ARCHAEOLOGIST, SCHOLAR)
  + History of &lsaquo;Species&rsaquo; (ARCHAEOLOGIST, SCHOLAR)
  + History of &lsaquo;Nationality&rsaquo; (ARCHAEOLOGIST, SCHOLAR)
- **IDENTIFY POISON** (Knowledge, Intellect, Perception)
  + Identify Nethercreature (DEMON HUNTER, WARLOCK)
  + Identify Plant (ARCHAEOLOGIST)
  + Identify Poison (ALCHEMIST, CORPOREALIST/BIOLOGIST,
    HEALER/APOTHECRIST/DOCTOR)
  + Identify Species (ARCHAEOLOGIST, CORPOREALIST/BIOLOGIST, NECROLOGIST)
  + Identify Substance (ALCHEMIST)
- **LIBRARY USE/DATABASE USE** (Knowledge, Intellect, Perception)
  + Library Use (ARCHAEOLOGIST, CORPOREALIST/BIOLOGIST, SCHOLAR, SPELLJACKER,
    THEURIST)
- **NAVIGATION** (Knowledge, Intellect)
  + Navigation (EXPLORER, PILOT, SMUGGLER)
  + Terrain (EXPLORER, OUTRIDER, PILOT, TRACKER/BOUNTY HUNTER, WANDERER)
  + Trade Routes (PILOT, SMUGGLER)
- **READ/WRITE &lsaquo;LANGUAGE&rsaquo;** (Knowledge, Intellect, Perception)
  + Read/Write &lsaquo;Languages&rsaquo; (ARCHAEOLOGIST, DEMON HUNTER,
    INFILTRATOR, OUTRIDER, SCHOLAR, SPELLSLINGER, THEURIST)
- **&lsaquo;SCIENCE&rsaquo;** (Knowledge, Intellect)
  + Anatomy (CORPOREALIST/BIOLOGIST, HEALER/APOTHECRIST/DOCTOR, NECROLOGIST)
  + Artificial Intelligence
  + Architecture (ARCHAEOLOGIST, CARTOGRAPHER)
  + Astral Events (DEMON HUNTER)
  + Astral Theory (SCHOLAR, THEURIST, WARLOCK)
  + Astronomical Events (ARCHAEOLOGIST)
  + Biology
  + Cellular Knowledge (CORPOREALIST/BIOLOGIST, NECROLOGIST, SCHOLAR)
  + Chemistry
  + Criminology (INVESTIGATOR, SHERRIF)
  + Ecology (SCHOLAR)
  + Engineering (SABOTEUR, SCHOLAR)
  + Finite Mathematics (ALCHEMIST, SCHOLAR, THEURIST)
  + Fractals (SCHOLAR, THEURIST)
  + Genetics (NECROLOGIST)
  + Geology
  + Inorganic Alchemy (ALCHEMIST)
  + Mechanics (SABOTEUR)
  + Organic Alchemy (ALCHEMIST, CORPOREALIST/BIOLOGIST,
    HEALER/APOTHECRIST/DOCTOR, NECROLOGIST, SCHOLAR)
  + Physics
  + Plant Physiology (SCHOLAR)
  + Thermodynamics (ALCHEMIST, SABOTEUR, SCHOLAR)
- **SECRET SOCIETIES** (Knowledge, Intellect, Perception)
  + Secret Societies (SHERRIF)
  + Sect Knowledge (ARCHAEOLOGIST, INFILTRATOR, THEURIST)
- **SPEAK &lsaquo;LANGUAGE&rsaquo;** (Knowledge, Intellect, Perception)
  + Speak &lsaquo;Languages&rsaquo; (INFILTRATOR, OUTRIDER, SPELLSLINGER)
- **THEURGY** (Knowledge, Intellect, Spirit)
- **URBAN GEOGRAPHY** (Knowledge, Intellect, Perception)
  + Urban Geography (CARDSHARPER, CARTOGRAPHER, INVESTIGATOR, SHERRIF)

### Magic (Dweomercraft, Mysticism, Witchcraft)

> The **Magic** attribute guages a character's affinity for the use of 
> mystical forces. Most skills based on this attribute are spells, 
> though others do exist, for example, the ability to determine what 
> incantation another chracter is attempting to perform.

### Mechanical (Mechanics, Sensory Extension, Symbiotic Attachment)

> **Mechanics** represents a character's ability to repair machinery, 
> vehicles, weapons, armor, android, and so on. It can also measure 
> ability in skills which require a combination of Reflexes and 
> Knowledge, like shield operation, riding, and driving (you must first 
> learn how to operate the device, but then you must rely on quickness 
> to use the device to its potential).

- **&lsaquo;CREATURE&rsaquo; RIDING** (Mechanical, Reflexes)
  + Ride &lsaquo;Creature&rsaquo; (KNIGHT)
  + Riding &lsaquo;Creature&rsaquo; (OUTRIDER)
  + Horse Riding
  + Giant Lizard Riding
  + Dragon Riding

### Perception (Awareness, Cognition, Observation, Sense)

> Sometimes a character may have the opportunity to notice something in
> his surroundings that might provide an important piece of
> information. For example, a character might spot a bulging pocket on
> an adversary, which may indicate the presence of a concealed weapon.
> The **Perception** attribute covers such instances as well as those
> skills that require the ability to read the emotions or logical
> reasoning of another, like bargaining, commanding, or persuading.

- **BALANCE** (Perception, Reflexes)
  + Balance (PILOT, SPELLJACKER, SPY, WEAPON MASTER)
- **BLINDFIGHTING** (Perception, Reflexes)
  + Blindfighting (BODYGUARD, EVOCATOR, SPELLSLINGER, WEAPON MASTER)
- **CARTOGRAPHY** (Perception, Coordination)
  + Cartography (ARCHAEOLOGIST, CARTOGRAPHER, EXPLORER, PILOT)
  + Chart Constellation (ARCHAEOLOGIST, CARTOGRAPHER, EXPLORER, OUTRIDER, PILOT)
- **CONTACTS** (Perception, Presence)
  + Contacts (CARDSHARPER, INVESTIGATOR, SHERRIF, SMUGGLER)
- **GAMBLING** (Perception, Presence)
  + Gambling (CARDSHARPER, SPELLSLINGER)
- **&lsaquo;INSTRUMENT&rsaquo;** (Perception, Presence, Reflexes)
- **INFORMATION GATHERING** (Perception, Presence)
  + Information Gathering (DEMON HUNTER, INVESTIGATOR, SHERRIF, SPELLJACKER,
    SPY, TRACKER/BOUNTY HUNTER)
- **SHADOWING** (Perception)
  + Tailing (INVESTIGATOR, SHERRIF, SPELLJACKER, SPY, TRACKER/BOUNTY HUNTER)
- **THEATRICS** (Perception, Presence)
  + Theatrics (INFILTRATOR, PERFORMER)
- **TRACKING** (Perception)
  * Listen (BODYGUARD, EXPLORER, INFILTRATOR, INVESTIGATOR, SABOTEUR, SHERRIF,
    SPELLJACKER, SPY, TRACKER/BOUNTY HUNTER)
  + Tracking (INVESTIGATOR, NECROLOGIST, TRACKER/BOUNTY HUNTER)
- **TRAPS** (Perception, Reflexes)
  + Traps (ARCHAEOLOGIST, BODYGUARD, EXPLORER, SABOTEUR, SPELLJACKER)
- **&lsaquo;VESSEL&rsaquo; PILOTING** (Perception)
  + Pilot &lsaquo;Vessel&rsaquo; (EXPLORER, PILOT, SMUGGLER)
  + Starship Piloting
  + Boat Piloting
  + Hovercraft Piloting
  + Car Driving
  + Truck Driving
  + Motorcycle Driving

### Confidence (Charm, Presence)

> This attribute [**Confidence**] represents a character's personal 
> effect on others. It includes such skills as oration, acting, and 
> grooming.

- **ASSUME IDENTITY** (Presence, Intellect)
  + Assume Identity (BODYGUARD, CARDSHARPER, INFILTRATOR, INVESTIGATOR, SABOTEUR, SMUGGLER, SPY)
- **HAGGLING** (Presence, Perception)
  + Bartering (SMUGGLER)
  + Haggling (SMUGGLER)
- **PUBLIC SPEAKING** (Presence)

### Psionic Power (Psychic Ability)

> Like Magic, this attribute [**Psionic Power**] applies only in game 
> worlds where this phenomenon exists, and represents a character's 
> ability to wield psychic powers, from danger sense to pyrotechnics to 
> telekinesis.

### Technical (Technology)

> The **Technical** attribute measures a character's aptitude for 
> technological equipment, from computers to electronic listening 
> devices to electronic security, a well as those skills that require a 
> combination of Knowledge and Coordination, like *first aid* and 
> *forgery*.

### Willpower (Mental Fortitude, Mind, Spirit)

> A character's **Willpower** represents his ability to withstand mental 
> attacks, whether they come from situational pressures, like stress, or 
> direct assault, like magical or psychic phenomena.

- **&lsaquo;CREATIVE ABILITY&rsaquo;** (Spirit, Perception)
  * Animal Training (PERFORMER)
  + Blacksmithing (WEAPON MASTER)
  + Cooking (EXPLORER, WANDERER)
  + Dancing (SPELLSLINGER)
  + Drawing (CARTOGRAPHER)
  * Illusory Magic (PERFORMER)
  + Juggling (PERFORMER)
  + Knots (TRACKER/BOUNTY HUNTER)
  + Poetry
  + Singing
  + Woodcarving (WEAPON MASTER)
- **MEDITATION** (Spirit, Willpower)
  + Meditation (WARLOCK, WEAPON MASTER)


Mentioned Skills
----------------

Page 5 offers an attribute and skill allocation sample.

- *Strength*
  + *Resist damage*
- *Reflexes*
  + *Dodge, starship piloting*
- *Coordination*
  + *Blaster*
- *Perception*
  + *Con, search*
- *Reasoning*
- *Knowledge*
  + *Starports*

Peppered throughout the book are mentions of commonly-used skills, some of
which are not described in the SKILLS chapter.

- *acrobatics* (p. 28)
- *alchemical concoction* (p. 30)
- *alchemy* (p. 25)
- *archery* (p. 9, p. 16, p. 68)
- *balance* (p. 9)
- *banish* (p. 30)
- *blaster* (p. 6, p. 7, p. 9, p. 56, p. 64)
- *blindfighting* (p. 30)
- *blowgun* (p. 27)
- *bow* (p. 27)
- *brawling* (p. 9, p. 18, p. 68)
- *car piloting* (p. 28)
- *chemical concoction* (p. 29)
- *chemistry* (p. 16, p. 25)
- *computer operations* (p. 29, p. 54)
- *con* (p. 7)
- *crossbow* (p. 27)
- *Dwarvish* (p. 27)
- *dodge* (p. 7, p. 8, p. 9, p. 11, p. 16, p. 18, p. 28, p. 65, p. 68, p. 69)
- *driving* (p. 18, p. 28, p. 73)
- *Elfin* (p. 27)
- *energy weapons* (p. 73)
- *engineering* (p. 25)
- *fencing* (p. 69)
- *first aid* (p. 20)
- *forgery* (p. 20)
- *Giantsh* (p. 27)
- *grenade* (p. 27)
- *healing* (p. 38, p. 72)
- *heavy laser pistol* (p. 56)
- *hoverbike piloting* (p. 55, p. 73)
- *jumping* (p. 28)
- *&lsaquo;language&rsaquo;* (p. 23, p. 27)
- *laser cannon* (p. 73)
- *lifting* (p. 18)
- *lockpicking* (p. 11, p. 28)
- *machinegun* (p. 27)
- *machineguns* (p. 73)
- *marksmanship* (p. 67)
- *&lsaquo;melee weapon&rsaquo;* (p. 27)
- *&lsaquo;missile weapon&rsaquo;* (p. 27)
- *piloting* (p. 36, p. 75)
- *pistol* (p. 6, p. 28, p. 67)
- *Russian* (p. 27)
- *resist damage* (p. 7, p. 18)
- *rifle* (p. 27)
- *search* (p. 7, p. 27)
- *security* (p. 34)
- *sensors* (p. 75)
- *shields* (p. 74)
- *shipwright* (p. 3)
- *sneak* (p. 38)
- *speak &lsaquo;language&rsaquo;* (p. 27, p. 28)
- *starfighter piloting* (p. 73)
- *starports* (p. 7)
- *starship gunnery* (p. 70)
- *starship piloting* (p. 6)
- *sword* (p. 18, p. 27, p. 68)
- *Troll* (p. 27)
- *warp magic* (p. 28)

---


External Links
--------------

- [Cognition - Wikipedia](https://en.wikipedia.org/wiki/Cognition)
- [Gymnastics - Wikipedia](https://en.wikipedia.org/wiki/Gymnastics)
- [Mechanics - Wikipedia](https://en.wikipedia.org/wiki/Mechanics)
- [Motor skill - Wikipedia](https://en.wikipedia.org/wiki/Motor_skill)
- [Piloting - Wikipedia](https://en.wikipedia.org/wiki/Piloting)
- [Technology - Wikipedia](https://en.wikipedia.org/wiki/Technology)
