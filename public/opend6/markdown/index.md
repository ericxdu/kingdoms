OpenD6
======

Well-documented breakdown of the [D6 genre book skill system][1].

List of [System Book skills][2] and professions.

[1]: genre-book-skills.html
[2]: system-book-skills.html

Game System Templates
---------------------

- [Anti Fantasy](antifa-system-template.html)
- [Children of Gaia](sidhe-system-template.html)
- [Solar Frontier](solar-system-template.html)
- [Stardust Empire](galaxy-system-template.html)
- [Magus Experiment](magus-system-template.html)

External Links
--------------

- [OpenD6 Products](https://ogc.rpglibrary.org/index.php?title=OpenD6)
- [OpenD6 Project](http://opend6project.org/)
