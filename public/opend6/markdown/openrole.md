Open Roles
==========

This system uses OpenD6 to craft a genre-spanning rules-light game.

Attributes and Skills
---------------------

Attributes are kept minimal and broadly defined. They're chosen in such
a way that it should rarely be a question which attribute roll is
required to perform any action.

### Coordination

Any action involving fine or gross motor skills.

+ **athletics**
+ **stealth**

### Intellect

Actions requiring cognition and memory recall.

+ **knowledge**
+ **perception**

### Willpower

Using force of will to convince others or resist temptation.

+ **persuasion**

### Strength

Raw physical power to lift or push.

### Endurance

Withstanding injury and general toughness
