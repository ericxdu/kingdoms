<img style="float:right;" src="images/d6logo150.png" />

Character Creation Template
===========================

Game World Name: Stardust Empire

Game Designer: Nox Banners Game Design

Character Information
---------------------

Species: humans and equivalent aliens have a 2D minimum and 4D maximum in any
attribute.

Age Requirements: to be determined.

Groups: freelancers, gangsters, governments, militias, rangers, mercenaries

Starting Attribute Dice: 18

Starting Skill Dice: 7


Attributes and Skills
---------------------

- Strength
  + Jumping
  + Lifting
  + Resist Damage
  + Scaling
  + Swimming

- Reflexes
  + Beast Riding
  + Dodge
  + Hand-to-hand Combat
  + Hovercraft Piloting
  + Starship Piloting
  + Truck Driving

- Coordination
  + Grenade
  + Machinegun
  + Pistol
  + Rifle
  + Sneak

- Perception
  + Assume Identity
  + Contacts
  + Haggling
  + Military Command

- Reasoning
  + Law

- Knowledge
  + Computer Operations
  + Forgery
  + Healing
  + Navigation


Game System Template
====================

Genre: soft science fiction

World Overview: Stardust Empire is a galaxy-spanning sci-fi setting taking
place after the collapse of a galactic empire. Many star systems are isolated
after centuries of interstellar commerce. Everything from local system
governments to interplanetary organized crime to interstellar warlords to
isolated utopias exist in sectors of space around the galaxy.

Technology Level: [artificial gravity][1], [hyperspace travel][2], 
[energy weapons][3], [space aliens][4], metaphysical abilities and technology

[1]: galaxy-system-gravitics.html
[2]: galaxy-system-hyperspace.html
[3]: galaxy-system-weapons.html
[4]: galaxy-system-aliens.html

Powers Section
--------------

### Types

+ Metaphysics

### Limitations & Restrictions

To be determined.

### Power Skill Names

To be determined.


Combat Section
--------------

### Damage System

Body Points Formula: 20 + Strength roll.

### Round Structure

Simultaneous

### Options

Speed


Miscellaneous Notes
-------------------

This is the same setting where my Freedoom mods take place. Freedoom is a
re-imagining of Doom with an aesthetic closer to science fiction.
