OpenD6 Proto Book Skills
========================

The genre books skills document has identified 37 base skills.

<table>
<tr><th>Coordination</th>		<th>Endurance</th>		<th>Reflexes</th>		<th>Strength</th></tr>
<tr><td>Marksmanship</td>		<td>Stamina</td>		<td>Acrobatics</td>		<td>Lifting</td></tr>
<tr><td>Sleight of Hand</td>		<td>Swimming</td>		<td>Brawling</td>		<td></td></tr>
<tr><td>Throwing</td>			<td></td>			<td>Dodge</td>			<td></td></tr>
<tr><td></td>				<td></td>			<td>Flying</td>			<td></td></tr>
<tr><td></td>				<td></td>			<td>Jumping</td>		<td></td></tr>
<tr><td></td>				<td></td>			<td>Melee Combat</td>		<td></td></tr>
<tr><td></td>				<td></td>			<td>Running</td>		<td></td></tr>
<tr><td></td>				<td></td>			<td>Sneak</td>			<td></td></tr>

<tr><th>Intellect</th>			<th>Knowledge</th>		<th>Mechanical</th>		<th>Perception</th></tr>
<tr><td>Business</td>			<td>Languages</td>		<td>Piloting</td>		<td>Gambling</td></tr>
<tr><td>Know-how</td>			<td>Scholar</td>		<td>Repair</td>			<td>Investigation</td></tr>
<tr><td>Navigation</td>			<td></td>			<td>Riding</td>			<td>Search</td></tr>
<tr><td></td>				<td></td>			<td></td>			<td>Streetwise</td></tr>
<tr><td></td>				<td></td>			<td></td>			<td>Survival</td></tr>

<tr><th>Confidence</th>			<th>Technical</th>		<th>Willpower</th>		<th></th></tr>
<tr><td>Command</td>			<td>Artist</td>			<td>Willpower</td>		<td></td></tr>
<tr><td>Con</td>			<td>Medicine</td>		<td></td>			<td></td></tr>
<tr><td>Intimidation</td>		<td>Security</td>		<td></td>			<td></td></tr>
<tr><td>Persuasion</td>			<td>Tech</td>			<td></td>			<td></td></tr>
</table>

External Links
--------------
