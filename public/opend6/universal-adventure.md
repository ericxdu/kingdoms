<img style="float:right;" src="images/d6logo150.png" />

Character Creation Template
===========================

Game World Name: 

Game Designer: Nox Banners Game Design

Character Information
---------------------

Species: 

Age Requirements:

Groups:

Starting Attribute Dice: 15

Starting Skill Dice: 7

Starting Money:


Attributes and Skills
---------------------

- Reflexes
  + Acrobatics
  + Brawling
  + Dodge
  + Flying
  + Marksmanship
  + Melee Combat
  + Piloting
  + Riding
  + Running
  + Sleight of Hand
  + Sneak
  + Throwing

- Knowledge
  + Artist
  + Business
  + Languages
  + Gambling
  + Investigation
  + Medicine
  + Navigation
  + Repair
  + Scholar
  + Search
  + Security
  + Streetwise
  + Survival
  + Tech

- Perception
  + Command
  + Con
  + Hide
  + Intimidation
  + Know-how
  + Persuasion
  + Willpower

- Strength

- Endurance


Game System Template
====================

Genre: 

World Overview: 

Technology Level: 

Powers Section
--------------

Combat Section
--------------

### Damage System

Wounds #: 3

### Round Structure

To be determined.

### Options


Miscellaneous Notes
-------------------
