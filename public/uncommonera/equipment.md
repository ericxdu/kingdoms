Tools in Uncommon Era
=====================

Devices in Uncommon Era are mechanical and energetic tools constructed 
to help with or perform certain tasks. The style of tools is mainly 
"raygun gothic" as demonstrated in these guidelines.

Powered tools are constructed around a "power core" which allows it to 
function. The tool either has a mechanical function, or emits energy in 
the form of a "ray" or an "aura". Rays have directed energy effects 
while auras have personal or area-of-effect energies.

The following are some examples of tools in UE.


Raysword
--------

This device projects a short ray of energy that can cut through 
materials or deflect other energy rays.


Flamethrower
------------

This device consumes liquid fuel and bursts forth with a cone of fire.


Shield Pendant
--------------

This wearable tool projects an aura of energy around the wearer that can 
deflect certain incoming projectiles and attacks.


Shade Cloak
-----------

A clothing tool which has the effect of rendering the wearer difficult 
to see when standing still or moving slowly.


Looking Glass
-------------

A handheld tool that looks like a mirror, but can show the user various 
information in the form of images. Some of these are designed for 
distanced two-way communication, while others can enhance the view of 
its surroundings.
