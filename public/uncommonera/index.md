Uncommon Era
============

Uncommon Era is a science fantasy universe in the alternate history 
genre focused on the interplanetary society of the Solar System. It is a 
shared universe intended for use by artists willing to engage with a 
free culture system of licensing.

The Setting
-----------

The Solar System is located in the Orion-Cygnus Arm of the Milky Way
Galaxy. Various species and cultures populate every last planet and
satellite. Technology centers around a singularity which is the basis
for portable energy sources, power projection rays, and energy
fields/auras. In addition people with the right skill can manifest
innate metaphysical powers.

Most celestial bodies in the Solar System are nominally habitable, and
range from temperate worlds like Earth to fantastic desert, ocean, icy,
and lush forested worlds. The technologies available include
fantastic elements like anti-gravity, air generation, force fields, and
more.

Uncommon Era can feature stories anywhere within a two millienia span,
beginning with early technology and exploration of the Solar System all
the way to advanced interplanetary civilization and beyond.

- [Locations](locations.html)
- [Characters](characters.html)
- [Equipment](equipment.html)
