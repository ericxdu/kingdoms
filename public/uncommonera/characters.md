Adventurers
===========

There are many roles a character in Uncommon Era may perform. An 
adventurer is a traveler and a fighter. Packing light and searching for 
problems to solve, they fall into one of three main archetypes.

The artwork on this page is NOT mine. It was sourced from concept art by 
the Ryzom project.


The Knight
----------

<img alt="Knight" height="300" src="images/knight.jpg" />

The Knight specializes in wielding heavy weapons like the fire axe and 
the grav mace. They are typically adept in metaphysical powers like 
wound healing and force fields. Due to the frontline nature of their 
tactics they typically don medium to heavy synthiron armor for 
protection.

<hr>

The Expert
---------

<img alt="Expert" height="300" src="images/rogue.jpg" style="float:right"/>

The Expert employs a variety of skills related to intelligence, 
dexterity, and agility. Many of the best pilots, hackers, and 
craftspeople are Experts. Their metaphysical abilities often include 
detection, mending, and warding powers. They typically wear synthleather 
armor and rely on an uncanny ability to avoid injury while slipping 
around enemy defenses with light or ranged weapons.

<hr>

The Magus
--------

<img alt="Mage" height="300" src="images/mage.jpg" />

The Magus is a martial artist who can manifest metaphysical energy which 
augments their attacks, movements, and evasiveness. They can call forth 
fire and lightning as well as more nuanced energies like sleep, charm, 
and fear. Due to their reliance on graceful, unrestricted movement the 
Mage typically wears no armor and has little need for martial weapons.

<hr>
