#!/usr/bin/perl

$doctype='<!DOCTYPE html>';
$csslink='<link rel="stylesheet" type="text/css" href="style.css" />';

$markdownfiles = "*.md";
my @files = glob( $markdownfiles );

foreach (@files ) {
   print $_ . "\n";
   open(HYPERTEXT, ">$_.html") or die "Couldn't open file $_.html, $!";
   print HYPERTEXT "$doctype\n";
   print HYPERTEXT "<html>\n";
   print HYPERTEXT "<head>\n";
   print HYPERTEXT "$csslink\n";
   print HYPERTEXT "</head>\n";
   print HYPERTEXT "\n";
   print HYPERTEXT "<body>\n";
   #    perl Markdown.pl $filename >> ${filename%$var1}$var2
   print HYPERTEXT "\tmarkdownoutput\n";
   print HYPERTEXT "</body>\n";
   print HYPERTEXT "</html>\n";
   close(HYPERTEXT) or die "Couldn't close file properly";
}
