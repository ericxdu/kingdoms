To edit, publish, and create new feeds in the most basic way, you need 
`0publish`.

    0install add 0publish http://0install.net/2006/interfaces/0publish

Export the environment variable if needed.

    export PATH=/home/yourusername/bin:$PATH

To make a new release, add a link to the archive online. You should have 
a copy of the archive in your present working directory, or use tag 
`--archive-file`. The tags `--archive-extract` and `--set-arch are 
optional`.

    0publish feed.xml \
        --set-version=1.0 \
        --set-released=today \
        --archive-url=http://www.website.com/download/release-1.0-i386.tar.gz \
        --archive-extract=release-1.0 \
        --set-arch=Linux-i386

You can now launch with:

    0launch ./feed.xml

Before you publish, you must set the URI, sign the feed, and upload to the
set location.

    0publish feed.xml --set-interface-uri=http://www.website.com/feed.xml
    0publish feed.xml --xmlsign

After you have uploaded the feed, you can run the feed from the published URI.

    0launch http://www.website.com/feed.xml

The version you added has a stability of "testing". If it does not exhibit
major bugs, you should change it to "stable" and upload the feed again.

    0publish feed.xml --stable

To add a new version of the software to the feed, use the new archive's 
URL and follow the instructions above, but replace `--set-version` with 
`--add-version` and upload the feed again.

<hr>

These are local archives. They should work if the associated archive is in the
local directory. To run, use:

    0launch feedname.xml

In order to publish them, you first need a gpg key.

    gpg --gen-key

You also need a webserver you can upload files to. Once you know the web 
address where the feed will live, you can "publish" the feed, then 
upload it. You should copy and rename it before publishing it, so you 
can keep the "local" version for future modifications. Just remove the 
"-local" from the filename, then use the following commands to publish. 
ALSO: you will need to change the feed location of any dependencies that 
you are uploading.

    0publish feed.xml --set-interface-uri=http://www.address.net/feed.xml
    0publish feed.xml --xmlsign

After you upload both feed.xml and your key.gpg to your webserver, you 
can now launch live using:

    0launch http://web.address.you/willuse.xml

Alternatively, you should be able to load the feed into the `0install` 
graphical frontend.
