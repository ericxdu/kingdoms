## Lore

Retux takes place in a snowy land called "Icy Island" nestled in a huge,
ice-cold sea.

Tux faces "snowman" bosses and an army of enemies made of ice and snow.
Many of the enemies appear to have sight and sentience
