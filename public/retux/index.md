ReTux
=====

<img alt="screenshot" width="400" height="240" src="screenshot1.png"/>

ReTux is a side-scrolling platformer video game released in 2016 for 
personal computers. There are dozens of challenging and entertaining 
levels with clever puzzle design, accessible gameplay, and a high degree 
of polish. The player takes on the role of Tux the penguin, who embarks 
on a quest to unseat a greedy tyrant who has taken over Icy Island. Read 
more about the [lore here](lore.html).

ReTux appears to have an internally consistent [design 
language](design.html).

Trivia
------

ReTux native resolution appears to be 800x480, with an aspect ratio of 
5:3 that is common among some mobile phone screens. The levels are 
tile-based with a tile size of 32x32, but Tux and all of the enemies 
exceed this size by at least several pixels. The pixel art of ReTux uses 
antialiasing, transparency, and color blending to achieve a smooth, 
paint-like art style at low resolutions. Parallax backgrounds evoke a 
feeling of depth while traversing a level.

Tux is approximately 48x48 pixels in size visually, with a hit box of 
[?] pixels.

Each area of Retux contains several levels accessible by a level 
select system known as a "worldmap". Each worldmap appears to have a 
theme indicated by the level names and the challenges within the levels.

- World 1: Numbers
  + the first five levels are named after numbers in counting sequence
- World 2: Springs
  + each level has "spring" in the name and involves a spring puzzle
- World 3: Ice and Iron
  + first appearance of the Ice Flower and armored enemies
  + first appearance of frozen lakes in the worldmap
  + puzzles require freezing armored enemies in order to jump on them
- World 4: Explosives and Destructables
  + first appearance of bombs, puzzles require destroying blocks
- World 5

Fish coins show up in distinct patterns in particular places of some 
levels. Some frequently recurring patterns are depicted below. Often 
these patterns mark a secret.

<pre>
 o oo
oo  o
o    
</pre>

Making Maps
-----------

My first map for ReTux is included with the game as a bonus level, 
thanks to a level design contest judged by the creator. Making maps for 
a video game can be a difficult and frustrating process, but if you 
follow lessons and tutorials carefully it is a fun and rewarding hobby! 
Links at the bottom of this page provide information on making custom 
levels for ReTux.

Links
-----

- [How to Design Levels](https://gamedevelopment.tutsplus.com/articles/how-to-design-levels-with-the-super-mario-world-method--cms-25177)
- [Level Editing Guide - ReTux](https://retux-game.github.io/leveledit/index.html)
