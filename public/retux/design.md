Retux Design Language
=====================

The Retux engine is versatile and generalized. Only the "Items" and
"NPCs" seem to have bespoke logic based on their appearence, while
things like "pipes", "spikes", "lava", and even terrain tiles have no
default behavior and must be defined by the level logic.

Pipes
-----

Pipes often indicate a "warp". Usually, the shaft of the pipe itself is
a background feature, while the opening can be stood upon or entered.

Lava and Spikes
---------------

Levels are programmed to kill Tux if he touches a spike tile or a lava
tile. The thin wavy top tile of lava is decorative and doesn't harm Tux.

Platforms
---------

Ground tiles and platform tiles appear to have a specific design language.

Block-shaped tiles with smooth tops in the "castle" tileset are always
physically solid. However the one-tile-tall, brick-paved platform tiles
are only solid on the top, and are always supported by layered columns
of smooth stone beneath which are not solid.
