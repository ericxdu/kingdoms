# Voxel Art and Utilities

This is a project to document the 3-dimensional structures necessary for 
good voxel-based art and buildings. Voxels are fixed-volume cubes or 
bricks which are laid out edge-to-edge in a fixed matrix, much like 
pixel art is in two dimensions. Some popular examples of voxel structure 
and mechanics are Minecraft/Minetest and Veloren.

## Tilemapping

Displaying 2D tiles designed to look like 3D voxels with a depth plane 
is not very different from a standard 2D tilemap. The offset x and y 
positions are such that tiles are rendered overlapping each other rather 
than edge-to-edge, and subsequent layers are drawn to cover up "bottom" 
layers. I'll be creating a 2D voxel map viewer with Lua in the future.

## Spiral Staircase

The narrowest structure that can be described as a "spiral staircase" is 
probably 3x3 if objects need 1x1 blocks of space to smoothly transition 
to higher/lower depth. For objects that are 2 blocks high, this also 
provides the minimum "headroom" for traversal in such a tight repeating 
space. "Landings" are placed at every corner (c) of the 3x3 space, and 
"steps" (s) are placed between each corner so that the object can 
transition to the next depth. "Steps" must be blocks that the object can 
traverse smoothly without flying or jumping. Light sources can be placed 
on the center column (o).

<pre>
csc
sos
csc
</pre>

## Strip Mining

One type of project involving voxel structure is to hollow out a large 
volume of space until you can visually examine every voxel within the 
volume, usually to find procedurally generated resources. Below is a 2D 
representation of an efficient method for achieving this. You must first 
get to your desired height and then hollow out a width-4 length-4 space 
as your base of operations.

<pre>
     ^  ^
     |  |  ^
     i  i  |
     o  o  o
     o  o  o
     o  o  ioooooioooooi->
     o  o  o
     o  o  o
     i  i  ioooooioooooi->
     o  o  o
     o  o  o
     o  o  ooooooiooioo->
     o  o  oooo  o  o
     o  o  oooo  o  o
 <-ooiooioooooo  o  o
              o  o  o
              o  o  o
<-ioooooioooooi  i  i
              o  o  o
              o  o  o
<-ioooooioooooi  o  o
              o  o  o
              o  o  o
              |  i  i
              v  |  |
                 v  v
</pre>

The arrows indicate an infinitely repeatable pattern. If lighting is 
required, put down a light source every three voxels from the 4x4 area, 
and every 6 voxels from each of the four main branches.
