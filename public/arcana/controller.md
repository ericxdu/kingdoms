Controller Theory
=================

Linux gamepad specification standardizes action buttons as a collection of two to four buttons, usually in a diamond pattern on the right side of the controller. Under the hood, the buttons may be arranged in any digital order, but from the outside it is useful use the standard to refer to the action buttons in a consistent way. Depending on whether it is based on the Playstation, Nintendo, Xbox, or Retropad the beginning, order, and naming of the buttons will appear to be different. Linux specification provides a standard way to refer to them based on position.

Buttons in a diamond pattern are referred to as North, South, West, and East. The first button is always North and the order proceeds to West, South, and East.

<table>
	<tr>
		<td colspan="2" style="padding:1em;text-align:center;">North<td>
	</tr>
	<tr>
		<td colspan="1" style="padding:1em;text-align:center;">West</td>
		<td colspan="1" style="padding:1em;text-align:center;">East</td>
	</tr>
	<tr>
		<td colspan="2" style="padding:1em;text-align:center;">South</td>
	</tr>
</table>


Canonical Game Controls 
-----------------------

- Doom (Vanilla)
  1. Firing
  2. Open
  3. 
  4. 

- Chocolate Doom
  1. Fire/Attack
  2. Strafe
  3. Speed
  4. Use

- Descent (Playstation)
  1. Forward
  2. Strafe Left
  3. Backward
  4. Strafe Right

- Colony Wars
  1. 
  2. 
  3. 
  4. 
