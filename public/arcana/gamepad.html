<h1>Linux Gamepad Specification</h1>

<p><address>
<time>2013</time> by
<a rel="author" href="dh.herrmann@gmail.com">David Herrmann</a>
</address></p>

<h2>Introduction</h2>

<p>Linux provides many different input drivers for gamepad hardware. To avoid
having user-space deal with different button-mappings for each gamepad, this
document defines how gamepads are supposed to report their data.</p>

<h2>Geometry</h2>

<p>As "gamepad" we define devices which roughly look like this::</p>

<pre><code>            ____________________________              __
           / [__ZL__]          [__ZR__] \               |
          / [__ TL __]        [__ TR __] \              | Front Triggers
       __/________________________________\__         __|
      /                                  _   \          |
     /      /\           __             (N)   \         |
    /       ||      __  |MO|  __     _       _ \        | Main Pad
   |    &lt;===DP===&gt; |SE|      |ST|   (W) -|- (E) |       |
    \       ||    ___          ___       _     /        |
    /\      \/   /   \        /   \     (S)   /\      __|
   /  \________ | LS  | ____ |  RS | ________/  \       |
  |         /  \ \___/ /    \ \___/ /  \         |      | Control Sticks
  |        /    \_____/      \_____/    \        |    __|
  |       /                              \       |
   \_____/                                \_____/

       |________|______|    |______|___________|
         D-Pad    Left       Right   Action Pad
                 Stick       Stick

                   |_____________|
                      Menu Pad
</code></pre>

<p>Most gamepads have the following features:</p>

<ul>
<li>Action-Pad
4 buttons in diamonds-shape (on the right side). The buttons are
differently labeled on most devices so we define them as NORTH,
SOUTH, WEST and EAST.</li>
<li>D-Pad (Direction-pad)
4 buttons (on the left side) that point up, down, left and right.</li>
<li>Menu-Pad
Different constellations, but most-times 2 buttons: SELECT - START
Furthermore, many gamepads have a fancy branded button that is used as
special system-button. It often looks different to the other buttons and
is used to pop up system-menus or system-settings.</li>
<li>Analog-Sticks
Analog-sticks provide freely moveable sticks to control directions. Not
all devices have both or any, but they are present at most times.
Analog-sticks may also provide a digital button if you press them.</li>
<li>Triggers
Triggers are located on the upper-side of the pad in vertical direction.
Not all devices provide them, but the upper buttons are normally named
Left- and Right-Triggers, the lower buttons Z-Left and Z-Right.</li>
<li>Rumble
Many devices provide force-feedback features. But are mostly just
simple rumble motors.</li>
</ul>

<h2>Detection</h2>

<p>All gamepads that follow the protocol described here map <code>BTN_GAMEPAD</code>. This is
an alias for <code>BTN_SOUTH/BTN_A</code>. It can be used to identify a gamepad as such.
However, not all gamepads provide all features, so you need to test for all
features that you need, first. How each feature is mapped is described below.</p>

<p>Legacy drivers often don't comply to these rules. As we cannot change them
for backwards-compatibility reasons, you need to provide fixup mappings in
user-space yourself. Some of them might also provide module-options that
change the mappings so you can advise users to set these.</p>

<p>All new gamepads are supposed to comply with this mapping. Please report any
bugs, if they don't.</p>

<p>There are a lot of less-featured/less-powerful devices out there, which re-use
the buttons from this protocol. However, they try to do this in a compatible
fashion. For example, the "Nintendo Wii Nunchuk" provides two trigger buttons
and one analog stick. It reports them as if it were a gamepad with only one
analog stick and two trigger buttons on the right side.
But that means, that if you only support "real" gamepads, you must test
devices for <em>all</em> reported events that you need. Otherwise, you will also get
devices that report a small subset of the events.</p>

<p>No other devices, that do not look/feel like a gamepad, shall report these
events.</p>

<h2>Events</h2>

<p>Gamepads report the following events:</p>

<ul>
<li><p>Action-Pad:</p>

<p>Every gamepad device has at least 2 action buttons. This means, that every
device reports <code>BTN_SOUTH</code> (which <code>BTN_GAMEPAD</code> is an alias for). Regardless
of the labels on the buttons, the codes are sent according to the
physical position of the buttons.</p>

<p>Please note that 2- and 3-button pads are fairly rare and old. You might
want to filter gamepads that do not report all four.</p>

<ul>
<li><p>2-Button Pad:</p>

<p>If only 2 action-buttons are present, they are reported as <code>BTN_SOUTH</code> and
<code>BTN_EAST</code>. For vertical layouts, the upper button is <code>BTN_EAST</code>. For
horizontal layouts, the button more on the right is <code>BTN_EAST</code>.</p></li>
<li><p>3-Button Pad:</p>

<p>If only 3 action-buttons are present, they are reported as (from left
to right): <code>BTN_WEST</code>, <code>BTN_SOUTH</code>, <code>BTN_EAST</code>
If the buttons are aligned perfectly vertically, they are reported as
(from top down): <code>BTN_WEST</code>, <code>BTN_SOUTH</code>, <code>BTN_EAST</code></p></li>
<li><p>4-Button Pad:</p>

<p>If all 4 action-buttons are present, they can be aligned in two
different formations. If diamond-shaped, they are reported as <code>BTN_NORTH</code>,
<code>BTN_WEST</code>, <code>BTN_SOUTH</code>, <code>BTN_EAST</code> according to their physical location.
If rectangular-shaped, the upper-left button is <code>BTN_NORTH</code>, lower-left
is <code>BTN_WEST</code>, lower-right is <code>BTN_SOUTH</code> and upper-right is <code>BTN_EAST</code>.</p></li>
</ul></li>
<li><p>D-Pad:</p>

<p>Every gamepad provides a D-Pad with four directions: Up, Down, Left, Right
Some of these are available as digital buttons, some as analog buttons. Some
may even report both. The kernel does not convert between these so
applications should support both and choose what is more appropriate if
both are reported.</p>

<ul>
<li><p>Digital buttons are reported as:</p>

<p><code>BTN_DPAD_*</code></p></li>
<li><p>Analog buttons are reported as:</p>

<p><code>ABS_HAT0X</code> and <code>ABS_HAT0Y</code></p></li>
</ul>

<p>(for ABS values negative is left/up, positive is right/down)</p></li>
<li><p>Analog-Sticks:</p>

<p>The left analog-stick is reported as <code>ABS_X</code>, <code>ABS_Y</code>. The right analog stick is
reported as <code>ABS_RX</code>, <code>ABS_RY</code>. Zero, one or two sticks may be present.
If analog-sticks provide digital buttons, they are mapped accordingly as
<code>BTN_THUMBL</code> (first/left) and <code>BTN_THUMBR</code> (second/right).</p>

<p>(for ABS values negative is left/up, positive is right/down)</p></li>
<li><p>Triggers:</p>

<p>Trigger buttons can be available as digital or analog buttons or both. User-
space must correctly deal with any situation and choose the most appropriate
mode.</p>

<p>Upper trigger buttons are reported as <code>BTN_TR</code> or <code>ABS_HAT1X</code> (right) and <code>BTN_TL</code>
or <code>ABS_HAT1Y</code> (left). Lower trigger buttons are reported as <code>BTN_TR2</code> or
<code>ABS_HAT2X</code> (right/ZR) and <code>BTN_TL2</code> or <code>ABS_HAT2Y</code> (left/ZL).</p>

<p>If only one trigger-button combination is present (upper+lower), they are
reported as "right" triggers (<code>BTN_TR</code>/<code>ABS_HAT1X</code>).</p>

<p>(ABS trigger values start at 0, pressure is reported as positive values)</p></li>
<li><p>Menu-Pad:</p>

<p>Menu buttons are always digital and are mapped according to their location
instead of their labels. That is:</p>

<ul>
<li><p>1-button Pad:</p>

<p>Mapped as <code>BTN_START</code></p></li>
<li><p>2-button Pad:</p>

<p>Left button mapped as <code>BTN_SELECT</code>, right button mapped as <code>BTN_START</code></p></li>
</ul>

<p>Many pads also have a third button which is branded or has a special symbol
and meaning. Such buttons are mapped as <code>BTN_MODE</code>. Examples are the Nintendo
"HOME" button, the Xbox "X" button or the Sony PlayStation "PS" button.</p></li>
<li><p>Rumble:</p>

<p>Rumble is advertised as <code>FF_RUMBLE</code>.</p></li>
<li><p>Profile:</p>

<p><p>Some pads provide a multi-value profile selection switch.  An example is the
XBox Adaptive and the XBox Elite 2 controllers.  When the active profile is
switched, its newly selected value is emitted as an <code>ABS_PROFILE</code> event.</p></li>
</ul></p>
