var global_putting;
var BALL_SIZE = 10;

function startGame() {
  myGameArea.start();
  var width = myGameArea.canvas.width;
  var height = myGameArea.canvas.height;
  ball = new Ball(width / 3, height / 4, BALL_SIZE);
}

function Ball(x, y, size) {
  this.x = x;
  this.y = y;
  this.dx = 0;
  this.dy = 0;
  this.size = size;
  this.decel = function(){
    var mag = Math.sqrt(this.dx * this.dx + this.dy * this.dy);
    var val = 0.05;
    if (val >= mag) {
      this.dx = 0;
      this.dy = 0;
    } else {
      var dx2 = (this.dx / mag) * val;
      var dy2 = (this.dy / mag) * val;
      this.dx -= dx2;
      this.dy -= dy2;
    }
  }
  this.draw = function(ctx){
    ctx.beginPath();
    ctx.arc(this.x + 2, this.y + 2, this.size, 0, 2 * Math.PI);
    ctx.fillStyle = "Black";
    ctx.fill();
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.size, 0, 2 * Math.PI);
    ctx.fillStyle = "White";
    ctx.fill();
  }
  this.move = function(width, height, friction){
    this.x += this.dx;
    this.y += this.dy;
    var left = this.size;
    var top = this.size;
    var right = width - this.size;
    var bottom = height - this.size;
    if (this.x < left && this.dx < 0) {
      this.dx = -this.dx;
      this.x = left;
      this.decel(0.8);
    } else if (this.x > right && this.dx > 0) {
      this.dx = -this.dx;
      this.x = right;
      this.decel(0.8);
    }
    if (this.y < top && this.dy < 0) {
      this.dy = -this.dy;
      this.y = top;
      this.decel(0.8);
    } else if (this.y > bottom && this.dy > 0) {
      this.dy = -this.dy;
      this.y = bottom;
      this.decel(0.8);
    }
  }
  this.moving = function() {
    return this.dx != 0 || this.dy != 0;
  }
  this.touches = function(mx, my){
    var xSq = mx - this.x;
    var ySq = my - this.y;
    var mag = Math.sqrt(xSq * xSq + ySq * ySq);
    if (mag < this.size) {
      return true;
    }
  }
}

var myGameArea = {
  canvas : document.createElement("canvas"),
  start : function() {
    this.canvas.width = 500;
    this.canvas.height = 500;
    this.canvas.style.cursor = "none";
    this.context = this.canvas.getContext("2d");
    document.body.insertBefore(this.canvas, document.body.childNodes[0]);
    this.interval = setInterval(updateGameArea, 20);
    window.addEventListener('mousedown', function (e) {
      var xSq = e.pageX - ball.x;
      var ySq = e.pageY - ball.y;
      var mag = Math.sqrt(xSq * xSq + ySq * ySq);
      if (ball.sunk) {
        ball = new Ball(e.pageX, e.pageY, BALL_SIZE);
      }
      if (mag < ball.size) {
        global_putting = true;
        myGameArea.x = e.pageX;
        myGameArea.y = e.pageY;
      }
    })
    window.addEventListener('mousemove', function (e) {
      myGameArea.x = e.pageX;
      myGameArea.y = e.pageY;
    })
    window.addEventListener('mouseup', function (e) {
      if (global_putting) {
        global_putting = null;
        ball.dx = (ball.x - myGameArea.x) / 20;
        ball.dy = (ball.y - myGameArea.y) / 20;
      }
    })
  },
  paint : function(g) {
    var width = this.canvas.width;
    var height = this.canvas.height;
    g.fillStyle = "DarkGreen";
    g.fillRect(0, 0, width, height);
    g.beginPath();
    g.arc(width / 2, height / 2, height * 0.45, 0, 2 * Math.PI);
    g.fillStyle = "Green";
    g.fill();
  }
}

function updateGameArea() {
  myGameArea.paint(myGameArea.context);
  ctx = myGameArea.context;
  var width = myGameArea.canvas.width;
  var height = myGameArea.canvas.height;
  ball.decel();
  ball.move(myGameArea.canvas.width, myGameArea.canvas.height, 0.2);
  ball.draw(myGameArea.context);
  if (global_putting) {
    ctx.beginPath();
    ctx.moveTo(ball.x, ball.y);
    ctx.lineTo(myGameArea.x, myGameArea.y);
    ctx.strokeStyle = "Black";
    ctx.stroke();
  }
  ctx.beginPath();
  ctx.moveTo(myGameArea.x - 9, myGameArea.y + 1)
  ctx.lineTo(myGameArea.x - 1, myGameArea.y + 1)
  ctx.moveTo(myGameArea.x + 3, myGameArea.y + 1)
  ctx.lineTo(myGameArea.x + 11, myGameArea.y + 1)
  ctx.moveTo(myGameArea.x + 1, myGameArea.y - 9)
  ctx.lineTo(myGameArea.x + 1, myGameArea.y - 1)
  ctx.moveTo(myGameArea.x + 1, myGameArea.y + 3)
  ctx.lineTo(myGameArea.x + 1, myGameArea.y + 11)
  ctx.strokeStyle = "Black";
  ctx.stroke();
  ctx.moveTo(myGameArea.x - 10, myGameArea.y)
  ctx.lineTo(myGameArea.x - 2, myGameArea.y)
  ctx.moveTo(myGameArea.x + 2, myGameArea.y)
  ctx.lineTo(myGameArea.x + 10, myGameArea.y)
  ctx.moveTo(myGameArea.x, myGameArea.y - 10)
  ctx.lineTo(myGameArea.x, myGameArea.y - 2)
  ctx.moveTo(myGameArea.x, myGameArea.y + 2)
  ctx.lineTo(myGameArea.x, myGameArea.y + 10)
  ctx.strokeStyle = "Yellow";
  ctx.stroke();
}
