Trackpad
========

Compact form-factor interfaces like laptops often include a rudimentary pointing device; a touch-sensitive panel or a tiny joystick. The typical configuration is this pointing device plus one or more buttons and a compact keyboard.

When targeting a trackpad interface, keep in mind the player will have limited dexterity due to the device, and likely cannot press keys at the same time as moving the cursor. Operating systems often make it impossible to use keys and pointer at the same time. Be wary of click-and-drag operations, as this is difficult when using most trackpads.

Trackpad Only
-------------

A trackpad-only interface is as described above, but has no keyboard. This interface allows the player to move a pointer and press at least one button. 
