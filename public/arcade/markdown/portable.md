Portable Games
--------------

The ease with which a video game can be installed and executed is an essential feature. The game should be obtained from canonical sources; from the official proprietor or trusted redistributors (file sources which are considered "shady" by an end-user are a problem). It should also be easy to copy them game to give to someone else, keep it in a personal archive for later reinstallation, or move it to a different computer or storage structure.


Portability
-----------

Portability means a game that can be played in many contexts, 
especially across different computer hardware and software platforms. 
This is often achieved by writing games in an interpreted language with 
an interpreter that runs on many platforms, or writing it in compiled 
language that can compile on many platforms. In either case, the 
interpreter and compiler ought to be open source as well as the game 
itself so that it can be updated and/or recompiled in perpetuity.

Its also important to consider hardware limitations. Some games may run 
on theoretically any system, but with the advent of affordable embedded 
computers like the Raspberry Pi and the Beaglebone Black, care should 
be taken to optimize games to run with limited RAM and low-end 
processors. Care should also be taken in choosing dependencies. A 
resources-lite game which depends on RAM-heavy and processor-intense 
frameworks and libraries is less portable.

- LibRetro
  - Lutro
- C
- L&Ouml;VE


Distribution
------------

- 0install
- AppImage
- CMake
- Deb
- ELF
- Flatpak
- GNOME Games
- GNU Make
- Lutris
- RPM


Hard Binaries
-------------

Consideration should be paid to games that do not have available source. Despite an inability to be modified and rebuilt, many games historically maintain a following wherein a community is able to keep the game playable via emulation or portable front-ends.
