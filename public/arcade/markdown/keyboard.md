Keyboard
========

Typical desktop interfaces include a keyboard and a pointing device. The pointing device is usually a mouse or a trackball with at least three buttons, but sometimes as few as one button.

When targeting a keyboard interface, keep in mind that <a target="_blank" href="https://www.sjbaker.org/wiki/index.php?title=Keyboards_Are_Evil">keyboards are evil</a>. Essentially, holding down more than two keys at a time can have unpredictable results. This could easily hinder desktop gamers, but solutions like WASD seem to skirt the issue. You can typically rely on modifier keys to be pressed at the same time as any other keys.

Keyboard Only
-------------




References
----------

- https://en.wikipedia.org/wiki/Computer_keyboard
- https://en.wikipedia.org/wiki/Keyboard_layout
- https://en.wikipedia.org/wiki/Backspace
- https://en.wikipedia.org/wiki/Space_bar
- https://en.wikipedia.org/wiki/Function_key
- https://en.wikipedia.org/wiki/Control_key
- https://en.wikipedia.org/wiki/Alt_key
- https://en.wikipedia.org/wiki/Shift_key
