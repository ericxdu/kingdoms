Zero Install
============

Zero Install is a cross-platform package manager with some interesting features. It can be used to deploy programs, libraries, and source code. Each package is described by an XML "feed" that contains various information and URLs to find it.

__Shared Libraries:__ A shared runtime library described by a Zero Install feed unpacks into a hidden folder in your user directory, without the need to install system-wide and pollute system libraries.

__Environment Variables:__ Zero Install can add a locally-installed library to the path variable, so the linker knows where to find it.

__Authentication:__ Packages are tagged with a checksum and Zero Install checks the contents match the hashing signature. Feeds can also be PGP-signed to verify they were created by a trusted maintainer.
