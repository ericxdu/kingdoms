
### Lua, LÖVE, Lutro

Haphazard drawing of shapes and images. &#128078; &#x1F44E;

In order for a sprite to retain its usefulness, some of it should be contained in a package that's easy to move, copy, and pass between functions.

#### The `unpack` builtin function.

With this technique, parameters for drawing the sprite are stored in a serial table in the order they will be passed to `love.graphics.draw`. This makes the table an object that could reasonably be considered _a sprite_ since the data is stored in exactly the way it is needed to be rendered.

    local sprite = {
      [1] = love.graphics.newImage('spritesheet.png'), --graphics page
      [2] = love.graphics.quad(0, 0, 16, 16, 256, 256), --tile to draw
      [3] = 50, --drawing x position
      [4] = 50, --drawing y position
    }

Drawing is accomplished simply by unpacking the table sequentially as an argument to `love.graphics.draw`.

    function love.draw()
      for _, obj in ipairs(sprites) do
        love.graphics.draw(unpack( obj ))
      end
    end

Extra information can be stored *outside* of the serial portion of the table, for instance delta-V for continuous motion, flags for state, functions, etc. without disrupting the unpacking process.

    sprite.killed = false
    sprite.dx = 100
    sprite.dy = 100

    function sprite.move(self, dt)
      if not self.killed then return end
      self.x = self.x + self.dx * dt
      self.y = self.y + self.dy * dt
    end

#### A custom `render` function.

A more methodical technique is to simple write a draw function in the table representing to sprite. In this case, the parameters for drawing can be stored in named fields just like all the other data for the object.

    local sprite = {
      image = love.graphics.newImage('spritesheet.png'),
      quad = love.graphisc.newQuad(0, 0, 16, 16, 256, 256),
      x = 100, y = 100,
      dx = 150, dy = 150,
      radius = 16,
    }

There are two ways to draw the sprite. The standard object-oriented method is for the function to simple render its `self`.

    function sprite.draw(self)
      love.graphics.draw(
        self.image,
        self.quad,
        self.x,
        self.y
      )
    end

The method I usually choose is to return the data needed for the main module to handle rendering.

    function sprite.draw(self)
      return self.image, self.quad, self.x, self.y
    end

    function love.draw()
      for _, obj in ipairs(sprites) do
        love.graphics.draw(obj.draw( ))
      end
    end
