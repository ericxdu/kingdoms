Hase "Worms like game with gravity"
===================================

Portable
--------

Compilation requires one custom library (that also compiles easily) and some SDL reference implementations. They can be installed manually or automated pretty easily. &check; Good

Accessible
----------

Screens are information-dense and some required (default) keys are not available on all keyboards. &square; Average

[Hase - gravity artillery shooter](http://ziz.gp2x.de/hase/index.htm)
