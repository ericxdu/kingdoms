The Linux Gamepad
=================

<div class="controller">

<div class="controller front-triggers">
	<div class="joystick-key wide">ZL</div>
	<div class="joystick-key wide">ZR</div>

	<div class="joystick-key wide">TL</div>
	<div class="joystick-key wide">TR</div>
</div>

<div class="controller direction-pad">
	<div class="joystick-key" style="grid-column-start:2;grid-column-end:3;">&Hat;</div>

	<div class="joystick-key" style="grid-column-start:1;grid-column-end:2;">&lt;</div>
	<div class="joystick-key" style="grid-column-start:3;grid-column-end:4;">&gt;</div>

	<div class="joystick-key" style="grid-column-start:2;grid-column-end:3;">&vee;</div>
</div>

<div class="controller menu-pad">
	<div class="joystick-key big" style="grid-column-start:2;grid-column-end:3;">@</div>

	<div class="joystick-key wide" style="grid-column-start:1;grid-column-end:2;">&vltri;</div>
	<div class="joystick-key wide" style="grid-column-start:3;grid-column-end:4;">&vrtri;</div>
</div>

<div class="controller action-pad">
	<div class="joystick-key blue" style="grid-column-start:2;grid-column-end:3;">N</div>

	<div class="joystick-key red" style="grid-column-start:1;grid-column-end:2;">W</div>
	<div class="joystick-key green" style="grid-column-start:3;grid-column-end:4;">E</div>

	<div class="joystick-key yellow" style="grid-column-start:2;grid-column-end:3;">S</div>
</div>

<div class="controller control-sticks">
	<div class="joystick-key big">L</div>
	<div class="joystick-key big">R</div>
</div>

</div>
