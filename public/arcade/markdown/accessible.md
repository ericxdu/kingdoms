Accessible Games
================

Video games should be accessible in the broadest sense. It should be easy to install and start up, as well as easy to understand and get started playing. Even very complex games should have an easy-to-understand initial game state and discoverable control scheme.


Accessibility
-------------

Accessibility refers to how easy it is to obtain, install, and play a 
game. While this includes consideration for physical disabilities, one 
of the main pitfalls in this area is a difficult installation medium. 
Your target user may not have access to Flatpak, or your build process 
may be so complicated that it fails to build under many circumstances.

The human interface is also important. Consider whether the target 
platform may have both a keyboard and a mouse, or just one of the two, 
or only a game controller. The control scheme itself can be a barrier 
to entry. Consider the discoverability of your controls, paying special 
attention to complicated patterns like hold-and-release, or assigning 
dozens of actions each to an individual letter on the keyboard. There 
are keys on a standard keyboard dedicated to meta-interaction with a 
program like the arrow keys, control, shift, alt, escape, tab, etc..

- Laptops (trackpad/keyboard, integrated graphics)
  + [The Battle for Wesnoth](http://www.wesnoth.org/)
  + &cross; Bad for first-person shooters.
- Consoles (joystick/gamepad, livingroom or handheld)
- Keyboard Only
- Mouse Only
- installation
  + distribution
- learning curve


Bad Examples
------------

- Hedgewars: control scheme discovery.


Other Considerations
--------------------

- physical and cognitive disabilities
