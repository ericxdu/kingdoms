Openbox
=======

Openbox is a free window manager from 2002 that has gone on to serve as the window manager for [LXDE](http://www.lxde.org/) and several distributions of GNU/Linux. Openbox can also be used as a standalone window manager with no desktop environment. In this state it provides a menu upon right-clicking the desktop as well as two startup files and two configuration files.


Autostart
---------

Useful commands to put in `~/.config/openbox/autostart`

    xsetroot -solid gray
    synclient TapButton1=1
    synclient TapButton2=3
    synclient TapButton3=2


Applications
------------

The Openbox default menu contains commands to launch various useful applications without checking whether they are present on the system. Here are package names for some of them.

    gvim
    obconf
    rxvt-unicode
    xchat
    xterm

The file `~/.config/openbox/menu.xml` can be modified to include only the applications present.

    
High DPI
--------

The following line in `~/.Xresources` will double the DPI, but only Openbox and some GNOME/KDE/XFCE apps respond to this.

    Xft.dpi: 192
