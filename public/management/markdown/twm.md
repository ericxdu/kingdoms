Tab Window Manager
==================

TWM is the official window manager for the X Window System. It features flat, standard colors and minimal tooling. It is also highly customizable via home directory configuration files. The goal of this document is to create a minimally usable desktop using TWM and other official X.Org apps.

Useful Packages
---------------

If not already present install TWM from the packaged `xorg-twm`. The default menu comes with one essential application provided by the package `xorg-xterm`. In order to set the desktop with a pleasant color you will need `xorg-xsetroot`. The official display manager is in `xorg-xdm`.

X Display Manager
-----------------

The official display manager must be enabled in place of the pre-installed one.

    systemctl disable gdm.service
    systemctl enable xdm.service

Startup File
------------

XDM will execute `~/.xsession` to setup X and start the window manager and applications. This is a blank file that you can write yourself. You can start by setting the desktop to a pleasant color and putting `twm` on the last line.

    xsetroot -solid gray
    twm

You must make `~/.xsession` executable with the following command.

    chmod +x ~/.xsession

Touchpad
--------

If your computer uses a touchpad for input the `synclient` command should be available to set it up for tapping and other actions. Add the following lines to `~/.xsession` for a good setup.

    synclient TapButton1=1
    synclient TapButton2=3
    synclient TapButton3=2

HiDPI
-----

If your monitor resolution is too small to see TWM tooling and fonts, the easiest solution is to upscale the whole display from a smaller monitor with the same aspect ratio. Find your display name with `xrandr` and add something like one of the following lines to `~/.xsession`. 

    xrandr --output eDP-1 --scale-from 1280x720
    xrandr --output eDP-1 --scale-from 960x540

Using
-----

TWM has no taskbar, system tray, or start menu. Packages can be installed to fill these purposes. Otherwise to access the main menu you left-click and hold on the desktop. By default Xterm is the only available application from the menu and you use it to launch other applications. You place windows manually when a program is launched and "iconify" windows to the desktop instead of minimizing them. The main menu has various commands to manipulate windows and icons.

By default, window focus follows the mouse cursor. If you move the cursor out of Xterm you can no longer type in the window. There is a menu option to force focus to stay on an application. The desktop doesn't dim or sleep after inactivity, but it will blank and you can un-blank simply by moving the mouse. No login is required to return from the blanked state.

The operating system can be fully controlled from the Xterm command-line, including starting applications, managing and installing packages, and changing system configuration. Quality-of-life enhancements can be achieved by installing useful programs and putting them in `~/.xsession` with the `&` character after them. Here is a sample `~/.xsession` file using what has been discussed so far.

    xsetroot -solid gray
    synclient TapButton1=1
    synclient TapButton2=3
    synclient TapButton3=2
    xrandr --output eDP=1 --scale-from 1280x720
    xclock -geometry 50x50-1+1 &
    twm

Application Menu
----------------

As TWM comes with a command menu on left-clicking the desktop, its useful to create an application menu by right-clicking the desktop. To do this you must add some lines to `~/.twmrc`. The following line calls a new menu if you right-click on the desktop.

    Button3 = : root : f.menu "applications"

Next create the basic main menu by adding the following lines. This menu configuration assumes you have installed the packages `xorg-xbiff`, `xorg-xcalc`, `xorg-xedit`, `xorg-xeyes`, `xorg-xload`, and  `xorg-xlogo`.

    menu "applications"
    {
    "Applications"  f.title
    "Xbiff" !"xbiff &"
    "Xcalc" !"xcalc &"
    "Xedit" !"xedit &"
    "Xeyes" !"xeyes &"
    "Xload" !"xload &"
    "Xlogo" !"xlogo &"
    }


Using with GDM or LightDM
-------------------------

In order for TWM to appear in your session options a file must be added to xsessions like `/usr/share/xsessions/twm.desktop` containing the following lines. 

    [Desktop Entry]
    Name=TWM
    Comment=X.org Tab Window Manager
    Exec=/usr/bin/twm
    TryExec=/usr/bin/twm
    Type=Application

TWM can behave differently if initiated by XDM instead of the original display manager.


References
----------

- https://wiki.archlinux.org/title/HiDPI
- https://wiki.archlinux.org/title/Xdg-menu
- https://wiki.archlinux.org/title/Twm
