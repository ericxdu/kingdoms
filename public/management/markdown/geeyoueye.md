Desktop - The Graphical User Interface
======================================

## `.desktop` files

## Installing Files

Apt Package Manager

Software Installer

Flatpak

## Creating Files

Open Text Editor and save somewhere.

Create new files from templates e.g. a blank text file named "blank" or "New File".

## Running Files

Set "Executable as Program" and "Run as Program" or double-click.

## Nautilus Custom Scripts

Create a new file in `~/.local/share/nautilus/scripts`. Make sure the first line is `#!/bin/sh` and toggle on the option "Executable as Program". For example, you can automate your Make workflow by supplying a few scripts to Nautilus.

The following script runs Make on the directory. You must right-click a file in the directory in order to run it, but this script ignores the filename.

`~/.local/share/nautilus/scripts/Run Make`

    #!/bin/sh
    make

The filename can be accessed by using the script variable `$1`. This can easily be used to update a single Make target if its source file is updated.

`~/.local/share/nautilus/scripts/Run Make on File`

    #!/bin/sh
    make $@

A Make clean action can be performed with this script.

`~/.local/share/nautilus/scripts/Run Make Clean`

    #!/bin/sh
    make clean

## Links

- Anatomy of a .desktop file
- [Bash Reference Manual](https://www.gnu.org/software/bash/manual/bash.html)
- Freedesktop.org

