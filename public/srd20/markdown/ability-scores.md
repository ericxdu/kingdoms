Generating Ability Scores
=========================

Your character will have six ability scores. Typically, you generate these scores by rolling some dice. See [Basics](srd/Basics.html) for information about how to roll dice.

1. Roll 4 six-sided dice and add the three highest results together. The total
   will be between 3 and 18.
2. At the gamemaker's discretion, you may re-roll the lowest die and total the
   new three highest results instead.
3. Record your final result on some scratch paper.
4. Repeat this process five more times. These are your ability scores. You
   don't yet need to assign them to specific abilities.

The next step is to choose a background from the [Standard Backgrounds](standard-backgrounds.html).


Character Types
---------------

There are several ways to determine a character's ability scores. The following guidelines allow you to choose between making a character who is average, elite, high-powered, or anything in between. Your GM will tell you which method(s) everyone can use to generate scores for a particular campaign. Most of the time a GM will call for elite or tougher characters and disallow the point buy system, but there are exceptions.

<h3 id="average">Average Characters</h3>

Use the scores 11, 10, 11, 10, 11, 10. Creatures who advance by creature type use these scores. See [Improving Monsters](srd/ImprovingMonsters.html) for details.

__Rolling:__ Roll 3d6 six times.

__Point Buy System:__ 15 points.

<h3 id="nonelite">Nonelite Characters</h3>

The nonelite array is 13, 12, 11, 10, 9, 8. A character who takes one of the [NPC Classes](srd/NPCClasses.html) uses these scores.

__Rolling:__ Roll 3d6 six times.

__Retry:__ Total modifiers must not be &minus;3 or lower and one score must be 12 or higher.

__Point Buy System:__ 15 points.

<h3 id="challenging">Challenging Characters</h3>

Use the scores 15, 13, 12, 11, 10, 8. Challenging characters are nearly but not quite as good as elite characters.

__Point Buy System:__ 22 points.

<h3 id="elite">Elite Characters</h3>

The elite array is 15, 14, 13, 12, 10, 8. A character who takes one of [Classes I](srd/ClassesI.html) or [Classes II](srd/ClassesII.html) typically uses these scores.

__Rolling:__ Roll 4d6 six times and total the three highest dice for each score.

__Retry:__ Total modifiers must be at least +1 and one score must be 14 or higher.

__Point Buy System:__ 25 points.

<h3 id="tougher">Tougher Characters</h3>

Use the scores 15, 14, 13, 12, 11, 10. Tougher characters are slightly better than elite characters.

__Rolling:__ Roll 4d6 six times, rerolling the lowest die _once_ during the entire process, and total the three highest dice for each score.

__Retry:__ Total modifiers must be at least +1 and one score must be 14 or higher.

__Point Buy System:__ 28 points.

<h3 id="high-powered">High-powered Characters</h3>

Use the scores 16, 15, 13, 12, 11, 10. Player-characters with these scores are meant for high-powered campaigns.

__Rolling:__ Roll 5d6 six times, totalling the three highest dice for each score.

__Retry:__ Total modifiers must be at least +2 and one score must be 15 or higher.

__Point Buy System:__ 32 points.


Point Buy System
----------------

<table style="float:left;">
<caption>Table: Ability Score Point Costs</caption>
<tbody>
  <tr>
    <th>Ability Score</th>
    <th>Point Cost</th>
    <th>Ability Score</th>
    <th>Point Cost</th>
  </tr>
  <tr>
    <td>9</td>
    <td>1</td>
    <td>14</td>
    <td>6</td>
  </tr>
  <tr>
    <td>10</td>
    <td>2</td>
    <td>15</td>
    <td>8</td>
  </tr>
  <tr>
    <td>11</td>
    <td>3</td>
    <td>16</td>
    <td>10</td>
  </tr>
  <tr>
    <td>12</td>
    <td>4</td>
    <td>17</td>
    <td>13</td>
  </tr>
  <tr>
    <td>13</td>
    <td>5</td>
    <td>18</td>
    <td>16</td>
  </tr>
</tbody>
</table>

Instead of randomly generating ability scores or using an array, you can buy ability scores using the point buy system if allowed by the GM. You must spend a finite amount of points according to your character type and buy all six scores at once. Each ability starts a 8, and you can increase it to be as high as 18 by spending the amount of points specified in Table: Ability Score Point Costs.

After buying your ability scores, they may be assigned according to your desired strengths and weaknesses and adjusted according to racial ability score adjustments.


<h2 style="clear:both;">Explanation</h2>

I've constructed the arrays according to clues in the SRD and published rulebooks (see [Improving Monsters](srd/ImprovingMonsters.html) for Ability Score Arrays) and assigned a corresponding points cost, die roll method, and standard limitations to each method.

D&D v.3.5 defines three types of creatures according to the quality of
their ability scores. Player-characters have elite ability scores,
while non-player characters have nonelite or average ability scores.
However the *Dungeon Master's Guide* defines several more ways to
generate ability scores which suggests a wider range of quality
creatures can be sorted by.
