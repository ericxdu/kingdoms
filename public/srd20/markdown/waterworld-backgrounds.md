Backgrounds
===========

Backgrounds are designed to expand the concept of choosing a "race" in 
D20. There are some advantages that are explicitly genetic, while others 
are broadly cultural. Compared to "race" a background can include a 
character's genetic makeup, cultural upbringing, regional affinities, 
and experience gained during the formative years of adolesence.

The common man is the baseline human and highly customizable. Other 
backgrounds offer advantages at the expense of extra feats and skill 
points. In addition to Waterworld, this list is a resource for human
variants. Variant humans typically have "any" as a favored class and
no ability score adjustments.


Common Man
----------

The common man varies greatly in build, appearence, and ability. Humans 
tend to be ambitious and individualized, frequently taking an interest 
in something and turning it into a specialization.

### Commoner Traits

* &bull; Medium-size: no special bonuses or penalties due to size.
* &bull; Base land speed is 30 feet.
* &bull; 1 extra feat at 1st level.
* &bull; 4 extra skill points at 1st level and 1 extra skill point at 
each additional level.
* &bull; Automatic Language: Anglic. Bonus Languages: Any.
* &bull; Favored Class: Any.


Psion
-----

The humans of Earth are slowly evolving the genetic ability to perform 
metaphysical feats, similar to the way engines of power are built to 
protect cities, power airships, and grow crops. Contrary to the careful 
intelligence of Earth's metaphysicists, the charmed exhibit this ability 
from charismatic force of self and can generally make magic happen at 
will.

### Psion Traits

* &bull; Medium-size: no special bonuses or penalties due to size.
* &bull; Base land speed is 30 feet.
* &bull; +2 genetic bonus on saving throws against enchantment spells or 
effects.
* &bull; +2 genetic bonus on Diplomacy and Gather Information checks.
* &bull; Automatic Languages: Anglic. Bonus Languages: Any.
* &bull; Spell-Like Abilities: 1/day—*light*, *mage hand*, 
*prestidigitation*. Caster level 1st; save DC 10 + psion’s Cha modifier. 
Uses per day can be increased by taking the [Extra Uses Per 
Day](waterworld-feats.html#extrausesperday) feat.
* &bull; Favored Class: Any.


Angler
------

### Angler Traits

* &bull; Medium-size: no special bonuses or penalties due to size.
* &bull; Base land speed is 30 feet.
* &bull; Weapon Proficiency: Anglers are proficient with tridents, 
harpoons, and nets.
* &bull; +2 cultural bonus on Use Rope checks. Anglers are taught from a 
young age about lashing and knot-tying.
* &bull; +2 cultural bonus on Balance, Jump, and Swim checks from 
decades of boating and diving experience.
* &bull; Automatic Languages: Anglic. Bonus Languages: Any.
* &bull; Favored Class: Any.


Olympian
--------

Olympians hail from a region/city with an athletic and team sport 
culture. Their home town has a stadium or sports college, and there are 
many associations and classes focused on team play and agility training.

### Olympian Traits

* &bull; Medium-size: no special bonuses or penalties due to size.
* &bull; Base land speed is 30 feet.
* &bull; +2 cultural bonus on Climb, Jump, and Move Silently checks.
* &bull; +1 cultural bonus on attack rolls with thrown weapons and slings.
* &bull; Automatic Languages: Anglic. Bonus Languages: Any.
* &bull; Favored Class: Any.


Waterborn
---------

Due to generations of selective breeding and gene therapy, some humans 
are born with genetic traits that allow them spend more time underwater 
and act with less restriction while swimming.

### Waterborn Traits

* &bull; Medium-size: No special bonuses or penalties due to size.
* &bull; Base land speed is 30 feet.
* &bull; Hold Breath: Waterborn can hold their breath for a number of rounds equal to 3 x their Constitution score; about half as long as some ocean mammals.
* &bull; +4 genetic bonus on Swim checks and can take 10 on swim checks even if distracted or endangered.
* &bull; Automatic Languages: Anglic. Bonus Languages: Any.
* &bull; Favored Class: Any.


External Links
--------------

[Humanoid Races](https://www.dandwiki.com/wiki/5e_Humanoid_Races)
