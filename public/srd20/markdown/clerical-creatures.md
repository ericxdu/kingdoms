Bugbear
-------

Chaos, Evil, Trickery, War.


Centaur
-------

Animal, Good, Plant.


Derro
-----

Chaos, Destruction, Evil, Trickery.


Cloud Giant
-----------

Good, Healing, Strength, Sun.

Death, Evil, Trickery.


Fire Giant
----------

Evil, Law, Trickery, War.


Frost Giant
-----------

Chaos, Destruction, Evil, War.


Storm Giant
-----------

Chaos, Good, Protection, War.


Gnoll
-----

Chaos, Evil, Trickery, War. Morningstar.


Goblin
------

Chaos, Evil, Trickery.


Hobgoblin
---------

Evil, Destruction, Trickery.


Kuo-toa
-------

Destruction, Evil, Water.


Lizardfolk
----------

Animal, Plant, Water.


Locathah
--------

Animal, Protection, Water.


Merfolk
-------

Animal, Protection, Water.


Orc
---

Chaos, Evil, Strength, War.


Sahuagin
--------

Evil, Law, Strength, War.


Xill
----

Evil, Law, Strength, Travel.


Yuan-ti
-------

Chaos, Evil, Destruction, Plant.
