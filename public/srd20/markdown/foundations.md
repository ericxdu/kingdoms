Foundations
===========

The fighter class, the cleric class, and the rogue class are considered 
fundamental. (CRII p. 21 or 171)

The different grades of base attack bonus are referred to by different terms
in the <cite>Player's Handbook</cite> and the <cite>Monster Manual</cite>.

<table>
<tr><th>Player's Handbook</th><th>Monster Manual</th></tr>
<tr><td>Good</td><td>HD (as fighter)</td></tr>
<tr><td>Average</td><td>HD &times;3&frasl;4 (as cleric)</td></tr>
<tr><td>Poort</td><td>HD &times;1&frasl;2 (as wizard)</td></tr>
</table>


Racial vs Cultural Traits
-------------------------

Some traits granted to a race are explicitly cultural and could be 
different if the character was raised in a different culture.

### Humans

<cite>Player's Handbook v.3.5</cite> and <cite>Dungeon Master's Guide
v.3.5</cite> note bonus languages and favored classes are a part of
the human culture of diversity and generalization.

Any bonus languages: "Humans mingle with all kinds of other folk and 
thus can learn any language found in an area." (*CRI* p. 13) "Half-elves 
have all the versatility and broad (if shallow) experience that humans 
have." (*CRI* p. 18)

Any favored class: "Half-human elves lack some of the flexibility of 
half-elves who are raised by humans." (*CRII* p. 171)

### Dwarves

Wording in the text implies that some dwarf traits are the result of
special training that is a part of dwarf culture.

Weapon Familiarity: the dwarven waraxe may be a culturally
significant weapon.

Racial bonus against orcs: "Dwarves are trained in the special combat 
techniques that allow them to fight their common enemies more 
effectively." (*CRI* p. 15)

Dodge bonus against giants: "This bonus represents special training that 
dwarves undergo, during which they learn tricks that previous 
generations developed in their battles with giants." (*CRI* p. 15)

### Elves

Thanks to Example Modified Race: Half-Human Elves in <cite>Dungeon
Master's Guide v.3.5</cite>, we know that weapon proficiencies and
wizardry are a part of elf culture.

Weapon proficiencies: "Living among people who esteem the arts of 
swordplay and archery, almost all half-human elves are familiar with 
these weapons." (*CRII* p. 171) "Elves esteem the arts of swordplay and 
archery, so all elves are familiar with these weapons." (*CRI* p. 16)

Favored class: "Half-human elves lack some of the flexibility of 
half-elves who are raised by humans." (*CRII* p. 171) "Wizardry comes 
naturally to elves (they sometimes claim to have invented it), and 
fighter/wizards are especially common among them." (*CRI* p. 16)

### Gnomes

Weapon Familiarity: the gnome hooked hammer may be a culturally
significant weapon.

Racial bonus against kobolds and goblinoids: "Gnomes battle these 
creatures frequently and practice special techniques for fighting them." 
(*CRI* p. 17)

Dodge bonus against giants: "This bonus represents special training that 
gnomes undergo, during which they learn tricks that previous generations 
developed in their battles with giants." (*CRI* p. 17)

### Half-Elves

Bonus on Diplomacy and Gather Information: "Half-human elves have no 
racial bonus on Diplomacy and Gather Information checks." (*CRII* p. 
171) "Half-elves get along naturally with all people." (*CRI* p. 18)

### Half-Orcs

### Halflings

Bonus on attack rolls with a thrown weapon and slings: "Throwing and 
slinging stones is a universal sport among halflings, and they develop 
especially good aim." (*CRI* p. 20)
