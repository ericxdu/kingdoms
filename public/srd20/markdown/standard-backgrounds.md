Standard Character Backgrounds
==============================

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

<table>
<caption>Table: Background Statistics</caption>
<tbody>
  <tr>
    <th>Background</th>
    <th>Size and Type</th>
    <th>Ability Adjustments</th>
    <th>Favored Class</th>
  </tr>
  <tr>
    <td><a href="#dwarves">Dwarf</a></td>
    <td>Medium humanoid</td>
    <td>+2 Constitution, +2 Wisdom, &minus;2 Charisma</td>
    <td>Fighter</td>
  </tr>
  <tr>
    <td><a href="#elves">Elf</a></td>
    <td>Medium humanoid</td>
    <td>+2 Dexterity, &minus;2 Constitution, +2 Intelligence</td>
    <td>Wizard</td>
  </tr>
  <tr>
    <td><a href="#gnomes">Gnome</a></td>
    <td>Small humanoid</td>
    <td>+2 Constitution, &minus;2 Strength, +2 Charisma</td>
    <td>Bard</td>
  </tr>
  <tr>
    <td><a href="#goblins">Goblin</a></td>
    <td>Small humanoid</td>
    <td>&minus;2 Strength, +4 Dexterity, &minus;2 Charisma</td>
    <td>Rogue</td>
  </tr>
  <tr>
    <td><a href="#half-elves">Half-elf</a></td>
    <td>Medium humanoid</td>
    <td>+2 Any</td>
    <td>Any</td>
  </tr>
  <tr>
    <td><a href="#half-orcs">Half-orc</a></td>
    <td>Medium humanoid</td>
    <td>+2 Any</td>
    <td>Barbarian</td>
  </tr>
  <tr>
    <td><a href="#halflings">Halfling</a></td>
    <td>Small humanoid</td>
    <td>+2 Dexterity, &minus;2 Strength, +2 Charisma</td>
    <td>Rogue</td>
  </tr>
  <tr>
    <td><a href="#humans">Human</a></td>
    <td>Medium humanoid</td>
    <td>+2 Any</td>
    <td>Any</td>
  </tr>
  <tr>
    <td><a href="#kobolds">Kobold</a></td>
    <td>Small humanoid</td>
    <td>&minus;4 Strength, +2 Dexterity, &minus;2 Constitution</td>
    <td>Sorcerer</td>
  </tr>
  <tr>
    <td><a href="#orcs">Orc</a></td>
    <td>Medium humanoid</td>
    <td>+4 Strength, &minus;2 Intelligence, &minus;2 Wisdom, &minus;2 Charisma</td>
    <td>Barbarian</td>
  </tr>
</tbody>
</table>

### Description

A background includes your character's general physique, heritage, 
genetic makeup, and cultural traits. You get to decide other 
characteristics like hair, skin, and eye color, clothing, decoration, 
personality, etc. on your own.

### Favored Class

A character's favored class doesn't count against him or her when 
determining experience point penalties for multiclassing.

### Race and Languages

All characters know how to speak Common. A dwarf, elf, gnome, half-elf, 
half-orc, or halfling also speaks a racial language, as appropriate. A 
character who has an Intelligence bonus at 1st level speaks other 
languages as well, one extra language per point of Intelligence bonus as 
a starting character.

Literacy: Any character except a barbarian can read and write all the 
languages he or she speaks.

Class-Related Languages: Clerics, druids, and wizards can choose certain 
languages as bonus languages even if they're not on the lists 
found in the race descriptions. These class-related languages are as 
follows:

Cleric: Abyssal, Celestial, Infernal.

Druid: Sylvan.

Wizard: Draconic.

### Small Characters

A Small character gets a +1 size bonus to Armor Class, a +1 size bonus 
on attack rolls, and a +4 size bonus on Hide checks. A Small 
character's carrying capacity is three-quarters of that of a 
Medium character.

A Small character generally moves about two-thirds as fast as a Medium 
character.

A Small character must use smaller weapons than a Medium character.


Dwarves <a id="dwarves"></a>
-------

Dwarves (singular dwarf) favor earth tones in their clothing and prefer simple and functional garb. The skin can be very dark, but it is always some shade of tan or brown. Hair color can be black, gray, or brown. Dwarves average 4 feet tall and weigh as much as adult humans.

&#9876; This background was updated to reflect Pathfinder dwarves.

### Dwarf Traits

* &#9876; +2 Constitution, +2 Wisdom, &minus;2 Charisma. <small>Was +2
  Constitution, &minus;2 Charisma.</small>
* &bull; Medium: As Medium creatures, dwarves have no special bonuses or 
  penalties due to their size.
* &bull; Dwarf base land speed is 20 feet. However, dwarves can move at 
  this speed even when wearing medium or heavy armor or when carrying a 
  medium or heavy load (unlike other creatures, whose speed is reduced 
  in such situations).
* &bull; Darkvision: Dwarves can see in the dark up to 60 feet. 
  Darkvision is black and white only, but it is otherwise like normal 
  sight, and dwarves can function just fine with no light at all.
* &bull; Stonecunning: This ability grants a dwarf a +2 racial 
  bonus on Search checks to notice unusual stonework, such as sliding 
  walls, stonework traps, new construction (even when built to match the 
  old), unsafe stone surfaces, shaky stone ceilings, and the like. 
  Something that isn't stone but that is disguised as stone also 
  counts as unusual stonework. A dwarf who merely comes within 10 feet 
  of unusual stonework can make a Search check as if he were actively 
  searching, and a dwarf can use the Search skill to find stonework 
  traps as a rogue can. A dwarf can also intuit depth, sensing his 
  approximate depth underground as naturally as a human can sense which 
  way is up.
* &bull; Weapon Proficiency: Dwarves receive the Martial Weapon 
  Proficiency feats for the battleaxe, heavy pick, and warhammer as 
  bonus feats.
* &bull; Weapon Familiarity: Dwarves may treat dwarfen waraxes and 
  dwarfen urgroshes as martial weapons, rather than exotic weapons.
* &bull; Stability: A dwarf gains a +4 bonus on ability checks made 
  to resist being bull rushed or tripped when standing on the ground 
  (but not when climbing, flying, riding, or otherwise not standing 
  firmly on the ground).
* &bull; +2 racial bonus on saving throws against poison.
* &bull; +2 racial bonus on saving throws against spells and 
  spell-like effects.
* &bull; +1 racial bonus on attack rolls against orcs and 
  goblinoids.
* &bull; +4 dodge bonus to Armor Class against monsters of the 
  giant type. Any time a creature loses its Dexterity bonus (if any) to 
  Armor Class, such as when it's caught flat-footed, it loses its 
  dodge bonus, too.
* &bull; +2 racial bonus on Appraise checks that are related to 
  stone or metal items.
* &bull; +2 racial bonus on Craft checks that are related to stone 
  or metal.
* &bull; Automatic Languages: Common and Dwarven. Bonus Languages: 
  Giant, Gnome, Goblin, Orc, Terran, and Undercommon.
* &bull; Favored Class: Fighter. A multiclass dwarf's fighter 
  class does not count when determining whether he takes an experience 
  point penalty for multiclassing


Elves <a id="elves"></a>
-----

Elves (singular elf) average 5 feet tall and typically weigh just over 100 pounds. They live on fruits and grains, though they occasionally hunt for fresh meat. Elves prefer colorful clothes, usually with a green-and-gray cloak that blends well with the colors of the forest.

&#9876; This background was updated to reflect Pathfinder elves.

### Elf Traits

* &dagger; +2 Dexterity, &minus;2 Constitution, +2 Intelligence.
  <small>Originally +2 Dexterity, &minus;2 Constitution.</small>
* &bull; Medium: As Medium creatures, elves have no special bonuses or 
  penalties due to their size.
* &bull; Elf base land speed is 30 feet.
* &bull; Immunity to magic sleep effects, and a +2 racial saving 
  throw bonus against enchantment spells or effects.
* &dagger; +2 racial bonus to caster level checks made to overcome
  spell resistance. +2 bonus to Spellcraft checks made to identify the
  properties of magic items.
* &bull; Low-Light Vision: An elf can see twice as far as a human in 
  starlight, moonlight, torchlight, and similar conditions of poor 
  illumination. She retains the ability to distinguish color and detail 
  under these conditions.
* &bull; Weapon Proficiency: Elves receive the Martial Weapon 
  Proficiency feats for the longsword, rapier, longbow (including 
  composite longbow), and shortbow (including composite shortbow) as 
  bonus feats.
* &dagger; +2 racial bonus on Perception checks. <small>+2 racial bonus
  on Listen, Search, and Spot checks.</small> An elf who merely passes
  within 5 feet of a secret or concealed door is entitled to a Search
  check to notice it as if she were actively looking for it.
* &bull; Automatic Languages: Common and Elven. Bonus Languages: 
  Draconic, Gnoll, Gnome, Goblin, Orc, and Sylvan.
* &bull; Favored Class: Wizard. A multiclass elf's wizard class 
  does not count when determining whether she takes an experience point 
  penalty for multiclassing.


Gnomes <a id="gnomes"></a>
------

Gnomes stand 3 to 3-1/2 feet tall and weigh 40 to 45 pounds. Their skin color ranges from dark tan to woody brown, their hair is fair, and their eyes can be any shade of blue. Gnome males prefer short, carefully trimmed beards. Gnomes generally wear leather or earth tones, though they decorate their clothes with intricate stitching or fine jewelry. Gnomes reach adulthood at about age 40, and they live about 350 years, though some can live almost 500 years.

&#9876; This background was updated to reflect Pathfinder gnomes.

### Gnome Traits

* &dagger; +2 Constitution, &minus;2 Strength, +2 Charisma.
  <small>Originally +2 Constitution, &minus;2 Strength.</small>
* &bull; Small: As a Small creature, a gnome gains a +1 size bonus 
  to Armor Class, a +1 size bonus on attack rolls, and a +4 
  size bonus on Hide checks, but he uses smaller weapons than humans 
  use, and his lifting and carrying limits are three-quarters of those 
  of a Medium character.
* &bull; Gnome base land speed is 20 feet.
* &bull; Low-Light Vision: A gnome can see twice as far as a human in 
  starlight, moonlight, torchlight, and similar conditions of poor 
  illumination. He retains the ability to distinguish color and detail 
  under these conditions.
* &bull; Weapon Familiarity: Gnomes may treat gnome hooked hammers as 
  martial weapons rather than exotic weapons.
* &bull; +2 racial bonus on saving throws against illusions.
* &bull; Add +1 to the Difficulty Class for all saving throws 
  against illusion spells cast by gnomes. This adjustment stacks with 
  those from similar effects.
* &#9876; +1 racial bonus on attack rolls against reptilians and 
  goblinoids.
* &bull; +4 dodge bonus to Armor Class against monsters of the 
  giant type. Any time a creature loses its Dexterity bonus (if any) to 
  Armor Class, such as when it's caught flat-footed, it loses its 
  dodge bonus, too.
* &bull; +2 racial bonus on Listen checks.
* &bull; +2 racial bonus on Craft (alchemy) checks.
* &bull; Automatic Languages: Common and Gnome. Bonus Languages: 
  Draconic, Dwarfen, Elven, Giant, Goblin, and Orc. In addition, a gnome 
  can speak with a burrowing mammal (a badger, fox, rabbit, or the like, 
  see below). This ability is innate to gnomes. See the speak with 
  animals spell description.
* &dagger; Spell-Like Abilities: A gnome with a Charisma score of at 
  least 11 has the following spell-like abilities: 1/day—_dancing 
  lights_, _ghost sound_, _prestidigitation_, and _speak with animals_.
  Caster level equal to character level;save DC 10 &plus; Cha modifier
  + spell level.
* &bull; Favored Class: Bard. A multiclass gnome's bard class does 
  not count when determining whether he takes an experience point 
  penalty.


Goblins <a id="goblins"></a>
-------

Goblins stand 3 to 3-1/2 feet tall and weigh 40 to 45 pounds. Their eyes are usually dull and glazed, varying in color from red to yellow. A goblin's skin color ranges from yellow through any shade of orange to a deep red; usually all members of a single tribe are about the same color.

&#9876; This background was updated to reflect Pathfinder goblins.

### Goblin Traits

* &#9876; &minus;2 Strength, +4 Dexterity, &minus;2 Charisma.
  <small>Originally &minus;2 Strength, +2 Dexterity, &minus;2
  Charisma.</small>
* &bull; Goblins are humanoids with the goblinoid subtype.
* &bull; Small: As a Small creature, a goblin gains a +1 size bonus 
  to Armor Class, a +1 size bonus on attack rolls, and a +4 
  size bonus on Hide checks, but she uses smaller weapons than humans 
  use, and her lifting and carrying limits are three-quarters of those 
  of a Medium character.
* &bull; Goblin base land speed is 30 feet.
* &bull; Darkvision: Goblins can see in the dark up to 60 feet. 
  Darkvision is black and white only, but it is otherwise like normal 
  sight, and goblins can function just fine with no light at all.
* &#9876; +4 racial bonus on Stealth and Ride checks. <small>Originally
  +4 racial bonus on Move Silently and Ride checks.</small>
* &bull; Automatic Languages: Common, Goblin. Bonus Languages: Draconic, 
  Elven, Giant, Gnoll, Orc.
* &bull; Favored Class: Rogue. A multiclass goblin's rogue class 
  does not count when determining whether she takes an experience point 
  penalty for multiclassing.


Half-elves <a id="half-elves"></a>
----------

Half-elves are not truly an elf subrace, but they are often mistaken for elves. Half-elves usually inherit a good blend of their parents' physical characteristics.

&#9876; This background was updated to reflect Pathfinder half-elves.

### Half-elf Traits

* &dagger; +2 to one ability score.
* &bull; Medium: As Medium creatures, half-elves have no special bonuses 
  or penalties due to their size.
* &bull; Half-elf base land speed is 30 feet.
* &bull; Immunity to sleep spells and similar magical effects, and a 
  +2 racial bonus on saving throws against enchantment spells or 
  effects.
* &bull; Low-Light Vision: A half-elf can see twice as far as a human in 
  starlight, moonlight, torchlight, and similar conditions of poor 
  illumination. He retains the ability to distinguish color and detail 
  under these conditions.
* &dagger; +2 racial bonus on Perception checks. <small>Was +1 racial
  bonus on Listen, Search, and Spot checks.</small>
* &dagger; Half-elves receiving Skill Focus as a bonus feat at 1st level.
  <small>Was +2 racial bonus on Diplomacy and Gather Information
  checks.</small>
* &dagger; Elven Blood: For all effects related to race, a half-elf is 
  considered an elf and a human.
* &bull; Automatic Languages: Common and Elven. Bonus Languages: Any 
  (other than secret languages, such as Druidic).
* &bull; Favored Class: Any. When determining whether a multiclass 
  half-elf takes an experience point penalty, his highest-level class 
  does not count.


Half-orcs <a id="half-orcs"></a>
---------

These orc&minus;human crossbreeds can be found in either orc or human society (where their status varies according to local sentiments), or in communities of their own. Half-orcs usually inherit a good blend of the physical characteristics of their parents. They are as tall as humans and a little heavier, thanks to their muscle. They have greenish pigmentation, sloping foreheads, jutting jaws, prominent teeth, and coarse body hair. Half-orcs who have lived among or near orcs have scars, in keeping with orcish tradition.

&#9876; This background was updated to reflect Pathfinder half-orcs.

### Half-orc Traits

* &dagger; +2 to one ability score. <small>Was +2 Strength, &minus;2
  Intelligence, &minus;2 Charisma.</small>
* A half-orc's starting Intelligence score is always at least 3. 
  If this adjustment would lower the character's score to 1 or 2, 
  his score is nevertheless 3.
* &bull; Medium: As Medium creatures, half-orcs have no special bonuses 
  or penalties due to their size.
* &bull; Half-orc base land speed is 30 feet.
* &bull; Darkvision: Half-orcs (and orcs) can see in the dark up to 60 
  feet. Darkvision is black and white only, but it is otherwise like 
  normal sight, and half-orcs can function just fine with no light at 
  all.
* &dagger; Intimidating: Half-orc receive a +2 racial bonus on
  Intimidate checks because of their fearsome nature.
* &dagger; Orc Blood: For all effects related to race, a half-orc is 
  considered an orc and a human.
* &dagger; Orc Ferocity: Once per day, when a half-orc is brought below
  0 hit points but not killed, he can fight on for 1 more round as if
  disabled. At the end of his next turn, unless brought to 1 or more hit
  points, he immediately falls unconscious and begins dying.
* &dagger; Half-orcs receive the Martial Weapon Proficiency feats for the
  greataxe and falchion as bonus feats.
* &bull; Weapon Familiarity: Half-orcs may treat the orc double-axe as a
  martial weapon rather than an exotic weapon.
* &bull; Automatic Languages: Common and Orc. Bonus Languages: Draconic, 
  Giant, Gnoll, Goblin, and Abyssal.
* &bull; Favored Class: Barbarian. A multiclass half-orc's 
  barbarian class does not count when determining whether she takes an 
  experience point penalty.


Halflings <a id="halflings"></a>
---------

Halflings stand about 3 feet tall and usually weigh between 30 and 35 pounds. They have brown or black eyes. Halfling men often have long sideburns, but beards are rare among them and mustaches almost unseen. Halflings prefer simple, comfortable, and practical clothes. Unlike members of most races, they prefer actual comfort to shows of wealth. Halflings reach adulthood in their early twenties and generally live into the middle of their second century.

&#9876; This background was updated to reflect Pathfinder halflings.

### Halfling Traits

* &dagger; +2 Dexterity, &minus;2 Strength, +2 Charisma. <small>Was +2
  Dexterity, &minus;2 Strength.</small>
* &dagger; Small: As a Small creature, a halfling gains a +1 size 
  bonus to Armor Class, a +1 size bonus on attack rolls, and a 
  +4 size bonus on Stealth checks, but he uses smaller weapons than 
  humans use, and his lifting and carrying limits are three-quarters of 
  those of a Medium character.
* &bull; Halfling base land speed is 20 feet.
* &dagger; +2 racial bonus on Acrobatics and Climb checks. <small>Was
  +2 racial bonus on Climb, Jump, and Move Silently checks.</small>
* &bull; +1 racial bonus on all saving throws.
* &bull; +2 morale bonus on saving throws against fear: This bonus 
  stacks with the halfling's +1 bonus on saving throws in 
  general.
* &dagger; Halflings receive the Martial Weapon Proficiency feat for
  slings as a bonus feat. <small>Was +1 racial bonus on attack rolls
  with thrown weapons and slings.</small>
* &dagger; Weapon Familiarity: Halflings may treat the gnome hooked
  hammer as a martial weapon rather than an exotic weapon.
* &dagger; +2 racial bonus on Perception checks. <small>Was +2 racial
  bonus on Listen checks.</small>
* &bull; Automatic Languages: Common and Halfling. Bonus Languages: 
  Dwarfen, Elven, Gnome, Goblin, and Orc.
* &bull; Favored Class: Rogue. A multiclass halfling's rogue class 
  does not count when determining whether he takes an experience point 
  penalty for multiclassing.


Humans <a id="humans"></a>
------

&#9876; This background was updated to reflect Pathfinder humans.

### Human Traits

* &dagger; +2 to one ability score.
* &bull; Medium: As Medium creatures, humans have no special bonuses or 
  penalties due to their size.
* &bull; Human base land speed is 30 feet.
* &bull; 1 extra feat at 1st level.
* &bull; 4 extra skill points at 1st level and 1 extra skill point at 
  each additional level.
* &bull; Automatic Language: Common. Bonus Languages: Any (other than 
  secret languages, such as Druidic). See the Speak Language skill.
* &bull; Favored Class: Any. When determining whether a multiclass human 
  takes an experience point penalty, his or her highest-level class does 
  not count.


Kobolds <a id="kobolds"></a>
-------

Kobolds are short, reptilian humanoids. Their scaly skin ranges from dark rusty brown to a rusty black color. They have glowing red eyes and their tail is nonprehensile. Kobolds wear ragged clothing, favoring red and orange. A kobold is 2 to 2-1/2 feet tall and weighs 35 to 45 pounds. Kobolds speak Draconic with a voice that sounds like that of a yapping dog.

&#9876; This background was updated to reflect Pathfinder kobolds.

### Kobold Traits

* &bull; &minus;4 Strength, +2 Dexterity, &minus;2 Constitution.
* &dagger; Small: As a Small creature, a kobold gains a +1 size bonus 
  to Armor Class, a +1 size bonus on attack rolls, and a +4 size bonus
  on Stealth checks, but she uses smaller weapons than humans use, and
  her lifting and carrying limits are three-quarters of those of a
  Medium character.
* &bull; Kobold base land speed is 30 feet.
* &bull; Darkvision: Kobolds can see in the dark up to 60 feet. 
  Darkvision is black and white only, but it is otherwise like normal 
  sight, and kobolds can function just fine with no light at all.
* &dagger; +2 racial bonus on Craft (trapmaking), Profession (miner), 
  and Perception checks.
* &bull; +1 natural armor bonus.
* &bull; Light Sensitivity: Kobolds are dazzled in bright sunlight or 
  within the radius of a daylight spell.
* &bull; Automatic Languages: Draconic. Bonus Languages: Common, 
  Undercommon.
* &bull; Favored Class: Sorcerer. A multiclass kobold's sorceror 
  class does not count when determining whether she takes an experience 
  point penalty for multiclassing.


Orcs <a id="orcs"></a>
----

An orc's hair usually is black. It has lupine ears and reddish eyes. Orcs prefer wearing vivid colors that many humans would consider unpleasant, such as blood red, mustard yellow, yellow-green, and deep purple. Their equipment is dirty and unkempt. An adult male orc is a little over 6 feet tall and weighs about 210 pounds. Females are slightly smaller.

&#9876; This background was updated to reflect Pathfinder orcs.

### Orc Traits

* &bull; +4 Strength, &minus;2 Intelligence, &minus;2 Wisdom, 
  &minus;2 Charisma.
* &bull; Orcs are humanoids with the orc subtype.
* &bull; Orc base land speed is 30 feet.
* &bull; Darkvision: Orcs (and half-orcs) can see in the dark up to 60 
  feet. Darkvision is black and white only, but it is otherwise like 
  normal sight, and orcs can function just fine with no light at all.
* &bull; Light Sensitivity: Orcs are dazzled in bright sunlight or 
  within the radius of a daylight spell.
* &dagger; Orc Ferocity: Once per day, when an orc is brought below
  0 hit points but not killed, he can fight on for 1 more round as if
  disabled. At the end of his next turn, unless brought to 1 or more hit
  points, he immediately falls unconscious and begins dying.
* &dagger; Orcs receive the Martial Weapon Proficiency feats for the
  greataxe and falchion as bonus feats.
* &bull; Weapon Familiarity: Orcs may treat the orc double-axe as a
  martial weapon rather than an exotic weapon.
* &bull; Automatic Languages: Common, Orc. Bonus Languages: Dwarfen, 
  Giant, Gnoll, Goblin, Undercommon.
* &bull; Favored Class: Barbarian. A multiclass orc's barbarian 
  class does not count when determining whether he takes an experience 
  point penalty.


Legal Information
=================

OPEN GAME LICENSE Version 1.0a
 
The following text is the property of Wizards of the Coast, Inc. and is Copyright 2000 Wizards of the Coast, Inc ("Wizards"). All Rights Reserved.
 
1. Definitions: (a)"Contributors" means the copyright and/or trademark owners who have contributed Open Game Content; (b)"Derivative Material" means copyrighted material including derivative works and translations (including into other computer languages), potation, modification, correction, addition, extension, upgrade, improvement, compilation, abridgment or other form in which an existing work may be recast, transformed or adapted; (c) "Distribute" means to reproduce, license, rent, lease, sell, broadcast, publicly display, transmit or otherwise distribute; (d)"Open Game Content" means the game mechanic and includes the methods, procedures, processes and routines to the extent such content does not embody the Product Identity and is an enhancement over the prior art and any additional content clearly identified as Open Game Content by the Contributor, and means any work covered by this License, including translations and derivative works under copyright law, but specifically excludes Product Identity. (e) "Product Identity" means product and product line names, logos and identifying marks including trade dress; artifacts; creatures characters; stories, storylines, plots, thematic elements, dialogue, incidents, language, artwork, symbols, designs, depictions, likenesses, formats, poses, concepts, themes and graphic, photographic and other visual or audio representations; names and descriptions of characters, spells, enchantments, personalities, teams, personas, likenesses and special abilities; places, locations, environments, creatures, equipment, magical or supernatural abilities or effects, logos, symbols, or graphic designs; and any other trademark or registered trademark clearly identified as Product identity by the owner of the Product Identity, and which specifically excludes the Open Game Content; (f) "Trademark" means the logos, names, mark, sign, motto, designs that are used by a Contributor to identify itself or its products or the associated products contributed to the Open Game License by the Contributor (g) "Use", "Used" or "Using" means to use, Distribute, copy, edit, format, modify, translate and otherwise create Derivative Material of Open Game Content. (h) "You" or "Your" means the licensee in terms of this agreement.
 
2. The License: This License applies to any Open Game Content that contains a notice indicating that the Open Game Content may only be Used under and in terms of this License. You must affix such a notice to any Open Game Content that you Use. No terms may be added to or subtracted from this License except as described by the License itself. No other terms or conditions may be applied to any Open Game Content distributed using this License.
 
3. Offer and Acceptance: By Using the Open Game Content You indicate Your acceptance of the terms of this License.
 
4. Grant and Consideration: In consideration for agreeing to use this License, the Contributors grant You a perpetual, worldwide, royalty-free, non-exclusive license with the exact terms of this License to Use, the Open Game Content.
 
5. Representation of Authority to Contribute: If You are contributing original material as Open Game Content, You represent that Your Contributions are Your original creation and/or You have sufficient rights to grant the rights conveyed by this License.
 
6. Notice of License Copyright: You must update the COPYRIGHT NOTICE portion of this License to include the exact text of the COPYRIGHT NOTICE of any Open Game Content You are copying, modifying or distributing, and You must add the title, the copyright date, and the copyright holder's name to the COPYRIGHT NOTICE of any original Open Game Content you Distribute.
 
7. Use of Product Identity: You agree not to Use any Product Identity, including as an indication as to compatibility, except as expressly licensed in another, independent Agreement with the owner of each element of that Product Identity. You agree not to indicate compatibility or co-adaptability with any Trademark or Registered Trademark in conjunction with a work containing Open Game Content except as expressly licensed in another, independent Agreement with the owner of such Trademark or Registered Trademark. The use of any Product Identity in Open Game Content does not constitute a challenge to the ownership of that Product Identity. The owner of any Product Identity used in Open Game Content shall retain all rights, title and interest in and to that Product Identity.
 
8. Identification: If you distribute Open Game Content You must clearly indicate which portions of the work that you are distributing are Open Game Content.
 
9. Updating the License: Wizards or its designated Agents may publish updated versions of this License. You may use any authorized version of this License to copy, modify and distribute any Open Game Content originally distributed under any version of this License.
 
10. Copy of this License: You MUST include a copy of this License with every copy of the Open Game Content You Distribute.
 
11. Use of Contributor Credits: You may not market or advertise the Open Game Content using the name of any Contributor unless You have written permission from the Contributor to do so.
 
12. Inability to Comply: If it is impossible for You to comply with any of the terms of this License with respect to some or all of the Open Game Content due to statute, judicial order, or governmental regulation then You may not Use any Open Game Material so affected.
 
13. Termination: This License will terminate automatically if You fail to comply with all terms herein and fail to cure such breach within 30 days of becoming aware of the breach. All sublicenses shall survive the termination of this License.
 
14. Reformation: If any provision of this License is held to be unenforceable, such provision shall be reformed only to the extent necessary to make it enforceable.
 
15. COPYRIGHT NOTICE
Open Game License v 1.0a Copyright 2000, Wizards of the Coast, Inc.
 
__System Reference Document__ Copyright 2000-2003, Wizards of the Coast, Inc.; Authors Jonathan Tweet, Monte Cook, Skip Williams, Rich Baker, Andy Collins, David Noonan, Rich Redman, Bruce R. Cordell, John D. Rateliff, Thomas Reid, James Wyatt, based on original material by E. Gary Gygax and Dave Arneson.

__Pathfinder Roleplaying Game Conversion Guide__. Copyright 2009, Paizo Publishing, LLC; Author: Jason Bulmahn.

__Pathfinder Roleplaying Game Core Rulebook__. Copyright 2009, Paizo Publishing, LLC; Author: Jason Bulmahn, based on material by Jonathan Tweet, Monte Cook, and Skip Williams.
 
END OF LICENSE
