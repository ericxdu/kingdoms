Metaphysical Abilities
======================

Some characters are born with or gain the ability to perform metaphysical feats. These manifest as spell-like abilities or supernatural abilities.

In the SRD, races and creatures that have spell-like abilities can typically use them once per day (1/day), three times per day (3/day), or more. Some are simply usable at-will with no daily restrictions. When applicable, they are listed with a caster level and a save DC. The paladin's *detect evil* ability is used at-will. A gnome's *speak with animals* ability is used once per day. The druid's wild shape ability is usable multiple times per day as specified per level.

It may be desirable to give humans genetic abilities that unlock as they age or gain experience, as dragons and some other creatures in the SRD do. This may be achieved by providing special feats, locking spell-like abilities behind ability score threshholds, or giving them to characters who have achieved a certain age category (see [Description](srd/Description.html)).

<table>
<caption>Player Character Age Categories</caption>
<tr><th></th><th>Age</th><th>Ability Score Penalties; Bonuses</th></tr>
<tr><th>Young</th><td>15-34 years</td><td>&mdash;</td></tr>
<tr><th>Middle Aged</th><td>35-52 years</td><td>–1 to Str, Dex, and Con; +1 to Int, Wis, and Cha</td></tr>
<tr><th>Old</th><td>53-69 years</td><td>–2 to Str, Dex, and Con; +1 to Int, Wis, and Cha</td></tr>
<tr><th>Venerable</th><td>70 years</td><td>–3 to Str, Dex, and Con; +1 to Int, Wis, and Cha</td></tr>
</table>

Note that starting at middle-age characters start to naturally increase their Intelligence, Wisdom, and Charisma scores. If spell-like abilities are tied to these scores, some of them may simply unlock from ability increases anyway.

Some of the spells listed in this document may need to be modified or omitted to fit the style of Waterworld, as it is an urban fantasy and not classical fantasy.


Spell List
----------

### Create Water

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Range**: Close (25 ft. + 5 ft./2 levels)
- **Effect**: Up to 2 gallons of water/level
- **Duration**: Instantaneous
- **Saving Throw**: None
- **Spell Resistance**: No

This spell generates wholesome, drinkable water, just like clean rain water. Water can be created in an area as small as will actually contain the liquid, or in an area three times as large—possibly creating a downpour or filling many small receptacles.

*Note*: Conjuration spells can’t create substances or objects within a creature. Water weighs about 8 pounds per gallon. One cubic foot of water contains roughly 8 gallons and weighs about 60 pounds.

### Cure Minor Wounds

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Range**: Touch
- **Target**: Creature touched
- **Duration**: Instantaneous
- **Saving Throw**: None
- **Spell Resistance**: No

When laying your hand upon a living creature, you channel positive energy that restores 1 point of damage.

<h3 id="dancinglights">Dancing Lights</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Casting Time**: 1 standard action
- **Range**: Medium (100 ft. + 10 ft./level)
- **Effect**: Up to four lights, all within a 10- ft.-radius area
- **Duration**: 1 minute (D)
- **Saving Throw**: None
- **Spell Resistance**: No

Depending on the version selected, you create up to four lights that resemble lanterns or torches (and cast that amount of light), or up to four glowing spheres of light (which look like will-o’-wisps), or one faintly glowing, vaguely humanoid shape. The *dancing lights* must stay within a 10-foot-radius area in relation to each other but otherwise move as you desire (no concentration required): forward or back, up or down, straight or turning corners, or the like. The lights can move up to 100 feet per round. A light winks out if the distance between you and it exceeds the spell’s range.

### Daze

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- [Mind-Affecting]
- **Range**: Close (25 ft. + 5 ft./2 levels)
- **Target**: One humanoid creature of 4 HD or less
- **Duration**: 1 round
- **Saving Throw**: Will negates
- **Spell Resistance**: Yes

This enchantment clouds the mind of a humanoid creature with 4 or fewer Hit Dice so that it takes no actions. Humanoids of 5 or more HD are not affected. A dazed subject is not stunned, so attackers get no special advantage against it.

### Detect Magic

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Range**: 60 ft.
- **Area**: Cone-shaped emanation
- **Duration**: Concentration, up to 1 min./level (D)
- **Saving Throw**: None
- **Spell Resistance**: No

You detect magical auras. The amount of information revealed depends on how long you study a particular area or subject.

*1st Round*: Presence or absence of magical auras.

*2nd Round*: Number of different magical auras and the power of the most potent aura.

*3rd Round*: The strength and location of each aura. If the items or creatures bearing the auras are in line of sight, you can make Spellcraft skill checks to determine the school of magic involved in each. (Make one check per aura; DC 15 + spell level, or 15 + half caster level for a nonspell effect.)

Magical areas, multiple types of magic, or strong local magical emanations may distort or conceal weaker auras.

*Aura Strength*: An aura’s power depends on a spell’s functioning spell level or an item’s caster level. If an aura falls into more than one category, *detect magic* indicates the stronger of the two.

### Flare

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Range**: Close (25 ft. + 5 ft./2 levels)
- **Effect**: Burst of light
- **Duration**: Instantaneous
- **Saving Throw**: Fortitude negates
- **Spell Resistance**: Yes

This cantrip creates a burst of light. If you cause the light to burst directly in front of a single creature, that creature is dazzled for 1 minute unless it makes a successful Fortitude save. Sightless creatures, as well as creatures already dazzled, are not affected by flare.

<h3 id="ghostsound">Ghost Sound</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Range**: Close (25 ft. + 5 ft./2 levels)
- **Effect**: Illusory sounds
- **Duration**: 1 round/level (D)
- **Saving Throw**: Will disbelief (if interacted with)
- **Spell Resistance**: No

Ghost sound allows you to create a volume of sound that rises, recedes, approaches, or remains at a fixed place. You choose what type of sound ghost sound creates when casting it and cannot thereafter change the sound’s basic character.

The volume of sound created depends on your level. You can produce as much noise as four normal humans per caster level (maximum twenty humans). Thus, talking, singing, shouting, walking, marching, or running sounds can be created. The noise a ghost sound spell produces can be virtually any type of sound within the volume limit. A horde of rats running and squeaking is about the same volume as eight humans running and shouting. A roaring lion is equal to the noise from sixteen humans, while a roaring dire tiger is equal to the noise from twenty humans.

Ghost sound can enhance the effectiveness of a silent image spell.

### Know Direction

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Casting Time**: 1 standard action
- **Range**: Personal
- **Target**: You
- **Duration**: Instantaneous

You instantly know the direction of north from your current position. The spell is effective in any environment in which “north” exists, but it may not work in extraplanar settings. Your knowledge of north is correct at the moment of casting, but you can get lost again within moments if you don’t find some external reference point to help you keep track of direction.

<h3 id="light">Light</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Casting Time**: 1 standard action
- **Range**: Touch
- **Target**: Object touched
- **Duration**: 10 min./level (D)
- **Saving Throw**: None
- **Spell Resistance**: No

This spell causes an object to glow like a torch, shedding bright light in a 20-foot radius (and dim light for an additional 20 feet) from the point you touch. The effect is immobile, but it can be cast on a movable object. Light taken into an area of magical darkness does not function.

A light spell (one with the light descriptor) counters and dispels a darkness spell (one with the darkness descriptor) of an equal or lower level.

### Lullaby

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Range**: Medium (100 ft. + 10 ft./level)
- **Area**: Living creatures within a 10-ft.-radius burst
- **Duration**: Concentration + 1 round/level (D)
- **Saving Throw**: Will negates
- **Spell Resistance**: Yes

Any creature within the area that fails a Will save becomes drowsy and inattentive, taking a –5 penalty on Listen and Spot checks and a –2 penalty on Will saves against sleep effects while the lullaby is in effect. Lullaby lasts for as long as the caster concentrates, plus up to 1 round per caster level thereafter.

<h3 id="magehand">Mage Hand</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Range**: Close (25 ft. + 5 ft./2 levels)
- **Target**: One nonmagical, unattended object weighing up to 5 lb.
- **Duration**: Concentration
- **Saving Throw**: None
- **Spell Resistance**: No

You point your finger at an object and can lift it and move it at will from a distance. As a move action, you can propel the object as far as 15 feet in any direction, though the spell ends if the distance between you and the object ever exceeds the spell’s range.

### Mending

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Range**: 10 ft.
- **Target**: One object of up to 1 lb.
- **Duration**: Instantaneous
- **Saving Throw**: Will negates (harmless, object)
- **Spell Resistance**: Yes (harmless, object)

Mending repairs small breaks or tears in objects (but not warps, such as might be caused by a warp wood spell). It will weld broken metallic objects such as a ring, a chain link, a medallion, or a slender dagger, providing but one break exists.

Ceramic or wooden objects with multiple breaks can be invisibly rejoined to be as strong as new. A hole in a leather sack or a wineskin is completely healed over by mending. The spell can repair a magic item, but the item’s magical abilities are not restored. The spell cannot mend broken magic rods, staffs, or wands, nor does it affect creatures (including constructs).

### Message

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- [Language-Dependent]
- **Range**: Medium (100 ft. + 10 ft./level)
- **Targets**: One creature/level
- **Duration**: 10 min./level
- **Saving Throw**: None
- **Spell Resistance**: No

You can whisper messages and receive whispered replies with little chance of being overheard. You point your finger at each creature you want to receive the message. When you whisper, the whispered message is audible to all targeted creatures within range. Magical silence, 1 foot of stone, 1 inch of common metal (or a thin sheet of lead), or 3 feet of wood or dirt blocks the spell. The message does not have to travel in a straight line. It can circumvent a barrier if there is an open path between you and the subject, and the path’s entire length lies within the spell’s range. The creatures that receive the message can whisper a reply that you hear. The spell transmits sound, not meaning. It doesn’t transcend language barriers.

*Note*: To speak a message, you must mouth the words and whisper, possibly allowing observers the opportunity to read your lips.

<h3 id="openclose">Open/Close</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Range**: Close (25 ft. + 5 ft./2 levels)
- **Target**: Object weighing up to 30 lb. or portal that can be opened or closed
- **Duration**: Instantaneous
- **Saving Throw**: Will negates (object)
- **Spell Resistance**: Yes (object)

You can open or close (your choice) a door, chest, box, window, bag, pouch, bottle, barrel, or other container. If anything resists this activity (such as a bar on a door or a lock on a chest), the spell fails. In addition, the spell can only open and close things weighing 30 pounds or less. Thus, doors, chests, and similar objects sized for enormous creatures may be beyond this spell’s ability to affect.

<h3 id="prestidigitation">Prestidigitation</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Range**: 10 ft.
- **Target, Effect, or Area**: See text
- **Duration**: 1 hour
- **Saving Throw**: See text
- **Spell Resistance**: No

Prestidigitations are minor tricks that novice spellcasters use for practice. Once cast, a prestidigitation spell enables you to perform simple magical effects for 1 hour. The effects are minor and have severe limitations. A prestidigitation can slowly lift 1 pound of material. It can color, clean, or soil items in a 1-foot cube each round. It can chill, warm, or flavor 1 pound of nonliving material. It cannot deal damage or affect the concentration of spellcasters. Prestidigitation can create small objects, but they look crude and artificial. The materials created by a prestidigitation spell are extremely fragile, and they cannot be used as tools, weapons, or spell components. Finally, a prestidigitation lacks the power to duplicate any other spell effects. Any actual change to an object (beyond just moving, cleaning, or soiling it) persists only 1 hour.

### Purify Food and Drink

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Range**: 10 ft.
- **Target**: 1 cu. ft./level of contaminated food and water
- **Duration**: Instantaneous
- **Saving Throw**: Will negates (object)
- **Spell Resistance**: Yes (object)

This spell makes spoiled, rotten, poisonous, or otherwise contaminated food and water pure and suitable for eating and drinking. This spell does not prevent subsequent natural decay or spoilage. Unholy water and similar food and drink of significance is spoiled by purify food and drink, but the spell has no effect on creatures of any type nor upon magic potions.

*Note*: Water weighs about 8 pounds per gallon. One cubic foot of water contains roughly 8 gallons and weighs about 60 pounds.

### Resistance

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Range**: Touch
- **Target**: Creature touched
- **Duration**: 1 minute
- **Saving Throw**: Will negates (harmless)
- **Spell Resistance**: Yes (harmless)

You imbue the subject with magical energy that protects it from harm, granting it a +1 resistance bonus on saves.

### Virtue

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Range**: Touch
- **Target**: Creature touched
- **Duration**: 1 minute
- **Saving Throw**: Fortitude negates (harmless)
- **Spell Resistance**: Yes (harmless)

The subject gains 1 temporary hit point.
