Modern Skills
=============

Supplementary skills for D&D characters to facilitate a generic game setting.


Skill Descriptions
------------------

<h3 id="computeruse">Computer Use (Int)</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

**Check**: Most normal computer operations don’t require a Computer Use check (though a character might have to make a
Research check; see the Research skill description). However, searching an unfamiliar network for a particular file,
writing computer programs, altering existing programs to perform differently (better or worse), and breaking through
computer security are all relatively difficult and require skill checks.

**Find File**: This skill can be used for finding files or data on an unfamiliar system. The DC for the check and the
time required are determined by the size of the site on which the character is searching.

Finding public information on the Internet does not fall under this category; usually, such a task requires a Research
check. This application of the Computer Use skill only pertains to finding files on private systems with which the
character is not familiar.

<table>
<tr><th>Size of Site</th><th>DC</th><th>Time</th></tr>
<tr><td>Personal computer</td><td>10</td><td>1 round</td></tr>
<tr><td>Small office network</td><td>15</td><td>2 rounds</td></tr>
<tr><td>Large office network</td><td>20</td><td>1 minute</td></tr>
<tr><td>Massive corporate network</td><td>25</td><td>10 minutes</td></tr>
</table>

**Defeat Computer Security**: This application of Computer Use can’t be used untrained. The DC is determined by the
quality of the security program installed to defend the system. If the check is failed by 5 or more, the security
system immediately alerts its administrator that there has been an unauthorized entry. An alerted administrator may
attempt to identify the character or cut off the character’s access to the system.

Sometimes, when accessing a difficult site, the character has to defeat security at more than one stage of the
operation. If the character beats the DC by 10 or more when attempting to defeat computer security, the character
automatically succeeds at all subsequent security checks at that site until the end of the character’s session (see
Computer Hacking below).

<table>
<tr><th>Level of Security</th><th>DC</th></tr>
<tr><td>Minimum</td><td>20</td></tr>
<tr><td>Average</td><td>25</td></tr>
<tr><td>Exceptional</td><td>35</td></tr>
<tr><td>Maximum</td><td>40</td></tr>
</table>
 
#### Computer Hacking

Breaking into a secure computer or network is often called hacking.

When a character hacks, he or she attempts to invade a site. A site is a virtual location containing files, data, or
applications. A site can be as small as a single computer, or as large as a corporate network connecting computers and
data archives all over the world—the important thing is that access to the site connects the user to everything within
it. Some sites can be accessed via the Internet; others are not connected to any outside network and can only be tapped
into by a user who physically accesses a computer connected to the site.

Every site is overseen by a system administrator—the person in charge of the site, and who maintains its security.
Often, the system administrator is the only person with access to all of a site’s functions and data. A site can have
more than one system administrator; large sites have a system administrator on duty at all times. A character is the
system administrator of his or her personal computer.

When a character hacks into a site, the visit is called a session. Once a character stops accessing the site, the
session is over. The character can go back to the site in the future; when he or she does, it’s a new session.

Several steps are required to hack into a site:

**Covering Tracks**: This step is optional. By making a Computer Use check (DC 20), a character can alter his or her
identifying information. This imposes a –5 penalty on any attempt made to identify the character if his or her activity
is detected.

**Access the Site**: There are two ways to do this: physically or over the Internet.

**Physical Access**: A character gains physical access to the computer, or a computer connected to the site.  If the
site being hacked is not connected to the Internet, this is probably the only way a character can access it. A variety
of skill checks may be required, depending on the method used to gain access.

**Internet Access**: Reaching a site over the net requires two Computer Use checks. The first check (DC 10) is needed
to find the site on the net. The second is a check to defeat computer security (see the Computer Use skill
description). Once a character has succeeded in both checks, the character has accessed the site.

Locate What You’re Looking For: To find the data (or application, or remote device) the character wants, make a
Computer Use check. See Find File under the skill description.

**Defeat File Security**: Many networks have additional file security.  If that’s the case, the character needs to make
another check to defeat computer security.

**Do Your Stuff**: Finally, the character can actually do what he or she came to do. If the character just wants to
look at records, no additional check is needed. (A character can also download data, although that often takes several
rounds—or even several minutes, for especially large amounts of information—to complete.) Altering or deleting records
sometimes requires yet another check to defeat computer security. Other operations can be carried out according to the
Computer Use skill description.

--- 

**Defend Security**: If the character is the system administrator for a site (which may be as simple as being the owner
of a laptop), he or she can defend the site against intruders. If the site alerts the character to an intruder, the
character can attempt to cut off the intruder’s access (end the intruder’s session), or even to identify the intruder.

To cut off access, make an opposed Computer Use check against the intruder. If the character succeeds, the intruder’s
session is ended. The intruder might be able to defeat the character’s security and access his or her site again, but
the intruder will have to start the hacking process all over. Attempting to cut off access takes a full round.

One surefire way to prevent further access is to simply shut the site down. With a single computer, that’s often no big
deal—but on a large site with many computers (or computers controlling functions that can’t be interrupted), it may be
time-consuming or even impossible.

To identify the intruder, make an opposed Computer Use check against the intruder. If the character succeeds, the
character learns the site from which the intruder is operating (if it’s a single computer, the character learns the
name of the computer’s owner). Identifying the intruder requires 1 minute and is a separate check from cutting off
access. This check can only be made if the intruder is accessing the character’s site for the entire length of the
check—if the intruder’s session ends before the character finishes the check, the character automatically fails.

**Degrade Programming**: A character can destroy or alter applications on a computer to make use of that computer
harder or impossible. The DC for the attempt depends on what the character tries to do. Crashing a computer simply
shuts it down. Its user can restart it without making a skill check (however, restarting takes 1 minute). Destroying
programming makes the computer unusable until the programming is repaired. Damaging programming imposes a –4 penalty on
all Computer Use checks made with the computer (sometimes this is preferable to destroying the programming, since the
user might not know that anything is wrong, and won’t simply decide to use a different computer).

A character can degrade the programming of multiple computers at a single site; doing so adds +2 to the DC for each
additional computer.

<table>
<tr><th>Scope of Alteration</th><th>DC</th><th>Time</th></tr>
<tr><td>Crash computer</td><td>10</td><td>1 minute</td></tr>
<tr><td>Destroy programming</td><td>15</td><td>10 minutes</td></tr>
<tr><td>Damage programming</td><td>20</td><td>10 minutes</td></tr>
</table>

Fixing the degraded programming requires 1 hour and a Computer Use check against a DC equal to the DC for degrading it
+5.

**Write Program**: A character can create a program to help with a specific task. Doing so grants the character a +2
circumstance bonus to the task.

A specific task, in this case, is one type of operation with one target.

The DC to write a program is 20; the time required is 1 hour.

**Operate Remote Device**: Many devices are computer-operated via remote links. If the character has access to the
computer that controls such systems, the character can either shut them off or change their operating parameters. The
DC depends on the nature of the operation. If the character fails the check by 5 or more, the system immediately alerts
its administrator that there has been an unauthorized use of the equipment. An alerted administrator may attempt to
identify the character or cut off his or her access to the system.

<table>
<tr><th>Type of Operation</th><th>DC</th><th>Time</th></tr>
<tr><td>Shut down passive remote (including cameras and door locks)</td><td>20</td><td>1 round per remote</td></tr>
<tr><td>Shut down active remote (including motion detectors and alarms)</td><td>25</td><td>1 round per remote</td></tr>
<tr><td>Reset parameters</td><td>30</td><td>1 minute per remote</td></tr>
<tr><td>Change passcodes</td><td>25</td><td>1 minute</td></tr>
<tr><td>Hide evidence of alteration</td><td>+10</td><td>1 minute</td></tr>
<tr><td>Minimum security</td><td>–5</td><td>—</td></tr>
<tr><td>Exceptional security</td><td>+10</td><td>—</td></tr>
<tr><td>Maximum security</td><td>+15</td><td>—</td></tr>
</table>

**Special**: A character can take 10 when using the Computer Use skill. A character can take 20 in some cases, but not
in those that involve a penalty for failure. (A character cannot take 20 to defeat computer security or defend
security.)

A character with the [Gearhead](modern-feats.html#gearhead) feat gets a +2 bonus on all Computer Use checks.

**Time**: Computer Use requires at least a full-round action. The GM may determine that some tasks require several
rounds, a few minutes, or longer, as described above.

<h3 id="craftelectronic">Craft (electronic) (Int; Trained Only)</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

This skill allows a character to build electronic equipment from scratch, such as audio and video equipment, timers and
listening devices, or radios and communication devices.

When building an electronic device from scratch, the character describes the kind of device he or she wants to
construct; then the Gamemaster decides whether the device is simple, moderate, complex, or advanced com­pared to
current technology.

<table>
<tr><th>Type of Scratch-Built Electronics (Examples)</th><th>Purchase DC</th><th>Craft DC</th><th>Time</th></tr>
<tr><td>Simple (timer or detonator)</td><td>8</td><td>15</td><td>1 hr.</td></tr>
<tr><td>Moderate (radio direction finder, electronic lock)</td><td>12</td><td>20</td><td>12 hr.</td></tr>
<tr><td>Complex (cell phone)</td><td>16</td><td>25</td><td>24 hr.</td></tr>
<tr><td>Advanced (computer)</td><td>22</td><td>30</td><td>60 hr.</td></tr>
</table>

**Special**: A character without an electrical tool kit takes a –4 penalty on Craft (electronic) checks.

A character with the [Builder](modern-feats.html#builder) feat gets a +2 bonus on all Craft (electronic) checks.

<h3 id="craftmechanical">Craft (mechanical) (Int) Trained Only</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

This skill allows a character to build mechanical devices from scratch, including engines and engine parts, weapons,
armor, and other gadgets. When building a mechanical device from scratch, the character describes the kind of device he
or she wants to construct; then the Gamemaster decides if the device is simple, moderate, complex, or advanced compared
to current technology. Type of Scratch-Built Mechanical Device (Examples)
	
<table>
<tr><th>Purchase DC</th><th>Craft DC</th><th>Time</th></tr>
<tr><td>Simple (tripwire trap)</td><td>5</td><td>15</td><td>1 hr</td></tr>
<tr><td>Moderate (engine component, light armor)</td><td>12</td><td>20</td><td>12 hr.</td></tr>
<tr><td>Complex (automobile engine, 9mm autoloader handgun)</td><td>16</td><td>25</td><td>24 hr.</td></tr>
<tr><td>Advanced (jet engine)</td><td>20</td><td>30</td><td>60 hr.</td></tr>
</table>

**Special**: A character without a mechanical tool kit takes a –4 penalty on Craft (mechanical) checks.

A character with the [Builder](modern-feats.html#builder) feat gets a +2 bonus on all Craft (mechanical) checks.

<h3 id="demolitions">Demolitions (Int; Trained Only)</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

**Check**: Setting a simple explosive to blow up at a certain spot doesn’t require a check, but connecting and setting
a detonator does. Also, placing an explosive for maximum effect against a structure calls for a check, as does
disarming an explosive device.

**Set Detonator**: Most explosives require a detonator to go off. Connecting a detonator to an explosive requires a
Demolitions check (DC 10). Failure means that the explosive fails to go off as planned. Failure by 10 or more means the
explosive goes off as the detonator is being installed.

A character can make an explosive difficult to disarm. To do so, the character chooses the disarm DC before making his
or her check to set the detonator (it must be higher than 10). The character’s DC to set the detonator is equal to the
disarm DC.

Place Explosive Device: Carefully placing an explosive against a fixed structure (a stationary, unattended inanimate
object) can maximize the damage dealt by exploiting vulnerabilities in the structure’s construction.

The GM makes the check (so that the character doesn’t know exactly how well he or she has done). On a result of 15 or
higher, the explosive deals double damage to the structure against which it is placed. On a result of 25 or higher, it
deals triple damage to the structure. In all cases, it deals normal damage to all other targets within its burst
radius.

**Disarm Explosive Device**: Disarming an explosive that has been set to go off requires a Demolitions check. The DC is
usually 10, unless the person who set the detonator chose a higher disarm DC. If the character fails the check, he or
she does not disarm the explosive. If the character fails by more than 5, the explosive goes off.

**Special**: A character can take 10 when using the Demolitions skill, but can’t take 20.

A character with the [Cautious](modern-feats.html#cautious) feat and at least 1 rank in this skill gets a +2 bonus on all Demolitions checks.

A character without a demolitions kit takes a –4 penalty on Demolitions checks.

Making an explosive requires the Craft (chemical) skill. See that skill description for details.

**Time**: Setting a detonator is usually a full-round action. Placing an explosive device takes 1 minute or more,
depending on the scope of the job.

<h3 id="drive">Drive (Dex)</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

**Check**: Routine tasks, such as ordinary driving, don’t require a skill check. Make a check only when some unusual
circumstance exists (such as inclement weather or an icy surface), or when the character is driving during a dramatic
situation (the character is being chased or attacked, for example, or is trying to reach a destination in a limited
amount of time). When driving, the character can attempt simple maneuvers or stunts. See Driving a Vehicle for more
details.

**Try Again?**: Most driving checks have consequences for failure that make trying again impossible.

**Special**: A character can take 10 when driving, but can’t take 20.

A character with the [Vehicle Expert](modern-feats.html#vehicleexpert) feat gets a +2 bonus on all Drive checks.

There is no penalty for operating a general-purpose motor vehicle. Other types of motor vehicles (heavy wheeled,
powerboat, sailboat, ship, and tracked) require the corresponding
[Surface Vehicle Operation](modern-feats.html#surfacevehicleoperation) feat, or the character takes a –4 penalty on
Drive checks.

**Time**: A Drive check is a move action.

<h3 id="gamble">Gamble (Wis)</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

**Check**: To join or start a game, a character must first pay a stake. The character sets the purchase DC of the stake
if he or she starts the game, or the GM sets it if the character joins a game. Stakes run from penny-ante (purchase DC
4) to astronomical (purchase DC 24). A character cannot take 20 when purchasing a stake.

If the stake is within the character’s means (it is equal to or less than his or her Wealth bonus), the character
stands no chance of winning any significant amount. The character might come out ahead, but the amount is not enough to
affect his or her Wealth bonus. Since paying the stake didn’t cost any points of Wealth bonus, the character doesn’t
lose anything either.

If the stake is higher than the character’s Wealth bonus (before applying any reductions from purchasing the stake),
the character gets a +1 bonus on his or her Gamble check for every point the purchase DC is above the character’s
Wealth bonus.

The character’s Gamble check is opposed by the Gamble checks of all other participants in the game. (If playing at a
casino, assume the house has a Gamble skill modifier equal to the stake purchase DC. Regardless of the stake purchase
DC, the house does not get a bonus on its Gamble check for the purchase DC.) If there are many characters
participating, the GM can opt to make a single roll for all of them, using the highest Gamble skill modifier among them
and adding a +2 bonus to the check.

If the character beats all other participants, he or she wins and gains an increase to his or her Wealth bonus. The
amount of the increase depends on the difference between the character’s check result and the next highest result among
the other participants.

<table>
<tr><th>Check Result Difference</th><th>Wealth Bonus Increase</th></tr>
<tr><td>1–9</td><td>+1</td></tr>
<tr><td>10–19</td><td>+2</td></tr>
<tr><td>20–29</td><td>+3</td></tr>
<tr><td>30­–39</td><td>+4</td></tr>
<tr><td>40 or more</td><td>+5</td></tr>
</table>

**Try Again?**: No, unless the character wants to put up another stake.

**Special**: A character can’t take 10 or take 20 when making a Gamble check.

A character with the [Confident](modern-feats.html#confident) feat gets a +2 bonus on all Gamble checks.

**Time**: A Gamble check requires 1 hour.

<h3 id="investigate">Investigate (Int; Trained Only)</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

**Check**: A character generally uses Search to discover clues and Investigate to analyze them. If the character has
access to a crime lab, the character uses the Investigate skill to collect and prepare samples for the lab. The result
of the Investigate check provides bonuses or penalties to the lab workers.

**Analyze Clue**: The character can make an Investigate check to apply forensics knowledge to a clue.  This function of
the Investigate skill does not give the character clues where none existed before. It simply allows the character to
extract extra information from a clue he or she has found.

The base DC to analyze a clue is 15. It is modified by the time that has elapsed since the clue was left, and whether
or not the scene was disturbed.

<table>
<tr><th>Circumstances</th><th>DC Modifier</th></tr>
<tr><td>Every day since event (max modifier +10)+2</td></tr>
<tr><td>Scene is outdoors</td><td>+5</td></tr>
<tr><td>Scene slightly disturbed</td><td>+2</td></tr>
<tr><td>Scene moderately disturbed</td><td>+4</td></tr>
<tr><td>Scene extremely disturbed</td><td>+6</td></tr>
</table>

**Collect Evidence**: The character can collect and prepare evidentiary material for a lab. This use of the Investigate
skill requires an evidence kit.

To collect a piece of evidence, make an Investigate check (DC 15). If the character succeeds, the evidence is usable by
a crime lab. If the character fails, a crime lab analysis can be done, but the lab takes a –5 penalty on any necessary
check. If the character fails by 5 or more, the lab analysis simply cannot be done. On the other hand, if the character
succeeds by 10 or more, the lab gains a +2 circumstance bonus on its checks to analyze the material.

This function of the Investigate skill does not provide the character with evidentiary items. It simply allows the
character to collect items he or she has found in a manner that best aids in their analysis later, at a crime lab.

**Try Again?**: Generally, analyzing a clue again doesn’t add new insight unless another clue is introduced. Evidence
collected cannot be recollected, unless there is more of it to take.

**Special**: A character can take 10 when making an Investigate check, but cannot take 20.

Collecting evidence requires an evidence kit. If the character does not have the appropriate kit, the character takes a
–4 penalty on his or her check.

A character with the [Attentive](modern-feats.html#attentive) feat and at least 1 rank in this skill gets a +2 bonus on
all Investigate checks.

**Time**: Analyzing a clue is a full-round action. Collecting evidence generally takes 1d4 minutes per object.

<h3 id="navigate">Navigate (Int)</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

**Check**: Make a Navigate check when a character is trying to find his or her way to a distant location without
directions or other specific guidance. Generally, a character does not need to make a check to find a local street or
other common urban site, or to follow an accurate map. However, the character might make a check to wend his or her way
through a dense forest or a labyrinth of underground storm drains.

For movement over a great distance, make a Navigate check. The DC depends on the length of the trip. If the character
succeeds, he or she moves via the best reasonable course toward his or her goal. If the character fails, he or she
still reaches the goal, but it takes the character twice as long (the character loses time backtracking and correcting
his or her path). If the character fails by more than 5, the or she travels the expected time, but only gets halfway to
his or her destination, at which point the character becomes lost.

A character may make a second Navigate check (DC 20) to regain his or her path. If the character succeeds, he or she
continues on to his or her destination; the total time for the trip is twice the normal time. If the character fails,
he or she loses half a day before the character can try again. The character keeps trying until he or she succeeds,
losing half a day for each failure.

<table>
<tr><th>Length of Trip</th><th>DC</th></tr>
<tr><td>Short (a few hours)</td><td>20</td></tr>
<tr><td>Moderate (a day or two)</td><td>22</td></tr>
<tr><td>Long (up to a week)</td><td>25</td></tr>
<tr><td>Extreme (more than a week)</td><td>28</td></tr>
</table>

When faced with multiple choices, such as at a branch in a tunnel, a character can make a Navigate check (DC 20) to
intuit the choice that takes the character toward a known destination.  If unsuccessful, the character chooses the
wrong path, but at the next juncture, with a successful check, the character realizes his or her mistake.

A character cannot use this function of Navigate to find a path to a site if the character has no idea where the site
is located. The GM may choose to make the Navigate check for the character in secret, so he or she doesn’t know from
the result whether the character is following the right or wrong path.

A character can use Navigate to determine his or her position on earth without the use of any high-tech equipment by
checking the constellations or other natural landmarks. The character must have a clear view of the night sky to make
this check. The DC is 15.

Special: A character can take 10 when making a Navigate check. A character can take 20 only when determining his or her
location, not when traveling.

A character with the [Guide](modern-feats.html#guide) feat gets a +2 bonus on all Navigate checks.

**Time**: A Navigate check is a full-round action.

<h3 id="pilot">Pilot (Dex; Trained Only)</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

**Check**: Typical piloting tasks don’t require checks. Checks are required during combat, for special maneuvers, or in
other extreme circumstances, or when the pilot wants to attempt something outside the normal parameters of the vehicle.
When flying, the character can attempt simple maneuvers and stunts (actions in which the pilot attempts to do something
complex very quickly or in a limited space).

Each vehicle’s description includes a maneuver modifier that applies to Pilot checks made by the operator of the
vehicle.

**Special**: A character can take 10 when making a Pilot check, but can’t take 20.

A character with the [Vehicle Expert](modern-feats.html#vehicleexpert) feat gets a +2 bonus on all Pilot checks.

There is no penalty for operating a general-purpose fixed-wing aircraft. Other types of aircraft (heavy aircraft,
helicopters, jet fighters, and spacecraft) require the corresponding
[Aircraft Operation](modern-feats.html#aircraftoperation) feat, or else the character takes a –4 penalty on Pilot
checks.

**Time**: A Pilot check is a move action.

<h3 id="repair">Repair (Int; Trained Only)</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

**Check**: Most Repair checks are made to fix complex electronic or mechanical devices. The DC is set by the GM. In
general, simple repairs have a DC of 10 to 15 and require no more than a few minutes to accomplish. More complex repair
work has a DC of 20 or higher and can require an hour or more to complete. Making repairs also involves a monetary cost
when spare parts or new components are needed, represented by a Wealth check. If the GM decides this isn’t necessary
for the type of repair the character is attempting, then no Wealth check is needed.

<table>
<tr><th>Repair Task (Example)</th><th>Purchase DC</th><th>Repair DC</th><th>Time</th></tr>
<tr><td>Simple (tool, simple weapon)</td><td>4</td><td>10</td><td>1 min.</td></tr>
<tr><td>Moderate (mechanical or electronic component)</td><td>7</td><td>15</td><td>10 min.</td></tr>
<tr><td>Complex (mechanical or electronic device)</td><td>10</td><td>20</td><td>1 hr.</td></tr>
<tr><td>Advanced (cutting-edge mechanical or electronic device)</td><td>13</td><td>25</td><td>10 hr.</td></tr>
</table>

**Jury-Rig**: A character can choose to attempt jury-rigged, or temporary, repairs. Doing this reduces the purchase DC
by 3 and the Repair check DC by 5, and allows the character to make the checks in as little as a full-round action.
However, a jury-rigged repair can only fix a single problem with a check, and the temporary repair only lasts until the
end of the current scene or encounter. The jury-rigged object must be fully repaired thereafter.

A character can also use jury-rig to hot-wire a car or jump-start an engine or electronic device. The DC for this is at
least 15, and it can be higher depending on the presence of security devices.

The jury-rig application of the Repair skill can be used untrained.

**Try Again?**: Yes, though in some specific cases, the GM may decide that a failed Repair check has negative
ramifications that prevent repeated checks.

**Special**: A character can take 10 or take 20 on a Repair check. When making a Repair check to accomplish a jury-rig
repair, a character can’t take 20.

Repair requires an electrical tool kit, a mechanical tool kit, or a multipurpose tool, depending on the task. If the
character do not have the appropriate tools, he or she takes a –4 penalty on the check.

Craft (mechanical) or Craft (electronic) can provide a +2 synergy bonus on Repair checks made for mechanical or
electronic devices (see Skill Synergy).

A character with the [Gearhead](modern-feats.html#gearhead) feat and at least 1 rank in this skill gets a +2 bonus on
all Repair checks.

**Time**: See the table for guidelines. A character can make a jury-rig repair as a full-round action, but the work
only lasts until the end of the current encounter.

<h3 id="research">Research (Int)</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

**Check**: Researching a topic takes time, skill, and some luck. The GM determines how obscure a particular topic is
(the more obscure, the higher the DC) and what kind of information might be available depending on where the character
is conducting his or her research.

Information ranges from general to protected. Given enough time (usually 1d4 hours) and a successful skill check, the
character gets a general idea about a given topic. This assumes that no obvious reasons exist why such information
would be unavailable, and that the character has a way to acquire restricted or protected information.

The higher the check result, the better and more complete the information. If the character wants to discover a
specific fact, date, map, or similar bit of information, add +5 to +15 to the DC.

**Try Again?**: Yes.

**Special**: A character can take 10 or take 20 on a Research check.

A character with the [Studious](modern-feats.html#studious) feat gets a +2 bonus on all Research checks.

Computer Use can provide a +2 synergy bonus on a Research check when searching computer records for data (see Skill
Synergy).

**Time**: A Research check takes 1d4 hours.

<h3 id="treatinjury">Treat Injury (Wis)</h3>

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

**Check**: The DC and effect depend on the task attempted.

**Long-Term Care (DC 15)**: With a medical kit, the successful application of this skill allows a patient to recover
hit points and ability points lost to temporary damage at an advanced rate—3 hit points per character level or 3
ability points restored per day of complete rest. A new check is made each day; on a failed check, recovery occurs at
the normal rate for that day of rest and care.

A character can tend up to as many patients as he or she has ranks in the skill. The patients need complete bed rest
(doing nothing all day). The character needs to devote at least ½ hour of the day to each patient the character is
caring for.

**Restore Hit Points (DC 15)**: With a medical kit, if a character has lost hit points, the character can restore some
of them. A successful check, as a full-round action, restores 1d4 hit points. The number restored can never exceed the
character’s full normal total of hit points. This application of the skill can be used successfully on a character only
once per day.

Revive Dazed, Stunned, or Unconscious Character (DC 15): With a first aid kit, the character can remove the dazed,
stunned, or unconscious condition from a character. This check is an attack action.

A successful check removes the dazed, stunned, or unconscious condition from an affected character. The character can’t
revive an unconscious character who is at –1 hit points or lower without first stabilizing the character.

Stabilize Dying Character (DC 15): With a medical kit, a character can tend to a character who is dying. As an attack
action, a successful Treat Injury check stabilizes another character. The stabilized character regains no hit points,
but he or she stops losing them. The character must have a medical kit to stabilize a dying character.

**Surgery (DC 20)**: With a surgery kit, a character can conduct field surgery. This application of the Treat Injury
skill carries a –4 penalty, which can be negated with the [Surgery](modern-feats.html#surgery) feat. Surgery requires
1d4 hours; if the patient is at negative hit points, add an additional hour for every point below 0 the patient has
fallen.

Surgery restores 1d6 hit points for every character level of the patient (up to the patient’s full normal total of hit
points) with a successful skill check.  Surgery can only be used successfully on a character once in a 24-hour period.

A character who undergoes surgery is fatigued for 24 hours, minus 2 hours for every point above the DC the surgeon
achieves. The period of fatigue can never be reduced below 6 hours in this fashion.

**Treat Disease (DC 15)**: A character can tend to a character infected with a treatable disease. Every time the
diseased character makes a saving throw against disease effects (after the initial contamination), the treating
character first makes a Treat Injury check to help the diseased character fend off secondary damage. This activity
takes 10 minutes. If the treating character’s check succeeds, the treating character provides a bonus on the diseased
character’s saving throw equal to his or her ranks in this skill.

**Treat Poison (DC 15)**: A character can tend to a poisoned character. When a poisoned character makes a saving throw
against a poison’s secondary effect, the treating character first makes a Treat Injury check as an attack action. If
the treating character’s check succeeds, the character provides a bonus on the poisoned character’s saving throw equal
to his or her ranks in this skill.

**Try Again?**: Yes, for restoring hit points, reviving dazed, stunned, or unconscious characters, stabilizing dying
characters, and surgery. No, for all other uses of the skill.

**Special**: The [Surgery](modern-feats.html#surgery) feat gives a character the extra training he or she needs to use
Treat Injury to help a wounded character by means of an operation.

A character can take 10 when making a Treat Injury check. A character can take 20 only when restoring hit points or
attempting to revive dazed, stunned, or unconscious characters.

Long-term care, restoring hit points, treating disease, treating poison, or stabilizing a dying character requires a
medical kit. Reviving a dazed, stunned, or unconscious character requires either a first aid kit or a medical kit.
Surgery requires a surgery kit. If the character does not have the appropriate kit, he or she takes a –4 penalty on the check.

A character can use the Treat Injury skill on his or herself only to restore hit points, treat disease, or treat
poison. The character takes a –5 penalty on your check any time he or she treats his or herself.

A character with the [Medical Expert](modern-feats.html#medicalexpert) feat gets a +2 bonus on all Treat Injury checks.

**Time**: Treat Injury checks take different amounts of time based on the task at hand, as described above.


Legal Information
-----------------

[MSRD Open Gaming License](srd/msrdlegal.html)
