SRD20 Project
=============

This project is a guide to playing games based on the System Reference 
Document published between 2000 and 2004. The <abbr title="System 
Reference Document">SRD</abbr> provides character backgrounds and 
abilities for building an archetypal hero in fantasy, modern, and 
science-fiction worlds.

### A Note on Terms

The word __background__ is used to refer to [race][2a]. Background 
traits are made up of a combination of genetic and cultural traits. Many 
traits are referred to as racial traits, but they are still part of a 
character's background. The word __creature__ is used to describe living 
entities, and enemies are called creatures instead of 
[monsters](srd/MonstersIntro-A.html). The person in charge of setting 
the stage and adjucating character interactions is referred to as 
__gamemaker__, often shortened to GM.


Basic Character Creation
------------------------

Follow these steps to create your character.

1. Review [Basics][1a] and follow the instructions for [Generating 
   Ability Scores](ability-scores.html).
2. Choose your background from [Standard Backgrounds](standard-backgrounds.html).
3. Choose a character archetype from [Classes I][3a] or [Classes II][3b].
4. Choose a number of [Heroic Skills](heroic-skills.html) based on 
   background and class.
5. Choose a number of [Feats](srd/Feats.html) based on background and class.
6. Review the guidelines for character [Description][6a]. A background 
   includes your character's general physique, heritage, genetic makeup, 
   and cultural traits; but you get to decide other characteristics like 
   hair, skin, and eye color, clothing, decoration, personality, etc..
11. If you are a spellcaster, review your spell list at [Spell List 
   I][11a] or [Spell List II][11b] and choose which spells your 
   character "knows" or "prepares" daily. Detailed spell descriptions 
   are listed alphabetically: [Spells A-B][11c] [Spells C][11d] [Spells 
   D-E][11e] [Spells F-G][11f] [Spells H-L][11g] [Spells M-O][11h] 
   [Spells P-R][11i] [Spells S][11j] [Spells T-Z][11k].


Experimental
------------

### New Classes

- [Channeler Backgrounds](channeler-backgrounds.html): Psionic races 
converted to spell-like and supernatural abilities.
- [Classless Characters](classless-characters.html): An idea for generic characters requiring less paperwork to build and maintain.
- [Fundamental Characters](fundamental-characters.html): SRD classes stripped of genre-specific theming.
- [New Classes: The Crew](heist-characters.html): An experimental set of characters for a modern setting.
- [New Class: The Healer](healer-class.html): A wielder of positive energy akin to but not as versatile as the cleric.

### Skills and Special Abilities

- [Ranked Skill System](skill-ranks.html): A new skill system without micro-managing skill points.
- [Score-based Abilities](ability-specials.html): Generic special abilities for use with customizable characters.


Observations
------------

Compiled notes from deep dives into the source material looking for patterns and new ways of framing core mechanics.

- [Hit Dice and Hit Points](hit-dice.html)
- [Non-standard Dice](nonstandard-dice.html)
- [Patterns](patterns.html)


Legendary Forest
----------------

- [Legendary Backgrounds](legendary-backgrounds.html)
- [Legendary Creatures](legendary-creatures.html)


Planetary Romance
-----------------

- [Planetary Backgrounds](planetary-backgrounds.html)


Waterworld
----------

- [Abilities](waterworld-abilities.html)
- [Backgrounds](waterworld-backgrounds.html)
- [Feats](waterworld-feats.html)
- [Creatures](waterworld-creatures.html)


Legal
-----

This project builds upon the System Reference Document, which was released by Wizards Of The Coast in 2000-2003, and the Modern System Reference Document, which was released 2002-2004 by Wizards Of The Coast. As an author of original content based on an "open-source" game system, there is a distinction between my work, the Open Game Content my work is related to, and the intellectual property which I reference in the context of fair use.

The System Reference Documents I use are covered by the terms of the Open Gaming License. The legal information for these documents are recorded in [System Reference Document - Legal](srd/Legal.html) and [Modern System Reference Document - Legal](srd/msrdlegal.html). Other OGL licensed work is used as well.

- &bull; At times I reproduce character descriptions, explanations of
  abilities, and creature statistics word-for-word from the SRD. In these
  cases I note that it is Open Game Content and whether my additional content
  is also licensed for public use.
- &bull; When I refer to game mechanics and explain how they work, that 
  may be considered a derivative work. However, it is unclear whether game
  mechanics can be considered intellectual property and I reserve my right to
  describe things that exist.
- &bull; When I refer to material that is not Open Game Content, I am using
  that material in the context of fair use. I do not copy text or images or
  try to pass off proprietary content as my own.
- &bull; The OGL itself is all rights reserved. This is a means for 
  <abbr title="Wizards of the Coast">WotC</abbr> to regulate their
  license.


Style Guidelines
----------------

### Capitalization

#### Capitalized

Always capitalize ability score names and their abbreviations e.g. Strength, Dexterity, Con, and Int. Also capitalize saving throws and their abbreviations: Fortitude, Fort, Reflex, Ref, and Will. Size categories&medium;like Large, Medium-size, and Medium&mdash; are always capitalized, as are the terms Hit Die and Hit Dice.

Paragraph leaders, which are short descriptors followed by a colon at the beginning of a paragraph, are always capitalized and followed by a colon. The first word after a paragraph leader is capitalized. Table headers and table captions are capitalized, as is the first word in any table data field, but the rest of the table data field is usually not.

#### Not Capitalized

Do not capitalize base attack bonus or race and class names like dwarf and fighter.

### Emphasis

Emphasize the names of spells and spell-like abilities e.g. _cure_ and _detect magic_, as well as the names of magically enhanced equipment like _+2 masterwork longsword_.

Emphasis can also be used to call out important words in a sentence when needed, but use it sparingly.
  
### Paragraphs and Lists

The first paragraph after a header is not indented, but every subsequent paragraph is; this distinguishes paragraphs from each other since they do not have top or bottom margins. List items are not indented, but their continuation lines are. Paragraphs and lists are taken care of by the stylesheet.


External Links
--------------

+ [Basic Fantasy](https://www.basicfantasy.org/index.html)
+ [Jordan's D&D Article Archives](https://uselessbabble.com/dnd/articles/)
+ [Microlite20](https://microlite20.org/microlite20-srd)
+ [Mini MAM](https://wiki.rpg.net/index.php/Mini_mam)
+ [OSR Resources - Howling Tower](http://www.howlingtower.com/p/old-school-renaissance-resources.html)
+ [Roll Play Dungeon Tile Set 64x64 px](https://opengameart.org/content/roll-play-dungeon-tile-set-64x64-px)
+ [What Classes Should be in D&D](https://critical-hits.com/blog/2012/05/25/what-classes-should-be-in-dnd/)

[1a]: srd/Basics.html
[2a]: srd/Races.html
[3a]: srd/ClassesI.html
[3b]: srd/ClassesII.html
[4a]: srd/SkillsI.html
[4b]: srd/SkillsII.html
[6a]: srd/Description.html
[7a]: srd/Equipment.html
[11a]: srd/SpellListI.html
[11b]: srd/SpellListII.html
[11c]: srd/SpellsA-B.html
[11d]: srd/SpellsC.html
[11e]: srd/SpellsD-E.html
[11f]: srd/SpellsF-G.html
[11g]: srd/SpellsH-L.html
[11h]: srd/SpellsM-O.html 
[11i]: srd/SpellsP-R.html
[11j]: srd/SpellsS.html
[11k]: srd/SpellsT-Z.html
