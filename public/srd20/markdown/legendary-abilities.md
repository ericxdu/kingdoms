### Dancing Lights <a id="dancinglights"></a>

Evocation [Light]

- __Level:__ Brd 0, Sor/Wiz 0
- __Components:__ V, S
- __Casting Time:__ 1 standard action
- __Range:__ Medium (100 ft. + 10 ft./level)
- __Effect:__ Up to four lights, all within a 10- ft.-radius area
- __Duration:__ 1 minute (D)
- __Saving Throw:__ None
- __Spell Resistance:__ No

Depending on the version selected, you create up to four lights that 
resemble lanterns or torches (and cast that amount of light), or up to 
four glowing spheres of light (which look like will-o’-wisps), or one 
faintly glowing, vaguely humanoid shape. The _dancing lights_ must stay 
within a 10-foot-radius area in relation to each other but otherwise 
move as you desire (no concentration required): forward or back, up or 
down, straight or turning corners, or the like. The lights can move up 
to 100 feet per round. A light winks out if the distance between you and 
it exceeds the spell’s range.

_Dancing lights_ can be made permanent with a _permanency_ spell.

### Detect Magic <a id="detectmagic"></a>

Divination

- __Level:__ Brd 0, Clr 0, Drd 0, Sor/Wiz 0
- __Components:__ V, S
- __Casting Time:__ 1 standard action
- __Range:__ 60 ft.
- __Area:__ Cone-shaped emanation
- __Duration:__ Concentration, up to 1 min./level (D)
- __Saving Throw:__ None
- __Spell Resistance:__ No

You detect magical auras. The amount of information revealed depends on 
how long you study a particular area or subject.

_1st Round:_ Presence or absence of magical auras.

_2nd Round:_ Number of different magical auras and the power of the most 
potent aura.

_3rd Round:_ The strength and location of each aura. If the items or 
creatures bearing the auras are in line of sight, you can make 
Spellcraft skill checks to determine the school of magic involved in 
each. (Make one check per aura; DC 15 + spell level, or 15 + half caster 
level for a nonspell effect.)

Magical areas, multiple types of magic, or strong local magical 
emanations may distort or conceal weaker auras.

_Aura Strength:_ An aura’s power depends on a spell’s functioning spell 
level or an item’s caster level. If an aura falls into more than one 
category, _detect magic_ indicates the stronger of the two.

	
<table>
  <tr>
    <th></th>
    <th colspan="4">————————— Aura Power —————————</th>
  </tr>
  <tr>
    <th>Spell or Object</th>
    <th>Faint</th>
    <th>Moderate</th>
    <th>Strong</th>
    <th>Overwhelming</th>
  </tr>
  <tr>
    <td>Functioning spell (spell level)</td>
	<td>3rd or lower</td>
	<td>4th–6th</td>
	<td>7th–9th</td>
	<td>10th+ (deity-level)</td>
  </tr>
  <tr>
    <td>Magic item (caster level)</td>
    <td>5th or lower</td>
    <td>6th–11th</td>
    <td>12th–20th</td>
    <td>21st+ (artifact)</td>
  </tr>
</table>

_Lingering Aura:_ A magical aura lingers after its original source 
dissipates (in the case of a spell) or is destroyed (in the case of a 
magic item). If _detect magic_ is cast and directed at such a location, 
the spell indicates an aura strength of dim (even weaker than a faint 
aura). How long the aura lingers at this dim level depends on its 
original power:

<table>
  <tr>
    <th>Original Strength</th>
    <th>Duration of Lingering Aura</th>
  </tr>
  <tr>
    <td>Faint</td>
    <td>1d6 rounds</td>
  </tr>
  <tr>
    <td>Moderate</td>
    <td>1d6 minutes</td>
  </tr>
  <tr>
    <td>Strong</td>
    <td>1d6x10 minutes</td>
  </tr>
  <tr>
    <td>Overwhelming</td>
    <td>1d6 days</td>
  </tr>
</table>
	

Outsiders and elementals are not magical in themselves, but if they are 
summoned, the conjuration spell registers.

Each round, you can turn to detect magic in a new area. The spell can 
penetrate barriers, but 1 foot of stone, 1 inch of common metal, a thin 
sheet of lead, or 3 feet of wood or dirt blocks it.

_Detect magic_ can be made permanent with a _permanency_ spell.

### Entangle <a id="entangle"></a>

Transmutation

- __Level:__ Drd 1, Plant 1, Rgr 1
- __Components:__ V, S, DF
- __Casting Time:__ 1 standard action
- __Range:__ Long (400 ft. + 40 ft./level)
- __Area:__ Plants in a 40-ft.-radius spread
- __Duration:__ 1 min./level (D)
- __Saving Throw:__ Reflex partial; see text
- __Spell Resistance:__ No

Grasses, weeds, bushes, and even trees wrap, twist, and entwine about 
creatures in the area or those that enter the area, holding them fast 
and causing them to become entangled. The creature can break free and 
move half its normal speed by using a full-round action to make a DC 20 
Strength check or a DC 20 Escape Artist check. A creature that succeeds 
on a Reflex save is not entangled but can still move at only half speed 
through the area. Each round on your turn, the plants once again attempt 
to entangle all creatures that have avoided or escaped entanglement.

_Note:_ The effects of the spell may be altered somewhat, based on the 
nature of the entangling plants.

### Ghost Sound <a id="ghostsound"></a>

Illusion (Figment)

- __Level:__ Brd 0, Sor/Wiz 0

- __Components:__ V, S, M

- __Casting Time:__ 1 standard action

- __Range:__ Close (25 ft. + 5 ft./2 levels)

- __Effect:__ Illusory sounds

- __Duration:__ 1 round/level (D)

- __Saving Throw:__ Will disbelief (if interacted with)

- __Spell Resistance:__ No

_Ghost sound_ allows you to create a volume of sound that rises, 
recedes, approaches, or remains at a fixed place. You choose what type 
of sound _ghost sound_ creates when casting it and cannot thereafter 
change the sound’s basic character.

The volume of sound created depends on your level. You can produce as 
much noise as four normal humans per caster level (maximum twenty 
humans). Thus, talking, singing, shouting, walking, marching, or running 
sounds can be created. The noise a _ghost sound_ spell produces can be 
virtually any type of sound within the volume limit. A horde of rats 
running and squeaking is about the same volume as eight humans running 
and shouting. A roaring lion is equal to the noise from sixteen humans, 
while a roaring dire tiger is equal to the noise from twenty humans.

_Ghost sound_ can enhance the effectiveness of a _silent image_ spell.

_Ghost sound_ can be made permanent with a _permanency_ spell.

_Material Component:_ A bit of wool or a small lump of wax.

### Invisibility <a id="invisibility"></a>

Illusion (Glamer)

- __Level:__ Brd 2, Sor/Wiz 2, Trickery 2
- __Components:__ V, S, M/DF
- __Casting Time:__ 1 standard action
- __Range:__ Personal or touch
- __Target:__ You or a creature or object weighing no more than 100 lb./level
- __Duration:__ 1 min./level (D)
- __Saving Throw:__ Will negates (harmless) or Will negates (harmless, object)
- __Spell Resistance:__ Yes (harmless) or Yes (harmless, object)

The creature or object touched becomes invisible, vanishing from sight,
even from darkvision. If the recipient is a creature carrying gear, that
vanishes, too. If you cast the spell on someone else, neither you nor
your allies can see the subject, unless you can normally see invisible
things or you employ magic to do so.

Items dropped or put down by an invisible creature become visible; items
picked up disappear if tucked into the clothing or pouches worn by the
creature. Light, however, never becomes invisible, although a source of
light can become so (thus, the effect is that of a light with no visible
source). Any part of an item that the subject carries but that extends
more than 10 feet from it becomes visible.

Of course, the subject is not magically _silenced_, and certain other
conditions can render the recipient detectable (such as stepping in a
puddle). The spell ends if the subject attacks any creature. For
purposes of this spell, an attack includes any spell targeting a foe or
whose area or effect includes a foe. (Exactly who is a foe depends on
the invisible character’s perceptions.) Actions directed at unattended
objects do not break the spell. Causing harm indirectly is not an
attack. Thus, an invisible being can open doors, talk, eat, climb
stairs, summon monsters and have them attack, cut the ropes holding a
rope bridge while enemies are on the bridge, remotely trigger traps,
open a portcullis to release attack dogs, and so forth. If the subject
attacks directly, however, it immediately becomes visible along with all
its gear. Spells such as _bless_ that specifically affect allies but not
foes are not attacks for this purpose, even when they include foes in
their area.

_Invisibility_ can be made permanent (on objects only) with a
_permanency_ spell.

_Arcane Material Component:_ An eyelash encased in a bit of gum arabic.

### Mage Hand <a id="magehand"></a>

Transmutation

- __Level: Brd 0, Sor/Wiz 0
- __Components: V, S
- __Casting Time: 1 standard action
- __Range: Close (25 ft. + 5 ft./2 levels)
- __Target: One nonmagical, unattended object weighing up to 5 lb.
- __Duration: Concentration
- __Saving Throw: None
- __Spell Resistance: No

You point your finger at an object and can lift it and move it at will 
from a distance. As a move action, you can propel the object as far as 
15 feet in any direction, though the spell ends if the distance between 
you and the object ever exceeds the spell’s range.
