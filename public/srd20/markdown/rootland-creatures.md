Bodoc
-----

Short stocky herbivore with large horns.


Javing
------

Sightless flying predator with leathery wings.


Mektoub
-------

Herbivorous pachyderm useful as a mount or pack animal.


Ragus
-----

Aggressive and speedy canine omnivore.


Rendor
------

Hooved herbivore with a hard helmet for ramming.


Yelk
----

Bulky herbivore with a shell on its back on which grows poisonous mushrooms.


Yubo
----

Small dog-like omnivore with lots of meat on its bones.
