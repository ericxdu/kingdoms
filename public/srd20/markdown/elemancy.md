Lithomancy
----------

#### Earthquake

Evocation [Earth]

- __Level:__ Clr 8, Destruction 8, Drd 8, Earth 7
- __Components:__ V, S, DF
- __Casting Time:__ 1 standard action
- __Range:__ Long (400 ft. + 40 ft./level)
- __Area:__ 80-ft.-radius spread (S)
- __Duration:__ 1 round
- __Saving Throw:__ See text
- __Spell Resistance:__ No

When you cast _earthquake_, an intense but highly localized tremor rips the ground. The shock knocks creatures down, collapses structures, opens cracks in the ground, and more. The effect lasts for 1 round, during which time creatures on the ground can’t move or attack. A spellcaster on the ground must make a Concentration check (DC 20 + spell level) or lose any spell he or she tries to cast. The earthquake affects all terrain, vegetation, structures, and creatures in the area. The specific effect of an _earthquake_ spell depends on the nature of the terrain where it is cast.

_Cave, Cavern, or Tunnel:_ The spell collapses the roof, dealing 8d6 points of bludgeoning damage to any creature caught under the cave-in (Reflex DC 15 half ) and pinning that creature beneath the rubble (see below). An _earthquake_ cast on the roof of a very large cavern could also endanger those outside the actual area but below the falling debris.

_Cliffs:_ Earthquake causes a cliff to crumble, creating a landslide that travels horizontally as far as it fell vertically. Any creature in the path takes 8d6 points of bludgeoning damage (Reflex DC 15 half ) and is pinned beneath the rubble (see below).

_Open Ground:_ Each creature standing in the area must make a DC 15 Reflex save or fall down. Fissures open in the earth, and every creature on the ground has a 25% chance to fall into one (Reflex DC 20 to avoid a fissure). At the end of the spell, all fissures grind shut, killing any creatures still trapped within.

_Structure:_ Any structure standing on open ground takes 100 points of damage, enough to collapse a typical wooden or masonry building, but not a structure built of stone or reinforced masonry. Hardness does not reduce this damage, nor is it halved as damage dealt to objects normally is. Any creature caught inside a collapsing structure takes 8d6 points of bludgeoning damage (Reflex DC 15 half ) and is pinned beneath the rubble (see below).

_River, Lake, or Marsh:_ Fissures open underneath the water, draining away the water from that area and forming muddy ground. Soggy marsh or swampland becomes quicksand for the duration of the spell, sucking down creatures and structures. Each creature in the area must make a DC 15 Reflex save or sink down in the mud and quicksand. At the end of the spell, the rest of the body of water rushes in to replace the drained water, possibly drowning those caught in the mud.

Pinned beneath Rubble: Any creature pinned beneath rubble takes 1d6 points of nonlethal damage per minute while pinned. If a pinned character falls unconscious, he or she must make a DC 15 Constitution check or take 1d6 points of lethal damage each minute thereafter until freed or dead.

#### Meld into Stone

Transmutation [Earth]

- __Level:__ Clr 3, Drd 3
- __Components:__ V, S, DF
- __Casting Time:__ 1 standard action
- __Range:__ Personal
- __Target:__ You
- __Duration:__ 10 min./level

_Meld into stone_ enables you to meld your body and possessions into a single block of stone. The stone must be large enough to accommodate your body in all three dimensions. When the casting is complete, you and not more than 100 pounds of nonliving gear merge with the stone. If either condition is violated, the spell fails and is wasted.

While in the stone, you remain in contact, however tenuous, with the face of the stone through which you melded. You remain aware of the passage of time and can cast spells on yourself while hiding in the stone. Nothing that goes on outside the stone can be seen, but you can still hear what happens around you. Minor physical damage to the stone does not harm you, but its partial destruction (to the extent that you no longer fit within it) expels you and deals you 5d6 points of damage. The stone’s complete destruction expels you and slays you instantly unless you make a DC 18 Fortitude save.

Any time before the duration expires, you can step out of the stone through the surface that you entered. If the spell’s duration expires or the effect is dispelled before you voluntarily exit the stone, you are violently expelled and take 5d6 points of damage.

The following spells harm you if cast upon the stone that you are occupying: _Stone to flesh_ expels you and deals you 5d6 points of damage. _Stone shape_ deals you 3d6 points of damage but does not expel you. _Transmute rock to mud_ expels you and then slays you instantly unless you make a DC 18 Fortitude save, in which case you are merely expelled. Finally, _passwall_ expels you without damage.


#### Move Earth

Transmutation [Earth]

- __Level:__ Drd 6, Sor/Wiz 6
- __Components:__ V, S, M
- __Casting Time:__ See text
- __Range:__ Long (400 ft. + 40 ft./level)
- __Area:__ Dirt in an area up to 750 ft. square and up to 10 ft. deep (S)
- __Duration:__ Instantaneous
- __Saving Throw:__ None
- __Spell Resistance:__ No

_Move earth_ moves dirt (clay, loam, sand), possibly collapsing embankments, moving hillocks, shifting dunes, and so forth.

However, in no event can rock formations be collapsed or moved. The area to be affected determines the casting time. For every 150-foot square (up to 10 feet deep), casting takes 10 minutes. The maximum area, 750 feet by 750 feet, takes 4 hours and 10 minutes to move.

This spell does not violently break the surface of the ground. Instead, it creates wavelike crests and troughs, with the earth reacting with glacierlike fluidity until the desired result is achieved. Trees, structures, rock formations, and such are mostly unaffected except for changes in elevation and relative topography.

The spell cannot be used for tunneling and is generally too slow to trap or bury creatures. Its primary use is for digging or filling moats or for adjusting terrain contours before a battle.

This spell has no effect on earth creatures.

_Material Component:_ A mixture of soils (clay, loam, and sand) in a small bag, and an iron blade.

#### Repel Metal or Stone

Abjuration [Earth]

- __Level:__ Drd 8
- __Components:__ V, S
- __Casting Time:__ 1 standard action
- __Range:__ 60 ft.
- __Area:__ 60-ft. line from you
- __Duration:__ 1 round/level (D)
- __Saving Throw:__ None
- __Spell Resistance:__ No

Like *repel wood*, this spell creates waves of invisible and intangible energy that roll forth from you. All metal or stone objects in the path of the spell are pushed away from you to the limit of the range. Fixed metal or stone objects larger than 3 inches in diameter and loose objects weighing more than 500 pounds are not affected. Anything else, including animated objects, small boulders, and creatures in metal armor, moves back. Fixed objects 3 inches in diameter or smaller bend or break, and the pieces move with the wave of energy. Objects affected by the spell are repelled at the rate of 40 feet per round.

Objects such as metal armor, swords, and the like are pushed back, dragging their bearers with them. Even magic items with metal components are repelled, although an _antimagic_ field blocks the effects.

The waves of energy continue to sweep down the set path for the spell’s duration. After you cast the spell, the path is set, and you can then do other things or go elsewhere without affecting the spell’s power.

#### Soften Earth and Stone

Transmutation [Earth]

- __Level:__ Drd 2, Earth 2
- __Components:__ V, S, DF
- __Casting Time:__ 1 standard action
- __Range:__ Close (25 ft. + 5 ft./2 levels)
- __Area:__ 10-ft. square/level; see text
- __Duration:__ Instantaneous
- __Saving Throw:__ None
- __Spell Resistance:__ No

When this spell is cast, all natural, undressed earth or stone in the spell’s area is softened. Wet earth becomes thick mud, dry earth becomes loose sand or dirt, and stone becomes soft clay that is easily molded or chopped. You affect a 10-footsquare area to a depth of 1 to 4 feet, depending on the toughness or resilience of the ground at that spot. Magical, enchanted, dressed, or worked stone cannot be affected. Earth or stone creatures are not affected.

A creature in mud must succeed on a Reflex save or be caught for 1d2 rounds and unable to move, attack, or cast spells. A creature that succeeds on its save can move through the mud at half speed, and it can’t run or charge.

Loose dirt is not as troublesome as mud, but all creatures in the area can move at only half their normal speed and can’t run or charge over the surface.

Stone softened into clay does not hinder movement, but it does allow characters to cut, shape, or excavate areas they may not have been able to affect before.

While _soften earth and stone_ does not affect dressed or worked stone, cavern ceilings or vertical surfaces such as cliff faces can be affected. Usually, this causes a moderate collapse or landslide as the loosened material peels away from the face of the wall or roof and falls.

A moderate amount of structural damage can be dealt to a manufactured structure by softening the ground beneath it, causing it to settle. However, most well-built structures will only be damaged by this spell, not destroyed.

#### Spike Stones

Transmutation [Earth]

- __Level:__ Drd 4, Earth 4
- __Components:__ V, S, DF
- __Casting Time:__ 1 standard action
- __Range:__ Medium (100 ft. + 10 ft./level)
- __Area:__ One 20-ft. square/level
- __Duration:__ 1 hour/level (D)
- __Saving Throw:__ Reflex partial
- __Spell Resistance:__ Yes

Rocky ground, stone floors, and similar surfaces shape themselves into long, sharp points that blend into the background.

_Spike stones_ impede progress through an area and deal damage. Any creature moving on foot into or through the spell’s area moves at half speed.

In addition, each creature moving through the area takes 1d8 points of piercing damage for each 5 feet of movement through the spiked area.

Any creature that takes damage from this spell must also succeed on a Reflex save to avoid injuries to its feet and legs. A failed save causes the creature’s speed to be reduced to half normal for 24 hours or until the injured creature receives a _cure_ spell (which also restores lost hit points). Another character can remove the penalty by taking 10 minutes to dress the injuries and succeeding on a Heal check against the spell’s save DC.

_Spike stones_ is a magic trap that can’t be disabled with the Disable Device skill.

_Note:_ Magic traps such as _spike stones_ are hard to detect. A rogue (only) can use the Search skill to find _spike stones_. The DC is 25 + spell level, or DC 29 for _spike stones_.

#### Stone Shape

Transmutation [Earth]

- __Level:__ Clr 3, Drd 3, Earth 3, Sor/Wiz 4
- __Components:__ V, S, M/DF
- __Casting Time:__ 1 standard action
- __Range:__ Touch
- __Target:__ Stone or stone object touched, up to 10 cu. ft. + 1 cu. ft./level
- __Duration:__ Instantaneous
- __Saving Throw:__ None
- __Spell Resistance:__ No

You can form an existing piece of stone into any shape that suits your purpose. While it’s possible to make crude coffers, doors, and so forth with _stone shape_, fine detail isn’t possible. There is a 30% chance that any shape including moving parts simply doesn’t work.

_Arcane Material Component:_ Soft clay, which must be worked into roughly the desired shape of the stone object and then touched to the stone while the verbal component is uttered.

#### Transmute Mud to Rock

Transmutation [Earth]

- __Level:__ Drd 5, Sor/Wiz 5
- __Components:__ V, S, M/DF
- __Casting Time:__ 1 standard action
- __Range:__ Medium (100 ft. + 10 ft./level)
- __Area:__ Up to two 10-ft. cubes/level (S)
- __Duration:__ Permanent
- __Saving Throw:__ See text
- __Spell Resistance:__ No

This spell transforms normal mud or quicksand of any depth into soft stone (sandstone or a similar mineral) permanently.

Any creature in the mud is allowed a Reflex save to escape before the area is hardened to stone.

_Transmute mud to rock_ counters and dispels _transmute rock to mud_.

_Arcane Material Component:_ Sand, lime, and water.

#### Transmute Rock to Mud

Transmutation [Earth]

- __Level:__ Drd 5, Sor/Wiz 5
- __Components:__ V, S, M/DF
- __Casting Time:__ 1 standard action
- __Range:__ Medium (100 ft. + 10 ft./level)
- __Area:__ Up to two 10-ft. cubes/level (S)
- __Duration:__ Permanent; see text
- __Saving Throw:__ See text
- __Spell Resistance:__ No

This spell turns natural, uncut or unworked rock of any sort into an equal volume of mud. Magical stone is not affected by the spell. The depth of the mud created cannot exceed 10 feet. A creature unable to levitate, fly, or otherwise free itself from the mud sinks until hip- or chest-deep, reducing its speed to 5 feet and causing a –2 penalty on attack rolls and AC. Brush thrown atop the mud can support creatures able to climb on top of it. Creatures large enough to walk on the bottom can wade through the area at a speed of 5 feet.

If _transmute rock to mud_ is cast upon the ceiling of a cavern or tunnel, the mud falls to the floor and spreads out in a pool at a depth of 5 feet. The falling mud and the ensuing cave-in deal 8d6 points of bludgeoning damage to anyone caught directly beneath the area, or half damage to those who succeed on Reflex saves.

Castles and large stone buildings are generally immune to the effect of the spell, since _transmute rock to mud_ can’t affect worked stone and doesn’t reach deep enough to undermine such buildings’ foundations. However, small buildings or structures often rest upon foundations shallow enough to be damaged or even partially toppled by this spell.

The mud remains until a successful _dispel magic_ or _transmute mud to rock_ spell restores its substance—but not necessarily its form. Evaporation turns the mud to normal dirt over a period of days. The exact time depends on exposure to the sun, wind, and normal drainage.

_Arcane Material Component:_ Clay and water.

#### Wall of Stone

Conjuration (Creation) [Earth]

- __Level:__ Clr 5, Drd 6, Earth 5, Sor/Wiz 5
- __Components:__ V, S, M/DF
- __Casting Time:__ 1 standard action
- __Range:__ Medium (100 ft. + 10 ft./level)
- __Effect:__ Stone wall whose area is up to one 5-ft. square/level (S)
- __Duration:__ Instantaneous
- __Saving Throw:__ See text
- __Spell Resistance:__ No

This spell creates a wall of rock that merges into adjoining rock surfaces. A _wall of stone_ is 1 inch thick per four caster levels and composed of up to one 5-foot square per level. You can double the wall’s area by halving its thickness. The wall cannot be conjured so that it occupies the same space as a creature or another object.

Unlike a _wall of iron_, you can create a _wall of stone_ in almost any shape you desire. The wall created need not be vertical, nor rest upon any firm foundation; however, it must merge with and be solidly supported by existing stone. It can be used to bridge a chasm, for instance, or as a ramp. For this use, if the span is more than 20 feet, the wall must be arched and buttressed. This requirement reduces the spell’s area by half. The wall can be crudely shaped to allow crenellations, battlements, and so forth by likewise reducing the area.

Like any other stone wall, this one can be destroyed by a _disintegrate_ spell or by normal means such as breaking and chipping. Each 5-foot square of the wall has 15 hit points per inch of thickness and hardness 8. A section of wall whose hit points drop to 0 is breached. If a creature tries to break through the wall with a single attack, the DC for the Strength check is 20 + 2 per inch of thickness.

It is possible, but difficult, to trap mobile opponents within or under a _wall of stone_, provided the wall is shaped so it can hold the creatures. Creatures can avoid entrapment with successful Reflex saves.

_Arcane Material Component:_ A small block of granite.


Pyromancy
---------

Aquamancy
---------

Aeromancy
---------
