Favored Benefits
================

The favored class is an interesting game mechanic. It relies on the player
wanting to avoid an experience penalty, but it adds flavor to certain
character backgrounds and opens up interesting character building
opportunities.

Take the elf for example, who's favored class is wizard. This doesn't so much
mean elves are commonly wizards, but that it is easier for an elf to pick up
wizard skills, regardless of what their primary class is. A high-level elf
warrior might take a level or two of wizard without experience difficulty, or
a high-level elf wizard could take a level or two of rogue without a second
thought. The end result is a race of potent magic users with the versatility
to fight and sneak as well.

<table>
  <caption>Table: Favored Classes</caption>
  <tr>
    <th>Class</th>
    <th>Favoring Races</th>
  </tr>
  <tr>
    <td>Any</td>
    <td>Half-elf, human</td>
  <tr>
  <tr>
    <td>Barbarian</td>
    <td>Grimlock, half-orc, hill giant, locathah, minotaur, ogre, orc, stone giant</td>
  <tr>
  <tr>
    <td>Bard</td>
    <td>Forest gnome, merfolk, rock gnome, satyr</td>
  <tr>
  <tr>
    <td>Cleric</td>
    <td>Drow (female), sahuagin (female), troglodyte</td>
  <tr>
  <tr>
    <td>Druid</td>
    <td>Lizardfolk</td>
  <tr>
  <tr>
    <td>Fighter</td>
    <td>Aquatic elf, average salamander, azer, deep dwarf, duergar, gargoyle, hill dwarf, hobgoblin, mountain dwarf, noble salamander, troll</td>
  <tr>
<tr><td>Monk</td><td></td><tr>
  <tr>
    <td>Paladin</td>
    <td>Aasimar</td>
  <tr>
  <tr>
    <td>Ranger</td>
    <td>Centaur, gnoll, hound archon, sahuagin (male), wood elf</td>
    <tr>
<tr><td>Rogue</td><td>Bugbear, deep halfling, doppelganger, goblin, janni, lightfoot halfling, svirfneblin, tallfellow, tiefling</td><tr>
  <tr>
    <td>Sorcerer</td>
    <td>Kobold, ogre mage, pixie, rakshasa, wild elf</td>
  <tr>
  <tr>
    <td>Wizard</td>
    <td>Drow (male), gray elf, high elf</td>
  <tr>
</table>


Variant: Base Attack Bonus
--------------------------

Instead of adding up your base attack bonus from each class, you use one base
attack bonus based on the average across all classes. Your favored class does
not count against this average.
