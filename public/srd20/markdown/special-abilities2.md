Special Abilities
=================

__Arcane Spells:__

__Divine Spells:__

__Evasion (Ex):__ At 2nd level or higher if you make a successful Reflex saving throw against an attack that normally deals half damage on a successful save, you instead take no damage. Evasion can be used only if you are wearing light armor or no armor. A helpless character does not gain the benefit of evasion. _Prerequisite:_ Base Reflex save +3.

__Familiar:__

__Fast Movement (Ex):__ Your movement speed is improved by +10 feet. This benefit applies only when wearing no armor, light armor, or medium armor and not carrying a heavy load. Apply this bonus before applying speed penalties from load carried or armor worn. 

__Greater Sneak Attack (Ex):__ Your sneak attack damage increases to +6d6. You may gain this ability four times, increasing the damage to +7d6, +8d6, and +9d6. _Prerequisites:_ Stealth 14 ranks, sneak attack, improved sneak attack.

__Improved Evasion (Ex):__

__Improved Sneak Attack (Ex):__ Your sneak attack damage increases to +3d6. You may gain this ability three times, increasing the damage to +4d6 and +5d6. _Prerequisites:_ Stealth 8 ranks, sneak attack.

__Improved Uncanny Dodge (Ex):__ You can no longer be flanked; you can react to opponents on opposite sides of yourself as easily as you can react to a single attacker.

__Increased Attack Bonus:__ You permanently increase your Base Attack Bonus by +1. You may gain this ability multiple times; its effects stack.

__Sneak Attack (Ex):__ If a you can catch an opponent when they are unable to defend effectively from your attack, you can strike a vital spot for extra damage. Any time your target would be denied a Dexterity bonus to AC (whether the target actually has a Dexterity bonus or not), you deal an extra 1d6 points of damage with your attack. Should you score a critical hit with a sneak attack, this extra damage is not multiplied. Ranged attacks can count as sneak attacks if the target is within 30 feet. You cannot sneak attack while striking a creature with concealment or whose vitals are beyond reach.

You may gain this ability twice, increasing damage to +2d6 the second time. _Prerequisites:_ Stealth 4 ranks.

__Uncanny Dodge (Ex):__ You retain your Dexterity bonus to AC regardless of 
being caught flat-footed or struck by a hidden attacker. (You still lose 
your Dexterity bonus to AC if you are immobilized.)
