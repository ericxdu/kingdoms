Heist Character Classes
=======================

Here is an effort to generalize the D20 system starting by rebuilding the class system from the ground up. We will use 
concepts like the Fighter's "bonus feats" and the thief's "special abilities", plus free access to D&D's excellent Feat 
system for character customization. Some skills from [D20 Modern][0] have been included to provide healing ability and the 
ability to use modern technology like cars and computers.

Characters have access to all [SRD feats][4] except as dictated by the GM. Additional feats where noted will link to a 
description of that feat. Class skills from SRD Skills [page I][3i] and [page II][3ii] are tailored to each class to add 
some flavor and incentive to choose that class, but skill points are a good limiter and the GM may decide to simply allow 
access to all skills as class skills, or make a player choose a number of class skills based on skill points at first level.

[0]: srd/msrdlegal.html
[3i]: srd/SkillsI.html
[3ii]: srd/SkillsII.html
[4]: srd/Feats.html


The Muscle
----------

### Game Rule Information

+ **Abilities**: Strength and Concentration
+ **Alignment**: Any
+ **Hit Die**: d8

#### Class Skills

The Muscle’s class skills (and the key ability for each skill) are: Climb (Str), Concentration (Con), Craft (Int), [Drive 
(Dex)][dr], Handle Animal (Cha), Intimidate (Cha), Jump (Str), Knowledge (Int), Profession (Wis), Read/Write Language 
(none), [Repair (Int)][re], Speak Language (none), and Swim (Str).

+ **Skill Points at 1st Level**: (2 + Int modifier) x 4.
+ **Skill Points at Each Additional Level**: 2 + Int modifier.

[dr]: modern-skills.html#drive
[re]: modern-skills.html#repair

#### Class Features

All of the following are class features of the muscle.

+ **Weapon and Armor Proficiency**: all simple weapons.
+ **Bonus Feats**: At 1st level, the muscle gets a bonus feat in addition to the feat that any 1st-level character gets and 
the bonus feat granted to a human character. The muscle gains an additional bonus feat at 2nd level and every two muscle 
levels thereafter (4th, 6th, 8th, 10th, 12th, 14th, 16th, 18th, and 20th). These bonus feats may be drawn from
[SRD Feats](srd/Feats.html) or from the following list: [Gearhead](modern-feats.html#gearhead),
[Surface Vehicle Operation](modern-feats.html#surfacevehicleoperation),
[Vehicle Expert](modern-feats.html#vehicleexpert). The muscle must still meet all prerequisites for a bonus feat,
including ability score and base attack bonus minimums.
+ **Special Abilities**:
- *Crippling Blow* (Ex): If the muscle can catch an opponent when he is unable to defend himself effectively from her 
attack, she can strike a vital spot to weaken and hamper them. This attack deals strength damage any time her target would 
be denied a Dexterity bonus to AC (whether the target actually has a Dexterity bonus or not), or when the muscle flanks her 
target. An opponent damaged by one of her attacks when vulnerable also takes 2 points of Strength damage. Ability points 
lost to damage return on their own at the rate of 1 point per day for each damaged ability. Ranged attacks can count as 
crippling blows only if the target is within 30 feet. The muscle can’t strike with crippling accuracy from beyond that 
range.
- *Feat*: The muscle may gain a bonus feat in place of a special ability.

<table style="width:100%;">
<caption>Table 1: The Muscle</caption>
<tr><th>Level</th><th>Base Attack Bonus</th><th>Fort Save</th><th>Ref Save</th><th>Will Save</th><th>Special</th></tr>
<tr><td>1st</td><td>+1</td><td>+2</td><td>+0</td><td>+0</td><td>Bonus feat, special ability</td></tr>
<tr><td>2nd</td><td>+2</td><td>+3</td><td>+0</td><td>+0</td><td>Bonus feat</td></tr>
<tr><td>3rd</td><td>+3</td><td>+3</td><td>+1</td><td>+1</td><td>Special ability</td></tr>
<tr><td>4th</td><td>+4</td><td>+4</td><td>+1</td><td>+1</td><td>Bonus feat</td></tr>
<tr><td>5th</td><td>+5</td><td>+4</td><td>+1</td><td>+1</td><td>Special ability</td></tr>
<tr><td>6th</td><td>+6/+1</td><td>+5</td><td>+2</td><td>+2</td><td>Bonus feat</td></tr>
<tr><td>7th</td><td>+7/+2</td><td>+5</td><td>+2</td><td>+2</td><td>Special ability</td></tr>
<tr><td>8th</td><td>+8/+3</td><td>+6</td><td>+2</td><td>+2</td><td>Bonus feat</td></tr>
<tr><td>9th</td><td>+9/+4</td><td>+6</td><td>+3</td><td>+3</td><td>Special ability</td></tr>
<tr><td>10th</td><td>+10/+5</td><td>+7</td><td>+3</td><td>+3</td><td>Bonus feat</td></tr>
<tr><td>11th</td><td>+11/+6/+1</td><td>+7</td><td>+3</td><td>+3</td><td>Special ability</td></tr>
<tr><td>12th</td><td>+12/+7/+2</td><td>+8</td><td>+4</td><td>+4</td><td>Bonus feat</td></tr>
<tr><td>13th</td><td>+13/+8/+3</td><td>+8</td><td>+4</td><td>+4</td><td>Special ability</td></tr>
<tr><td>14th</td><td>+14/+9/+4</td><td>+9</td><td>+4</td><td>+4</td><td>Bonus feat</td></tr>
<tr><td>15th</td><td>+15/+10/+5</td><td>+9</td><td>+5</td><td>+5</td><td>Special ability</td></tr>
<tr><td>16th</td><td>+16/+11/+6/+1</td><td>+10</td><td>+5</td><td>+5</td><td>Bonus feat</td></tr>
<tr><td>17th</td><td>+17/+12/+7/+2</td><td>+10</td><td>+5</td><td>+5</td><td>Special ability</td></tr>
<tr><td>18th</td><td>+18/+13/+8/+3</td><td>+11</td><td>+6</td><td>+6</td><td>Bonus feat</td></tr>
<tr><td>19th</td><td>+19/+14/+9/+4</td><td>+11</td><td>+6</td><td>+6</td><td>Special ability</td></tr>
<tr><td>20th</td><td>+20/+15/+10/+5</td><td>+12</td><td>+6</td><td>+6</td><td>Bonus feat</td></tr>
</table>


The Thief
---------

### Game Rule Information

+ **Abilities**: Dexterity
+ **Alignment**: Any
+ **Hit Die**: d8

#### Class Skills

The Thief’s class skills (and the key ability for each skill) are: Balance (Dex), Craft (Int), [Drive (Dex)][dr], Escape 
Artist (Dex), Hide (Dex), Knowledge (Int), Move Silently (Dex), [Pilot (Dex)][pi], Profession (Wis), Read/Write Language 
(none), Ride (Dex), Sleight of Hand (Dex), Speak Language (none), and Tumble (Dex).

+ **Skill Points at 1st Level**: (4 + Int modifier) x 4.
+ **Skill Points at Each Additional Level**: 4 + Int modifier.

[dr]: modern-skills.html#drive
[pi]: modern-skills.html#pilot

#### Class Features

All of the following are class features of the thief.

+ **Weapon and Armor Proficiency**: all simple weapons.
+ **Bonus Feats**: At 1st level, the thief gets a bonus feat in addition to the feat that any 1st-level character gets and 
the bonus feat granted to a human character. The thief gains an additional bonus feat at 2nd level and every two thief 
levels thereafter (4th, 6th, 8th, 10th, 12th, 14th, 16th, 18th, and 20th). These bonus feats may be drawn from
[SRD Feats](srd/Feats.html) or from the following list: [Aircraft Operation](modern-feats.html#aircraftoperation),
[Surface Vehicle Operation](modern-feats.html#surfacevehicleoperation),
[Vehicle Expert](modern-feats.html#vehicleexpert). The thief must still meet all prerequisites for a bonus feat,
including ability score and base attack bonus minimums.
+ **Special Abilities**: At every odd-numbered level (1st, 3rd, 5th, 7th, 9th, 11th, 13th, 15th, 17th, and 19th) the
thief gains a special ability from among the following options.
- *Evasion* (Ex): the thief can avoid even magical and unusual attacks with great agility. If she makes a successful Reflex 
saving throw against an attack that normally deals half damage on a successful save (such as a red dragon’s fiery breath or 
a fireball), she instead takes no damage. Evasion can be used only if the thief is wearing light armor or no armor. A 
helpless thief (such as one who is unconscious or paralysed) does not gain the benefit of evasion.
- *Uncanny Dodge* (Ex): the thief can react to danger before her senses would normally allow her to do so. She retains her 
Dexterity bonus to AC (if any) even if she is caught flat-footed or struck by an invisible attacker. However, she still 
loses her Dexterity bonus to AC if immobilized.
- *Improved Uncanny Dodge* (Ex): this ability can only be taken if the thief already has Uncanny Dodge. The thief can no 
longer be flanked; she can react to opponents on opposite sides of her as easily as she can react to a single attacker. This 
defense denies another thief the ability to sneak attack the character by flanking her, unless the attacker has at least 
four more thief levels than the target does. Levels from other classes that grant uncanny dodge stack to determine the 
minimum thief level required to flank the character.
- *Defensive Roll* (Ex): The thief can roll with a potentially lethal blow to take less damage from it than she otherwise 
would. Once per day, when she would be reduced to 0 or fewer hit points by damage in combat (from a weapon or other blow, 
not a spell or special ability), the thief can attempt to roll with the damage. To use this ability, the thief must 
attempt a Reflex saving throw (DC = damage dealt). If the save succeeds, she takes only half damage from the blow; if it 
fails, she takes full damage. She must be aware of the attack and able to react to it in order to execute her defensive 
roll— if she is denied her Dexterity bonus to AC, she can’t use this ability.
- *Opportunist* (Ex): Once per round, the thief can make an attack of opportunity against an opponent who has just been 
struck for damage in melee by another character. This attack counts as the thief’s attack of opportunity for that round. 
Even a thief with the Combat Reflexes feat can’t use the opportunist ability more than once per round.
- *Feat*: The thief may gain a bonus feat in place of a special ability.

<table style="width:100%;">
<caption>Table 2: The Thief</caption>
<tr><th>Level</th><th>Base Attack Bonus</th><th>Fort Save</th><th>Ref Save</th><th>Will Save</th><th>Special</th></tr>
<tr><td>1st</td><td>+0</td><td>+0</td><td>+2</td><td>+0</td><td>Bonus feat, special ability</td></tr>
<tr><td>2nd</td><td>+1</td><td>+0</td><td>+3</td><td>+0</td><td>Bonus feat</td></tr>
<tr><td>3rd</td><td>+2</td><td>+1</td><td>+3</td><td>+1</td><td>Special ability</td></tr>
<tr><td>4th</td><td>+3</td><td>+1</td><td>+4</td><td>+1</td><td>Bonus feat</td></tr>
<tr><td>5th</td><td>+3</td><td>+1</td><td>+4</td><td>+1</td><td>Special ability</td></tr>
<tr><td>6th</td><td>+4</td><td>+2</td><td>+5</td><td>+2</td><td>Bonus feat</td></tr>
<tr><td>7th</td><td>+5</td><td>+2</td><td>+5</td><td>+2</td><td>Special ability</td></tr>
<tr><td>8th</td><td>+6/+1</td><td>+2</td><td>+6</td><td>+2</td><td>Bonus feat</td></tr>
<tr><td>9th</td><td>+6/+1</td><td>+3</td><td>+6</td><td>+3</td><td>Special ability</td></tr>
<tr><td>10th</td><td>+7/+2</td><td>+3</td><td>+7</td><td>+3</td><td>Bonus feat</td></tr>
<tr><td>11th</td><td>+8/+3</td><td>+3</td><td>+7</td><td>+3</td><td>Special ability</td></tr>
<tr><td>12th</td><td>+9/+4</td><td>+4</td><td>+8</td><td>+4</td><td>Bonus feat</td></tr>
<tr><td>13th</td><td>+9/+4</td><td>+4</td><td>+8</td><td>+4</td><td>Special ability</td></tr>
<tr><td>14th</td><td>+10/+5</td><td>+4</td><td>+9</td><td>+4</td><td>Bonus feat</td></tr>
<tr><td>15th</td><td>+11/+6/+1</td><td>+5</td><td>+9</td><td>+5</td><td>Special ability</td></tr>
<tr><td>16th</td><td>+12/+7/+2</td><td>+5</td><td>+10</td><td>+5</td><td>Bonus feat</td></tr>
<tr><td>17th</td><td>+12/+7/+2</td><td>+5</td><td>+10</td><td>+5</td><td>Special ability</td></tr>
<tr><td>18th</td><td>+13/+8/+3</td><td>+6</td><td>+11</td><td>+6</td><td>Bonus feat</td></tr>
<tr><td>19th</td><td>+14/+9/+4</td><td>+6</td><td>+11</td><td>+6</td><td>Special ability</td></tr>
<tr><td>20th</td><td>+15/+10/+5</td><td>+6</td><td>+12</td><td>+6</td><td>Bonus feat</td></tr>
</table>


The Brain
---------

### Game Rule Information

+ **Abilities**: Intelligence
+ **Alignment**: Any
+ **Hit Die**: d6

#### Class Skills

The Brain’s class skills (and the key ability for each skill) are: [Computer Use (Int)][co], Craft (Int), Decipher Script 
(Int), [Demolitions (Int)][de], Disable Device (Int), Forgery (Int), [Investigate (Int)][in], Knowledge (Int), [Navigate 
(Int)][na], Profession (Wis), Read/Write Language (none), [Repair (Int)][re], Search (Int), and Speak Language (none).

+ **Skill Points at 1st Level**: (8 + Int modifier) x 4.
+ **Skill Points at Each Additional Level**: 8 + Int modifier.

[co]: modern-skills.html#computeruse
[de]: modern-skills.html#demolitions
[in]: modern-skills.html#investigate
[na]: modern-skills.html#navigate
[re]: modern-skills.html#repair

#### Class Features

All of the following are class features of the brain.

+ **Weapon and Armor Proficiency**: all simple weapons.
+ **Bonus Feats**: At 1st level, the brain gets a bonus feat in addition to the feat that any 1st-level character gets and 
the bonus feat granted to a human character. The brain gains an additional bonus feat at 2nd level and every two brain 
levels thereafter (4th, 6th, 8th, 10th, 12th, 14th, 16th, 18th, and 20th). These bonus feats may be drawn from
[SRD Feats](srd/Feats.html) or from the following list: [Attentive](modern-feats.html#attentive),
[Cautious](modern-feats.html#cautious), [Gearhead](modern-feats.html#gearhead), [Guide](modern-feats.html#guide). The
brain must still meet all prerequisites for a bonus feat, including ability score and base attack bonus minimums.
+ **Special Abilities**:
- *Skill Mastery*: The brain becomes so certain in the use of certain skills that she can use them reliably even under 
adverse conditions. Upon gaining this ability, she selects a number of skills equal to 3 + her Intelligence modifier. When 
making a skill check with one of these skills, she may take 10 even if stress and distractions would normally prevent her 
from doing so. The brain may gain this special ability multiple times, selecting additional skills for it to apply to each 
time.
- *Feat*: The brain may gain a bonus feat in place of a special ability.

<table style="width:auto;">
<caption>Table 4: The Brain</caption>
<tr><th>Level</th><th>Base Attack Bonus</th><th>Fort Save</th><th>Ref Save</th><th>Will Save</th><th>Special</th></tr>
<tr><td>1st</td><td>+0</td><td>+0</td><td>+0</td><td>+2</td><td>Bonus feat, special ability</td></tr>
<tr><td>2nd</td><td>+1</td><td>+0</td><td>+0</td><td>+3</td><td>Bonus feat</td></tr>
<tr><td>3rd</td><td>+1</td><td>+1</td><td>+1</td><td>+3</td><td>Special ability</td></tr>
<tr><td>4th</td><td>+2</td><td>+1</td><td>+1</td><td>+4</td><td>Bonus feat</td></tr>
<tr><td>5th</td><td>+2</td><td>+1</td><td>+1</td><td>+4</td><td>Special ability</td></tr>
<tr><td>6th</td><td>+3</td><td>+2</td><td>+2</td><td>+5</td><td>Bonus feat</td></tr>
<tr><td>7th</td><td>+3</td><td>+2</td><td>+2</td><td>+5</td><td>Special ability</td></tr>
<tr><td>8th</td><td>+4</td><td>+2</td><td>+2</td><td>+6</td><td>Bonus feat</td></tr>
<tr><td>9th</td><td>+4</td><td>+3</td><td>+3</td><td>+6</td><td>Special ability</td></tr>
<tr><td>10th</td><td>+5</td><td>+3</td><td>+3</td><td>+7</td><td>Bonus feat</td></tr>
<tr><td>11th</td><td>+5</td><td>+3</td><td>+3</td><td>+7</td><td>Special ability</td></tr>
<tr><td>12th</td><td>+6/+1</td><td>+4</td><td>+4</td><td>+8</td><td>Bonus feat</td></tr>
<tr><td>13th</td><td>+6/+1</td><td>+4</td><td>+4</td><td>+8</td><td>Special ability</td></tr>
<tr><td>14th</td><td>+7/+2</td><td>+4</td><td>+4</td><td>+9</td><td>Bonus feat</td></tr>
<tr><td>15th</td><td>+7/+2</td><td>+5</td><td>+5</td><td>+9</td><td>Special ability</td></tr>
<tr><td>16th</td><td>+8/+3</td><td>+5</td><td>+5</td><td>+10</td><td>Bonus feat</td></tr>
<tr><td>17th</td><td>+8/+3</td><td>+5</td><td>+5</td><td>+10</td><td>Special ability</td></tr>
<tr><td>18th</td><td>+9/+4</td><td>+6</td><td>+6</td><td>+11</td><td>Bonus feat</td></tr>
<tr><td>19th</td><td>+9/+4</td><td>+6</td><td>+6</td><td>+11</td><td>Special ability</td></tr>
<tr><td>20th</td><td>+10/+5</td><td>+6</td><td>+6</td><td>+12</td><td>Bonus feat</td></tr>
</table>


The Fence
---------

### Game Rule Information

+ **Abilities**: Wisdom and Charisma
+ **Alignment**: Any
+ **Hit Die**: d6

#### Class Skills

The fence’s class skills (and the key ability for each skill) are: Bluff (Cha), Craft (Int), Diplomacy (Cha), Disguise 
(Cha), [Gamble (Wis)][ga], Gather Information (Cha), Handle Animal (Cha), [Investigate (Int)][in], Knowledge (Int), Listen 
(Wis), Perform (Cha), Profession (Wis), Read/Write Language (none), Sense Motive (Wis), Speak Language (none), Spot (Wis), 
Survival (Wis), and [Treat Injury (Wis)][tr].

+ **Skill Points at 1st Level**: (6 + Int modifier) x 4.
+ **Skill Points at Each Additional Level**: 6 + Int modifier.

[ga]: modern-skills.html#gamble
[in]: modern-skills.html#investigate
[tr]: modern-skills.html#treatinjury

#### Class Features

All of the following are class features of the fence.

+ **Weapon and Armor Proficiency**: all simple weapons.
+ **Bonus Feats**: At 1st level, the fence gets a bonus feat in addition to the feat that any 1st-level character gets and 
the bonus feat granted to a human character. The fence gains an additional bonus feat at 2nd level and every two fence
levels thereafter (4th, 6th, 8th, 10th, 12th, 14th, 16th, 18th, and 20th). These bonus feats may be drawn from
[SRD Feats](srd/Feats.html) or from the following list: [Attentive](modern-feats.html#attentive),
[Confident](modern-feats.html#confident), [Surgery](modern-feats.html#surgery). The fence must still meet all
prerequisites for a bonus feat, including ability score and base attack bonus minimums.
+ **Special Abilities**:
- *Slippery Mind* (Ex): This ability represents the fence’s ability to wriggle free from magical effects that would 
otherwise control or compel her. If a fence with slippery mind is affected by an enchantment spell or effect and fails her 
saving throw, she can attempt it again 1 round later at the same DC. She gets only this one extra chance to succeed on her 
saving throw.
- *Feat*: The fence may gain a bonus feat in place of a special ability.

<table style="width:100%;">
<caption>Table 5: The Fence</caption>
<tr><th>Level</th><th>Base Attack Bonus</th><th>Fort Save</th><th>Ref Save</th><th>Will Save</th><th>Special</th></tr>
<tr><td>1st</td><td>+0</td><td>+2</td><td>+0</td><td>+2</td><td>Bonus feat, special ability</td></tr>
<tr><td>2nd</td><td>+1</td><td>+3</td><td>+0</td><td>+3</td><td>Bonus feat</td></tr>
<tr><td>3rd</td><td>+2</td><td>+3</td><td>+1</td><td>+3</td><td>Special ability</td></tr>
<tr><td>4th</td><td>+3</td><td>+4</td><td>+1</td><td>+4</td><td>Bonus feat</td></tr>
<tr><td>5th</td><td>+3</td><td>+4</td><td>+1</td><td>+4</td><td>Special ability</td></tr>
<tr><td>6th</td><td>+4</td><td>+5</td><td>+2</td><td>+5</td><td>Bonus feat</td></tr>
<tr><td>7th</td><td>+5</td><td>+5</td><td>+2</td><td>+5</td><td>Special ability</td></tr>
<tr><td>8th</td><td>+6/+1</td><td>+6</td><td>+2</td><td>+6</td><td>Bonus feat</td></tr>
<tr><td>9th</td><td>+6/+1</td><td>+6</td><td>+3</td><td>+6</td><td>Special ability</td></tr>
<tr><td>10th</td><td>+7/+2</td><td>+7</td><td>+3</td><td>+7</td><td>Bonus feat</td></tr>
<tr><td>11th</td><td>+8/+3</td><td>+7</td><td>+3</td><td>+7</td><td>Special ability</td></tr>
<tr><td>12th</td><td>+9/+4</td><td>+8</td><td>+4</td><td>+8</td><td>Bonus feat</td></tr>
<tr><td>13th</td><td>+9/+4</td><td>+8</td><td>+4</td><td>+8</td><td>Special ability</td></tr>
<tr><td>14th</td><td>+10/+5</td><td>+9</td><td>+4</td><td>+9</td><td>Bonus feat</td></tr>
<tr><td>15th</td><td>+11/+6</td><td>+9</td><td>+5</td><td>+9</td><td>Special ability</td></tr>
<tr><td>16th</td><td>+12/+7</td><td>+10</td><td>+5</td><td>+10</td><td>Bonus feat</td></tr>
<tr><td>17th</td><td>+12/+7</td><td>+10</td><td>+5</td><td>+10</td><td>Special ability</td></tr>
<tr><td>18th</td><td>+13/+8</td><td>+11</td><td>+6</td><td>+11</td><td>Bonus feat</td></tr>
<tr><td>19th</td><td>+14/+9/+4</td><td>+11</td><td>+6</td><td>+11</td><td>Special ability</td></tr>
<tr><td>20th</td><td>+15/+10/+5</td><td>+12</td><td>+6</td><td>+12</td><td>Bonus feat</td></tr>
</table>
