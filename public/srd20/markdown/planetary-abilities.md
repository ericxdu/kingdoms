Planetary Romance Abilities
===========================

_Note:_ These are standalone ability descriptors. For class spell 
abilities see [Spells A-B](srd/SpellsA-B.html), [Spells 
C](srd/SpellsC.html), [Spells D-E](srd/SpellsD-E.html), [Spells 
F-G](srd/SpellsF-G.html), [Spells H-L](srd/SpellsH-L.html), [Spells 
M-O](srd/SpellsM-O.html), [Spells P-R](srd/SpellsP-R.html), [Spells 
S](srd/SpellsS.html), and [Spells T-Z](srd/SpellsT-Z.html). For psionic 
abilities see [Psionic Powers A-C](srd/PsionicPowersA-C.html), [Psionic 
Powers D-F](srd/PsionicPowersD-F.html), [Psionic Powers 
G-P](srd/PsionicPowersG-P.html), and [Psionic Powers 
Q-W](srd/PsionicPowersQ-W.html).

Spell-like abilities in this setting are known as "psionics". 

### Enlarge Person <a id="enlargeperson"></a>

Transmutation

- __Level:__ 1
- __Casting Time:__ 1 round
- __Range:__ Close (25 ft. + 5 ft./2 levels)
- __Target:__ One humanoid creature
- __Duration:__ 1 min./level (D)
- __Saving Throw:__ Fortitude negates
- __Spell Resistance:__ Yes

This spell causes instant growth of a humanoid creature, doubling its 
height and multiplying its weight by 8. This increase changes the 
creature’s size category to the next larger one. The target gains a +2 
size bonus to Strength, a –2 size penalty to Dexterity (to a minimum of 
1), and a –1 penalty on attack rolls and AC due to its increased size.

A humanoid creature whose size increases to Large has a space of 10 feet 
and a natural reach of 10 feet. This spell does not change the target’s 
speed.

If insufficient room is available for the desired growth, the creature 
attains the maximum possible size and may make a Strength check (using 
its increased Strength) to burst any enclosures in the process. If it 
fails, it is constrained without harm by the materials enclosing it—the 
spell cannot be used to crush a creature by increasing its size.

All equipment worn or carried by a creature is similarly enlarged by the 
spell. Melee and projectile weapons affected by this spell deal more 
damage. Other magical properties are not affected by this spell. Any 
_enlarged_ item that leaves an _enlarged_ creature’s possession (including a 
projectile or thrown weapon) instantly returns to its normal size. This 
means that thrown weapons deal their normal damage, and projectiles deal 
damage based on the size of the weapon that fired them. Magical 
properties of _enlarged_ items are not increased by this spell.

Multiple magical effects that increase size do not stack,.

_Enlarge person_ counters and dispels reduce person.

_Enlarge person_ can be made permanent with a permanency spell.

_Material Component:_ A pinch of powdered iron.

### Invisibility <a id="invisibility"></a>

Illusion (Glamer)

- __Level:__ 2
- __Casting Time:__ 1 standard action
- __Range:__ Personal or touch
- __Target:__ You or a creature or object weighing no more than 100 lb./level
- __Duration:__ 1 min./level (D)
- __Saving Throw:__ Will negates (harmless) or Will negates (harmless, object)
- __Spell Resistance:__ Yes (harmless) or Yes (harmless, object)

The creature or object touched becomes invisible, vanishing from sight, 
even from darkvision. If the recipient is a creature carrying gear, that 
vanishes, too. If you cast the spell on someone else, neither you nor 
your allies can see the subject, unless you can normally see invisible 
things or you employ magic to do so.

Items dropped or put down by an invisible creature become visible; items 
picked up disappear if tucked into the clothing or pouches worn by the 
creature. Light, however, never becomes invisible, although a source of 
light can become so (thus, the effect is that of a light with no visible 
source). Any part of an item that the subject carries but that extends 
more than 10 feet from it becomes visible.

Of course, the subject is not magically _silenced_, and certain other 
conditions can render the recipient detectable (such as stepping in a 
puddle). The spell ends if the subject attacks any creature. For 
purposes of this spell, an attack includes any spell targeting a foe or 
whose area or effect includes a foe. (Exactly who is a foe depends on 
the invisible character’s perceptions.) Actions directed at unattended 
objects do not break the spell. Causing harm indirectly is not an 
attack. Thus, an invisible being can open doors, talk, eat, climb 
stairs, summon monsters and have them attack, cut the ropes holding a 
rope bridge while enemies are on the bridge, remotely trigger traps, 
open a portcullis to release attack dogs, and so forth. If the subject 
attacks directly, however, it immediately becomes visible along with all 
its gear. Spells such as _bless_ that specifically affect allies but not 
foes are not attacks for this purpose, even when they include foes in 
their area.

_Invisibility_ can be made permanent (on objects only) with a 
_permanency_ spell.

_Arcane Material Component:_ An eyelash encased in a bit of gum arabic.
