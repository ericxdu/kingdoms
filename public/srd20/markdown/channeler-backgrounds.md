Channeler Character Backgrounds
===============================

<small>This material is Open Game Content, and is licensed for public use under
the terms of the Open Game License v1.0a.</small>

These backgrounds are adapted from the psionic races of the System 
Reference Document to serve as common interplanetary species.

Psionics in this document are synonymous with spell-like abilities.

Channeling is similar to the ability of clerics to channel negative and 
positive energy.


Blues
-----

Blue characters possess the following racial traits.

__-2 Strength, +2 Dexterity, +2 Intelligence.__

__Small:__ As a Small creature, a blue gains a +1 size bonus to Armor 
Class, a +1 size bonus on attack rolls, and a +4 size bonus on Stealth 
checks, but it uses smaller weapons than normal, and its lifting and 
carrying limits are three-quarters of those of a Medium character.

__Speed:__ Blue base land speed is 30 feet.

__Darkvision:__ Blues can see in the dark up to 60 feet. Darkvision is 
black and white only, but it is otherwise like normal sight, and blues can 
function just fine with no light at all.

__Channeling:__ Blue spellcasters gain bonus spells as if the governing 
ability score were 1 point higher. This benefit does not grant them the 
ability to cast spells unless they gain that ability through levels in a 
spellcaster class. See [Basics](srd/Basics.html) for Table: Ability 
Modifiers and Bonus Spells.

__Racial Skills:__ A blue character has a +4 racial bonus on Move 
Silently checks and Ride checks.


Dromites
--------

Dromites stand about 3 feet tall and usually weigh slightly more than 30 
pounds. They have iridescent compound eyes. Dromites wear heavy boots 
and light clothing, and are sometimes content with just a sturdy 
harness.

Dromite characters possess the following racial traits.

__-2 Strength, +2 Dexterity, +2 Charisma.__

__Monstrous Humanoid:__ Dromites are not subject to spells or effects that 
affect humanoids only, such as charm person or dominate person.

__Small:__ As a Small creature, a dromite gains a +1 size bonus to Armor 
Class, a +1 size bonus on attack rolls, and a +4 size bonus on Hide 
checks, but it uses smaller weapons than humans use, and its lifting and 
carrying limits are three-quarters of those of a Medium character.

__Speed:__ Dromite base land speed is 20 feet.

__Chitin:__ A dromite’s skin is hardened, almost like an exoskeleton, and 
grants the character a +1 natural armor bonus to AC and one of the 
following kinds of resistance to energy: cold 5, electricity 5, fire 5, or 
sonic 5. The player chooses what type of energy resistance is gained when 
the character is created. (This choice also dictates which caste the 
dromite belongs to.) This natural energy resistance stacks with any future 
energy resistance gained through other effects.

__Channeling:__ Dromite spellcasters gain bonus spells as if the governing 
ability score were 1 point higher. This benefit does not grant them the 
ability to cast spells unless they gain that ability through levels in a 
spellcaster class. See [Basics](srd/Basics.html) for Table: Ability 
Modifiers and Bonus Spells.

__Energy Ray (Sp):__ Once per day as a standard action, a dromite can 
create a ray of energy that shoots forth from its fingertip and strikes a 
target within 25 feet + 5 feet per 4 character levels, dealing 1d6 points 
of damage if it succeeds on a ranged touch attack with the ray. The ray 
deals the kind of energy damage that the dromite's chitin has resistance 
to (for example, an dromite who has resistance to cold 5 deals cold damage 
with its energy ray).

__Scent (Ex):__ Its antennae give a dromite the scent ability. A dromite 
can detect opponents by scent within 30 feet. If the opponent is upwind, 
the range increases to 60 feet; if downwind, it drops to 15 feet. Strong 
scents, such as smoke or rotting garbage, can be detected at twice the 
ranges noted above. Overpowering scents, such as skunk musk or troglodyte 
stench, can be detected at triple normal range. When a dromite detects a 
scent, the exact location of the source is not re vealed—only its presence 
somewhere within range. The dromite can take a move action to note the 
direction of the scent. Whenever the dromite comes within 5 feet of the 
source, the dromite pinpoints the source’s location.

__Compound Eyes:__ This feature of its anatomy gives a dromite a +2 racial 
bonus on Perception checks.


Duergar
-------

Duergar characters possess the following racial traits.

__+2 Constitution, +2 Wisdom, -2 Charisma.__

__Medium:__ As Medium creatures, duergar have no special bonuses or 
penalties due to their size.

__Speed:__ Duergar base land speed is 20 feet. However, duergar can move 
at this speed even when wearing medium or heavy armor or when carrying a 
medium or heavy load (unlike other creatures, whose speed is reduced in 
such situations).

__Darkvision:__ Duergar can see in the dark up to 60 feet. Darkvision is 
black and white only, but it is otherwise like normal sight, and duergar 
can function just fine with no light at all.

+2 racial bonus on saves against poison, spells and spell-like effects.

__Stability:__ Duergar are exceptionally stable on their feet. A duergar 
receives a +4 bonus on ability checks made to resist being bull rushed or 
tripped when standing on the ground (but not when climbing, flying, riding 
or otherwise not standing firmly on the ground).

__Psionics (Sp):__ 1/day&mdash;_enlarge person_ and _invisibility_ as a wizard 
of the duergar's character level (minimum caster level 3rd); these 
abilities affect only the duergar and whatever it carries.

__Channeling:__ Duergar spellcasters gain bonus spells as if the governing 
ability score were 3 points higher. This benefit does not grant them the 
ability to cast spells unless they gain that ability through levels in a 
spellcaster class. See [Basics](srd/Basics.html) for Table: Ability 
Modifiers and Bonus Spells.

__Light Sensitivity (Ex):__ Duergar are dazzled in sunlight or within the 
radius of a daylight spell.


Elans
-----

Elan characters possess the following racial traits.

__-2 Constitution, +2 Intelligence.__

__Aberration:__ Elans are not subject to spells or effects that affect 
humanoids only, such as _charm person_ or _dominate person_.

__Medium:__ As Medium creatures, elans have no special bonuses or 
penalties due to their size.

__Speed:__ Elan base land speed is 30 feet.

__Channeling:__ Elan spellcasters gain bonus spells as if the governing 
ability score were 2 points higher. This benefit does not grant them the 
ability to cast spells unless they gain that ability through levels in a 
spellcaster class. See [Basics](srd/Basics.html) for Table: Ability 
Modifiers and Bonus Spells.

__Supernatural Abilities:__ Elans can channel energy to manifest 
supernatural powers. They can use these abilities a number of times per 
day equal to 1&frasl;2 character level &times; Charisma modifier (mimimum 
2). Furthermore, they can &ldquo;lose&rdquo; any prepared arcane spell in 
order to use one of these abilities instead.

_Resistance (Su):_ Elans can channel energy to increase their resistance 
to various forms of attack. As an immediate action, an elan can expend one 
nergy channeling use to gain a +4 racial bonus on saving throws until the 
beginning of her next action.

_Resilience (Su):_ When an elan takes damage, she can channel energy to 
reduce its severity. As an immediate action, she can reduce the damage she 
is about to take by 2 hit points for every energy channeling use she 
expends.

_Repletion (Su):_ An elan can sustain her body without need of food or 
water. If she expends a single energy channeling use, an elan does not 
need to eat or drink for 24 hours.


Half-giants
-----------

Half-giant characters possess the following racial traits.

__+2 Strength, -2 Dexterity.__

__Giant:__ Half-giants are not subject to spells or effects that affect 
humanoids only, such as charm person or dominate person.

__Medium:__ As Medium creatures, half-giants have no special bonuses or 
penalties due to their size.

__Speed:__ Half-giant base land speed is 30 feet.

__Low-Light Vision:__ A half-giant can see twice as far as a human in 
starlight, moonlight, torchlight, and similar conditions of poor 
illumination. He retains the ability to distinguish color and detail under 
these conditions.

__Fire Acclimated:__ Half-giants have a +2 racial bonus on saving throws 
against all fire spells and effects. Half-giants are accustomed to 
enduring high temperatures.

__Powerful Build:__ The physical stature of half-giants lets them function 
in many ways as if they were one size category larger.

Whenever a half-giant is subject to a size modifier or special size 
modifier for an opposed check (such as during grapple checks, bull rush 
attempts, and trip attempts), the half-giant is treated as one size larger 
if doing so is advantageous to him.

A half-giant is also considered to be one size larger when determining 
whether a creature’s special attacks based on size (such as improved grab 
or swallow whole) can affect him. A half-giant can use weapons designed 
for a creature one size larger without penalty. However, his space and 
reach remain those of a creature of his actual size. The benefits of this 
racial trait stack with the effects of powers, abilities, and spells that 
change the subject’s size category.

__Channeling:__ Half-giant spellcasters gain bonus spells as if the 
governing ability score were 2 points higher. This benefit does not grant 
them the ability to cast spells unless they gain that ability through 
levels in a spellcaster class. See [Basics](srd/Basics.html) for Table: 
Ability Modifiers and Bonus Spells.

_Stomp (Sp):_ Once per day as a standard action, an half-giant can 
precipitate a shock wave that travels along the ground, toppling creatures 
and loose objects. The shock wave affects only creatures standing on the 
ground within a 20-foot cone. Creatures that fail a Reflex save with a DC 
equal to 11 &plus; the half-giant's Charisma modifier are thrown to the 
ground, become prone, and take 1d4 points of nonlethal damage.



Maenads
-------

Maenad characters possess the following racial traits.

__Medium:__ As Medium creatures, maenads have no special bonuses or 
penalties due to their size.

__Speed:__ Maenad base land speed is 30 feet.

__Channeling:__ Maenad spellcasters gain bonus spells as if the governing 
ability score were 2 points higher. This benefit does not grant them the 
ability to cast spells unless they gain that ability through levels in a 
spellcaster class. See [Basics](srd/Basics.html) for Table: Ability 
Modifiers and Bonus Spells.

_Sonic Ray (Sp):_ Once per day as a standard action, a maenad can 
generate a tremendous scream of rage that strikes a target within 25 feet 
&plus; 5 feet per 4 character levels, dealing 1d6&minus;1 points of sonic 
damage if he succeeds on a ranged touch attack with the ray. This damage 
ignores hardness.

__Outburst (Ex):__ Once per day, for up to 4 rounds, a maenad can 
subjugate her mentality to gain a boost of raw physical power. When she 
does so, she takes a –2 penalty to Intelligence and Wisdom but gains a +2 
bonus to Strength.


Xephs
-----

Xeph characters possess the following racial traits.

__-2 Strength, +2 Dexterity.__

__Medium:__ As Medium creatures, xeph have no special bonuses or penalties 
due to their size.

__Speed:__ Xeph base land speed is 30 feet.

__Darkvision:__ Xeph can see in the dark up to 60 feet. Darkvision is 
black and white only, but it is otherwise like normal sight, and xeph can 
function just fine with no light at all.

__Racial Bonuses:__ +1 racial bonus on saving throws against powers, 
spells and spell-like effects. Xephs have an innate resistance to 
psionics and magic.

__Channeling:__ Xeph spellcasters gain bonus spells as if the governing 
ability score were 1 point higher. This benefit does not grant them the 
ability to cast spells unless they gain that ability through levels in a 
spellcaster class. See [Basics](srd/Basics.html) for Table: Ability 
Modifiers and Bonus Spells.

_Burst (Su):_ Three times per day, a xeph can increase his or her speed by 
10 feet, plus 10 feet per four character levels beyond 1st, to a maximum 
increase of 30 feet at 9th character level and higher. These bursts of 
speed are considered a competence bonus to the xeph's base speed. A burst 
of speed lasts 3 rounds.


Appendices
==========

<table id="exoticbackgrounds">
<caption>Source Table: Exotic Backgrounds Extended</caption>
<tbody>
  <tr>
    <th>Name</th>
    <th>Size and Type</th>
    <th>Level Adjustment</th>
    <th>Ability Adjustments</th>
    <th>Favored Class</th>
    <th>Channeling</th>
  </tr>
  <tr>
    <td>Blue</td>
    <td>Small humanoid</td>
    <td>+1</td>
    <td>&minus;2 Strength, +2 Intelligence, &minus;2 Charisma</td>
    <td>Wizard</td>
    <td>1</td>
  </tr>
  <tr>
    <td>Dromite</td>
    <td>Small monstrous humanoid</td>
    <td>+1</td>
    <td>+2 Charisma, &minus;2 Strength, &minus;2 Wisdom</td>
    <td>Sorcerer</td>
    <td>1</td>
  </tr>
  <tr>
    <td>Duergar, psionic</td>
    <td>Medium humanoid</td>
    <td>+1</td>
    <td>+2 Constitution, &minus;4 Charisma</td>
    <td>Bard</td>
    <td>3</td>
  </tr>
  <tr>
    <td>Elan</td>
    <td>Medium aberration</td>
    <td>+0</td>
    <td>&minus;2 Charisma</td>
    <td>Wizard</td>
    <td>2</td>
  </tr>
  <tr>
    <td>Half-giant, psionic</td>
    <td>Medium giant</td>
    <td>+1</td>
    <td>+2 Strength, +2 Constitution, &minus;2 Dexterity</td>
    <td>Bard</td>
    <td>2</td>
  </tr>
  <tr>
    <td>Maenad</td>
    <td>Medium humanoid</td>
    <td>+0</td>
    <td>&mdash;</td>
    <td>Sorcerer</td>
    <td>2</td>
  </tr>
  <tr>
    <td>Xeph</td>
    <td>Medium humanoid</td>
    <td>+0</td>
    <td>+2 Dexterity, &minus;2 Strength</td>
    <td>Monk</td>
    <td>1</td>
  </tr>
</tbody>
</table>


Legal Information
=================

OPEN GAME LICENSE Version 1.0a
 
The following text is the property of Wizards of the Coast, Inc. and is Copyright 2000 Wizards of the Coast, Inc ("Wizards"). All Rights Reserved.
 
1. Definitions: (a)"Contributors" means the copyright and/or trademark owners who have contributed Open Game Content; (b)"Derivative Material" means copyrighted material including derivative works and translations (including into other computer languages), potation, modification, correction, addition, extension, upgrade, improvement, compilation, abridgment or other form in which an existing work may be recast, transformed or adapted; (c) "Distribute" means to reproduce, license, rent, lease, sell, broadcast, publicly display, transmit or otherwise distribute; (d)"Open Game Content" means the game mechanic and includes the methods, procedures, processes and routines to the extent such content does not embody the Product Identity and is an enhancement over the prior art and any additional content clearly identified as Open Game Content by the Contributor, and means any work covered by this License, including translations and derivative works under copyright law, but specifically excludes Product Identity. (e) "Product Identity" means product and product line names, logos and identifying marks including trade dress; artifacts; creatures characters; stories, storylines, plots, thematic elements, dialogue, incidents, language, artwork, symbols, designs, depictions, likenesses, formats, poses, concepts, themes and graphic, photographic and other visual or audio representations; names and descriptions of characters, spells, enchantments, personalities, teams, personas, likenesses and special abilities; places, locations, environments, creatures, equipment, magical or supernatural abilities or effects, logos, symbols, or graphic designs; and any other trademark or registered trademark clearly identified as Product identity by the owner of the Product Identity, and which specifically excludes the Open Game Content; (f) "Trademark" means the logos, names, mark, sign, motto, designs that are used by a Contributor to identify itself or its products or the associated products contributed to the Open Game License by the Contributor (g) "Use", "Used" or "Using" means to use, Distribute, copy, edit, format, modify, translate and otherwise create Derivative Material of Open Game Content. (h) "You" or "Your" means the licensee in terms of this agreement.
 
2. The License: This License applies to any Open Game Content that contains a notice indicating that the Open Game Content may only be Used under and in terms of this License. You must affix such a notice to any Open Game Content that you Use. No terms may be added to or subtracted from this License except as described by the License itself. No other terms or conditions may be applied to any Open Game Content distributed using this License.
 
3. Offer and Acceptance: By Using the Open Game Content You indicate Your acceptance of the terms of this License.
 
4. Grant and Consideration: In consideration for agreeing to use this License, the Contributors grant You a perpetual, worldwide, royalty-free, non-exclusive license with the exact terms of this License to Use, the Open Game Content.
 
5. Representation of Authority to Contribute: If You are contributing original material as Open Game Content, You represent that Your Contributions are Your original creation and/or You have sufficient rights to grant the rights conveyed by this License.
 
6. Notice of License Copyright: You must update the COPYRIGHT NOTICE portion of this License to include the exact text of the COPYRIGHT NOTICE of any Open Game Content You are copying, modifying or distributing, and You must add the title, the copyright date, and the copyright holder's name to the COPYRIGHT NOTICE of any original Open Game Content you Distribute.
 
7. Use of Product Identity: You agree not to Use any Product Identity, including as an indication as to compatibility, except as expressly licensed in another, independent Agreement with the owner of each element of that Product Identity. You agree not to indicate compatibility or co-adaptability with any Trademark or Registered Trademark in conjunction with a work containing Open Game Content except as expressly licensed in another, independent Agreement with the owner of such Trademark or Registered Trademark. The use of any Product Identity in Open Game Content does not constitute a challenge to the ownership of that Product Identity. The owner of any Product Identity used in Open Game Content shall retain all rights, title and interest in and to that Product Identity.
 
8. Identification: If you distribute Open Game Content You must clearly indicate which portions of the work that you are distributing are Open Game Content.
 
9. Updating the License: Wizards or its designated Agents may publish updated versions of this License. You may use any authorized version of this License to copy, modify and distribute any Open Game Content originally distributed under any version of this License.
 
10. Copy of this License: You MUST include a copy of this License with every copy of the Open Game Content You Distribute.
 
11. Use of Contributor Credits: You may not market or advertise the Open Game Content using the name of any Contributor unless You have written permission from the Contributor to do so.
 
12. Inability to Comply: If it is impossible for You to comply with any of the terms of this License with respect to some or all of the Open Game Content due to statute, judicial order, or governmental regulation then You may not Use any Open Game Material so affected.
 
13. Termination: This License will terminate automatically if You fail to comply with all terms herein and fail to cure such breach within 30 days of becoming aware of the breach. All sublicenses shall survive the termination of this License.
 
14. Reformation: If any provision of this License is held to be unenforceable, such provision shall be reformed only to the extent necessary to make it enforceable.
 
15. COPYRIGHT NOTICE
Open Game License v 1.0a Copyright 2000, Wizards of the Coast, Inc.
 
__System Reference Document__ Copyright 2000-2003, Wizards of the Coast, Inc.; Authors Jonathan Tweet, Monte Cook, Skip Williams, Rich Baker, Andy Collins, David Noonan, Rich Redman, Bruce R. Cordell, John D. Rateliff, Thomas Reid, James Wyatt, based on original material by E. Gary Gygax and Dave Arneson.

END OF LICENSE
