Classless Characters
====================

<table style="float:right;max-width:50%">
<caption>Table: Character Level Benefits</caption>
<tbody>
  <tr>
    <th>Character Level</th>
    <th>XP</th>
    <th>Base Save Bonus</th>
    <th>Base Attack Bonus</th>
    <th>Skill Check Bonus</th>
    <th>Gained Feats</th>
    <th>Ability Score Increases</th>
  </tr>
  <tr>
    <td>1st</td>
    <td>0</td>
    <td>+0</td>
    <td>+1</td>
    <td>+4</td>
    <td>1st and 2nd</td>
    <td></td>
  </tr>
  <tr>
    <td>2nd</td>
    <td>1,000</td>
    <td>+1</td>
    <td>+1</td>
    <td>+5</td>
    <td>3rd</td>
    <td></td>
  </tr>
  <tr>
    <td>3rd</td>
    <td>3,000</td>
    <td>+1</td>
    <td>+2</td>
    <td>+6</td>
    <td>4th</td>
    <td></td>
  </tr>
  <tr>
    <td>4th</td>
    <td>6,000</td>
    <td>+2</td>
    <td>+2</td>
    <td>+7</td>
    <td>5th</td>
    <td>1st</td>
  </tr>
  <tr>
    <td>5th</td>
    <td>10,000</td>
    <td>+2</td>
    <td>+3</td>
    <td>+8</td>
    <td>6th</td>
    <td></td>
  </tr>
  <tr>
    <td>6th</td>
    <td>15,000</td>
    <td>+3</td>
    <td>+3</td>
    <td>+9</td>
    <td>7th</td>
    <td></td>
  </tr>
  <tr>
    <td>7th</td>
    <td>21,000</td>
    <td>+3</td>
    <td>+4</td>
    <td>+10</td>
    <td>8th</td>
    <td></td>
  </tr>
  <tr>
    <td>8th</td>
    <td>28,000</td>
    <td>+4</td>
    <td>+4</td>
    <td>+11</td>
    <td>9th</td>
    <td>2nd</td>
  </tr>
  <tr>
    <td>9th</td>
    <td>36,000</td>
    <td>+4</td>
    <td>+5</td>
    <td>+12</td>
    <td>10th</td>
    <td></td>
  </tr>
  <tr>
    <td>10th</td>
    <td>45,000</td>
    <td>+5</td>
    <td>+5</td>
    <td>+13</td>
    <td>11th</td>
    <td></td>
  </tr>
</tbody>
</table>

This page describes a way to eschew the traditional SRD character 
classes and opt for a flat, highly customizable character creation and 
advancement system. Classless characters are not balanced against SRD 
core classes. They should only be used in a campaign where the core 
classes are prohibited.

This experimental system makes major changes to the SRD character 
system. As shown in Table: Character Level Benefits, save and attack 
bonuses as well as gained feats are fundamentally changed.

### Skill Ranks

Choose 6 skills (7 if you are human) to be your "trained" skills. See 
[Skills I](srd/SkillsI.html) and [Skills II](srd/SkillsII.html) for 
skill descriptions. When you make a skill check using any of these 
skills, you add 3 &plus; your character level to the result plus any 
other modifiers as normal. Once per level, you may remove one "trained" 
skill and replace it with another skill.

__Variant__: Retraining costs 250 XP &times; your character level. This 
experience is spent in the same way a magic user spends XP to create a 
magic item, i.e. you cannot spend so much XP that you would lose a 
level, but if you have gained enough XP to attain a new level, you can 
immediately spend XP on retraining a skill instead of keeping the XP to 
advance a level.

### Gaining Feats

Feats are at the heart of character customization under the classless 
system. You choose **two feats** upon creating a character and choose an 
**additional feat** each time your character gains a level. Human 
characters gain one extra feat at creation for a total of three. Any 
class level prerequisites become character level prerequisites.

This method replaces the normal feat system of gaining one feat at 1st 
level and every level divisible by three. Characters simply gain one new 
feat every level.

### Weapon and Armor Proficiency

You may choose **one simple weapon** with which to be proficient. You 
are not proficient with any type of armor or shields. You may take 
proficiency feats to become proficient with any more armor or weapons.

__Variant__: You may spend 1 skill point to become proficient with any 
simple weapon. If using the trained skills variant, you may sacrifice 1 
skill at 1st level to become proficient with four simple weapons.

__Variant__: You are proficient with all simple weapons. You are not 
proficient with any type of armor or shields. You may take proficiency 
feats to become proficient with any more armor or weapons.

### Hit Points

At 1st level, your character gets hp equal to **8 &plus; your 
Constitution modifier**. At every subsequent level your character gains 
**1d8 &plus; your Constitution modifier** in additional hit points. This 
puts classless characters on par with the cleric, druid, monk, ranger, 
and most monsters where staying power in combat is concerned.

Humanoids get a d8 Hit Die according to Table: Creature Improvement by 
Type in [Improving Monsters](srd/ImprovingMonsters.html), and d8 is an 
average Hit Die among the core classes. It is also the amount of HP a 
*cure* spell restores.

### Attack Bonuses

Starting at 1st level and every two levels thereafter, characters gain a 
+1 bonus to their base attack bonus, similar to the epic base attack 
bonus described in [Epic Level Basics](srd/EpicLevelBasics). This 
results in an attack bonus equal to **half your character level rounded 
up**. All other relevant modifiers apply.

This puts the average character just slightly ahead of the wizard in 
combat ability. Combat-oriented characters like soldiers can take 
special feats to increase their combat focus and become excellent 
fighters.

### Saving Throws

Starting at 2nd level and every two levels thereafter, characters gain a 
+1 bonus to all saving throws, similar to the epic save bonus described 
in [Epic Level Basics](srd/EpicLevelBasics). This results in a save 
bonus equal to **half your character level rounded down**. All other 
relevant modifiers apply.

This puts every character on par with each other and eliminates major 
survivability gaps at higher levels. Characters can take special feats 
to increase individual save bonuses for specialization.

### Experience

The classless system is intended for settings and campaigns that are 
less focused on math and combat statistics and more on role-playing. 
This form of the game favors the "free-form experience" (CRII p. 39) and 
"story awards" (CRII p. 40) methods of awarding experience to players.


Creature Levels
---------------

<table style="width:100%;">
<tr><th></th><th>Hit Die</th><th>Attack Bonus</th><th>Good Saving Throws</th><th>Skill Points</th></tr>
<tr><td>Humanoid</td><td>d8</td><td>HD &times;3&frasl;4 (as cleric)</td><td>Varies (any one)</td><td>2 &plus; Int mod per HD</td></tr>
<tr><td>Humanoid</td><td>d8</td><td>HD &times;1&frasl;2 (as wizard) &plus; 1</td><td>Fort, Ref, Will</td><td>6 &plus; Int mod per HD</td></tr>
</table>

<table style="float:right;">
<caption>Table: The Humanoid</caption>
<tr>
  <th>Level</th>
  <th>Hit Dice</th>
  <th>Base Attack Bonus</th>
  <th>Fort Save</th>
  <th>Ref Save</th>
  <th>Will Save</th>
  <th>Skill Points</th>
  <th>Special</th>
</tr>
<tr><td>1st</td><td>1d8</td><td>+1</td><td>+0</td><td>+0</td><td>+0</td><td>(6 &plus; Int mod) &times; 4</td><td>Feat, feat</td></tr>
<tr><td>2nd</td><td>2d8</td><td>+1</td><td>+1</td><td>+1</td><td>+1</td><td>6 &plus; Int mod</td><td>Feat</td></tr>
<tr><td>3rd</td><td>3d8</td><td>+2</td><td>+1</td><td>+1</td><td>+1</td><td>6 &plus; Int mod</td><td>Feat</td></tr>
<tr><td>4th</td><td>4d8</td><td>+2</td><td>+2</td><td>+2</td><td>+2</td><td>6 &plus; Int mod</td><td>Feat</td></tr>
<tr><td>5th</td><td>5d8</td><td>+3</td><td>+2</td><td>+2</td><td>+2</td><td>6 &plus; Int mod</td><td>Feat</td></tr>
<tr><td>6th</td><td>6d8</td><td>+3</td><td>+3</td><td>+3</td><td>+3</td><td>6 &plus; Int mod</td><td>Feat</td></tr>
<tr><td>7th</td><td>7d8</td><td>+4</td><td>+3</td><td>+3</td><td>+3</td><td>6 &plus; Int mod</td><td>Feat</td></tr>
<tr><td>8th</td><td>8d8</td><td>+4</td><td>+4</td><td>+4</td><td>+4</td><td>6 &plus; Int mod</td><td>Feat</td></tr>
<tr><td>9th</td><td>9d8</td><td>+5</td><td>+4</td><td>+4</td><td>+4</td><td>6 &plus; Int mod</td><td>Feat</td></tr>
<tr><td>10th</td><td>10d8</td><td>+5</td><td>+5</td><td>+5</td><td>+5</td><td>6 &plus; Int mod</td><td>Feat</td></tr>
<tr><td>11th</td><td>11d8</td><td>+6</td><td>+5</td><td>+5</td><td>+5</td><td>6 &plus; Int mod</td><td>Feat</td></tr>
<tr><td>12th</td><td>12d8</td><td>+6</td><td>+6</td><td>+6</td><td>+6</td><td>6 &plus; Int mod</td><td>Feat</td></tr>
<tr><td>13th</td><td>13d8</td><td>+7</td><td>+6</td><td>+6</td><td>+6</td><td>6 &plus; Int mod</td><td>Feat</td></tr>
<tr><td>14th</td><td>14d8</td><td>+7</td><td>+7</td><td>+7</td><td>+7</td><td>6 &plus; Int mod</td><td>Feat</td></tr>
<tr><td>15th</td><td>15d8</td><td>+8</td><td>+7</td><td>+7</td><td>+7</td><td>6 &plus; Int mod</td><td>Feat</td></tr>
<tr><td>16th</td><td>16d8</td><td>+8</td><td>+8</td><td>+8</td><td>+8</td><td>6 &plus; Int mod</td><td>Feat</td></tr>
<tr><td>17th</td><td>17d8</td><td>+9</td><td>+8</td><td>+8</td><td>+8</td><td>6 &plus; Int mod</td><td>Feat</td></tr>
<tr><td>18th</td><td>18d8</td><td>+9</td><td>+9</td><td>+9</td><td>+9</td><td>6 &plus; Int mod</td><td>Feat</td></tr>
<tr><td>19th</td><td>19d8</td><td>+10</td><td>+9</td><td>+9</td><td>+9</td><td>6 &plus; Int mod</td><td>Feat</td></tr>
<tr><td>20th</td><td>20d8</td><td>+10</td><td>+10</td><td>+10</td><td>+10</td><td>6 &plus; Int mod</td><td>Feat</td></tr>
</table>

In the SRD, creatures without a class are treated as having a "creature 
class" equal to its Hit Dice in levels. If a creature has 1 Hit Die or a 
fractional Hit Die, the first Hit Die it gains in a normal character 
class replaces its racial Hit Die.

Classless characters advance in level without a character class as if 
they had a creature class with HD advancement. This creature class is 
more powerful than the "humanoid" monster type by necessity, or else it 
would be underpowered as a player-character.

__Humanoid Type__: .

*Features*: A humanoid has the following features.

&mdash;8-sided Hit Dice.

&mdash;Base attack bonus equal to 1&frasl;2 total Hit Dice, rounded up.

&mdash;Saving throws equal to 1&frasl;2 total Hit Dice, rounded down.

&mdash;6 + Int modifier of skills at rank 3 + HD.

*Traits*:

&mdash;Proficient with all simple weapons.

&mdash;Not proficient with any type of armor or shields.

&mdash;Two feats at 1 HD, and one feat for every additional HD.

__Human Subtype__: .

*Features*: A human has the following features.

*Traits*:

&mdash;Medium: No special bonuses or penalties due to size.

&mdash;Base land speed 30 feets.

&mdash;1 bonus feat at 1 HD.

&mdash;1 bonus skill at rank 3 + HD.
