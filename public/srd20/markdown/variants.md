# D20 Rules Variants

This document contains observations, abstractions, extensions, 
variations, and ruminations on the d20 system of role-playing adventure 
gaming. Many of the rules can be found in the D20 System Reference 
Document, which was licensed by <cite>Wizards of the Coast</cite> in 
2000 under the terms of the Open Game License. [Legal 
information](d20srd/Legal.html) about this release can be read here and 
the entire [SRD collection](d20srd/SRD.zip) can be downloaded here.


<a id="savingthrows">
## VARIANT: SIMPLIFIED SAVING THROWS

To simplify saving throw bonuses, each character receives a base save
bonus according to their character level. 

<table style="float:right;">
<tr><th>Class</th><th>Bonus Feats</th></tr>
<tr><td>Barbarian</td><td>Fighter's Fortitude</td></tr>
<tr><td>Bard</td><td>Rogue's Reflexes, Wizard's Willpower</td></tr>
<tr><td>Cleric</td><td>Fighter's Fortitude, Wizard's Willpower</td></tr>
<tr><td>Druid</td><td>Fighter's Fortitude, Wizard's Willpower</td></tr>
<tr><td>Fighter</td><td>Fighter's Fortitude</td></tr>
<tr><td>Monk</td><td>Fighter's Fortitude, Rogue's Reflexes, Wizard's Willpower</td></tr>
<tr><td>Paladin</td><td>Fighter's Fortitude</td></tr>
<tr><td>Ranger</td><td>Fighter's Fortitude, Rogue's Reflexes</td></tr>
<tr><td>Rogue</td><td>Rogue's Reflexes</td></tr>
<tr><td>Sorceror</td><td>Wizard's Willpower</td></tr>
<tr><td>Wizard</td><td>Wizard's Willpower</td></tr>
</table>

  - **Base Save Bonus Increase**: every 3rd character level all of 
your base save bonuses increase by +1.

  - **Bonus feat**: a character gains a class-specific bonus feat 
at 1st level as a class feature.

### FIGHTER'S FORTITUDE

- **Benefits**: you get a +2 bonus on all Fortitude saving throws.
The bonus increases to +3 at 4th, +4 at 10th, +5 at 16th, and +6 at
20th based on your Fighter's Fortitude class level. Your class level
for Fighter's Fortitude is the total level of all your classes that
grant Fighter's Fortitude. 

### ROGUE'S REFLEXES

  - **Benefits**: same as Fighter's Fortitude, except it applies to Reflex 
saving throws.

### WIZARD'S WILLPOWER

  - **Benefits**: same as Fighter's Fortitude, except it applies to Will 
saving throws.


## VARIANT: CONSTITUTION SCORE AND DYING

A character can die by reaching -10 hit points (see [Injury and 
Death](d20srd/CombatI.html)) or by taking roughly 10 damage to 
Constitution (see [Ability Score 
Loss](d20srd/AbilitiesandConditions.html)). This variant correlates the 
two death conditions.

DYING: a dying character's negative hit points are checked against their 
Constitution score. If the negative hit points exceed the Constitution 
score, the character has died. If the character has a Constitution score 
higher than 10, that character doesn't die when their hit points reach 
-10.

MASSIVE DAMAGE: if a single attack deals damage equal to or exceeding 5x 
a characters's Constitution score and it doesn't kill them, they must 
make a DC 15 Fortitude save to avoid dying.


## VARIANT: ABILITY SCORES

There are several ways to determine a character's ability scores. The 
following guidelines allow you to choose between making a character who 
is average, elite, high-powered, or anything in between. I've 
constructed the arrays according to clues in the SRD and published 
rulebooks (see [Ability Score Arrays](d20srd/ImprovingMonsters.html)) 
and assigned a corresponding points cost, die roll method, and standard 
limitations to each method.

**Average**: use the scores 11, 10, 11, 10, 11, 10, in that order. Many 
creatures and non-player characters have these exact scores, modified by 
their racial bonuses. See the [Pixie](d20srd/MonstersS.html) for an 
example. You can also get average scores by rolling 3d6 six times, or 
buying them for 15 points.

**Nonelite**: The nonelite array is 13, 12, 11, 10, 9, 8. These scores 
are no better than average, but allow for a character with strengths and 
weaknesses. Characters with these scores are considered low-powered and 
they are usually reserved for creatures who take levels in an NPC class. 
These scores can be bought for 15 points. You can also get nonelite 
ability scores by rolling 3d6 six times, but the total modifiers must 
not be -3 or lower, and one score must be 12 or higher.

<table style="float:right;">
<caption>Table: Ability Score Point Costs (start at 8)</caption>
<tr><th>Ability Score</th><th>Point Cost</th>
<th>Ability Score</th><th>Point Cost</th></tr>
<tr><td>9</td><td>1</td><td>14</td><td>6</td></tr>
<tr><td>10</td><td>2</td><td>15</td><td>8</td></tr>
<tr><td>11</td><td>3</td><td>16</td><td>10</td></tr>
<tr><td>12</td><td>4</td><td>17</td><td>13</td></tr>
<tr><td>13</td><td>5</td><td>18</td><td>16</td></tr>
</table>

**Challenging**: use the scores 15, 13, 12, 11, 10, 8. Challenging 
characters are nearly but not quite as good as elite characters. 
Challenging ability scores can be bought for 22 points.

**Elite**: The elite array is 15, 14, 13, 12, 10, 8. These scores are 
usually reserved for player characters and NPC who take levels in one of 
the eleven base classes. Using the point buy system, these scores can be 
bought for 25 points. You can get elite ability scores by rolling 4d6 
six times and totalling the three highest dice for each score. Modifiers 
must total at least +1, and one score must be 14 or higher.

**Tougher**: use the scores 15, 14, 13, 12, 11, 10. Tougher characters 
are slightly better than elite characters. The scores can be bought for 
28 points. You can also get tougher ability scores by rolling 4d6 six 
times, rerolling the lowest die *once* during the entire process, and 
totallying the three highest dice for each score. Modifiers must total 
at least +1, and one score must be 14 or higher.

**High-powered**: use the scores 16, 15, 13, 12, 11, 10. 
Player-characters with these scores are meant for high-powered 
campaigns. You can buy high-powered ability scores for 32 points or roll 
5d6 six times, totalling the three highest dice for each score. 
Modifiers must total at least +2 and one score must be 15 or higher.


## BEHIND THE CURTAIN: BEFORE 1ST LEVEL

<table style="float:right;">
<caption>Before and After 1st Level</caption>
<tr>
<th>Character Level</th>
<th>Gained Feat</th>
<th>Good Save Bonus</th>
<th>Favored Enemy</th>
<th>Barbarian Rage</th>
<th>Fighter Bonus Feat</th>
</tr>
<tr><td>0th </td><td>1st</td><td>+2</td><td>1st</td><td>1/day</td><td>Bonus feat</td></tr>
<tr><td>1st </td><td>   </td><td>+2</td><td>   </td><td>     </td><td>          </td></tr>
<tr><td>2nd </td><td>   </td><td>+3</td><td>   </td><td>     </td><td>Bonus feat</td></tr>
<tr><td>3rd </td><td>2nd</td><td>+3</td><td>   </td><td>     </td><td>          </td></tr>
<tr><td>4th </td><td>   </td><td>+4</td><td>   </td><td>2/day</td><td>Bonus feat</td></tr>
<tr><td>5th </td><td>   </td><td>+4</td><td>2nd</td><td>     </td><td>          </td></tr>
<tr><td>6th </td><td>3rd</td><td>+5</td><td>   </td><td>     </td><td>Bonus feat</td></tr>
<tr><td>7th </td><td>   </td><td>+5</td><td>   </td><td>     </td><td>          </td></tr>
<tr><td>8th </td><td>   </td><td>+6</td><td>   </td><td>3/day</td><td>Bonus feat</td></tr>
<tr><td>9th </td><td>4th</td><td>+6</td><td>   </td><td>     </td><td>          </td></tr>
<tr><td>10th</td><td>   </td><td>+7</td><td>3rd</td><td>     </td><td>Bonus feat</td></tr>
</table>

Achieving 1st level is not the same as gaining levels. 1st level 
represents a lifetime of training and practice, which is why a 1st-level 
character has a minimum age. A character gains abilities and attributes 
immediately upon reaching 2nd level and beyond, but some benefits 
granted to a 1st-level character may have been gained by that character 
*before* they officialy reached 1st level.

A good indicator for a benefit gained before 1st level is those 
abilities which have a pattern that does not match up with the 1st-level 
gain. For example: gaining a feat every 3rd level, gaining a favored 
enemy every 5th level, and gaining +1 to a save every 2nd level.


## VARIANT: SIMPLIFIED SKILL RANKS

Tracking skill ranks in multiple different classes can become detailed 
work with little benefit to gameplay or character development. This 
variant is a way to track skill ranks while ignoring skill points. It is 
accomplished by giving your character an overall Skill Rank and applying 
a Rank Penalty under certain circumstances.

  - **CLASS RANK BONUS**: your class rank bonus is equal to 3 + 
your character level. You add this bonus to all skill check rolls 
you make with a class skill you are trained in.

  - **CROSS RANK BONUS**: your cross rank bonus is equal to half 
your class rank bonus, rounded down. You add this bonus to all 
skill check rolls you make with a cross-class skill you are 
trained in.

<table style="float:right;">
<tr><th>Top Class</th><th>Trained Skills</th></tr>
<tr><td>Cleric, fighter, paladin, sorceror, wizard</td><td>2 + Int modifier</td></tr>
<tr><td>Barbarian, druid, monk</td><td>4 + Int modifier</td></tr>
<tr><td>Bard, ranger</td><td>6 + Int modifier</td></tr>
<tr><td>Rogue</td><td>8 + Int modifier</td></tr>
</table>

  - **TRAINED SKILLS**: you can have a number of "trained" skills 
without penalty according to your "top class". Your top class is 
determined by whether you have even or uneven class levels, as 
explained below.

  - **Even Levels**: if all of your class levels are within 1 
level of each other, your top class is determined by the class 
which has the highest number of skills in the table.

  - **Uneven Levels**: if any of your classes are two or more 
levels apart, your top class is determined by your highest-level 
class listed in the table. Your favored class does not count as an 
uneven level.

  - **RANK BONUS AND PENALTY**: if at any time you have more 
trained skills than your top class allows, you suffer a -1 rank 
penalty for every extra skill you have trained beyond that number.


## VARIANT: HIT POINTS AS PLOT ARMOR

Hit Point loss "...gives you scars, bangs up your armor, and gets blood 
on your tunic, but it doesn't slow you down..." (CRI p145). HP are very 
similar to "plot armor". They keep a character from being killed or 
seriously injured until the narrative calls for it, and they stop 
protecting the creature in unfortunate situations like a coup de grace 
or massive damage.

PLAYERS KNOW REMAINING HP: the characters of a story are guided by the 
plot to avoid confrontation, press an attack, or make a retreat. In 
role-playing games this guidance comes in the form of Hit Points.

CURE SPELLS: a paladin prays for divine favor, a bard shouts an 
encouraging word, a druid makes an appeal to primal tenacity, a cleric 
blesses you with divine grace.

POTIONS OF HEALING


## BEHIND THE CURTAIN: MECHANICAL ARCHETYPES

The character classes in D20 can be categorized by a mix of basic 
qualities. See [Classes I](d20srd/ClassesI.html) and [Classes 
II](d20srd/ClassesII.html) for source material.

### Depth of Training (by age category)

<table>
<tr><th>Intuitive</th><td>Barbarian, rogue, sorceror    </td></tr>
<tr><th>Practiced</th><td>Bard, fighter, paladin, ranger</td></tr>
<tr><th>Academic </th><td>Cleric, druid, monk, wizard   </td></tr>
</table>

**Intuitive**: abilities which are innate or learned quickly through 
practical experience at a young age.

**Practiced**: taught from an established tradition usually by an 
experienced mentor.

**Academic**: graduated or in classes from an organized association of 
teachers.

### Staying Power (by HD Type)

<table>
<tr><th>Weakling</th><td>Wizard, sorceror           </td></tr>
<tr><th>Scrappy </th><td>Bard, rogue                </td></tr>
<tr><th>Fighting</th><td>Cleric, druid, monk, ranger</td></tr>
<tr><th>Warrior </th><td>Fighter, paladin           </td></tr>
<tr><th>Titanic </th><td>Barbarian                  </td></tr>
</table>

### Battle Prowess (by Base Attack Bonus)

<table style="float:right;">
<tr><th>as Fighter (Good)  </th><td>Barbarian, fighter, paladin, ranger</td></tr>
<tr><th>as Cleric (Average)</th><td>Bard, cleric, druid, monk, rogue   </td></tr>
<tr><th>as Wizard (Poor)   </th><td>Sorceror, wizard                   </td></tr>
</table>

**as Fighter**: experienced combatants who train with martial weapons 
are good at making successful attacks.

**as Cleric**: adventurers who expect to see some combat and have an 
average attack ability.

**as Wizard**: magicians who are focused on spellcasting ability and are 
poor at attacking with weapons.

### Martial Training (by Weapon Proficiency)

<table style="float:right;">
<tr><th>Weapon Proficiency</th><th>Classes</th></tr>
<tr><th>Martial</th><td>Barbarian, fighter, paladin, ranger</td></tr>
<tr><th>Simple </th><td>Bard+, cleric, rogue+, sorceror      </td></tr>
<tr><th>Limited</th><td>Druid, monk, wizard                </td></tr>
</table>

**Martial**: professional warriors who train with all simple and martial 
weapons.

**Simple**: adventurers who expect to get into some fights and thus 
train with simple weapons. Those with a "+" additionally train with 
specific martial and exotic weapons.

**Limited**: adventurers who train exclusively with a set of specific 
martial, simple, and exotic weapons due to tradition or time constrains.

### Skilled (by Skill Points)

<table>
<caption>Skill Points (Least to Most)</caption>
<tr><th>Basic       </th><td>Cleric, fighter, paladin, sorceror, wizard</td></tr>
<tr><th>Focused     </th><td>Barbarian, druid, monk                    </td></tr>
<tr><th>Saavy       </th><td>Bard, ranger                              </td></tr>
<tr><th>Expert      </th><td>Rogue                                     </td></tr>
</table>


## BEHIND THE CURTAIN: HIT DICE, HIT POINTS, AND OPPOSING ROLLS

Hit Dice and damage rolls oppose each other. For instance, a 1st-level 
Cleric (1d8 HD) can take a lucky hit from a heavy mace (1d8) without 
being killed. A 5th-level Cleric (5d8) can withstand 5 average hits from 
a heavy mace (1d8/x5) in a fight.

Con bonuses and Str bonuses oppose each other. A Dwarf with 3 character 
levels and a +2 Con bonus can shrug off the +2 Str bonus to damage rolls 
from a half-orc's attack 3 times.


## VARIANT: HIT POINTS AND WOUNDS

<hr style="clear:both;">

## References

  - [3.5 D&amp;D Books](https://the-eye.eu/public/Site-Dumps/adambibby.ca/download/dnd/3.5%20D%26D%20Books/)
  - [4d6 Drop Lowest](https://anydice.com/articles/4d6-drop-lowest/)
  - [5th Edition SRD](https://5thsrd.com/)
  - [d20 Modern System Reference Document](http://www.spellbooksoftware.com/d20mrsd/srdhome.html)
  - [d20 System Archive](http://www.wizards.com/default.asp?x=d20/article/srdarchive)
  - [Dungeons & Dragons 3.5 System Reference Document](https://archive.org/details/dnd35srd)
  - [The Hypertext d20 SRD](https://www.d20srd.org/index.htm)
  - [Jordan's D&D Article Archives](https://uselessbabble.com/dnd/articles/)
  - background from [Roll Play Dungeon Tile Set 64x64 px](https://opengameart.org/content/roll-play-dungeon-tile-set-64x64-px)
