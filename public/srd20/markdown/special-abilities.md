Special Abilities
=================

The rogue hints at a hidden subsystem in d20. For a 10th level rogue, 
special abilities are chosen from a list not unlike the fighter's bonus 
feats, but more specialized. In d20 Modern, each class gets access to a 
sort of special ability list called "talents", and this progression 
completely replaces the more rigid core classes.

Special abilities have less prerequisites than feats, because they are 
usually tied to a specific class. This makes it hard to generalize them; 
if any class could gain the rogue's special abilities, it could throw 
the whole system out of balance. However special abilities are closely 
related to feats. A paladin, for instance, can choose the Holy Warrior 
subclass that eschews four levels of spellcasting ability in exchange 
for four fighter bonus feats.

The character creation and advancement process benefits from choosing 
among various special abilities, in addition to normal feats.


Extraordinary Abilities
-----------------------

Some special abilities are shared by multiple classes, suggesting they 
have universal utility. For simplification purposes, only prestige 
classes included in the *Dungeon Master's Guide* are listed below. 
Standard classes from many supplements are included.

__AC Bonus (Ex)__: Monk, ninja.

__Evasion (Ex)__: Monk, ranger, rogue, shadowdancer.

__Fast Movement (Ex)__: Barbarian, monk.

__Improved Evasion (Ex)__: Monk, rogue, shadowdancer.

__Sneak Attack (Ex)__: Arcane trickster, assassin, blackguard, rogue, 
spellthief.

If a character gets a sneak attack bonus from multiple sources, the 
bonuses on damage stack. Characters above 20th level can further improve 
their sneak attack bonus by taking the improved sneak attack feat.

The ninja has a similar ability called sudden strike (*Complete 
Adventurer* p. 8). The extra damage from sudden strike only stack with 
the extra damage from sneak attack when both would apply to the same 
target. For the purpose of qualifying for feats, prestige classes, etc., 
the ninja's sudden strike ability stacks with sneak attack (*Complete 
Adventurer p. 8).

Martial Abilities
-----------------

A character with a poor base attack bonus gets a +1 increase to it every 
even-numbered level. If the character had the option to gain the 
following special ability at every odd-numbered level, he could take it 
five times to have an average base attack bonus or ten times to have a 
good base attack bonus. 

### Martial Training

You are better trained at hitting targets with melee or ranged weapons.

__Benefit__: Your base attack bonus improves by +1.

__Special__: You can gain this ability up to ten times. Its effects stack.


Class Feats
-----------

Unlike the fighter's bonus feats, some classes grant bonus feats in a 
more limited manner, but allow them to be taken with fewer prerequisites 
and used under the right circumstances.

__Improved Grapple/Stunning Fist__: 1st level monk.

__Track__: 1st level ranger.

__Combat Reflexes/Deflect Arrows__: 2nd level monk.

__Rapid Shot/Two-Weapon Fighting__: 2nd level ranger. Light armor or no 
armor.

__Endurance__: 3rd level ranger.

__Improved Disarm/Improved Trip__: 6th level monk.

__Manyshot/Improved Two-Weapon Fighting__: 6th level ranger. Light armor 
or no armor.

__Improved Precise Shot/Greater Two-Weapon Fighting__: 11th level 
ranger. Light armor or no armor.


Paladins and Rangers Without Spellcasting
-----------------------------------------

Paladins and rangers gain the ability to cast spells at 4th level. As 
divine spellcasters without the chance of spell failure or a limit to 
spells known, this ability is an exceptionally useful and adaptable 
tool. These characters can keep a repertoire or spells ready each day 
for frequent use or for the occasional need, or they can rest and 
meditate on a problem until they can prepare the right spell to overcome 
it.

Variant paladins and rangers can exchange any spellcasting ability for 
unique special abilities instead.


Spellcasting Abilities
----------------------

<table>
  <tr>
    <th></th>
    <th>Arcane</th>
    <th>Divine</th>
    <th>Ki</th>
  </tr>
  <tr>
    <th>Charisma</th>
    <td>Bard, sorcerer</td>
    <td>Divine grace, lay on hands, smite evil, turn or rebuke undead</td>
  </tr>
  <tr>
    <th>Intelligence</th>
    <td>Wizard</td>
    <td></td>
  </tr>
  <tr>
    <th>Religious Wisdom</th>
    <td></td>
    <td>Cleric</td>
    <td>Quivering palm</td>
  </tr>
  <tr>
    <th>Natural Wisdom</th>
    <td></td>
    <td>Druid</td>
  </tr>
</table>

Chracters can aquire spell-like abilities by race or class, but most 
cast spells using a chart of "spells per day" from a class.

"Spells per day" charts grant access to a new level of spells in a 
pattern similar to bonus feats. For the Bard, its 2nd level, 4th level, 
and every three levels thereafter up to spell level sixth. For clerics, 
druids, and wizards its 1st level and every two levels up to spell level 
ninth. Paladins and rangers get spells at 4th level and every four 
levels up to spell level fourth. Prestige classes present yet more 
progression patterns.

The [Fundamental Classes](fundamental-classes.html) document describes 
three classes that gain a special ability at 1st level and every two 
levels thereafter. This is an ideal set-up to provide feat-like special 
abilities that grant well-paced access to spells up to spell level 
fourth, sixth, ninth, and beyond.

### Arcane Spellcasting

### Cantrips [1st-level Feat]

### Divine Spellcasting

### Greater Arcane Spellcasting

### Greater Divine Spellcasting

### Improved Arcane Spellcasting

### Improved Divine Spellcasting
