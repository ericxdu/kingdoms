Ability Specials
================

<table style="float:right;">
  <tr>
    <th>Strength</th>
    <td>Extreme effort &check;, ignore hardness &check;, melee smash &check;</td>
  </tr>
  <tr>
    <th>Dexterity</th>
    <td>Defensive &check;, increased speed &check;</td>
  </tr>
  <tr>
    <th>Constitution</th>
    <td>Damage reduction &check;, energy resistance &check;, unbreakable &check;</td>
  </tr>
  <tr>
    <th>Intelligence</th>
    <td>Research &check; (needs work), strategy &check;</td>
  </tr>
  <tr>
    <th>Wisdom</th>
    <td>Empathic &check;, healing &check; (needs work), insightful &check;</td>
  </tr>
  <tr>
    <th>Charisma</th>
    <td>Charm &check;, fast-talk &check;, leadership &check;</td>
  </tr>
</table>

The talent trees of <abbr title="Modern System Reference 
Document">MSRD</abbr> are versatile and inspired by <abbr title="System 
Reference Document">SRD</abbr> core classes. They are reproduced here 
converted to feat format for convenience. All references to "action 
points" are changed to uses per day. References to MSRD skills or feats 
are struck out or converted to SRD.

__Score-based Special Ability System__: Your class may restrict which 
special abilities you can choose e.g. by saying "choose any Wisdom or 
Charisma special ability from this list" or "you must have a 
Constitution score of at least 11 to choose the corresponding special 
ability". You must still meet all listed prerequisites to gain the 
ability.

Alternatively the class feature may simply say "choose any special 
ability from this list" in which case you need only abide by the 
prerequisite(s) listed for that special ability.

__Class Level__: For the purposes of the following descriptions, "class 
level" refers to the class in which you gained the special ability. 
Multiclass characters have fewer class levels than total character 
levels for the purposes of the following descriptions.

__Revisions__: Athough based on MSRD, this abilities list is part of a 
flexible class system and subject to change for balance and flavor 
purposes.

Special Descriptions
--------------------

### Extreme Effort [Strength]

You can push yourself to make an extreme effort. The effort must relate 
either to a Strength check or a Strength-based skill check. You must 
decide to use this ability before making the check.

__Benefit:__ The effort requires a full-round action and provides a +2 
bonus on the check.

_Alternate Name:_ Mighty Force.

### Improved Extreme Effort [Strength]

You can push yourself to make an extreme effort. The effort must relate 
either to a Strength check or a Strength-based skill check. You must 
decide to use this ability before making the check.

__Prerequisite:__ Extreme Effort.

__Benefit:__ The effort requires a full-round action and provides a +2 
bonus that stacks with the bonus provided by extreme effort (+4 total).

_Alternate Name:_ Improved Mighty Force.

### Advanced Extreme Effort [Strength]

You can push yourself to make an extreme effort. The effort must relate 
either to a Strength check or a Strength-based skill check. You must 
decide to use this ability before making the check.

__Prerequisites:__ Extreme Effort, Improved Extreme Effort.

__Benefit:__ The effort requires a full-round action and provides a +2 
bonus that stacks with the bonuses provided by extreme effort and 
improved extreme effort (+6 total).

_Alternate Name:_ Advanced Mighty Force.

### Ignore Hardness [Strength]

You have an innate talent for finding weaknesses in objects. This allows 
you to ignore some of an object’s hardness when making a melee attack to 
break it.

__Benefit:__ You ignore 2 points of an object’s hardness.


### Improved Ignore Hardness [Strength]

You have an innate talent for finding weaknesses in objects. This allows 
you to ignore some of an object’s hardness when making a melee attack to 
break it.

__Prerequisite:__ Ignore hardness.

__Benefit:__ You ignore 2 additional points of an object’s hardness (for 
a total of 4).


### Advanced Ignore Hardness [Strength]

You have an innate talent for finding weaknesses in objects. This allows 
you to ignore some of an object’s hardness when making a melee attack to 
break it.

__Prerequisites:__ Ignore Hardness, Improved Ignore Hardness.

__Benefit:__ You ignore 2 additional points of an object’s hardness (for 
a total of 6).


### Melee Smash [Strength]

You have an innate talent that increases melee damage.

__Benefit:__ You receive a +1 bonus on melee damage.

_Alternate Name:_ Mighty Blow.

### Improved Melee Smash [Strength]

You have an innate talent that increases melee damage.

__Prerequisite:__ Melee Smash.

__Benefit:__ You receive an additional +1 bonus on melee damage (+2 
total).

_Alternate Name:_ Improved Mighty Blow.

### Advanced Melee Smash [Strength]

You have an innate talent that increases melee damage.

__Prerequisites:__ Melee Smash, Improved Melee Smash.

__Benefit:__ You receive an additional +1 bonus on melee damage (+3 
total).

_Alternate Name:_ Advanced Mighty Blow.

### Evasion [Dexterity]

You gain the ability to improve your innate defensive talents.

__Benefit:__ If you are exposed to any effect that normally allows a 
character to attempt a Reflex saving throw for half damage, you suffer 
no damage if you make a successful saving throw. Evasion can only be 
used when wearing light armor or no armor.

### Uncanny Dodge [Dexterity]

You gain the ability to improve your innate defensive talents.

__Prerequisite:__ Evasion.

__Benefit:__ You retain your Dexterity bonus to Defense regardless of 
being caught flat-footed or struck by a hidden attacker. (You still lose 
your Dexterity bonus to Defense if you are immobilized.)

### Improved Uncanny Dodge [Dexterity]

You gain the ability to improve your innate defensive talents.

__Prerequisites:__ Evasion, Uncanny Dodge.

__Benefit:__ You can no longer be flanked; you can react to opponents on 
opposite sides of yourself as easily as you can react to a single 
attacker.

### Defensive Roll [Dexterity]

You gain the ability to improve your innate defensive talents.

__Prerequisites:__ Evasion, Uncanny Dodge.

__Benefit:__ You can roll with a potentially lethal attack to take less 
damage from it. Once per day, when you would be reduced to 0 hit points 
or less by damage in combat (from a ranged or melee attack), you can 
attempt to roll with the damage.

To use this ability, you must attempt a Reflex saving throw (DC = damage 
dealt). If the save succeeds, you take only half damage. You must be 
able to react to the attack to execute a defensive roll&mdash;if you are 
immobilized, you can&rsquo;t use this talent.

Since this effect would not normally allow a character to make a Reflex 
save for half damage, your evasion talent doesn&rsquo;t apply to the 
defensive roll.

### Opportunist [Dexterity]

You gain the ability to improve your innate defensive talents.

__Prerequisite:__ Evasion.

__Benefit:__ Once per round, you can make an attack of opportunity 
against an opponent who has just been struck for damage in melee by 
another character. This attack counts as your attack of opportunity for 
that round. Even with with the Combat Reflexes feat you can't use this 
talent more than once per round.

### Increased Speed [Dexterity]

You can increase your natural base speed.

__Benefit:__ Your base speed increases by 5 feet.

_Alternate Name:_ Fast Movement.

### Improved Increased Speed [Dexterity]

You can increase your natural base speed.

__Prerequisite:__ Increased Speed.

__Benefit:__ Your base speed increases by 5 feet. This talent stacks 
with increased speed (10 feet total).

_Alternate Name:_ Improved Fast Movement.

### Advanced Increased Speed [Dexterity]

You can increase your natural base speed.

__Prerequisites:__ Increased Speed, Improved Increased Speed.

__Benefit:__ Your base speed increases by 5 feet. This talent stacks 
with increased speed and improved increased speed (15 feet total).

_Alternate Name:_ Advanced Fast Movement.

### Damage Reduction [Constitution]

You have an innate talent to ignore a set amount of damage from most 
weapons, but not from energy.

__Prerequisite:__ Any energy Resistance ability or Remain Conscious, 
Robust, or Second Wind.

__Benefit:__ You ignore 1 point of damage from melee and ranged weapons 
(DR 1/&mdash;).

### Improved Damage Reduction [Constitution]

__Prerequisites:__ Damage Reduction and either one energy Resistance 
ability or Remain Conscious, Robust, or Second Wind.

__Benefit:__ You ignore an additional 1 point of damage from melee and 
ranged weapons (DR 2/&mdash; total).

### Advanced Damage Reduction [Constitution]

__Prerequisites:__ Damage Reduction, Improved Damage Reduction, and 
either one energy Resistance ability or Remain Conscious, Robust, or 
Second Wind.

__Benefit:__ You ignore an additional 1 point of damage from melee and 
ranged weapons (DR 3/&mdash; total).

### Acid Resistance [Constitution]

You are particularly resistant to acid energy effects.

__Benefit:__ You ignore an amount of acid damage equal to your 
Constitution modifier.

### Cold Resistance [Constitution]

You are particularly resistant to cold energy effects.

__Benefit:__ You ignore an amount of cold damage equal to your 
Constitution modifier.

### Electricity Resistance [Constitution]

You are particularly resistant to electricity energy effects.

__Benefit:__ You ignore an amount of electricity damage equal to your 
Constitution modifier.

### Fire Resistance [Constitution]

You are particularly resistant to fire energy effects.

__Benefit:__ You ignore an amount of fire damage equal to your 
Constitution modifier.

### Sonic/Concussion Resistance [Constitution]

You are particularly resistant to sonic energy effects.

__Benefit:__ You ignore an amount of sonic or concussion damage equal to 
your Constitution modifier.

### Remain Conscious [Constitution]

You are particularly resilient.

__Benefit:__ You gain the ability to continue to perform actions when 
you would otherwise be considered unconscious and dying. When your hit 
points reach &minus;1, you can perform as though you were disabled, 
making either an attack action or a move action every round until you 
reache &minus;10 hit points (and dies) or your hit points return to 1 or 
higher. You can choose to succumb to unconsciousness if you thinks that 
doing so might prevent you from taking more damage.

### Robust [Constitution]

You are particularly resilient.

__Benefit:__ You become especially robust, gaining a number of hit 
points equal to your class level as soon as you selects this talent. 
Thereafter, you gain +1 hit point with each class level you gain.

### Second Wind [Constitution]

You are particularly resilient.

__Benfit:__ Once per day you can gain a second wind. When you do this, 
you recover a number of hit points equal to your Constitution modifier. 
This does not increase your hit points beyond the character's full 
normal total.

### Stamina [Constitution]

You are particularly resilient.

__Prerequisite:__ Robust.

__Benefit:__ You recover twice as fast as normal. So, you recover 2 hit 
points per class level per evening of rest, 2 points of temporary 
ability damage per evening of rest, and awaken in half the normal time 
after being knocked unconscious.

### Savant [Intelligence]

You have a natural aptitude for study and fact-finding.

__Benefit:__ Select one of the skills listed in the following paragraph. 
You must have ranks in the skill if it is Trained Only. You get to add a 
bonus equal to your class level when making checks with that skill. You 
can take this talent multiple times; each time it applies to a different 
skill.

<s>Computer Use</s>, Craft (any single skill), Decipher Script, 
<s>Demolitions</s>, Disable Device, Forgery, <s>Investigate</s>, 
Knowledge (any single skill), <s>Navigate</s>, <s>Repair</s>, 
<s>Research</s>, Search.

### Linguist [Intelligence]

You have a natural aptitude for study and fact-finding.

__Prerequisite:__ At least 1 rank in 
either Read/Write Language or Speak Language for each of three different 
languages.

__Benefit:__ With this talent, you become a master linguist. Whenever you 
encounters a new language, either spoken or written, that you do not 
know you can make an Intelligence check to determine if you can 
understand it. The check is made with a bonus equal to your class level. 
For a written language, the bonus applies to a Decipher Script check 
instead.

The DC for the check depends on the situation: DC 15 if the language is 
in the same group as a language you have as a Read/Write Language or 
Speak Language skill; DC 20 if the language is unrelated to any other 
languages you know; and DC 25 if the language is ancient or unique. With 
this special ability, you can glean enough meaning from a conversation 
or document to ascertain the basic message, but this ability in no way 
simulates actually being able to converse or fluently read and write in 
a given language.

A single check covers roughly one minute of a spoken language or one 
page of a written language.

### Exploit Weakness [Intelligence]

You have the brainpower to see solutions in most situations. 

__Prerequisite:__ Savant or Linguist.

__Benefit:__ After 1 round of combat, you can designate one opponent and 
try to find ways to gain an advantage by using brains over brawn. You 
use a move action and make an Intelligence check (DC 15) with a bonus 
equal to your class level. If the check succeeds, for the rest of the 
combat you use your Intelligence bonus instead of either Strength or 
Dexterity bonus on attack rolls as you find ways to outthink your 
opponent and notice weaknesses in their fighting style.

### Plan [Intelligence]

You have the brainpower to see solutions in most situations. 

__Prerequisite:__ Savant or Linguist.

__Benefit:__ Prior to an encounter you can develop a plan of action to handle 
the situation. Using this talent requires preparation; you can’t use 
this talent when surprised or otherwise unprepared for a particular 
situation. Creating a plan requires 1 minute.

After creating the plan you make an Intelligence check (DC 10) with a 
bonus equal to your class level. The result of the check provides you 
and allies with a circumstance bonus. You can’t take 10 or 20 when 
making this check.

### Empathy [Wisdom]

You innate talents give you a great capacity for empathy.

__Benefit:__ You have a knack for being sensitive to the feelings and 
thoughts of others without having those feelings and thoughts 
communicated in any objectively explicit manner. This innate talent 
provides a bonus on checks involving interaction skills (Bluff, 
Diplomacy, Handle Animal, Intimidate, Perform, and Sense Motive), 
provided you spend at least 1 minute observing your target prior to 
making the skill check. The bonus is equal to your class level.

### Improved Aid Another [Wisdom]

You innate talents give you a great capacity for empathy.

__Prerequisite:__ Empathy.

__Benefit:__ Your bonus on attempts to aid another increases 
by +1 on a successful aid another check. This ability can be selected 
multiple times, each time increasing the bonus by +1.

### Intuition [Wisdom]

You innate talents give you a great capacity for empathy.

__Prerequisite:__ Empathy.

__Benefit:__ You have an innate ability to sense trouble in the air. You 
can make a Will saving throw (DC 15). On a successful save, You gets a 
hunch that everything is all right, or you get a bad feeling about a 
specific situation, based on the GM’s best guess relating to the 
circumstances. This ability is usable a number of times per day equal to 
your class level.

### Healing Knack [Wisdom]

You have a talent for healing.

__Benefit:__ You have a knack for the healing arts. You receive a +2 
bonus on all Heal skill checks.

### Healing Touch [Wisdom]

You have a talent for healing.

__Prerequisite__: Healing Knack.

__Benefit:__ You ability to restore damage with a medical kit or 
perform surgery with a surgery kit increases by +2 hit points. 

### Improved Healing Touch [Wisdom]

You have a talent for healing.

__Prerequisites:__ Healing Knack, Healing Touch.

__Benefit:__ Your ability to restore damage with a medical 
kit or perform surgery with a surgery kit increases by +2 hit points, 
which stacks with healing touch for a total of +4 hit points. 

### Skill Emphasis [Wisdom]

Your innate insightfulness serves you well.

__Benefit:__ You choose a single skill and receive a +3 bonus on 
all checks with that skill. This bonus does not allow you to make checks 
for a trained-only skill if you have no ranks in the skill.

_Alternate Name:_ Skill Focus.

### Aware [Wisdom]

Your innate insightfulness serves you well.

__Prerequisite:__ Skill Emphasis.

__Benefit:__ You are intuitively aware of his or her surroundings. You add 
your base Will saving throw bonus to Listen or Spot checks to avoid 
surprise.

### Faith [Wisdom]

Your innate insightfulness serves you well.

__Prerequisite:__ Skill Emphasis.

__Benefit:__ You have a great deal of faith in your self, in a higher power, 
or in both. This unswerving belief allows you to add your Wisdom 
modifier to the die roll once per day to improve the result of an attack 
roll, skill check, saving throw, or ability check.

### Cool Under Pressure [Wisdom]

Your innate insightfulness serves you well.

__Prerequisite:__ Skill Emphasis plus either Faith or Aware.

__Benefit:__ You select a number of skills equal to 3 + your 
Wisdom modifier. When making a check with one of these skills, you can 
take 10 even when distracted or under duress.

_Alternate Name:_ Skill Mastery.

### Charm [Charisma]

You have an innate talent for being charming and captivating.

__Benefit:__ You get a competence bonus on all Charisma-based skill 
checks made to influence members of your chosen gender. (Some characters 
are charming to members of the opposite gender, others to members of the 
same gender.) The bonus is equal to your class level.

You can only charm NPCs with attitudes of indifferent or better. The 
charm bonus can’t be used against characters who are unfriendly or 
hostile.

This ability can be taken more than once (for another gender).

### Favor [Charisma]

You have an innate talent for being charming and captivating.

__Prerequisite:__ Charm.

__Benefits:__ You have the ability to acquire minor aid from anyone you 
meet. By making a favor check, you can gain important information 
without going through the time and trouble of doing a lot of research. 
Favors can also be used to acquire the loan of equipment or documents, 
or to receive other minor assistance in the course of an adventure.

Once per day, to make a favor check, roll a d20 and add the character’s 
favor bonus, equal to your class level. The GM sets the DC based on the 
scope of the favor being requested. The DC ranges from 10 for a simple 
favor to as high as 30 for formidable and highly dangerous, expensive, 
or illegal favors. You can’t take 10 or 20 on this check, nor can you 
retry the check for the same (or virtually the same) favor. Favors 
should help advance the plot of an adventure. A favor that would enable 
a character to avoid an adventure altogether should always be 
unavailable to the character, regardless of the result of a favor check.

The DM should carefully monitor your use of favors to ensure that this 
ability isn’t abused. The success or failure of a mission shouldn’t 
hinge on the use of a favor, and getting a favor shouldn’t replace good 
roleplaying or the use of other skills. The DM may disallow any favor 
deemed to be disruptive to the game.

### Captivate [Charisma]

You have an innate talent for being charming and captivating.

__Prerequisites:__ Charm, Favor.

__Benefits:__ You have the ability to temporarily beguile a target 
through the use of words and bearing. The target must have an 
Intelligence score of 3 or higher to be susceptible to a captivate 
attempt, must be within 30 feet of you, must be flat-footed or not in 
combat, and must be able to see, hear, and understand you.

To captivate a target, you must use an attack action and make a Charisma 
check (DC 15), adding your class level as a bonus. If the Charisma check 
succeeds, the target can try to resist.

The target resists the captivation attempt by making a Will saving throw 
(DC 10 + your class level + your Cha bonus). If the saving throw fails, 
you become the target’s sole focus. The target pays no attention to 
anyone else for 1 round and remains flat-footed. This focusing of the 
target’s attention allows other characters to take actions of which the 
captivated target is unaware. The effect ends immediately if the target 
is attacked or threatened.

You can concentrate to keep a target captivated for additional rounds. 
You concentrate all your effort on the task, and the target gets to make 
a new Will save each round. The effect ends when you stop concentrating, 
or when the target succeeds on the save. This is a Mind-Affecting 
ability.

### Fast-Talk [Charisma]

You have an innate talent for bending the truth and dazzling others with 
a combination of words, mannerisms, and charm.

__Benefit:__ You have a way with words when attempting to con and 
deceive. With this talent, you applies your class level as a competence 
bonus on any Bluff, Diplomacy, or Gamble checks you make while 
attempting to lie, cheat, or otherwise bend the truth.

### Dazzle [Charisma]

You have an innate talent for bending the truth and dazzling others with 
a combination of words, mannerisms, and charm.

__Prerequisite:__ Fast-talk.

__Benefit:__ You have the ability to dazzle a target through sheer force 
of personality, a winning smile, and fast-talking. The target must have 
an Intelligence score of 3 or higher to be susceptible to a dazzle 
attempt, must be within 30 feet of you, and must be able to see, hear, 
and understand you.

To dazzle a target, you must use an attack action and make a Charisma 
check (DC 15), adding your class level as a bonus. If the Charisma check 
succeeds, the target can try to resist.

The target resists the dazzle attempt by making a Will saving throw (DC 
10 + your class level + your Cha bonus). If the save fails, the target 
receives a –1 penalty on attack rolls, ability checks, skill checks, and 
saving throws for a number of rounds equal to you class level.

This talent can be selected multiple times, each time worsening the 
dazzled penalty by –1.  This is a Mind-Affecting ability. 

### Taunt [Charisma]

You have an innate talent for bending the truth and dazzling others with 
a combination of words, mannerisms, and charm.

__Prerequisites:__ Fast-Talk, Dazzle.

__Benefit:__ You have the ability to temporarily rattle a target through 
the use of insults and goading. The target must have an Intelligence 
score of 3 or higher to be susceptible to a taunt, must be within 30 
feet of you, and must be able to hear and understand you.

To taunt a target, you must use an attack action and make a Charisma 
check (DC 15), adding your class level as a bonus. If the Charisma check 
succeeds, the target can try to resist.

The target resists the taunt by making a Will saving throw (DC 10 + your 
class level + your Cha bonus). If the save fails, the target becomes 
dazed (unable to act, but can defend normally) for 1 round.

A taunt can be played on an opponent any number of times.  This is a 
Mind-Affecting ability.

### Coordinate [Charisma]

You have a talent for leadership.

__Benefit:__ You have a knack for getting people to work together. When 
you can spend a full round directing your allies and make a Charisma 
check (DC 10), you provides any of your allies within 30 feet a +1 bonus 
on their attack rolls and skill checks. The bonus lasts for a number of 
rounds equal to your Charisma modifier.

You can coordinate a number of allies equal to one-half your class 
level, rounded down (to a minimum of one ally).

### Inspiration [Charisma]

You have a talent for inspiring morale and competence in your allies.

__Prerequisite:__ Coordinate.

__Benefit:__ You can inspire your allies, bolstering them and improving 
their chances of success. An ally must listen to and observe you for a 
full round for the inspiration to take hold, and you must make a 
Charisma check (DC 10). The effect lasts for a number of rounds equal to 
your Charisma modifier.

An inspired ally gains a +2 morale bonus on saving throws, attack rolls, 
and damage rolls.

You can't inspire yourself. You can inspire a number of allies equal to 
one-half your class level, rounded down (to a minimum of one ally).

### Greater Inspiration [Charisma]

You are an expert at inspiring morale and competence in your allies.

__Prerequisites:__ Coordinate, Inspiration.

__Benefit:__ You can inspire your allies to even greater heights, 
bolstering them and improving their chances of success. An ally must 
listen to and observe you for a full round for the greater inspiration 
to take hold, and you must make a Charisma check (DC 10). The effect 
lasts for a number of rounds equal to your Charisma modifier.

An inspired ally gains an additional +1 morale bonus on saving throws, 
attack rolls, and damage rolls, which stacks with the bonus from 
inspiration for a total of a +3 morale bonus.

You can't inspire yourself. You can inspire a number of allies equal to 
one-half your class level, rounded down (to a minimum of one ally).
