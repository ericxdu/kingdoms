Planetary Romance Sourcebook
============================

Creatures and characters for the Uncommon Era campaign setting.

<table>
  <caption>Table: Creature Attributes</caption>
  <tr>
    <th>Creature</th>
    <th>Size, Type (Subtype)</th>
    <th>Hit Dice</th>
    <th>Challenge Rating</th>
    <th>Alignment</th>
    <th>Level Adjustment</th>
  </tr>
  <tr>
    <td>Formian Worker</td>
    <td>Small Outsider (Lawful, Extraplanar)</td>
    <td>1</td>
    <td>1&frasl;2</td>
    <td>Always lawful neutral</td>
    <td>&mdash;</td>
  </tr>
  <tr>
    <td>Formian Warrior</td>
    <td>Medium Outsider (Lawful, Extraplanar)</td>
    <td>4</td>
    <td>3</td>
    <td>Always lawful neutral</td>
    <td>&mdash;</td>
  </tr>
  <tr>
    <td>Formian Taskmaster</td>
    <td>Medium Outsider (Lawful, Extraplanar)</td>
    <td>6</td>
    <td>7</td>
    <td>Always lawful neutral</td>
    <td>&mdash;</td>
  </tr>
  <tr>
    <td>Formian Myrmarch</td>
    <td>Large Outsider (Lawful, Extraplanar)</td>
    <td>12</td>
    <td>10</td>
    <td>Always lawful neutral</td>
    <td>&mdash;</td>
  </tr>
  <tr>
    <td>Formian Queen</td>
    <td>Large Outsider (Lawful, Extraplanar)</td>
    <td>20</td>
    <td>17</td>
    <td>Always lawful neutral</td>
    <td>&mdash;</td>
  </tr>
  <tr>
    <td>Janni</td>
    <td>Medium Outsider (native)</td>
    <td>6</td>
    <td>4</td>
    <td>Usually neutral</td>
    <td>+5</td>
  </tr>
  <tr>
    <td>Lillend</td>
    <td>Large Outsider (Chaotic, Extraplanar, Good)</td>
    <td>7</td>
    <td>7</td>
    <td>Always chaotic good</td>
    <td>+6</td>
  </tr>
  <tr>
    <td>Rakshasa</td>
    <td>Medium Outsider (Native)</td>
    <td>7</td>
    <td>10</td>
    <td>Always lawful evil</td>
    <td>+7</td>
  </tr>
  <tr>
    <td>Triton</td>
    <td>Medium Outsider (Native, Water)</td>
    <td>3</td>
    <td>2</td>
    <td>Usually neutral good</td>
    <td>+2</td>
  </tr>
  <tr>
    <td>Xill</td>
    <td>Medium Outsider (Extraplanar)</td>
    <td>5</td>
    <td>6</td>
    <td>Always lawful evil</td>
    <td>+4</td>
  </tr>
</table>


Ethereal Marauder
-----------------

- **Medium Magical Beast** (Extraplanar)
- **Hit Dice:** 2d10 (11 hp)
- **Initiative:** +5
- **Speed:** 40 ft. (8 squares)
- **Armor Class:** 14 (+1 Dex, +3 natural), touch 11, flat-footed 13
- **Base Attack/Grapple:** +2/+4
- **Attack:** Bite +4 melee (1d6+3)
- **Full Attack:** Bite +4 melee (1d6+3)
- **Space/Reach:** 5 ft./5 ft.
- **Special Attacks:** —
- **Special Qualities:** Darkvision 60 ft., ethereal jaunt
- **Saves:** Fort +3, Ref +4, Will +1
- **Abilities:** Str 14, Dex 12, Con 11, Int 7, Wis 12, Cha 10
- **Skills:** Listen +5, Move Silently +5, Spot +4
- **Feats:** Improved Initiative
- **Environment:** Ethereal Plane
- **Organization:** Solitary
- **Challenge Rating:** 3
- **Treasure:** None
- **Alignment:** Always neutral
- **Advancement:** 3–4 HD (Medium); 5–6 HD (Large)
- **Level Adjustment:** —

Ethereal marauders live and hunt on the Ethereal Plane. Ethereal marauders’ coloration ranges from bright blue to deep violet. An ethereal marauder stands about 4 feet tall, but its overall length is about 7 feet. It weighs about 200 pounds.

Ethereal marauders speak no known languages. Survivors of their attacks on the Material Plane claim that they emit an eerie, high whine that varies in pitch depending on the creature’s speed and health.

### Combat

Once a marauder locates prey, it shifts to the Material Plane to attack, attempting to catch its victim flat-footed. The creature bites its victim, then retreats quickly back to the Ethereal Plane. When badly hurt or wounded, a marauder escapes to its home plane rather than continuing the fight.

__Ethereal Jaunt (Su):__ An ethereal marauder can shift from the Ethereal Plane to the Material Plane as a free action, and shift back again as a move action. The ability is otherwise identical with the *ethereal jaunt* spell (caster level 15th).

__Skills:__ Ethereal marauders have a +2 racial bonus on Listen, Move Silently, and Spot checks.


Formian
-------

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>
	
<table>
  <tr>
    <th></th>
    <th>Formian Worker</th>
    <th>Formian Warrior</th>
    <th>Formian Taskmaster</th>
  </tr>
  <tr>
    <th></th>
    <th>Small Outsider (Lawful, Extraplanar)</th>
    <th>Medium Outsider (Lawful, Extraplanar)</th>
    <th>Medium Outsider (Lawful, Extraplanar)</th>
  </tr>
  <tr>
    <th>Hit Dice:</th>
    <td>1d8+1 (5 hp)</td>
    <td>4d8+8 (26 hp)</td>
    <td>6d8+12 (39 hp)</td>
  </tr>
  <tr>
    <th>Initiative:</th>
    <td>+2</td>
    <td>+3</td>
    <td>+7</td>
  </tr>
  <tr>
    <th>Speed:</th>
    <td>40 ft. (8 squares)</td>
    <td>40 ft. (8 squares)</td>
    <td>40 ft. (8 squares)</td>
  </tr>
  <tr>
    <th>Armor Class:</th>
    <td>17 (+1 size, +2 Dex, +4 natural), touch 13, flat-footed 15</td>
    <td>18 (+3 Dex, +5 natural), touch 13, flat-footed 15</td>
    <td>19 (+3 Dex, +6 natural), touch 13, flat-footed 16</td>
  </tr>
  <tr>
    <th>Base Attack/Grapple:</th>
    <td>+1/–2</td>
    <td>+4/+7</td>
    <td>+6/+10</td>
  </tr>
  <tr>
    <th>Attack:</th>
    <td>Bite +3 melee (1d4+1)</td>
    <td>Sting +7 melee (2d4+3 plus poison)</td>
    <td>Sting +10 melee (2d4+4 plus poison)</td>
  </tr>
  <tr>
    <th>Full Attack:</th>
    <td>Bite +3 melee (1d4+1)</td>
    <td>Sting +7 melee (2d4+3 plus poison) and 2 claws +5 melee (1d6+1) and bite +5 melee (1d4+1)</td>
    <td>Sting +10 melee (2d4+4 plus poison) and 2 claws +8 melee (1d6+2)</td>
  </tr>
  <tr>
    <th>Space/Reach:</th>
    <td>5 ft./5 ft.</td>
    <td>5 ft./5 ft.</td>
    <td>5 ft./5 ft.</td>
  </tr>
  <tr>
    <th>Special Attacks:</th>
    <td>&mdash;</td>
    <td>Poison</td>
    <td>Dominate monster, dominated creature, poison</td>
  </tr>
  <tr>
    <th>Special Qualities:</th>
    <td>Cure serious wounds, hive mind, immunity to poison, petrification, and cold, make whole, resistance to electricity 10, fire 10, and sonic 10</td>
    <td>Hive mind, immunity to poison, petrification, and cold, resistance to electricity 10, fire 10, and sonic 10, spell resistance 18</td>
    <td>Hive mind, immunity to poison, petrification, and cold, resistance to electricity 10, fire 10, and sonic 10, spell resistance 21, telepathy 100 ft.</td>
  </tr>
  <tr>
    <th>Saves:</th>
    <td>Fort +3, Ref +4, Will +2</td>
    <td>Fort +6, Ref +7, Will +5</td>
    <td>Fort +7, Ref +8, Will +8</td>
  </tr>
  <tr>
    <th>Abilities:</th>
    <td>Str 13, Dex 14, Con 13, Int 6, Wis 10, Cha 9</td>
    <td>Str 17, Dex 16, Con 14, Int 10, Wis 12, Cha 11</td>
    <td>Str 18, Dex 16, Con 14, Int 11, Wis 16, Cha 19</td>
  </tr>
  <tr>
    <th>Skills:</th>
    <td>Climb +10, Craft (any one) +5, Hide +6, Listen +4, Search +2, Spot +4</td>
    <td>Climb +10, Hide +10, Jump +14, Listen +8, Move Silently +10, (+3 following tracks), Tumble +12</td>
    <td>Climb +13, Diplomacy +6, Hide +12, Intimidate +13, Listen +12, Search +7, Spot +8, Survival +1 Move Silently +12, Search +9, Sense Motive +12, Spot +12, Survival +3 (+5 following tracks)</td>
  </tr>
  <tr>
    <th>Feats:</th>
    <td>Skill Focus (Craft [selected skill])</td>
    <td>Dodge, Multiattack</td>
    <td>Dodge, Improved Initiative, Multiattack</td>
  </tr>
  <tr>
    <th>Environment:</th>
    <td>A lawful-aligned plane</td>
    <td>A lawful-aligned plane</td>
    <td>A lawful-aligned plane</td>
  </tr>
  <tr>
    <th>Organization:</th>
    <td>Team (2–4) or crew (7–18)</td>
    <td>Solitary, team (2–4), or troop (6–11)</td>
    <td>Solitary (1 plus 1 dominated creature) or conscription team (2–4 plus 1 dominated creature per team member)</td>
  </tr>
  <tr>
    <th>Challenge Rating:</th>
    <td>1/2</td>
    <td>3</td>
    <td>7</td>
  </tr>
  <tr>
    <th>Treasure:</th>
    <td>None</td>
    <td>None</td>
    <td>Standard</td>
  </tr>
  <tr>
    <th>Alignment:</th>
    <td>Always lawful neutral</td>
    <td>Always lawful neutral</td>
    <td>Always lawful neutral</td>
  </tr>
  <tr>
    <th>Advancement:</th>
    <td>2–3 HD (Medium)</td>
    <td>5–8 HD (Medium); 9–12 HD (Large)</td>
    <td>7–9 HD (Medium); 10–12 HD (Large)</td>
  </tr>
  <tr>
    <th>Level Adjustment:</th>
    <td>&mdash;</td>
    <td>&mdash;</td>
    <td>&mdash;</td>
  </tr>
</table>

<table>
  <tr>
    <th></th>
    <th>Formian Myrmarch</th>
    <th>Formian Queen</th>
    </tr>
  <tr>
    <th></th>
    <th>Large Outsider (Lawful, Extraplanar)</th>
    <th>Large Outsider (Lawful, Extraplanar)</th>
  </tr>
  <tr>
    <th>Hit Dice:</th>
    <td>12d8+48 (102 hp)</td>
    <td>20d8+100 (190 hp)</td>
  </tr>
  <tr>
    <th>Initiative:</th>
    <td>+8</td>
    <td>–5</td>
  </tr>
  <tr>
    <th>Speed:</th>
    <td>50 ft. (10 squares)</td>
    <td>0 ft.</td>
  </tr>
  <tr>
    <th>Armor Class:</th>
    <td>28 (–1 size, +4 Dex, +15 natural), touch 13, flat-footed 24</td>
    <td>23 (–1 size, +14 natural), touch 9, flat-footed 23</td>
  </tr>
  <tr>
    <th>Base Attack/Grapple:</th>
    <td>+12/+20</td>
    <td>+20/+24</td>
  </tr>
  <tr>
    <th>Attack:</th>
    <td>Sting +15 melee (2d4+4 plus poison) or javelin +15 ranged (1d6+4)</td>
    <td>&mdash;</td>
  </tr>
  <tr>
    <th>Full Attack:</th>
    <td>Sting +15 melee (2d4+4 plus poison) and bite +13 melee (2d6+2); or javelin +15/+10 ranged (1d6+4)</td>
    <td>&mdash;</td>
  </tr>
  <tr>
    <th>Space/Reach:</th>
    <td>10 ft./5 ft.</td>
    <td>10 ft./5 ft.</td>
  </tr>
  <tr>
    <th>Special Attacks:</th>
    <td>Poison, spell-like abilities</td>
    <td>Spell-like abilities, spells</td>
  </tr>
  <tr>
    <th>Special Qualities:</th>
    <td>Fast healing 2, hive mind, immunity to poison, petrification, and cold, resistance to electricity 10, fire 10, and sonic 10, spell resistance 25</td>
    <td>Fast healing 2, hive mind, immunity to poison, petrification, and cold, resistance to electricity 10, fire 10, and sonic 10, spell resistance 30, telepathy</td>
  </tr>
  <tr>
    <th>Saves:</th>
    <td>Fort +12, Ref +12, Will +11</td>
    <td>Fort +19, Ref &mdash;, Will +19</td>
  </tr>
  <tr>
    <th>Abilities:</th>
    <td>Str 19, Dex 18, Con 18, Int 16, Wis 16, Cha 17</td>
    <td>Str &mdash;, Dex &mdash;, Con 20, Int 20, Wis 20, Cha 21</td>
  </tr>
  <tr>
    <th>Skills:</th>
    <td>Climb +19, Concentration +18, Diplomacy +20, Hide +15, Knowledge (any one) +18, Listen +18, Move Silently +19, Search +18, Sense Motive +18, Spot +18, Survival +3 (+5 following tracks)</td>
    <td>Appraise +28, Bluff +28, Concentration +28, Diplomacy +32, Disguise +5 (+7 acting), Intimidate +30, Knowledge (any three) +28, Listen +30, Sense Motive +28, Spellcraft +28 (+30 scrolls), Spot +30, Use Magic Device +28 (+30 scrolls)</td>
  </tr>
  <tr>
    <th>Feats:</th>
    <td>Dodge, Improved Initiative, Mobility, Multiattack, Spring Attack</td>
    <td>Alertness, Eschew MaterialsB, Great Fortitude, Improved Counterspell, Iron Will, item creation feat (any one), Maximize Spell, Spell Focus (enchantment)</td>
  </tr>
  <tr>
    <th>Environment:</th>
    <td>A lawful-aligned plane</td>
    <td>A lawful-aligned plane</td>
  </tr>
  <tr>
    <th>Organization:</th>
    <td>Solitary, team (2–4), or platoon (1 plus 7–18 workers and 6–11 warriors)</td>
    <td>Hive (1 plus 100–400 workers, 11–40 warriors, 4–7 taskmasters with 1 dominated creature each, and 5–8 myrmarchs)</td>
  </tr>
  <tr>
    <th>Challenge Rating:</th>
    <td>10</td>
    <td>17</td>
  </tr>
  <tr>
    <th>Treasure:</th>
    <td>Standard</td>
    <td>Double standard</td>
  </tr>
  <tr>
    <th>Alignment:</th>
    <td>Always lawful neutral</td>
    <td>Always lawful neutral</td>
  </tr>
  <tr>
    <th>Advancement:</th>
    <td>13–18 HD (Large); 19–24 HD (Huge)</td>
    <td>21–30 HD (Huge); 31–40 HD (Gargantuan)</td>
  </tr>
  <tr>
    <th>Level Adjustment:</th>
    <td>&mdash;</td>
    <td>&mdash;</td>
  </tr>
</table>

A formian resembles a cross between an ant and a centaur. All formians are covered in a brownish-red carapace; size and appearance differs for 
each variety.

### Combat

Formians are generally aggressive, seeking to subdue all they encounter. If they perceive even the slightest threat to their hive-city or to 
their queen, they attack immediately and fight to the death. Any formian also attacks immediately if ordered to do so by a superior.

**Hive Mind (Ex)**: All formians within 50 miles of their queen are in constant communication. If one is aware of a particular danger, they 
all are. If one in a group is not flatfooted, none of them are. No formian in a group is considered flanked unless all of them are.

### Worker

While workers cannot speak, they can convey simple concepts (such as danger) by body movements. Through the hive mind, however, they can 
communicate just fine&mdash;although their intelligence still limits the concepts that they can grasp.

A worker is about 3 feet long and about 2-1/2 feet high at the front. It weighs about 60 pounds. Its hands are suitable only for manual labor.

#### Combat

Formian workers fight only to defend their hive-cities, using their mandibled bite.

A formian worker’s natural weapons, as well as any weapons it wields, are treated as lawful-aligned for the purpose of overcoming damage 
reduction.

**_Cure Serious Wounds_ (Sp)**: Eight workers together can heal a creature’s wounds as though using the _cure serious wounds_ spell (caster level 
7th). This is a full-round action for all eight workers.

**_Make Whole_ (Sp)**: Three workers together can repair an object as though using the _make whole_ spell (caster level 7th). This is a fullround 
action for all three workers.

### Warrior

Warriors communicate through the hive mind to convey battle plans and make reports to their commanders. They cannot speak otherwise.

A warrior is about is about 5 feet long and about 4-1/2 feet high at the front. It weighs about 180 pounds.

#### Combat

Warriors are wicked combatants, using claws, bite, and a poisonous sting all at once. Through the hive mind, they attack with coordinated and 
extremely efficient tactics.

A formian warrior’s natural weapons, as well as any weapons it wields, are treated as lawful-aligned for the purpose of overcoming damage 
reduction.

**Poison (Ex)**: Injury, Fortitude DC 14, initial and secondary damage 1d6 Str. The save DC is Constitution-based.

### Taskmaster

These formians communicate only telepathically and derive sustenance from the mental energies of those they dominate.

A taskmaster is about the same size as a warrior.

#### Combat

Taskmasters rely on their dominated slaves to fight for them if at all possible. If necessary, though, they can defend themselves with claws 
and a poison sting.

A formian taskmaster’s natural weapons, as well as any weapons it wields, are treated as lawful-aligned for the purpose of overcoming damage 
reduction.

**Dominate Monster (Su)**: A taskmaster can use a _dominate monster_ ability as the spell from a 10th-level caster (Will DC 17 negates), 
although the subject may be of any kind and may be up to Large size. Creatures that successfully save cannot be affected by the same 
taskmaster’s dominate monster ability for 24 hours. A single taskmaster can dominate up to four subjects at a time. The save DC is 
Charisma-based.

**Dominated Creature (Ex)**: A taskmaster is never encountered alone. One dominated nonformian creature always accompanies it (choose or 
determine randomly any creature of CR 4).

**Poison (Ex)**: Injury, Fortitude DC 15, initial and secondary damage 1d6 Str. The save DC is Constitution-based.

### Myrmarch

Myrmarchs are the elite of formian society. Much more than those beneath them, these creatures are individuals, with goals, desires, and 
creative thought.

A myrmarch is about is about 7 feet long and about 5-1/2 feet high at the front. It weighs about 1,500 pounds. Its claws are capable of fine 
manipulation, like human hands. Each myrmarch wears a bronze helm to signify its position (the more elaborate the helm, the more prestigious 
the position).

Myrmarchs speak Formian and Common.

#### Combat

Myrmarchs’ claws are like hands and thus serve no combat purpose.

Myrmarchs occasionally employ javelins for ranged attacks, coated with poison from their own stingers.

They fight intelligently, aiding those under them (if any such are present) and commanding them through the hive mind. If chaotic creatures 
are present, however, a myrmarch is singleminded in its quest to destroy them.

A formian myrmarch’s natural weapons, as well as any weapons it wields, are treated as lawful-aligned for the purpose of overcoming damage 
reduction.

**Poison (Ex)**: Injury, Fortitude DC 20, initial and secondary damage 2d6 Dex. The save DC is Constitution-based.

**Spell-Like Abilities**: At will&mdash;_charm monster_ (DC 17), _clairaudience/ clairvoyance_, _detect chaos_, _detect thoughts_ (DC 15),
_magic circle against chaos_, _greater teleport_; 1/day&mdash;_dictum_ (DC 20), _order’s wrath_ (DC 17). Caster level 12th. The save DCs are 
Charisma-based.

### Queen

The formian queen cannot move. With her telepathic abilities, though, she can send instructions to and get reports from any formian within her 
range.

She is about 10 feet long, perhaps 4 feet high, and weighs about 3,500 pounds.

The queen speaks Formian and Common, although she can communicate with any creature telepathically.

#### Combat

The queen does not fight. She has no ability to move. If necessary, a team of workers and myrmarchs (or dominated slaves) haul her enormous 
bulk to where she needs to go. This sort of occurrence is very rare, however, and most of the time the queen remains within her well-defended 
chambers.

Despite her utter lack of physical activity, the queen can cast spells and use spell-like abilities to great effect in her own defense as well 
as the defense of the hive-city.

**Spells**: The queen casts arcane spells as a 17th-level sorcerer.

_Typical Sorcerer Spells Known_ (6/8/7/7/7/7/6/6/4, base save DC 15 + spell level): 0&mdash;_acid splash_, _arcane mark_, _daze_, _detect magic_, 
_light_, _mage hand_, _read magic_, _resistance_, _touch of fatigue_; 1st&mdash;_comprehend languages_, _identify_, _mage armor_, _magic missile_, 
_shield_; 2nd&mdash; _hypnotic pattern_, _invisibility_, _protection from arrows_, _resist energy_, _scorching ray_; 3rd&mdash;_dispel magic_, _heroism_, 
_nondetection_, _slow_; 4th&mdash; _confusion_, _detect scrying_, _black tentacles_, _scrying_; 5th&mdash;_cone of cold_, _dismissal_, _teleport_,
_wall of force_; 6th&mdash;_analyze dweomer_, _geas/quest_, _repulsion_; 7th&mdash;_summon monster VII_, _vision_, _waves of exhaustion_;
8th&mdash;_prismatic wall_, _temporal stasis_.

**Spell-Like Abilities**: At will&mdash;_calm emotions_ (DC 17), _charm monster_ (DC 19), _clairaudience/clairvoyance_, _detect chaos_,
_detect thoughts_, _dictum_ (DC 22), _divination_, _hold monster_ (DC 20), _magic circle against chaos_, _order’s wrath_ (DC 19),
_shield of law_ (DC 23), _true seeing_. Caster level 17th. The save DCs are Charisma-based.

**Telepathy (Su)**: The queen can communicate telepathically with any intelligent creature within 50 miles whose presence she is aware of.


Janni
-----

- **Medium Outsider (Native)

- **Hit Dice:** 6d8+6 (33 hp)

- **Initiative:** +6

- **Speed:** 20 ft. (4 squares), fly 15 ft. (perfect) in chainmail; base land speed 30 ft., base fly speed 20 ft. (perfect)

- **Armor Class:** 18 (+2 Dex, +1 natural, +5 chainmail), touch 12, flat-footed 16

- **Base Attack/Grapple:** +6/+9

- **Attack:** Scimitar +9 melee (1d6+4/18–20) or longbow +8 ranged (1d8/x3)

- **Full Attack:** Scimitar +9/+4 melee (1d6+4/18–20) or longbow +8/+3 ranged (1d8/x3)

- **Space/Reach:** 5 ft./5 ft.

- **Special Attacks:** Change size, spell-like abilities

- **Special Qualities:** Darkvision 60 ft., elemental endurance, plane shift, resistance to fire 10, telepathy 100 ft.

- **Saves:** Fort +6, Ref +7, Will +7

- **Abilities:** Str 16, Dex 15, Con 12, Int 14, Wis 15, Cha 13

- **Skills:** Appraise +11, Concentration +10, Craft (any two) +11, Diplomacy +3, Escape Artist +6, Listen +11, Move Silently +6, Ride +11, Sense Motive +11, Spot +11, Use Rope +2 (+4 with bindings)

- **Feats:** Combat Reflexes, Dodge, Improved InitiativeB, Mobility

- **Environment:** Warm deserts

- **Organization:** Solitary, company (2–4), or band (6–15)

- **Challenge Rating:** 4

- **Treasure:** Standard

- **Alignment:** Usually neutral

- **Advancement:** 7–9 HD (Medium); 10–18 HD (Large)

- **Level Adjustment:** +5

The jann (singular janni) are the weakest of the genies. Jann are formed out of all four elements and must therefore spend most of their time on the Material Plane.

Jann speak Common, one elemental language (Aquan, Auran, Ignan, or Terran) and one alignment language (Abyssal, Celestial, or Infernal).

### Combat

Jann are physically strong and courageous, and do not take kindly to insult or injury. If they meet a foe they cannot defeat in a standup fight, they use flight and invisibility to regroup and maneuver to a more advantageous position.

Change Size (Sp): Twice per day, a janni can magically change a creature’s size. This works just like an enlarge person or reduce person spell (the janni chooses when using the ability), except that the ability can work on the janni. A DC 13 Fortitude save negates the effect. The save DC is Charisma-based. This is the equivalent of a 2nd-level spell.

Spell-Like Abilities: 3/day&mdash;invisibility (self only), speak with animals. Caster level 12th. Once per day a janni can create food and water (caster level 7th) and can use ethereal jaunt (caster level 12th) for 1 hour. The save DCs are Charisma-based.

Elemental Endurance (Ex): Jann can survive on the Elemental Planes of Air, Earth, Fire, or Water for up to 48 hours. Failure to return to the Material Plane before that time expires causes a janni to take 1 point of damage per additional hour spent on the elemental plane, until it dies or returns to the Material Plane.
Jann as Characters

Janni characters possess the following racial traits.

&mdash; +6 Strength, +4 Dexterity, +2 Constitution, +4 Intelligence, +4 Wisdom, +2 Charisma.

&mdash;Medium size.

&mdash;A janni’s base land speed is 30 feet. It also has a fly speed of 20 feet (perfect).

&mdash;Darkvision out to 60 feet.

&mdash;Racial Hit Dice: A janni begins with six levels of outsider, which provide 6d8 Hit Dice, a base attack bonus of +6, and base saving throw bonuses of Fort +5, Ref +5, and Will +5.

&mdash;Racial Skills: A janni’s outsider levels give it skill points equal to 9 x (8 + Int modifier). Its class skills are Appraise, Concentration, Craft (any), Escape Artist, Listen, Move Silently, Ride, Sense Motive, and Spot.

&mdash;Racial Feats: A janni’s outsider levels give it three feats. A janni receives Improved Initiative as a bonus feat.

&mdash; +1 natural armor bonus.

&mdash;Special Attacks (see above): Change size, spell-like abilities.

&mdash;Special Qualities (see above): Elemental endurance, plane shift, resistance to fire 10, telepathy. 100 ft.

&mdash;Automatic Languages: Common. Bonus Languages: Abyssal, Aquan, Auran, Celestial, Ignan, Infernal, Terran.

&mdash;Favored Class: Rogue.

&mdash;Level adjustment +5.


Lillend
-------

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Large Outsider (Chaotic, Extraplanar, Good)**
- **Hit Dice:** 7d8+14 (45 hp)
- **Initiative:** +3
- **Speed:** 20 ft. (4 squares), fly 70 ft. (average)
- **Armor Class:** 17 (–1 size, +3 Dex, +5 natural), touch 12, flat-footed 14
- **Base Attack/Grapple:** +7/+16
- **Attack:** Short sword +11 melee (1d8+5/19–20)
- **Full Attack:** Short sword +11/+6 melee (1d8+5/19–20) and tail slap +6 melee (2d6+2)
- **Space/Reach:** 10 ft./10 ft.
- **Special Attacks:** Constrict 2d6+5, improved grab, spells, spell-like abilities
- **Special Qualities:** Darkvision 60 ft., immunity to poison, resistance to fire 10
- **Saves:** Fort +7, Ref +10, Will +8
- **Abilities:** Str 20, Dex 17, Con 15, Int 14, Wis 16, Cha 18
- **Skills:** Appraise +12, Concentration +12, Diplomacy +16, Knowledge (arcana) +12, Listen +13, Perform (any one) +14, Sense Motive +13, Spellcraft +14, Spot +13, Survival +17
- **Feats:** Combat Casting, Extend Spell, Lightning Reflexes
- **Environment:** A chaos-aligned plane
- **Organization:** Solitary or covey (2–4)
- **Challenge Rating:** 7
- **Treasure:** Standard
- **Alignment:** Always chaotic good
- **Advancement:** 8–10 HD (Large); 11–21 HD (Huge)
- **Level Adjustment:** +6

A typical lillend’s coils are 20 feet long. The creature weighs about 3,800 pounds. A few lillends have male torsos.

Lillends speak Celestial, Infernal, Abyssal, and Common.

### Combat

Lillends are generally peaceful unless they intend vengeance against someone they believe guilty of harming, or even threatening, a favored art form, artwork, or artist. Then they become implacable foes. They use their spells and spell-like abilities to confuse and weaken opponents before entering combat. A covey of lillends usually discusses strategy before a battle.

A lillend’s natural weapons, as well as any weapons it wields, are treated as chaotic-aligned and good-aligned for the purpose of overcoming damage reduction.

**Constrict (Ex):** A lillend deals 2d6+5 points of damage with a successful grapple check. Constricting uses the entire lower portion of its body, so it cannot take any move actions when constricting, though it can still attack with its sword.

**Improved Grab (Ex):** To use this ability, a lillend must hit with its tail slap attack. It can then attempt to start a grapple as a free action without provoking an attack of opportunity. If it wins the grapple check, it establishes a hold and can constrict.

Spells: A lillend casts arcane spells as a 6th-level bard.

Typical Bard Spells Known (3/4/3; save DC 14 + spell level): 0&mdash; dancing lights, daze, detect magic, lullaby, mage hand, read magic; 1st&mdash; charm person, cure light wounds, identify, sleep; 2nd&mdash;hold person, invisibility, sound burst.

Spell-Like Abilities: 3/day&mdash;darkness, hallucinatory terrain (DC 18), knock, light; 1/day&mdash;charm person (DC 15), speak with animals, speak with plants. Caster level 10th. The save DCs are Charisma-based.

A lillend also has the bardic music ability as a 6th-level bard.

Skills: Lillends have a +4 racial bonus on Survival checks.


Rakshasa
--------

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>
	
- **Medium Outsider (Native)**
- **Hit Dice:** 7d8+21 (52 hp)
- **Initiative:** +2
- **Speed:** 40 ft. (8 squares)
- **Armor Class:** 21 (+2 Dex, +9 natural), touch 12, flat-footed 19
- **Base Attack/Grapple:** +7/+8
- **Attack:** Claw +8 melee (1d4+1)
- **Full Attack:** 2 claws +8 melee (1d4+1) and bite +3 melee (1d6)
- **Space/Reach:** 5 ft./5 ft.
- **Special Attacks:** Detect thoughts, spells
- **Special Qualities:** Change shape, damage reduction 15/good and piercing, darkvision 60 ft., spell resistance 27
- **Saves:** Fort +8, Ref +7, Will +6
- **Abilities:** Str 12, Dex 14, Con 16, Int 13, Wis 13, Cha 17
- **Skills:** Bluff +17*, Concentration +13, Diplomacy +7, Disguise +17 (+19 acting)&ast;, Intimidate +5, Listen +13, Move Silently +13, Perform (oratory) +13, Sense Motive +11, Spellcraft +11, Spot +11
- **Feats:** Alertness, Combat Casting, Dodge
- **Environment:** Warm marshes
- **Organization:** Solitary
- **Challenge Rating:** 10
- **Treasure:** Standard coins; double goods; standard items
- **Alignment:** Always lawful evil
- **Advancement:** By character class
- **Level Adjustment:** +7

A closer look at a rakshasa reveals that the palms of its hands are where the backs of the hands would be on a human.

A rakshasa is about the same height and weight as a human.

Rakshasas speak Common, Infernal, and Undercommon.

### Combat

In close combat, which a rakshasa disdains as ignoble, it employs its sharp claws and powerful bite. Whenever possible, it uses its other abilities to make such encounters unnecessary.

__Detect Thoughts (Su):__ A rakshasa can continuously use detect thoughts as the spell (caster level 18th; Will DC 15 negates). It can suppress or resume this ability as a free action. The save DC is Charisma-based.

__Spells:__ A rakshasa casts spells as a 7th-level sorcerer.

_Typical Sorcerer Spells Known (6/7/7/5; save DC 13 + spell level):_ 0&mdash;*detect magic*, *light*, *mage hand*, *message*, *read magic*, *resistance*, *touch of fatigue*; 1st&mdash;*charm person*, *mage armor*, *magic missile*, *shield*, *silent image*; 2nd&mdash;*bear’s endurance*, *invisibility*, *acid arrow*; 3rd&mdash;*haste*, *suggestion*.

__Change Shape (Su):__ A rakshasa can assume any humanoid form, or revert to its own form, as a standard action. In humanoid form, a rakshasa loses its claw and bite attacks (although it often equips itself with weapons and armor instead). A rakshasa remains in one form until it chooses to assume a new one. A change in form cannot be dispelled, but the rakshasa reverts to its natural form when killed. A true seeing spell reveals its natural form.

__Skills:__ A rakshasa has a +4 racial bonus on Bluff and Disguise checks. *When using change shape, a rakshasa gains an additional +10 circumstance bonus on Disguise checks. If reading an opponent’s mind, its circumstance bonus on Bluff and Disguise checks increases by a further +4.

### Rakshasas as Characters

Rakshasa characters possess the following racial traits.

&mdash; +2 Strength, +4 Dexterity, +6 Constitution, +2 Intelligence, +2 Wisdom, +6 Charisma.

&mdash;Medium size.

&mdash;A rakshasa’s base land speed is 40 feet.

&mdash;Darkvision out to 60 feet.

&mdash;Racial Hit Dice: A rakshasa begins with seven levels of outsider, which provide 7d8 Hit Dice, a base attack bonus of +7, and base saving throw bonuses of Fort +5, Ref +5, and Will +5.

&mdash;Racial Skills: A rakshasa’s outsider levels give it skill points equal to 10 x (8 + Int modifier). Its class skills are Bluff, Disguise, Listen, Move Silently, Perform, Sense Motive, and Spot. A rakshasa has a +4 racial bonus on Bluff and Disguise checks, and it can gain further bonuses by using change shape (+10 on Disguise checks) and detect thoughts (+4 on Bluff and Disguise checks).

&mdash;Racial Feats: A rakshasa’s outsider levels give it three feats.

&mdash; +9 natural armor bonus.

&mdash;Natural Weapons: Bite (1d6) and 2 claws (1d4).

&mdash;Detect Thoughts (Su): The save DC is 13 + the character’s Cha modifier.

&mdash;Spells: A rakshasa character casts spells as a 7th-level sorcerer. If the character takes additional levels of sorcerer, these levels stack with the rakshasa’s base spellcasting ability for spells known, spells per day, and other effects dependent on caster level. A rakshasa character likewise uses the sum of its racial spellcasting levels and class levels to determine the abilities of its familiar.

&mdash;Special Qualities (see above): Change shape, damage reduction 15/good and piercing, spell resistance equal to 27 + class levels.

&mdash;Automatic Languages: Common, Infernal. Bonus Languages: Sylvan, Undercommon.

&mdash;Favored Class: Sorcerer.

&mdash;Level adjustment +7.


Triton
------

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Medium Outsider (Native, Water)**
- **Hit Dice:** 3d8+3 (16 hp)
- **Initiative:** +0
- **Speed:** 5 ft. (1 square), swim 40 ft.
- **Armor Class:** 16 (+6 natural), touch 10, flat-footed 16
- **Base Attack/Grapple:** +3/+4
- **Attack:** Trident +4 melee (1d8+1) or heavy crossbow +3 ranged (1d10/19–20)
- **Full Attack:** Trident +4 melee (1d8+1) or heavy crossbow +3 ranged (1d10/19–20)
- **Space/Reach:** 5 ft./5 ft.
- **Special Attacks:** Spell-like abilities
- **Special Qualities:** Darkvision 60 ft.
- **Saves:** Fort +4, Ref +3, Will +4
- **Abilities:** Str 12, Dex 10, Con 12, Int 13, Wis 13, Cha 11
- **Skills:** Craft (any one) +7, Diplomacy +2, Hide +6, Listen +7, Move Silently +6, Ride +6, Search +7, Sense Motive +7, Spot +7, Survival +7 (+9 following tracks), Swim +9
- **Feats:** Mounted Combat, Ride-By Attack
- **Environment:** Temperate aquatic
- **Organization:** Company (2–5), squad (6–11), or band (20–80)
- **Challenge Rating:** 2
- **Treasure:** Standard
- **Alignment:** Usually neutral good
- **Advancement:** 4–9 HD (Medium)
- **Level Adjustment:** +2

A triton has silvery skin that fades into silver-blue scales on the lower half of its body. A triton’s hair is deep blue or blue-green.

A triton is about the same size and weight as a human. Tritons speak Common and Aquan.

### Combat

The reclusive tritons prefer to avoid combat, but they fiercely defend their homes. They attack with either melee or ranged weapons as the circumstances warrant. When encountered outside their lair, they are 90% likely to be mounted on friendly sea creatures such as porpoises.

_Spell-Like Abilities:_ 1/day&mdash;*summon nature’s ally IV*. Caster level 7th. Tritons often choose water elementals for their companions.

__Skills:__ A triton has a +8 racial bonus on any Swim check to perform some special action or avoid a hazard. It can always choose to take 10 on a Swim check, even if distracted or endangered. It can use the run action while swimming, provided it swims in a straight line.


Xill
----

<small>This material is Open Game Content, and is licensed for public use under the terms of the Open Game License v1.0a.</small>

- **Medium Outsider (Extraplanar)**
- **Hit Dice:** 5d8+10 (32 hp)
- **Initiative:** +7
- **Speed:** 40 ft. (8 squares)
- **Armor Class:** 20 (+3 Dex, +7 natural), touch 13, flat-footed 17
- **Base Attack/Grapple:** +5/+7
- **Attack:** Short sword +7 melee (1d6+2/19–20) or claw +7 melee (1d4+2) or longbow +8 ranged (1d8/x3)
- **Full Attack:** 2 short swords +5 melee (1d6+2/19–20, 1d6+1/19–20) and 2 claws +5 melee (1d4+1); or 4 claws +5 melee (1d4+2, 1d4+1); or 2 longbows +4 ranged (1d8/x3)
- **Space/Reach:** 5 ft./5 ft.
- **Special Attacks:** Implant, improved grab, paralysis
- **Special Qualities:** Darkvision 60 ft., planewalk, spell resistance 21
- **Saves:** Fort +6, Ref +7, Will +5
- **Abilities:** Str 15, Dex 16, Con 15, Int 12, Wis 12, Cha 11
- **Skills:** Balance +13, Climb +10, Diplomacy +2, Escape Artist +11, Intimidate +8, Listen +9, Move Silently +11, Sense Motive +8, Spot +9, Tumble +11, Use Rope +3 (+5 with bindings)
- **Feats:** Improved Initiative, MultiattackB, Multiweapon Fighting
- **Environment:** Ethereal Plane
- **Organization:** Solitary or gang (2–5)
- **Challenge Rating:** 6
- **Treasure:** Standard
- **Alignment:** Always lawful evil
- **Advancement:** 6–8 HD (Medium); 9–15 HD (Large)
- **Level Adjustment:** +4

A xill stands 4 to 5 feet tall and weighs about 100 pounds. Xills speak Infernal.

### Combat

Xills are dangerous opponents, attacking with all four limbs. More civilized ones use weapons, usually fighting with two at a time so as to leave two claws free for grab attacks. Xills typically lie in wait on the Ethereal Plane for suitable prey to happen by, then ambush it using their planewalk ability. They make full use of their Tumble skill in combat: Usually, one or two distract physically powerful enemies by attacking, then assuming a defensive stance while their fellows maneuver to advantage.

__Implant (Ex):__ As a standard action, a xill can lay eggs inside a paralyzed creature. The young emerge about 90 days later, literally devouring the host from inside. A remove disease spell rids a victim of the egg, as does a DC 25 Heal check. If the check fails, the healer can try again, but each attempt (successful or not) deals 1d4 points of damage to the patient.

__Improved Grab (Ex):__ To use this ability, a xill must hit with one or more claw attacks. It can then attempt to start a grapple as a free action without provoking an attack of opportunity. It receives a +2 bonus on the grapple check for each claw that hits. If it wins the grapple check and maintains the hold in the next round, it automatically bites the foe at that time. The bite deals no damage but injects a paralyzing venom.

__Paralysis (Ex):__ Those bitten by a xill must succeed on a DC 14 Fortitude save or be paralyzed for 1d4 hours. The save DC is Constitution-based.

__Planewalk (Su):__ These planar travelers like to slip between the folds of space to attack enemies as though from thin air. They can cross from the Ethereal Plane with a move action but take 2 rounds to cross back, during which time they are immobile. As a xill fades away, it becomes harder to hit: Opponents have a 20% miss chance in the first round and a 50% miss chance in the second. A xill can planewalk with a willing or helpless creature.


Extraplanar Creatures
---------------------

<table>
  <caption>Table: Native Outsiders</caption>
  <tr>
    <th>Creature</th>
    <th>Subtypes</th>
    <th>Environment</th>
    <th>Alignment</th>
  </tr>
  <tr>
    <td>Couatl</td>
    <td>&mdash;</td>
    <td>Warm forest</td>
    <td>Always lawful good</td>
  </tr>
  <tr>
    <td>Janni</td>
    <td>&mdash;</td>
    <td>Warm desert</td>
    <td>Usually neutral</td>
  </tr>
  <tr>
    <td>Rakshasa</td>
    <td>&mdash;</td>
    <td>Warm marsh</td>
    <td>Always lawful evil</td>
  </tr>
  <tr>
    <td>Triton</td>
    <td>Water</td>
    <td>Temperate aquatic</td>
    <td>Usually neutral good</td>
  </tr>
</table>

<table>
  <caption>Table: Outer Extraplanar</caption>
  <tr>
    <th></th>
    <th>Lawful</th>
    <th>Any</th>
    <th>Chaotic</th>
  </tr>
  <tr>
    <th>Good</th>
    <td>Archon</td>
    <td>Angel, celestial</td>
    <td>Lillend</td>
  </tr>
  <tr>
    <th>Any</th>
    <td>Formian</td>
    <td>Devourer</td>
    <td>Titan</td>
  </tr>
  <tr>
    <th>Evil</th>
    <td>Devil</td>
    <td>Fiendish</td>
    <td>Abyssal, demon</td>
  </tr>
</table>

<table>
  <caption>Table: Psionic Creatures</caption>
  <tr>
    <th>Creature</th>
    <th>Abilities</th>
    <th>Favored Class</th>
    <th>Level Adjustment</th>
  </tr>
  <tr>
    <td>Aboleth</td>
    <td></td>
    <td>Wizard (aboleth mage)</td>
    <td>&mdash;</td>
  </tr>
  <tr>
    <td>Couatl</td>
    <td></td>
    <td>Sorcerer (natural spellcaster)</td>
    <td>+7</td>
  </tr>
  <tr>
    <td>Duergar</td>
    <td><em>Enlarge person</em>, <em>invisibility</em></td>
    <td>Fighter (psychic warrior)</td>
    <td>+1</td>
  </tr>
  <tr>
    <td>Githyanki<sup>1</sup></td>
    <td><em>Daze</em>, <em>mage hand</em></td>
    <td>Fighter (psion)</td>
    <td>+2</td>
  </tr>
  <tr>
    <td>Githzerai<sup>1</sup></td>
    <td><em>Daze</em>, <em>feather fall</em>, <em>shatter</em></td>
    <td>Monk (wilder)</td>
    <td>+2</td>
  </tr>
  <tr>
    <td>Mind flayer<sup>1</sup></td>
    <td></td>
    <td>Wizard</td>
    <td>+7</td>
  </tr>
<tfoot>
  <tr>
    <td colspan="4">1 This creature is not Open Game Content</td>
  </tr>
</tfoot>
</table>


Types, Subtypes, and Abilities
------------------------------

__Chaotic Subtype:__ A subtype usually applied only to outsiders native to the chaotic-aligned Outer Planes. Most creatures that have this subtype also have chaotic alignments; however, if their alignments change they still retain the subtype. Any effect that depends on alignment affects a creature with this subtype as if the creature has a chaotic alignment, no matter what its alignment actually is. The creature also suffers effects according to its actual alignment. A creature with the chaotic subtype overcomes damage reduction as if its natural weapons and any weapons it wields were chaotic-aligned (see Damage Reduction, below).

__Extraplanar Subtype:__ A subtype applied to any creature when it is on a plane other than its native plane. A creature that travels the planes can gain or lose this subtype as it goes from plane to plane. Monster entries assume that encounters with creatures take place on the Material Plane, and every creature whose native plane is not the Material Plane has the extraplanar subtype (but would not have when on its home plane). Every extraplanar creature in this book has a home plane mentioned in its description. Creatures not labeled as extraplanar are natives of the Material Plane, and they gain the extraplanar subtype if they leave the Material Plane. No creature has the extraplanar subtype when it is on a transitive plane, such as the Astral Plane, the Ethereal Plane, and the Plane of Shadow.

__Lawful:__ A subtype usually applied only to outsiders native to the lawful-aligned Outer Planes. Most creatures that have this subtype also have lawful alignments; however, if their alignments change, they still retain the subtype. Any effect that depends on alignment affects a creature with this subtype as if the creature has a lawful alignment, no matter what its alignment actually is. The creature also suffers effects according to its actual alignment. A creature with the lawful subtype overcomes damage reduction as if its natural weapons and any weapons it wields were lawful-aligned (see Damage Reduction, above).

__Native Subtype:__ A subtype applied only to outsiders. These creatures have mortal ancestors or a strong connection to the Material Plane and can be raised, reincarnated, or resurrected just as other living creatures can be. Creatures with this subtype are native to the Material Plane (hence the subtype’s name). Unlike true outsiders, native outsiders need to eat and sleep. 

__Outsider Type:__ An outsider in Uncommon Era is an alien from an exotic locale.

_Features:_ An outsider has the following features.

&mdash;8-sided Hit Dice.

&mdash;Base attack bonus equal to total Hit Dice (as fighter).

&mdash;Good Fortitude, Reflex, and Will saves.

&mdash;Skill points equal to (8 + Int modifier, minimum 1) per Hit Die, with quadruple skill points for the first Hit Die.

_Traits:_ An outsider possesses the following traits (unless otherwise noted in a creature’s entry).

&mdash;Darkvision out to 60 feet.

&mdash;Outsiders have a different physiology than normal living beings. Spells such as _raise dead_, _reincarnate_, and _resurrection_ don’t work on an outsider. It takes a different magical effect, such as _limited wish_, _wish_, _miracle_, or _true resurrection_ to restore it to life. An outsider with the native subtype can be raised, reincarnated, or resurrected just as other living creatures can be.

&mdash;Proficient with all simple and martial weapons and any weapons mentioned in its entry.

&mdash;Proficient with whatever type of armor (light, medium, or heavy) it is described as wearing, as well as all lighter types. Outsiders not indicated as wearing armor are not proficient with armor. Outsiders are proficient with shields if they are proficient with any form of armor.

&mdash;Outsiders breathe, but do not need to eat or sleep (although they can do so if they wish). Native outsiders breathe, eat, and sleep. 
