It's difficult to rigidly define the difference between an original
game, a modification, an asset pack, etc.. Here are listed some
notable games and mods for Doom, as well as how they are packaged and
how they interact with Doom engines.


iD Software
-----------

### Doom: The Ultimate Doom

*The Ultimate Doom* expanded *Doom* by adding a new "episode" to the
"New Game" menu, *Thy Flesh Consumed*. Consisting of 8 new levels and
a story that continues from the end of *Episode 3: Inferno*,
*Thy Flesh Consumed* came with an updated game engine that was
being developed to support the release of *Doom II* and re-arranged
some of the internal behavior and variables while essentially
remaining the same game.

### Doom II: Hell on Earth

*Doom II* is the sequel to *Doom* presenting a brand-new story
continuing from the end of *The Ultimate Doom's* episode
*Thy Flesh Consumed* as a single campaign of 30 levels. *Doom II*
added a new weapon, new monsters, and new map features requiring
upgrade to the engine and a rewrite of the state tables and game
assets.

### TNT: Evilution

Community mapset. Boom.

### The Plutonia Experiment


Digital Café
------------

### Chex Quest

Modified engine.

### Chex Quest II


Mods, Total Conversions, Expansions, Etc.
-----------------------------------------

### Valiant

Enhanced bestiary and weapons.

### Freedoom: Phase 1

Vanilla-compatible assets and maps.

### Freedoom: Phase 2

### SIGIL

New episode for *The Ultimate Doom*.
