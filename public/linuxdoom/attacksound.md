`Mapobject` info tables in the engine have an `attacksound` field.
This means every monster can have a custom sound assigned for when it
attacks. However in practice this field is rarely used and sometimes
completely redundant.

Monsters with an `attacksound` field
------------------------------------

### MT_POSSESSED (unused)

+ attacksound: sfx_pistol

[`A_PosAttack`](https://github.com/id-Software/DOOM/blob/77735c3ff0772609e9c8d29e3ce2ab42ff54d20b/linuxdoom-1.10/p_enemy.c#L802)
plays `sfx_pistol` as a hard-coded behavior. `attacksound` is not used.

### MT_SERGEANT

+ attacksound: sfx_sgtatk

[`A_SargAttack`](https://github.com/id-Software/DOOM/blob/77735c3ff0772609e9c8d29e3ce2ab42ff54d20b/linuxdoom-1.10/p_enemy.c#L935)
does not play any sound, but [A_Chase](https://github.com/id-Software/DOOM/blob/77735c3ff0772609e9c8d29e3ce2ab42ff54d20b/linuxdoom-1.10/p_enemy.c#L672) 
plays the monster's `attacksound` before the monster enters its
`meleestate`.

### MT_SHADOWS

+ attacksound: sfx_sgtatk

[`A_SargAttack`](https://github.com/id-Software/DOOM/blob/77735c3ff0772609e9c8d29e3ce2ab42ff54d20b/linuxdoom-1.10/p_enemy.c#L935)
does not play any sound, but [A_Chase](https://github.com/id-Software/DOOM/blob/77735c3ff0772609e9c8d29e3ce2ab42ff54d20b/linuxdoom-1.10/p_enemy.c#L672) 
plays the monster's `attacksound` before the monster enters its
`meleestate`.

### MT_SKULL

+ attacksound: sfx_sklatk

[`A_SkullAttack`](https://github.com/id-Software/DOOM/blob/77735c3ff0772609e9c8d29e3ce2ab42ff54d20b/linuxdoom-1.10/p_enemy.c#L1419) 
plays the monster's `attacksound` when initiating its charge attack.

### MT_SPIDER (unused)

+ attacksound: sfx_shotgn

[`A_SPosAttack`](https://github.com/id-Software/DOOM/blob/77735c3ff0772609e9c8d29e3ce2ab42ff54d20b/linuxdoom-1.10/p_enemy.c#L821)
plays `sfx_shotgn` as a hard-coded behavior. `attacksound` is not
used. The same is true for MT_SHOTGUY.


Why?
----

There are a couple of possible reasons `attacksound` is a largely
redundant `mobj` attribute. The two monsters that have an unused
`attacksound` also have no melee attack or `meleestate`, suggesting 
`attacksound` is meant as a sound effect for melee attacks.
Projectiles themselves have a `spawnsound` that always plays when it
is spawned, but hitscan attacks rely on their calling function to play
the sound. `MT_SARGEANT` being the only melee attacker with
`attacksound` is likely because the sound needs to play as it enters
its `meleestate` in order to sync up with the visual effect.
