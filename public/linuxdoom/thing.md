DeHackEd Things
===============

A Thing in Doom is any map object typically represented by a sprite on 
the map. Things include pickups, decorations, and monsters.
<br><br>
Defining a Thing in DeHackEd usually means using a "slot" originally 
occupied by a classic Doom monster. Some source ports provide additional 
slots not occupied by any used monsters, allowing you to define brand 
new Things and place them in maps. Source ports like Chocolate Doom and 
Boom only provide the original 137 Things for you to work with.
<br><br>
Replacing certain Things means adopting some hard-coded behavior that 
cannot be changed by DeHackEd like infighting, blast damage immunity, 
projectile immunity, and so on.

Definition Block
----------------

    Thing 12 (Troop -> DoomImp)
    Initial frame = 442          #S_TROO_STND
    Hit points = 60
    First moving frame = 444     #S_TROO_RUN1
    Alert sound = 39             #sfx_bgsit1
    Reaction time = 8
    Attack sound = 0
    Injury frame = 455           #S_TROO_PAIN
    Pain chance = 200
    Pain sound = 27              #sfx_popain
    Close attack frame = 452     #S_TROO_ATK1
    Far attack frame = 452       #S_TROO_ATK1
    Death frame = 457            #S_TROO_DIE1
    Exploding frame = 462        #S_TROO_XDIE1
    Death sound = 62             #sfx_bgdth1
    Speed = 8
    Width = 1310720              #20*FRACUNIT
    Height = 3670016             #56*FRACUNIT
    Mass = 100
    Missile damage = 0
    Action sound = 76            #sfx_bgact
    Bits = 4194310               #SOLID+SHOOTABLE+COUNTKILL
    Respawn frame = 470          #S_TROO_RAISE1

Bits Values
-----------

<table>
<thead>
  <tr><th>Mnemonic</th><th>Bit Add Value</th><th>Description</th></tr>
</thead>
<tbody>
	<tr><td>SPECIAL</td><td>1</td><td>Can be picked up</td></tr>
	<tr><td>SOLID</td><td>2</td><td>Obstacle</td></tr>
	<tr><td>SHOOTABLE</td><td>4</td><td>can be hit</td></tr>
	<tr><td>NOSECTOR</td><td>8</td><td>Not in sector map (not drawn)</td></tr>
	<tr><td>NOBLOCKMAP</td><td>16</td><td>Not in collision map (not hittable)</td></tr>
	<tr><td>AMBUSH</td><td>32</td><td>Not alerted by weapons</td></tr>
	<tr><td>JUSTHIT</td><td>64</td><td>will try to attack right back</td></tr>
	<tr><td>JUSTATTACKED</td><td>128</td><td>take at least 1 step before attacking</td></tr>
	<tr><td>SPAWNCEILING</td><td>256</td><td>initially hang from ceiling</td></tr>
	<tr><td>NOGRAVITY</td><td>512</td><td>don't apply gravity during play</td></tr>
	<tr><td>DROPOFF</td><td>1024</td><td>can jump from high places</td></tr>
	<tr><td>PICKUP</td><td>2048</td><td>will pick up items</td></tr>
	<tr><td>NOCLIP</td><td>4096</td><td>goes through walls</td></tr>
	<tr><td>SLIDE</td><td>8192</td><td>keep info about sliding along walls</td></tr>
	<tr><td>FLOAT</td><td>16384</td><td>allow movement to any height</td></tr>
	<tr><td>TELEPORT</td><td>32768</td><td>don't cross lines or look at teleport heights</td></tr>
	<tr><td>MISSILE</td><td>65536</td><td>don't hit same species, explode on block</td></tr>
	<tr><td>DROPPED</td><td>131072</td><td>dropped, not spawned (like ammo clip)</td></tr>
	<tr><td>SHADOW</td><td>262144</td><td>use fuzzy draw like spectres</td></tr>
	<tr><td>NOBLOOD</td><td>524288</td><td>puffs instead of blood when shot</td></tr>
	<tr><td>CORPSE</td><td>1048576</td><td>so it will slide down steps when dead</td></tr>
	<tr><td>INFLOAT</td><td>2097152</td><td>float but not to target height</td></tr>
	<tr><td>COUNTKILL</td><td>4194304</td><td>count toward the kills total</td></tr>
	<tr><td>COUNTITEM</td><td>8388608</td><td>count toward the items total</td></tr>
	<tr><td>SKULLFLY</td><td>16777216</td><td>special handling for flying skulls</td></tr>
	<tr><td>NOTDMATCH</td><td>33554432</td><td>do not spawn in deathmatch</td></tr>
</tbody>
</table>

Extensions
----------

Boom added "mnemonic" translations for the <code>Bits</code> field. See 
[Dehacked Support in Boom](boomdeh.txt) for details.

    Thing 17 (BruiserShot)
    Bits = NOBLOCKMAP+MISSILE+DROPOFF+NOGRAVITY+TRANSLUCENT

PrBoom added a field to make the thing drop an item on death.

    Thing 11 (Chainguy)
    Dropped item = 74            #Chaingun


Thing Number
------------

Table copied from <https://doomwiki.org/>

<table class="wikitable" style="font-size: 95%;">
<tr>
<th>Thing number </th>
<th> Usage </th>
<th> SLADE name </th>
<th> Identifier </th>
<th> ZDoom class
</th></tr>
<tr>
<td> 1 </td>
<td> <a href="https://doomwiki.org/wiki/Player" title="Player">Player</a> </td>
<td>  </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_PLAYER</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:DoomPlayer" class="extiw" title="zdoom:Classes:DoomPlayer">DoomPlayer</a>
</td></tr>
<tr>
<td> 2 </td>
<td> <a href="https://doomwiki.org/wiki/Trooper" title="Trooper" class="mw-redirect">Trooper</a> </td>
<td> Former Human </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_POSSESSED</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:ZombieMan" class="extiw" title="zdoom:Classes:ZombieMan">ZombieMan</a>
</td></tr>
<tr>
<td> 3 </td>
<td> <a href="https://doomwiki.org/wiki/Sergeant" title="Sergeant" class="mw-redirect">Sergeant</a> </td>
<td> Former Human Sergeant </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_SHOTGUY</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:ShotgunGuy" class="extiw" title="zdoom:Classes:ShotgunGuy">ShotgunGuy</a>
</td></tr>
<tr>
<td> 4 </td>
<td> <a href="https://doomwiki.org/wiki/Arch-vile" title="Arch-vile">Arch-vile</a> </td>
<td> Arch-Vile </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_VILE</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Archvile" class="extiw" title="zdoom:Classes:Archvile">Archvile</a>
</td></tr>
<tr>
<td> 5 </td>
<td> Arch-vile Fire </td>
<td>  </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_FIRE</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:ArchvileFire" class="extiw" title="zdoom:Classes:ArchvileFire">ArchvileFire</a>
</td></tr>
<tr>
<td> 6 </td>
<td> <a href="https://doomwiki.org/wiki/Revenant" title="Revenant">Revenant</a> </td>
<td> Revenant </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_UNDEAD</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Revenant" class="extiw" title="zdoom:Classes:Revenant">Revenant</a>
</td></tr>
<tr>
<td> 7 </td>
<td> Revenant Rocket </td>
<td>  </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_TRACER</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:RevenantTracer" class="extiw" title="zdoom:Classes:RevenantTracer">RevenantTracer</a>
</td></tr>
<tr>
<td> 8 </td>
<td> Fireball Trail </td>
<td>  </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_SMOKE</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:RevenantTracerSmoke" class="extiw" title="zdoom:Classes:RevenantTracerSmoke">RevenantTracerSmoke</a>
</td></tr>
<tr>
<td> 9 </td>
<td> <a href="https://doomwiki.org/wiki/Mancubus" title="Mancubus">Mancubus</a> </td>
<td> Mancubus </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_FATSO</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Fatso" class="extiw" title="zdoom:Classes:Fatso">Fatso</a>
</td></tr>
<tr>
<td> 10 </td>
<td> Mancubus Fireball </td>
<td>  </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_FATSHOT</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:FatShot" class="extiw" title="zdoom:Classes:FatShot">FatShot</a>
</td></tr>
<tr>
<td> 11 </td>
<td> <a href="https://doomwiki.org/wiki/Heavy_weapon_dude" title="Heavy weapon dude">Chaingunner</a> </td>
<td> Chaingunner </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_CHAINGUY</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:ChaingunGuy" class="extiw" title="zdoom:Classes:ChaingunGuy">ChaingunGuy</a>
</td></tr>
<tr>
<td> 12 </td>
<td> <a href="https://doomwiki.org/wiki/Imp" title="Imp">Imp</a> </td>
<td> Imp </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_TROOP</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:DoomImp" class="extiw" title="zdoom:Classes:DoomImp">DoomImp</a>
</td></tr>
<tr>
<td> 13 </td>
<td> <a href="https://doomwiki.org/wiki/Demon" title="Demon">Demon</a> </td>
<td> Demon </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_SERGEANT</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Demon" class="extiw" title="zdoom:Classes:Demon">Demon</a>
</td></tr>
<tr>
<td> 14 </td>
<td> <a href="https://doomwiki.org/wiki/Spectre" title="Spectre">Spectre</a> </td>
<td> Spectre </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_SHADOWS</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Spectre" class="extiw" title="zdoom:Classes:Spectre">Spectre</a>
</td></tr>
<tr>
<td> 15 </td>
<td> <a href="https://doomwiki.org/wiki/Cacodemon" title="Cacodemon">Cacodemon</a> </td>
<td> Cacodemon </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_HEAD</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Cacodemon" class="extiw" title="zdoom:Classes:Cacodemon">Cacodemon</a>
</td></tr>
<tr>
<td> 16 </td>
<td> <a href="https://doomwiki.org/wiki/Baron_of_Hell" title="Baron of Hell">Baron of Hell</a> </td>
<td> Baron of Hell </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_BRUISER</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:BaronOfHell" class="extiw" title="zdoom:Classes:BaronOfHell">BaronOfHell</a>
</td></tr>
<tr>
<td> 17 </td>
<td> Baron Fireball </td>
<td>  </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_BRUISERSHOT</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:BaronBall" class="extiw" title="zdoom:Classes:BaronBall">BaronBall</a>
</td></tr>
<tr>
<td> 18 </td>
<td> <a href="https://doomwiki.org/wiki/Hell_Knight" title="Hell Knight" class="mw-redirect">Hell Knight</a> </td>
<td> Hell Knight </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_KNIGHT</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:HellKnight" class="extiw" title="zdoom:Classes:HellKnight">HellKnight</a>
</td></tr>
<tr>
<td> 19 </td>
<td> <a href="https://doomwiki.org/wiki/Lost_Soul" title="Lost Soul" class="mw-redirect">Lost Soul</a> </td>
<td> Lost Soul </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_SKULL</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:LostSoul" class="extiw" title="zdoom:Classes:LostSoul">LostSoul</a>
</td></tr>
<tr>
<td> 20 </td>
<td> <a href="https://doomwiki.org/wiki/Spiderdemon" title="Spiderdemon">Spiderdemon</a> </td>
<td> Spider Mastermind </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_SPIDER</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:SpiderMastermind" class="extiw" title="zdoom:Classes:SpiderMastermind">SpiderMastermind</a>
</td></tr>
<tr>
<td> 21 </td>
<td> <a href="https://doomwiki.org/wiki/Arachnotron" title="Arachnotron">Arachnotron</a> </td>
<td> Arachnotron </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_BABY</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Arachnotron" class="extiw" title="zdoom:Classes:Arachnotron">Arachnotron</a>
</td></tr>
<tr>
<td> 22 </td>
<td> <a href="https://doomwiki.org/wiki/Cyberdemon" title="Cyberdemon">Cyberdemon</a> </td>
<td> Cyberdemon </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_CYBORG</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Cyberdemon" class="extiw" title="zdoom:Classes:Cyberdemon">Cyberdemon</a>
</td></tr>
<tr>
<td> 23 </td>
<td> <a href="https://doomwiki.org/wiki/Pain_Elemental" title="Pain Elemental" class="mw-redirect">Pain Elemental</a> </td>
<td> Pain Elemental </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_PAIN</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:PainElemental" class="extiw" title="zdoom:Classes:PainElemental">PainElemental</a>
</td></tr>
<tr>
<td> 24 </td>
<td> <a href="https://doomwiki.org/wiki/SS_Nazi" title="SS Nazi" class="mw-redirect">SS Nazi</a> </td>
<td> Wolfenstein 3D SS </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_WOLFSS</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:WolfensteinSS" class="extiw" title="zdoom:Classes:WolfensteinSS">WolfensteinSS</a>
</td></tr>
<tr>
<td> 25 </td>
<td> <a href="https://doomwiki.org/wiki/Commander_Keen" title="Commander Keen">Commander Keen</a> </td>
<td> Hanging Keen </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_KEEN</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:CommanderKeen" class="extiw" title="zdoom:Classes:CommanderKeen">CommanderKeen</a>
</td></tr>
<tr>
<td> 26 </td>
<td> <a href="https://doomwiki.org/wiki/Big_Brain" title="Big Brain" class="mw-redirect">Big Brain</a> </td>
<td> Boss Brain </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_BOSSBRAIN</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:BossBrain" class="extiw" title="zdoom:Classes:BossBrain">BossBrain</a>
</td></tr>
<tr>
<td> 27 </td>
<td> <a href="https://doomwiki.org/wiki/Demon_Spawner" title="Demon Spawner" class="mw-redirect">Demon Spawner</a> </td>
<td> Boss Shooter </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_BOSSSPIT</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:BossEye" class="extiw" title="zdoom:Classes:BossEye">BossEye</a>
</td></tr>
<tr>
<td> 28 </td>
<td> <a href="https://doomwiki.org/wiki/Spawn_spot" title="Spawn spot">Demon Spawn Spot</a> </td>
<td> Boss Spawn Spot </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_BOSSTARGET</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:BossTarget" class="extiw" title="zdoom:Classes:BossTarget">BossTarget</a>
</td></tr>
<tr>
<td> 29 </td>
<td> Demon Spawn Cube </td>
<td>  </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_SPAWNSHOT</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:SpawnShot" class="extiw" title="zdoom:Classes:SpawnShot">SpawnShot</a>
</td></tr>
<tr>
<td> 30 </td>
<td> Demon Spawn Fire </td>
<td>  </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_SPAWNFIRE</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:SpawnFire" class="extiw" title="zdoom:Classes:SpawnFire">SpawnFire</a>
</td></tr>
<tr>
<td> 31 </td>
<td> <a href="https://doomwiki.org/wiki/Barrel" title="Barrel">Barrel</a> </td>
<td> Barrel </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_BARREL</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:ExplosiveBarrel" class="extiw" title="zdoom:Classes:ExplosiveBarrel">ExplosiveBarrel</a>
</td></tr>
<tr>
<td> 32 </td>
<td> Imp Fireball </td>
<td>  </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_TROOPSHOT</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:DoomImpBall" class="extiw" title="zdoom:Classes:DoomImpBall">DoomImpBall</a>
</td></tr>
<tr>
<td> 33 </td>
<td> Caco Fireball </td>
<td>  </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_HEADSHOT</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:CacodemonBall" class="extiw" title="zdoom:Classes:CacodemonBall">CacodemonBall</a>
</td></tr>
<tr>
<td> 34 </td>
<td> Rocket (in air) </td>
<td>  </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_ROCKET</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Rocket" class="extiw" title="zdoom:Classes:Rocket">Rocket</a>
</td></tr>
<tr>
<td> 35 </td>
<td> Plasma Bullet </td>
<td>  </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_PLASMA</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:PlasmaBall" class="extiw" title="zdoom:Classes:PlasmaBall">PlasmaBall</a>
</td></tr>
<tr>
<td> 36 </td>
<td> BFG Shot </td>
<td>  </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_BFG</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:BFGBall" class="extiw" title="zdoom:Classes:BFGBall">BFGBall</a>
</td></tr>
<tr>
<td> 37 </td>
<td> Arachnotron Fireball </td>
<td>  </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_ARACHPLAZ</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:ArachnotronPlasma" class="extiw" title="zdoom:Classes:ArachnotronPlasma">ArachnotronPlasma</a>
</td></tr>
<tr>
<td> 38 </td>
<td> Bullet Puff </td>
<td>  </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_PUFF</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:BulletPuff" class="extiw" title="zdoom:Classes:BulletPuff">BulletPuff</a>
</td></tr>
<tr>
<td> 39 </td>
<td> Blood Splat </td>
<td>  </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_BLOOD</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Blood" class="extiw" title="zdoom:Classes:Blood">Blood</a>
</td></tr>
<tr>
<td> 40 </td>
<td> <a href="https://doomwiki.org/wiki/Teleporter" title="Teleporter">Teleport Flash</a> </td>
<td>  </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_TFOG</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:TeleportFog" class="extiw" title="zdoom:Classes:TeleportFog">TeleportFog</a>
</td></tr>
<tr>
<td> 41 </td>
<td> Item Respawn Fog </td>
<td>  </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_IFOG</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:ItemFog" class="extiw" title="zdoom:Classes:ItemFog">ItemFog</a>
</td></tr>
<tr>
<td> 42 </td>
<td> Teleport Exit </td>
<td>  </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_TELEPORTMAN</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:TeleportDest" class="extiw" title="zdoom:Classes:TeleportDest">TeleportDest</a>
</td></tr>
<tr>
<td> 43 </td>
<td> BFG Hit </td>
<td>  </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_EXTRABFG</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:BFGExtra" class="extiw" title="zdoom:Classes:BFGExtra">BFGExtra</a>
</td></tr>
<tr>
<td> 44 </td>
<td> <a href="https://doomwiki.org/wiki/Security_armor" title="Security armor" class="mw-redirect">Green Armor</a> </td>
<td> Green Armour </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC0</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:GreenArmor" class="extiw" title="zdoom:Classes:GreenArmor">GreenArmor</a>
</td></tr>
<tr>
<td> 45 </td>
<td> <a href="https://doomwiki.org/wiki/Combat_armor" title="Combat armor" class="mw-redirect">Blue Armor</a> </td>
<td> Blue Armour </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC1</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:BlueArmor" class="extiw" title="zdoom:Classes:BlueArmor">BlueArmor</a>
</td></tr>
<tr>
<td> 46 </td>
<td> <a href="https://doomwiki.org/wiki/Health_bonus" title="Health bonus">Health Potion</a> </td>
<td> Health Potion </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC2</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:HealthBonus" class="extiw" title="zdoom:Classes:HealthBonus">HealthBonus</a>
</td></tr>
<tr>
<td> 47 </td>
<td> <a href="https://doomwiki.org/wiki/Armor_bonus" title="Armor bonus">Armor Helmet</a> </td>
<td> Armour Helmet </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC3</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:ArmorBonus" class="extiw" title="zdoom:Classes:ArmorBonus">ArmorBonus</a>
</td></tr>
<tr>
<td> 48 </td>
<td> <a href="https://doomwiki.org/wiki/Keys" title="Keys" class="mw-redirect">Blue Key Card</a> </td>
<td> Blue Keycard </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC4</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:BlueCard" class="extiw" title="zdoom:Classes:BlueCard">BlueCard</a>
</td></tr>
<tr>
<td> 49 </td>
<td> <a href="https://doomwiki.org/wiki/Keys" title="Keys" class="mw-redirect">Red Key Card</a> </td>
<td> Red Keycard </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC5</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:RedCard" class="extiw" title="zdoom:Classes:RedCard">RedCard</a>
</td></tr>
<tr>
<td> 50 </td>
<td> <a href="https://doomwiki.org/wiki/Keys" title="Keys" class="mw-redirect">Yellow Key Card</a> </td>
<td> Yellow Keycard </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC6</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:YellowCard" class="extiw" title="zdoom:Classes:YellowCard">YellowCard</a>
</td></tr>
<tr>
<td> 51 </td>
<td> <a href="https://doomwiki.org/wiki/Keys" title="Keys" class="mw-redirect">Yellow Skull Key</a> </td>
<td> Yellow Skull Key </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC7</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:YellowSkull" class="extiw" title="zdoom:Classes:YellowSkull">YellowSkull</a>
</td></tr>
<tr>
<td> 52 </td>
<td> <a href="https://doomwiki.org/wiki/Keys" title="Keys" class="mw-redirect">Red Skull Key</a> </td>
<td> Red Skull Key </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC8</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:RedSkull" class="extiw" title="zdoom:Classes:RedSkull">RedSkull</a>
</td></tr>
<tr>
<td> 53 </td>
<td> <a href="https://doomwiki.org/wiki/Keys" title="Keys" class="mw-redirect">Blue Skull Key</a> </td>
<td> Blue Skull Key </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC9</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:BlueSkull" class="extiw" title="zdoom:Classes:BlueSkull">BlueSkull</a>
</td></tr>
<tr>
<td> 54 </td>
<td> <a href="https://doomwiki.org/wiki/Stimpack" title="Stimpack">Stim Pack</a> </td>
<td> Stimpack </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC10</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Stimpack" class="extiw" title="zdoom:Classes:Stimpack">Stimpack</a>
</td></tr>
<tr>
<td> 55 </td>
<td> <a href="https://doomwiki.org/wiki/Medikit" title="Medikit">Medical Kit</a> </td>
<td> Medikit </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC11</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Medikit" class="extiw" title="zdoom:Classes:Medikit">Medikit</a>
</td></tr>
<tr>
<td> 56 </td>
<td> <a href="https://doomwiki.org/wiki/Soul_sphere" title="Soul sphere" class="mw-redirect">Soul Sphere</a> </td>
<td> Soul Sphere </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC12</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Soulsphere" class="extiw" title="zdoom:Classes:Soulsphere">Soulsphere</a>
</td></tr>
<tr>
<td> 57 </td>
<td> <a href="https://doomwiki.org/wiki/Invulnerability" title="Invulnerability">Invulnerability Sphere</a> </td>
<td> Invulnerability Sphere </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_INV</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:InvulnerabilitySphere" class="extiw" title="zdoom:Classes:InvulnerabilitySphere">InvulnerabilitySphere</a>
</td></tr>
<tr>
<td> 58 </td>
<td> <a href="https://doomwiki.org/wiki/Berserk" title="Berserk">Berserk Pack</a> </td>
<td> Berserk Pack </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC13</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Berserk" class="extiw" title="zdoom:Classes:Berserk">Berserk</a>
</td></tr>
<tr>
<td> 59 </td>
<td> <a href="https://doomwiki.org/wiki/Partial_invisibility" title="Partial invisibility">Blur Sphere</a> </td>
<td> Partial Invisibility Sphere </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_INS</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:BlurSphere" class="extiw" title="zdoom:Classes:BlurSphere">BlurSphere</a>
</td></tr>
<tr>
<td> 60 </td>
<td> <a href="https://doomwiki.org/wiki/Radiation_suit" title="Radiation suit" class="mw-redirect">Radiation Suit</a> </td>
<td> Radiation Shielding Suit </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC14</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:RadSuit" class="extiw" title="zdoom:Classes:RadSuit">RadSuit</a>
</td></tr>
<tr>
<td> 61 </td>
<td> <a href="https://doomwiki.org/wiki/Computer_map" title="Computer map" class="mw-redirect">Computer Map</a> </td>
<td> Computer Area Map </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC15</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Allmap" class="extiw" title="zdoom:Classes:Allmap">Allmap</a>
</td></tr>
<tr>
<td> 62 </td>
<td> <a href="https://doomwiki.org/wiki/Light_amplification_visor" title="Light amplification visor">Lite Amp. Visor</a> </td>
<td> Lite-Amp Goggles </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC16</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Infrared" class="extiw" title="zdoom:Classes:Infrared">Infrared</a>
</td></tr>
<tr>
<td> 63 </td>
<td> <a href="https://doomwiki.org/wiki/Megasphere" title="Megasphere">Mega Sphere</a> </td>
<td> Megasphere </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MEGA</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Megasphere" class="extiw" title="zdoom:Classes:Megasphere">Megasphere</a>
</td></tr>
<tr>
<td> 64 </td>
<td> <a href="https://doomwiki.org/wiki/Clip" title="Clip">Ammo Clip</a> </td>
<td> Ammo Clip </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_CLIP</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Clip" class="extiw" title="zdoom:Classes:Clip">Clip</a>
</td></tr>
<tr>
<td> 65 </td>
<td> <a href="https://doomwiki.org/wiki/Box_of_bullets" title="Box of bullets">Box of Ammo</a> </td>
<td> Box of Ammo </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC17</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:ClipBox" class="extiw" title="zdoom:Classes:ClipBox">ClipBox</a>
</td></tr>
<tr>
<td> 66 </td>
<td> <a href="https://doomwiki.org/wiki/Rocket" title="Rocket">Rocket</a> </td>
<td> Rocket </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC18</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:RocketAmmo" class="extiw" title="zdoom:Classes:RocketAmmo">RocketAmmo</a>
</td></tr>
<tr>
<td> 67 </td>
<td> <a href="https://doomwiki.org/wiki/Box_of_rockets" title="Box of rockets">Box of Rockets</a> </td>
<td> Box of Rockets </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC19</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:RocketBox" class="extiw" title="zdoom:Classes:RocketBox">RocketBox</a>
</td></tr>
<tr>
<td> 68 </td>
<td> <a href="https://doomwiki.org/wiki/Energy_cell_(Doom)" title="Energy cell (Doom)">Energy Cell</a> </td>
<td> Energy Cell </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC20</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Cell" class="extiw" title="zdoom:Classes:Cell">Cell</a>
</td></tr>
<tr>
<td> 69 </td>
<td> <a href="https://doomwiki.org/wiki/Energy_cell_pack" title="Energy cell pack">Energy Pack</a> </td>
<td> Energy Pack </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC21</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:CellPack" class="extiw" title="zdoom:Classes:CellPack">CellPack</a>
</td></tr>
<tr>
<td> 70 </td>
<td> <a href="https://doomwiki.org/wiki/4_shotgun_shells" title="4 shotgun shells">Shells</a> </td>
<td> Shotgun Shells </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC22</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Shell" class="extiw" title="zdoom:Classes:Shell">Shell</a>
</td></tr>
<tr>
<td> 71 </td>
<td> <a href="https://doomwiki.org/wiki/Box_of_shells" title="Box of shells" class="mw-redirect">Box of Shells</a> </td>
<td> Box of Shells </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC23</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:ShellBox" class="extiw" title="zdoom:Classes:ShellBox">ShellBox</a>
</td></tr>
<tr>
<td> 72 </td>
<td> <a href="https://doomwiki.org/wiki/Backpack" title="Backpack">Backpack</a> </td>
<td> Backpack </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC24</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Backpack" class="extiw" title="zdoom:Classes:Backpack">Backpack</a>
</td></tr>
<tr>
<td> 73 </td>
<td> <a href="https://doomwiki.org/wiki/BFG9000" title="BFG9000">BFG9000</a> </td>
<td> BFG9000 </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC25</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:BFG9000" class="extiw" title="zdoom:Classes:BFG9000">BFG9000</a>
</td></tr>
<tr>
<td> 74 </td>
<td> <a href="https://doomwiki.org/wiki/Chaingun" title="Chaingun">Chaingun</a> </td>
<td> Chaingun </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_CHAINGUN</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Chaingun" class="extiw" title="zdoom:Classes:Chaingun">Chaingun</a>
</td></tr>
<tr>
<td> 75 </td>
<td> <a href="https://doomwiki.org/wiki/Chainsaw" title="Chainsaw">Chainsaw</a> </td>
<td> Chainsaw </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC26</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Chainsaw" class="extiw" title="zdoom:Classes:Chainsaw">Chainsaw</a>
</td></tr>
<tr>
<td> 76 </td>
<td> <a href="https://doomwiki.org/wiki/Rocket_Launcher" title="Rocket Launcher" class="mw-redirect">Rocket Launcher</a> </td>
<td> Rocket Launcher </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC27</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:RocketLauncher" class="extiw" title="zdoom:Classes:RocketLauncher">RocketLauncher</a>
</td></tr>
<tr>
<td> 77 </td>
<td> <a href="https://doomwiki.org/wiki/Plasma_Gun" title="Plasma Gun" class="mw-redirect">Plasma Gun</a> </td>
<td> Plasma Gun </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC28</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:PlasmaRifle" class="extiw" title="zdoom:Classes:PlasmaRifle">PlasmaRifle</a>
</td></tr>
<tr>
<td> 78 </td>
<td> <a href="https://doomwiki.org/wiki/Shotgun" title="Shotgun">Shotgun</a> </td>
<td> Shotgun </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_SHOTGUN</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Shotgun" class="extiw" title="zdoom:Classes:Shotgun">Shotgun</a>
</td></tr>
<tr>
<td> 79 </td>
<td> <a href="https://doomwiki.org/wiki/Super_Shotgun" title="Super Shotgun" class="mw-redirect">Super Shotgun</a> </td>
<td> Super Shotgun </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_SUPERSHOTGUN</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:SuperShotgun" class="extiw" title="zdoom:Classes:SuperShotgun">SuperShotgun</a>
</td></tr>
<tr>
<td> 80 </td>
<td> Tall Lamp </td>
<td> Tall Techno Lamp </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC29</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:TechLamp" class="extiw" title="zdoom:Classes:TechLamp">TechLamp</a>
</td></tr>
<tr>
<td> 81 </td>
<td> Tall Lamp 2 </td>
<td> Short Techno Lamp </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC30</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:TechLamp2" class="extiw" title="zdoom:Classes:TechLamp2">TechLamp2</a>
</td></tr>
<tr>
<td> 82 </td>
<td> Short Lamp </td>
<td> Yellow Lamp </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC31</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Column" class="extiw" title="zdoom:Classes:Column">Column</a>
</td></tr>
<tr>
<td> 83 </td>
<td> Tall Green Pillar </td>
<td> Tall Green Pillar </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC32</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:TallGreenColumn" class="extiw" title="zdoom:Classes:TallGreenColumn">TallGreenColumn</a>
</td></tr>
<tr>
<td> 84 </td>
<td> Short Green Pillar </td>
<td> Short Green Pillar </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC33</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:ShortGreenColumn" class="extiw" title="zdoom:Classes:ShortGreenColumn">ShortGreenColumn</a>
</td></tr>
<tr>
<td> 85 </td>
<td> Tall Red Pillar </td>
<td> Tall Red Pillar </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC34</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:TallRedColumn" class="extiw" title="zdoom:Classes:TallRedColumn">TallRedColumn</a>
</td></tr>
<tr>
<td> 86 </td>
<td> Short Red Pillar </td>
<td> Short Red Pillar </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC35</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:ShortRedColumn" class="extiw" title="zdoom:Classes:ShortRedColumn">ShortRedColumn</a>
</td></tr>
<tr>
<td> 87 </td>
<td> Pillar with Skull </td>
<td> Red Pillar + Skull </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC36</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:SkullColumn" class="extiw" title="zdoom:Classes:SkullColumn">SkullColumn</a>
</td></tr>
<tr>
<td> 88 </td>
<td> Pillar with Heart </td>
<td> Pillar + Pumping Heart </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC37</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:HeartColumn" class="extiw" title="zdoom:Classes:HeartColumn">HeartColumn</a>
</td></tr>
<tr>
<td> 89 </td>
<td> Eye in Symbol </td>
<td> Evil Eye </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC38</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:EvilEye" class="extiw" title="zdoom:Classes:EvilEye">EvilEye</a>
</td></tr>
<tr>
<td> 90 </td>
<td> Flaming Skulls </td>
<td> Flaming Skulls </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC39</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:FloatingSkull" class="extiw" title="zdoom:Classes:FloatingSkull">FloatingSkull</a>
</td></tr>
<tr>
<td> 91 </td>
<td> Grey Tree </td>
<td> Short Grey Tree </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC40</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:TorchTree" class="extiw" title="zdoom:Classes:TorchTree">TorchTree</a>
</td></tr>
<tr>
<td> 92 </td>
<td> Tall Blue Torch </td>
<td> Tall Blue Torch </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC41</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:BlueTorch" class="extiw" title="zdoom:Classes:BlueTorch">BlueTorch</a>
</td></tr>
<tr>
<td> 93 </td>
<td> Tall Green torch </td>
<td> Tall Green Torch </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC42</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:GreenTorch" class="extiw" title="zdoom:Classes:GreenTorch">GreenTorch</a>
</td></tr>
<tr>
<td> 94 </td>
<td> Tall Red Torch </td>
<td> Tall Red Torch </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC43</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:RedTorch" class="extiw" title="zdoom:Classes:RedTorch">RedTorch</a>
</td></tr>
<tr>
<td> 95 </td>
<td> Small Blue Torch </td>
<td> Short Blue Torch </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC44</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:ShortBlueTorch" class="extiw" title="zdoom:Classes:ShortBlueTorch">ShortBlueTorch</a>
</td></tr>
<tr>
<td> 96 </td>
<td> Small Green Torch </td>
<td> Short Green Torch </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC45</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:ShortGreenTorch" class="extiw" title="zdoom:Classes:ShortGreenTorch">ShortGreenTorch</a>
</td></tr>
<tr>
<td> 97 </td>
<td> Small Red Torch </td>
<td> Short Red Torch </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC46</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:ShortRedTorch" class="extiw" title="zdoom:Classes:ShortRedTorch">ShortRedTorch</a>
</td></tr>
<tr>
<td> 98 </td>
<td> Brown Stub </td>
<td> Tree Stump </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC47</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Stalagtite" class="extiw" title="zdoom:Classes:Stalagtite">Stalagtite</a>
</td></tr>
<tr>
<td> 99 </td>
<td> Technical Column </td>
<td> Tech Column </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC48</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:TechPillar" class="extiw" title="zdoom:Classes:TechPillar">TechPillar</a>
</td></tr>
<tr>
<td> 100 </td>
<td> Candle </td>
<td> Candle </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC49</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Candlestick" class="extiw" title="zdoom:Classes:Candlestick">Candlestick</a>
</td></tr>
<tr>
<td> 101 </td>
<td> Candelabra </td>
<td> Candelabra </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC50</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Candelabra" class="extiw" title="zdoom:Classes:Candelabra">Candelabra</a>
</td></tr>
<tr>
<td> 102 </td>
<td> Swaying Body </td>
<td> Hanging Swaying Body (Blocking) </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC51</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:BloodyTwitch" class="extiw" title="zdoom:Classes:BloodyTwitch">BloodyTwitch</a>
</td></tr>
<tr>
<td> 103 </td>
<td> Hanging Arms Out </td>
<td> Hanging Body Arms Out (Blocking) </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC52</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Meat2" class="extiw" title="zdoom:Classes:Meat2">Meat2</a>
</td></tr>
<tr>
<td> 104 </td>
<td> One-legged Body </td>
<td> Hanging One-legged Body (Blocking) </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC53</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Meat3" class="extiw" title="zdoom:Classes:Meat3">Meat3</a>
</td></tr>
<tr>
<td> 105 </td>
<td> Hanging Torso </td>
<td> Hanging Legs (Blocking) </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC54</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Meat4" class="extiw" title="zdoom:Classes:Meat4">Meat4</a>
</td></tr>
<tr>
<td> 106 </td>
<td> Hanging Leg </td>
<td> Hanging Leg (Blocking) </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC55</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Meat5" class="extiw" title="zdoom:Classes:Meat5">Meat5</a>
</td></tr>
<tr>
<td> 107 </td>
<td> Hanging Arms Out 2 </td>
<td> Hanging Body Arms Out </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC56</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:NonsolidMeat2" class="extiw" title="zdoom:Classes:NonsolidMeat2">NonsolidMeat2</a>
</td></tr>
<tr>
<td> 108 </td>
<td> Hanging Torso 2 </td>
<td> Hanging Legs </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC57</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:NonsolidMeat4" class="extiw" title="zdoom:Classes:NonsolidMeat4">NonsolidMeat4</a>
</td></tr>
<tr>
<td> 109 </td>
<td> One-legged Body 2 </td>
<td> Hanging One-legged Body </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC58</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:NonsolidMeat3" class="extiw" title="zdoom:Classes:NonsolidMeat3">NonsolidMeat3</a>
</td></tr>
<tr>
<td> 110 </td>
<td> Hanging Leg 2 </td>
<td> Hanging Leg </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC59</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:NonsolidMeat5" class="extiw" title="zdoom:Classes:NonsolidMeat5">NonsolidMeat5</a>
</td></tr>
<tr>
<td> 111 </td>
<td> Swaying Body 2 </td>
<td> Hanging Swaying Body </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC60</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:NonsolidTwitch" class="extiw" title="zdoom:Classes:NonsolidTwitch">NonsolidTwitch</a>
</td></tr>
<tr>
<td> 112 </td>
<td> Dead Cacodemon </td>
<td> Dead Cacodemon </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC61</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:DeadCacodemon" class="extiw" title="zdoom:Classes:DeadCacodemon">DeadCacodemon</a>
</td></tr>
<tr>
<td> 113 </td>
<td> Dead Marine </td>
<td> Dead Player </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC62</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:DeadMarine" class="extiw" title="zdoom:Classes:DeadMarine">DeadMarine</a>
</td></tr>
<tr>
<td> 114 </td>
<td> Dead Trooper </td>
<td> Dead Former Human </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC63</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:DeadZombieMan" class="extiw" title="zdoom:Classes:DeadZombieMan">DeadZombieMan</a>
</td></tr>
<tr>
<td> 115 </td>
<td> Dead Demon </td>
<td> Dead Demon </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC64</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:DeadDemon" class="extiw" title="zdoom:Classes:DeadDemon">DeadDemon</a>
</td></tr>
<tr>
<td> 116 </td>
<td> Dead Lost Soul </td>
<td> Dead Lost Soul </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC65</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:DeadLostSoul" class="extiw" title="zdoom:Classes:DeadLostSoul">DeadLostSoul</a>
</td></tr>
<tr>
<td> 117 </td>
<td> Dead Imp </td>
<td> Dead Imp </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC66</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:DeadDoomImp" class="extiw" title="zdoom:Classes:DeadDoomImp">DeadDoomImp</a>
</td></tr>
<tr>
<td> 118 </td>
<td> Dead Sergeant </td>
<td> Dead Former Human Sergeant </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC67</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:DeadShotgunGuy" class="extiw" title="zdoom:Classes:DeadShotgunGuy">DeadShotgunGuy</a>
</td></tr>
<tr>
<td> 119 </td>
<td> Guts and Bones </td>
<td> Guts and Bones </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC68</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:GibbedMarine" class="extiw" title="zdoom:Classes:GibbedMarine">GibbedMarine</a>
</td></tr>
<tr>
<td> 120 </td>
<td> Guts and Bones 2 </td>
<td> Guts and Bones 2 </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC69</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:GibbedMarineExtra" class="extiw" title="zdoom:Classes:GibbedMarineExtra">GibbedMarineExtra</a>
</td></tr>
<tr>
<td> 121 </td>
<td> Skewered Heads </td>
<td> Heads on Pole </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC70</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:HeadsOnAStick" class="extiw" title="zdoom:Classes:HeadsOnAStick">HeadsOnAStick</a>
</td></tr>
<tr>
<td> 122 </td>
<td> Pool of Blood </td>
<td> Pool of Blood </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC71</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:Gibs" class="extiw" title="zdoom:Classes:Gibs">Gibs</a>
</td></tr>
<tr>
<td> 123 </td>
<td> Pole with Skull </td>
<td> Skull on Pole </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC72</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:HeadOnAStick" class="extiw" title="zdoom:Classes:HeadOnAStick">HeadOnAStick</a>
</td></tr>
<tr>
<td> 124 </td>
<td> Pile of Skulls </td>
<td> Pile of Skulls </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC73</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:HeadCandles" class="extiw" title="zdoom:Classes:HeadCandles">HeadCandles</a>
</td></tr>
<tr>
<td> 125 </td>
<td> Impaled Body </td>
<td> Impaled body </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC74</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:DeadStick" class="extiw" title="zdoom:Classes:DeadStick">DeadStick</a>
</td></tr>
<tr>
<td> 126 </td>
<td> Twitching Body </td>
<td> Impaled Body (Twitching) </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC75</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:LiveStick" class="extiw" title="zdoom:Classes:LiveStick">LiveStick</a>
</td></tr>
<tr>
<td> 127 </td>
<td> Large Tree </td>
<td> Large Brown Tree </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC76</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:BigTree" class="extiw" title="zdoom:Classes:BigTree">BigTree</a>
</td></tr>
<tr>
<td> 128 </td>
<td> <a href="https://doomwiki.org/wiki/Barrel" title="Barrel">Burning Barrel</a> </td>
<td> Burning Barrel </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC77</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:BurningBarrel" class="extiw" title="zdoom:Classes:BurningBarrel">BurningBarrel</a>
</td></tr>
<tr>
<td> 129 </td>
<td> Hanging Body 1 </td>
<td> Hanging Body, Guts Removed </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC78</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:HangNoGuts" class="extiw" title="zdoom:Classes:HangNoGuts">HangNoGuts</a>
</td></tr>
<tr>
<td> 130 </td>
<td> Hanging Body 2 </td>
<td> Hanging Body, Guts &amp; Brain Removed </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC79</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:HangBNoBrain" class="extiw" title="zdoom:Classes:HangBNoBrain">HangBNoBrain</a>
</td></tr>
<tr>
<td> 131 </td>
<td> Hanging Body 3 </td>
<td> Hanging Torso, Looking Down </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC80</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:HangTLookingDown" class="extiw" title="zdoom:Classes:HangTLookingDown">HangTLookingDown</a>
</td></tr>
<tr>
<td> 132 </td>
<td> Hanging Body 4 </td>
<td> Hanging Torso, Open Skull </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC81</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:HangTSkull" class="extiw" title="zdoom:Classes:HangTSkull">HangTSkull</a>
</td></tr>
<tr>
<td> 133 </td>
<td> Hanging Body 5 </td>
<td> Hanging Torso, Looking Up </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC82</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:HangTLookingUp" class="extiw" title="zdoom:Classes:HangTLookingUp">HangTLookingUp</a>
</td></tr>
<tr>
<td> 134 </td>
<td> Hanging Body 6 </td>
<td> Hanging Torso, Brain Removed </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC83</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:HangTNoBrain" class="extiw" title="zdoom:Classes:HangTNoBrain">HangTNoBrain</a>
</td></tr>
<tr>
<td> 135 </td>
<td> Pool Of Blood 1 </td>
<td> Pool of Blood 2 </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC84</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:ColonGibs" class="extiw" title="zdoom:Classes:ColonGibs">ColonGibs</a>
</td></tr>
<tr>
<td> 136 </td>
<td> Pool Of Blood 2 </td>
<td> Pool of Blood 3 </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC85</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:SmallBloodPool" class="extiw" title="zdoom:Classes:SmallBloodPool">SmallBloodPool</a>
</td></tr>
<tr>
<td> 137 </td>
<td> Brains </td>
<td> Pool of Brains </td>
<td> <span style="font-family: &#39;this-font-does-not-exist&#39;, monospace, sans-serif; font-size: 1em;">MT_MISC86</span> </td>
<td> <a href="https://zdoom.org/wiki/Classes:BrainStem" class="extiw" title="zdoom:Classes:BrainStem">BrainStem</a>
</td></tr></table>
