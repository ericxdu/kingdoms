- [Fortress](https://www.doomworld.com/idgames/levels/doom2/deathmatch/d-f/fortress)
  + idgames://4590
  + Gameplay:
  + Engine:
  + Copyright/Permissions: Authors (MAY) use this level as a base to
    build additional levels.
  + Distribution: You MAY distribute this WAD, provided you include
    this file, with no modifications.
- [Suspended in Dusk](https://www.doomworld.com/idgames/levels/doom2/s-u/sid)
  + idgames://13851
  + Gameplay:
  + Engine:
  + Copyright/Permissions: Authors may use the contents of this file
    as a base for modification or reuse, as long as I'm credited.
  + Distribution: You MAY distribute this file, provided you include
    this text file, with no modifications.
- []()
  + idgames:
  + Gameplay:
  + Engine:
  + Copyright/Permissions:
  + Distribution:
- []()
  + idgames:
  + Gameplay:
  + Engine:
  + Copyright/Permissions:
  + Distribution:
