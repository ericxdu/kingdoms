# HIDEOUS DESTRUCTOR

Hideous Destructor is a free GZDoom mod for Freedoom. The mod requires
a lot of keybindings to play so its an excellent subject for
controller theory.


## Configure Controls

At a minimum, the following keys must be bound before you can play
Hideous Destructor. These bindings are in **HD Options**-->**Configure
HD Controls**. The rest of this guide assumes you have bound these keys
and have not changed any other GZDoom defaults.

<pre>
   Weapon Reload: R
     Weapon Zoom: F
Secondary Reload: H
Weapon Fire Mode: J
     Mag Manager: K
   Weapon Unload: L
     Drop Weapon: Q
      Toggle Run: CapsLock
          Crouch: Ctrl
     Jump/Mantle: Space
          Sprint: Shift
</pre>


## Basic Play

To start you must choose **New Game** and pick a loadout, a chapter,
and a skill level. Hideous Destructor has standard mouse-and-keyboard
FPS controls. 

<pre>
Mouse: aim and FIRE, ALT-FIRE
WASD: move
E: open door, press switch, respawn
R: reload

Keys 1-8: switch between weapons (double-press for some)

Wounds: you can see blood on the ground if you are injured. If this happens,
you must press 9 and hold FIRE to bandage your wound.

Walking and running: press Capslock to toggle.

Catching fire: move Mouse around quickly to put yourself out.
</pre>


## Intermediate Play

You can pick up items from the ground to obtain ammo, equipment, and
new weapons.

    E: pick up item/weapon
    Hold E: view weapon inventory
    Q: drop current weapon
    Double Press K: enter Item Manager
      FIRE/ALT-FIRE: select item
      Q: drop selected item


## Advanced Play

    [ and ]: cycle through usable items
    Enter: prepare item
      E: view usage instructions


## Controller Theory

This is an examination of minimal controls for Hideous Destructor. 
Defaults shown as --- must be bound to play HD. H, J, K, and L were
chosen because they are easy to remember (1, 2, 3, 4) and are not bound
by default to any other actions in GZDoom.

    Legend
      GZDoom Name      HD Name         Default/Mine
    Action
      Fire             FIRE{F}         Left Mouse
      Secondary Fire   ALTFIRE{AF}     Right Mouse
      Weapon Reload    RELOAD{R}       --- R
      Weapon Zoom      ZOOM            --- F
      Use / Open                       E
      Jump             JUMP/MANTLE     Space
      Crouch Toggle                    X
      Run              SPRINT          Shift
    Weapon
      Weapon State 1   ALTRELOAD       --- H
      Weapon State 2   FIREMODE{FM}    --- J
      Weapon State 3   MANAGER         --- K
      Weapon State 4   UNLOAD{U}       --- L
    Movement
      Movement         MOVEMENT        WASD
      Toggle Run       WALK/RUN        Capslock
    Inventory
      Activate item                    Enter
      Drop weapon      DROP            --- Q

Several of the controls work in different contexts.

    FIRE and ALTFIRE are re-used on inventory and management screens.

    DROP [Q] is an important action used in various contexts.

    ZOOM [F] is an alternate action used in various contexts.

    FIREMODE [J] is an alternate action used in various contexts.

    ZOOM combined with A or D produces the LEAN action.


## Papercuts

Some problems that may come up when learning to play. Overcoming these 
is important for new users.

  - how many keys do I need to bind?!
  - how do I drop items?
  - some weapons are inaccessible from number keys?
  - how do I use items?


## Conclusion

This guide is an attempt to make starting with Hideous Destructor as 
smooth as possible. Hideous Destructor is a well-designed innovation on 
top of the Doom engine set in the Freedoom universe. However, the game 
asks a lot from the player even before getting into the more advanced 
aspects of the game. For instance, it asks the player to bind at least 8 
keys and choose from 1 of 20 loadouts before even starting. As such I 
recommend it but not for casual pick-up-and-play.


## LINKS

  - [Controller Theory Gamepad](linuxhd.txt)
  - [Hideous Destructor 4.6.1a](https://forum.zdoom.org/viewtopic.php?t=12973)
  - [Hideous Destructor HQ](https://accensi.gitlab.io/hdportal/)
  - [HideousDestructor at Codeberg](https://codeberg.org/mc776/HideousDestructor)
  - [Multiplayer](https://zdoom.org/wiki/Multiplayer)
