The engine loads 108 named sound effects from the WAD. The sounds are
referred to in the `dehacked` lump by number. Every `thing`
has five sounds associated with it which can be assigned by the lump.

    Thing 2 (Trooper)
    Alert sound = 39
    Attack sound = 12
    Pain sound = 27
    Death sound = 62
    Action sound = 76

- Compatibility
  + 11025 Hz
  + Mono
  + WAV format

## Generalization

Boom defines a specific set of situations where a `thing` will
make one of its five sounds. In addition, many `functions` make
sounds of their own, and there are `functions` that exist only
to make sounds as part of the `thing` behavior script like
`A_HOOF` and `A_METAL`.

## Appendix A: List of Sounds by Number

<pre>
<ol start="0">
<li>sfx_None</li>
<li>sfx_pistol</li>
<li>sfx_shotgn</li>
<li>sfx_sgcock</li>
<li>sfx_dshtgn</li>
<li>sfx_dbopn</li>
<li>sfx_dbcls</li>
<li>sfx_dbload</li>
<li>sfx_plasma</li>
<li>sfx_bfg</li>
<li>sfx_sawup</li>
<li>sfx_sawidl</li>
<li>sfx_sawful</li>
<li>sfx_sawhit</li>
<li>sfx_rlaunc</li>
<li>sfx_rxplod</li>
<li>sfx_firsht</li>
<li>sfx_firxpl</li>
<li>sfx_pstart</li>
<li>sfx_pstop</li>
<li>sfx_doropn</li>
<li>sfx_dorcls</li>
<li>sfx_stnmov</li>
<li>sfx_swtchn</li>
<li>sfx_swtchx</li>
<li>sfx_plpain</li>
<li>sfx_dmpain</li>
<li>sfx_popain</li>
<li>sfx_vipain</li>
<li>sfx_mnpain</li>
<li>sfx_pepain</li>
<li>sfx_slop</li>
<li>sfx_itemup</li>
<li>sfx_wpnup</li>
<li>sfx_oof</li>
<li>sfx_telept</li>
<li>sfx_posit1</li>
<li>sfx_posit2</li>
<li>sfx_posit3</li>
<li>sfx_bgsit1</li>
<li>sfx_bgsit2</li>
<li>sfx_sgtsit</li>
<li>sfx_cacsit</li>
<li>sfx_brssit</li>
<li>sfx_cybsit</li>
<li>sfx_spisit</li>
<li>sfx_bspsit</li>
<li>sfx_kntsit</li>
<li>sfx_vilsit</li>
<li>sfx_mansit</li>
<li>sfx_pesit</li>
<li>sfx_sklatk</li>
<li>sfx_sgtatk</li>
<li>sfx_skepch</li>
<li>sfx_vilatk</li>
<li>sfx_claw</li>
<li>sfx_skeswg</li>
<li>sfx_pldeth</li>
<li>sfx_pdiehi</li>
<li>sfx_podth1</li>
<li>sfx_podth2</li>
<li>sfx_podth3</li>
<li>sfx_bgdth1</li>
<li>sfx_bgdth2</li>
<li>sfx_sgtdth</li>
<li>sfx_cacdth</li>
<li>sfx_skldth</li>
<li>sfx_brsdth</li>
<li>sfx_cybdth</li>
<li>sfx_spidth</li>
<li>sfx_bspdth</li>
<li>sfx_vildth</li>
<li>sfx_kntdth</li>
<li>sfx_pedth</li>
<li>sfx_skedth</li>
<li>sfx_posact</li>
<li>sfx_bgact</li>
<li>sfx_dmact</li>
<li>sfx_bspact</li>
<li>sfx_bspwlk</li>
<li>sfx_vilact</li>
<li>sfx_noway</li>
<li>sfx_barexp</li>
<li>sfx_punch</li>
<li>sfx_hoof</li>
<li>sfx_metal</li>
<li>sfx_chgun</li>
<li>sfx_tink</li>
<li>sfx_bdopn</li>
<li>sfx_bdcls</li>
<li>sfx_itmbk</li>
<li>sfx_flame</li>
<li>sfx_flamst</li>
<li>sfx_getpow</li>
<li>sfx_bospit</li>
<li>sfx_boscub</li>
<li>sfx_bossit</li>
<li>sfx_bospn</li>
<li>sfx_bosdth</li>
<li>sfx_manatk</li>
<li>sfx_mandth</li>
<li>sfx_sssit</li>
<li>sfx_ssdth</li>
<li>sfx_keenpn</li>
<li>sfx_keendt</li>
<li>sfx_skeact</li>
<li>sfx_skesit</li>
<li>sfx_skeatk</li>
<li>sfx_radio</li>
</ol>
</pre>
