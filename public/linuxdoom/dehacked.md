Using DeHackEd
==============

With advanced Doom ports like Boom and PrBoom+, modifying Doom is as 
simple as packing a text lump named "dehacked.lmp" in the WAD. When 
using DeuTeX to build the WAD, make sure "dehacked.lmp" is in a folder 
named "lumps" and make sure "dehacked" is in your wadinfo file like so.

<pre>
[lumps]
dehacked
</pre>

The DeHackEd lump is a simple text file with blocks that serve to define 
and modify Things, Weapons, Frames, and more.
