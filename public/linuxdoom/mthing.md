# Monster Things

Codenames and details for 25 Boom *Things* that are commonly implemented 
as enemies in games and mods. "Monster Things" are a subset of "Map 
Things" that have behavior controlled by parameters you can change 
using BEX, with a few hard-coded exceptions.<br />
<br />
Each *Thing* entry notes the name of the monster that implements that 
*Thing* in Doom, Freedoom, and Xeno Quest. Hideous Destructor 
implementations are a best guess association based on the [HD 
manual](hd_manual.html). Mugshots are all from the freely licensed 
[Freedoom manual](files/freedoom-manual-0.12.1.pdf).

## PLAYER (Thing 1)

- ![Player](files/play.png "Player")
- Hard-coded behavior
  + shares state with all other player things
- Implementation
  + Doom: Our Hero (Player)
  + Freedoom: Savior of Humanity
  + Hideous Destructor: [Operator](hd_manual.html#HDOperator)

---

## POSSESSED (Thing 2)

- ![Zombie](files/poss.png "Zombie")
- Hard-coded behavior
  + drops Thing 64 on death
- Implementation
  + Doom: Former Human (Trooper)
  + Freedoom: Zombie
  + Hideous Destructor: [Undead Homeboy](hd_manual.html#UndeadHomeboy)

---

## SHOTGUY (Thing 3)

- ![Shotgun Zombie](files/spos.png "Shotgun Zombie")
- Hard-coded behavior
  + drops Thing 78 on death
- Implementation
  + Doom: Former Human Sergeant (Sergeant)
  + Freedoom: Shotgun Zombie
  + Hideous Destructor: [Jack-Booted Thug](hd_manual.html#Jackboot)

---

## VILE (Thing 4)

- ![Necromancer](files/vile.png "Necromancer")
- Hard-coded behavior
  + other monsters will never target this monster
- Implementation
  + Doom: Arch-vile (Archvile)
  + Freedoom: Necromancer
  + Hideous Destructor: [Necromancer](hd_manual.html/#Necromancer)

---

## UNDEAD (Thing 6)

- ![Dark Soldier](files/skel.png "Dark Soldier")
- Implementation
  + Doom: Revenant
  + Freedoom: Dark Soldier
  + Hideous Destructor: [Dark Soldier](hd_manual.html#Boner)

---

## FATSO (Thing 9)

- ![Combat Slug](files/fatt.png "Combat Slug")
- Hard-coded behavior
  + special map action on death
- Implementation
  + Doom: Mancubus
  + Freedoom: Combat Slug
  + Hideous Destructor: [Combat Slug](hd_manual.html#CombatSlug)

---

## CHAINGUY (Thing 11)

- ![Minigun Zombie](files/cpos.png "Minigun Zombie")
- Hard-coded behavior
  + drops Thing 74 on death
- Implementation
  + Doom: Heavy weapon dude (Chaingun Sargeant)
  + Freedoom: Minigun Zombie
  + Hideous Destructor: [Machine Gunner](hd_manual.html#VulcanetteZombie)

---

## TROOP (Thing 12)

- ![Serpentipede](files/troo.png "Serpentipede")
- Implementation
  + Doom: Imp
  + Freedoom: Serpentipede
  + Hideous Destructor: [Serpentipedes](hd_manual.html#Serpentipede)
  + Valiant: Super Imp

---

## SERGEANT (Thing 13)

- ![Flesh Worm](files/sarg.png "Flesh Worm")
- Implementation
  + Doom: Demon
  + Freedoom: Flesh Worm
  + Hideous Destructor: [Babuin](hd_manual.html#Babuin)
  + Valiant: Super Demon

---

## SHADOWS (Thing 14)

- ![Stealth Worm](files/sar2.png "Stealth Worm")
- Implementation
  + Doom: Spectre
  + Freedoom: Stealth Worm
  + Hideous Destructor: [Ninja Pirate](hd_manual.html#NinjaPirate)

---

## HEAD (Thing 15)

- ![Trilobite](files/head.png "Trilobite")
- Implementation
  + Doom: Cacodemon
  + Freedoom: Trilobite
  + Hideous Destructor: [Trilobite](hd_manual.html#Trilobite)

---

## BRUISER (Thing 16)

- ![Pain Lord](files/boss.png "Pain Lord")
- Hard-coded behavior
  + special map action on death
  + immune to damage from BRUISER or KNIGHT
- Implementation
  + Doom: Baron of Hell
  + Freedoom: Pain Lord
  + Hideous Destructor: [Pain Lord](hd_manual.html#PainLord)
  + Valiant: Super Mancubus

---

## KNIGHT (Thing 18)

- ![Pain Bringer](files/bos2.png "Pain Bringer")
- Hard-coded behavior
  + immune to damage from BRUISER or KNIGHT
- Implementation
  + Doom: Hell Knight
  + Freedoom: Pain Bringer
  + Hideous Destructor: [Pain Bringer](hd_manual.html#PainBringer)
  + Valiant: Pyro Knight

---

## SKULL (Thing 19)

- ![Deadflare](files/skul.png "Deadflare")
- Implementation
  + Doom: Lost Soul
  + Freedoom: Deadflare
  + Hideous Destructor: [The Begotten](hd_manual.html#FlyingSkull)

---

## SPIDER (Thing 20)

- ![Large Technospider](files/spid.png "Large Technospider")
- Hard-coded behavior
  + special map action on death
  + immune to splash damage
- Implementation
  + Doom: Spiderdemon (Spider Mastermind)
  + Freedoom: Large Technospider
  + Hideous Destructor: [Large Technospider](hd_manual.html#Technorantula)
  + Valiant: Cybruiser

---

## BABY (Thing 21)

- ![Technospider](files/bspi.png "Technospider")
- Hard-coded behavior
  + special map action on death
- Implementation
  + Doom: Arachnotron
  + Freedoom: Technospider
  + Hideous Destructor: [Technospider](hd_manual.html#TechnoSpider)

---

## CYBORG (Thing 22)

- ![Assault Tripod](files/cybr.png "Assault Tripod")
- Hard-coded behavior
    + special map action on death
    + immune to splash damage
- Implementation
    + Doom: Cyberdemon
    + Freedoom: Assault Tripod
    + Hideous Destructor: [Assault Tripod](hd_manual.html#SatanRobo)

---

## PAIN (Thing 23)

- ![Summoner](files/pain.png "Summoner")
- Implementation
  + Doom: Pain Elemental
  + Freedoom: Summoner
  + Hideous Destructor: [Hive Pit](hd_manual.html#SkullSpitter)

---

## WOLFSS (Thing 24)

- ![Sailor](files/sswv.png "Sailor")
- Hard-coded behavior
  + drops Thing 64 on death
- Implementation
  + Doom: Wolfenstein SS (SS Nazi)
  + Freedoom: Sailor (banned)
  + Valiant: Chaingunner


## KEEN (Thing 25)

- ![Pod Creature](files/keen.png "Pod Creature")
- Hard-coded behavior
  + special map action on death
- Implementation
  + Doom: Commander Keen
  + Freedoom: Pod Creature


## BOSSBRAIN (Thing 26)

- ![Big Brain](files/bbrn.png "Big Brain")
- Hard-coded behavior
  + special map action on death
- Implementation
  + Doom: Boss Brain
  + Freedoom: Big Brain
  + Hideous Destructor: [The Tyrant](hd_manual.html#TheTyrant)


## BOSSSPIT (Thing 27)

- ![Spawner](files/idkm.png "Spawner")
- Hard-coded behavior
  + interacts with [Thing 28](https://doomwiki.org/wiki/Spawn_spot)
- Implementation
  + Doom: Demon Spawner
  + Freedoom: Spawner


## SPAWNSHOT (Thing 29)

- ![Spawn Cube](files/bosf.png "Spawn Cube")
- Hard-coded behavior
  + interacts with [Thing 28](https://doomwiki.org/wiki/Spawn_spot)
- Implementation
  + Doom: Demon Spawn Cube
  + Freedoom: Spawn Cube
  + Hideous Destructor: [Putto](hd_manual.html#Putto)


## BARREL (Thing 31)

- ![Barrel](files/bar1.png "Barrel")
- Implementation
  + Doom: Exploding Barrel
  + Freedoom: Exploding Barrel
  + Hideous Destructor: [Frag Containment Storage Cell](hd_manual.html#HDBarrel)


## FLAMING BARREL (Thing 128)

- ![Flaming Barrel](files/fcan.png "Flaming Barrel")
- Implementation
  + Doom: decoration
  + Freedoom: decoration
  + Hideous Destructor: [Frag Containment Oxidation Processor](hd_manual.html#HDFireCan)


## MISC37 (Thing 88)

- ![Blood Fountain](files/col5.png "Blood Fountain")
- Implementation
  + Doom: Pillar with Heart
  + Freedoom: Blood Fountain
  + Valiant: Lightning Orb

---

## MISC40 (Thing 91)

- ![Grey Tree](files/tre1.png "Grey Tree")
- Implementation
  + Doom: Grey Tree
  + Freedoom: Short Grey Tree
  + Valiant: Ghost Arch-vile

---

## MISC65 (Thing 116)

- Implementation
  + Doom: Dead Lost Soul
  + Valiant: Suicide Bomber

---

## MISC76 (Thing 127)

- ![Large Tree](files/tre2.png "Large Tree")
- Implementation
  + Doom: Large Tree
  + Freedoom: Large Brown Tree
  + Valiant: Ghost Revenant
