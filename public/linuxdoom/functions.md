## Generalization

Each `frame` of the engine script can have a codepointer that defines
whether an engine `function` is called on that frame, and which one.

DeHackEd used `pointer` sections to define which function is called.

BEX offers a `[codeptr]` section wherein the modder lists which
`function` is called on each frame.

BEX should implement a `pointer` field for each Frame listed, to make
it generally easier to define a `function` call for the frame.
