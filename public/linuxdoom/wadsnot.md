Non-Distributable WADs
======================

- [Bury My Heart Knee Deep](https://www.doomworld.com/idgames/levels/doom2/j-l/kneedeep)
  + idgames://19268
  + Gameplay: Metroid-like Doom map
  + Engine: GZDoom, Odamex, PrBoom+
  + Copyright/Permissions: Authors may NOT use the contents of this
  file as a base for modification or reuse.
  + Distribution: You MAY distribute this file, provided you include
  this text file, with no modifications.
- [Congestion 1024](https://www.doomworld.com/idgames/levels/doom2/Ports/megawads/1024)
  + idgames://13958
  + Gameplay:
  + Engine:
  + Copyright/Permissions: Authors may NOT use the contents of this
  file as a base for modification or reuse.
  + Distribution: You MAY distribute this file, provided you include
  this text file, with no modifications.
- [Cyberdreams](https://www.doomworld.com/idgames/levels/doom2/megawads/cydreams)
  + Gameplay: singleplayer pacifist puzzles
  + Engine: PrBoom, ?
  + Copyright/Permissions: Authors MAY NOT use these levels as a base
  to build additional levels.
  + Distribution: You MAY distribute this WAD project, provided you
  include this file, with no modifications.
- [Operation Hydra](https://www.doomworld.com/idgames/levels/doom2/g-i/hydra)
  + idgames://17383
  + Gameplay:
  + Engine:
  + Copyright/Permissions: Authors MAY NOT use the contents of this
  file as a base for modification or reuse.
  + Distribution: You MAY distribute this file, provided you include
  this text file, with no modifications.
