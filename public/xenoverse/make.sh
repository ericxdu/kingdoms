#!/bin/bash
doctype='<!DOCTYPE html>'
csslink='<link rel="stylesheet" type="text/css" href="style.css" />'
var1=md
var2=html
for filename in *.$var1;
do
    echo '<!DOCTYPE html>' > ${filename%$var1}$var2
    echo '<html>' >> ${filename%$var1}$var2
    echo '<head>' >> ${filename%$var1}$var2
    echo $csslink >> ${filename%$var1}$var2
    echo '</head>' >> ${filename%$var1}$var2
    echo '' >> ${filename%$var1}$var2
    echo '<body>' >> ${filename%$var1}$var2
    markdown $filename >> ${filename%$var1}$var2
    echo '</body>' >> ${filename%$var1}$var2
done
