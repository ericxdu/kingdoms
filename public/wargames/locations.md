Bennay
------

Planet with green and off-white coloring and brown highlights. Orbited by a single rocky satellite. In the distance is a blueish-ringed planet with a light-colored surface. This location is at a medium distance from the planetary system's star.

Missions: Flight (beginner).


Bennay Zone
-----------

Missions: Flight (advanced).


Fortuna
-------

A predominantly green-colored planet with white markings surrounded by brown borders. Sometimes visible is a single satellite. This planet is very close to its star.

Missions: Weapons (intermediate).

Midas
