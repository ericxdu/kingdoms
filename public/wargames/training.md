Training 1: Flight (novice)
---------------------------

Location: Launch from League Starport and open jump gate on completion. A large, green planet with white and orange highlights is visible with a large brown satellite orbiting. A blue, ringed planet is visible in the distance, and the sky is dominated by a large yellow star.

Fleet Craft: League Starport x 1.
